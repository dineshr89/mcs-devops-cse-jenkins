﻿function ofModalOnLoad() {
    //initial load
    showHideProgParticipantInfo();

}

function showHideProgParticipantInfo() {
    if ($('#DataProgramSettingsUpdate_Form_txtIsParticipating').val() != undefined && $('#DataProgramSettingsUpdate_Form_txtIsParticipating').val() != '') {
        if ($('#DataProgramSettingsUpdate_Form_txtIsParticipating').val() == 0) {
            $('#DataProgParticipantInfo').hide();
        }
        else if ($('#DataProgramSettingsUpdate_Form_txtIsParticipating').val() == 1) {
            $('#DataProgParticipantInfo').show();


            testSFTPConnection($('#test-connection'));
        }
    }
    else {
        $('#DataProgParticipantInfo').hide();
    }
}

function testSFTPConnection(btn_test) {

    var data = {};
    var url = "https://api.cstorepro.com/sftp/TestConnection";
    data.sftpurl = $('#DataProgramSettingsUpdate_Form_txtSftpURL').val();
    data.username = $('#DataProgramSettingsUpdate_Form_txtUserName').val();
    data.password = $('#DataProgramSettingsUpdate_Form_txtPassword').val();

    if (data.sftpurl !== "" && data.username !== "" && data.password !== "") {

        var sftppath = '';
        if ($('#DataProgramSettingsUpdate_Form_txtSftpPath').val() != undefined && $('#DataProgramSettingsUpdate_Form_txtSftpPath').val() != "") {
            sftppath = $('#DataProgramSettingsUpdate_Form_txtSftpPath').val();
            data.sftppath = sftppath;
        }
        else {
            sftppath = '';
        }

        btn_test.html("testing....");
        btn_test.addClass("disabled");
        btn_test.prop('disabled', true);

        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.onload = function () {
            //console.log(xhr.responseText);
        };

        //console.log(JSON.stringify(data));

        xhr.send(JSON.stringify(data));

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                //console.log('xhr.readyState=', xhr.readyState);
                //console.log('xhr.status=', xhr.status);
                //console.log('response=', xhr.responseText);

                var data = $.parseJSON(xhr.responseText);
                var sftpTest = data['isValidConnection'];

                if (sftpTest === "true") {
                    $('#program_sftp_notvalid').addClass("hide");
                    $('#program_sftp_valid').removeClass("hide");
                } else {
                    $('#program_sftp_notvalid').removeClass("hide");
                    $('#program_sftp_valid').addClass("hide");

                }


                btn_test.html("Test connection");
                btn_test.removeClass("disabled");
                btn_test.prop('disabled', false);
                $("#program_error_container").addClass("hide");
            }
        };
    } else {

        $('#program_sftp_notvalid').removeClass("hide");
        $('#program_sftp_valid').addClass("hide");
        $("#program_error_container").html("Need to specify and URL, username, password and email address").removeClass("hide");
    }



    //$.post(url, data, function (result) { //console.log(result); },'jsonp');
    /*
    $.ajax({
        url: url,
        data: data,
        type: 'POST',
        crossDomain: true,
        dataType: 'jsonp', // Notice! JSONP <-- P (lowercase)
        success: function (json) {
            // do stuff with json (in this case an array)
            //console.log(json);
            alert("Success");
            updateDataExchangeCurrentField(cur_programid, cur_paramname, 1);
        },
        error: function () {
            alert("Error");
        }
    });
    */
}


$(document).ready(function () {


    $(document).on('change', '#DataProgramSettingsUpdate_Form_txtIsParticipating', function () {
        showHideProgParticipantInfo();
    });
    //console.log("Hi");
    
    $(document).on('click', '.test-connection', function () {
        testSFTPConnection($(this));
    });



});

