﻿$(document).ready(function () {
    $(document).on('click', "a[viewid='DataProgramSetup']", function (e) {
        e.preventDefault();
        
        var intDataProgramID;
        var dtReportDate;
        let storeID = document.querySelector("#saviour").value;
        

        //Validate storeID
        if ($.isNumeric(storeID)) {
            intStoreID = storeID;
        } else {
            intStoreID = -1;
        }

        //Validate programID
        //if ($.isNumeric($(this).data("programid"))) {
        //    intDataProgramID = $(this).data("programid");
        //} else {
        //    intDataProgramID = -1;
        //}

        ////Validate reportDate
        //if (EJUParseDateOnly($(this).data("reportdate")) !== null) {
        //    dtReportDate = $(this).data("reportdate");
        //} else {
        //    dtReportDate = '';
        //}

        var str_url = '/CStoreProAdmin/Content/DataProgram/Resources/handler/DataProgramSetup.ashx?StoreID=' + intStoreID;

        var ajaxRequest = $.ajax({
            type: "POST"
            , url: str_url
        });

        ajaxRequest.done(function (response) {

            var contentType;
            var responseStatus = "0";
            var responseMsg = "";
            var responseData = "";

            contentType = ajaxRequest.getResponseHeader("content-type");
            if (contentType.toLowerCase().indexOf("xml") >= 0) {

                responseStatus = response.all[1].innerHTML;
                responseMsg = response.all[3].innerHTML;
                responseData = response.all[4].lastChild.data;

                EWFUpdatePageContent('#divStoreDetailTab_DataProgramSetup', responseData);
                
                if (responseStatus === "1") {
                    document.querySelector("#divStoreDetailTab_DataProgramSetup").innerHTML = responseData;

                    document.querySelectorAll("#divStoreDetailTab_DataProgramSetup a[data-ofmodal-remotefile]").forEach(a => {
                        a.setAttribute('data-ofmodal-remotefile', a.getAttribute('data-ofmodal-remotefile') +"&storeID="+ document.querySelector("#saviour").value);
                    });

                } else {
                    document.querySelector("#divStoreDetailTab_DataProgramSetup").innerHTML = "<div style='{text-align:'center';}'><h5>Data Program not Enabled!</h5></div>";
                    //TODO: on failure call row refresh and say that there was an error while launching lambda function
                }
                OFModal.rebind(true);
            }
        });
        ajaxRequest.fail(function (xhr, msg) {
            //console.log(xhr);
            //console.log(msg);
        });
        ajaxRequest.always(function () {

        });

    });
});