﻿$(document).ready(function () {
    $(document).on('click', '.dataprogram-reprocess', function (e) {
        e.preventDefault();
        var intDataProgramID;
        var dtReportDate;

        var btn_reprocess = $(this);
        //Disable button
        $(this).removeClass("dataprogram-reprocess");
        $(this).addClass("disabled");
        $(this).attr("disabled", "disabled");
        $(this).text("processing");

        //Validate storeID
        if ($.isNumeric($(this).data("storeid"))) {
            intStoreID = $(this).data("storeid");
        } else {
            intStoreID = -1;
        }

        //Validate programID
        if ($.isNumeric($(this).data("programid"))) {
            intDataProgramID = $(this).data("programid");
        } else {
            intDataProgramID = -1;
        }

        //Validate reportDate
        if (EJUParseDateOnly($(this).data("reportdate")) !== null) {
            dtReportDate = $(this).data("reportdate");
        } else {
            dtReportDate = '';
        }

        var str_url = '/CStoreProAdmin/Content/DataProgram/Resources/handler/DataProgramReprocess.ashx?ReportDate=' + dtReportDate + '&DataProgramID=' + intDataProgramID + '&StoreID=' + intStoreID;

        var ajaxRequest = $.ajax({
            type: "POST"
            , url: str_url
        });

        ajaxRequest.done(function (response) {

            var contentType;
            var responseStatus = "0";
            var responseMsg = "";
            var responseData = "";

            contentType = ajaxRequest.getResponseHeader("content-type");
            if (contentType.toLowerCase().indexOf("json") >= 0) {

                responseStatus = response.resStatus;
                responseData = response.strResponseData;
                responseMsg = response.strMessage;
                if (responseStatus === "1") {

                    btn_reprocess.parent('div').next('div').removeClass('hide').html("Request is being processed");

                } else {
                    //TODO: on failure call row refresh and say that there was an error while launching lambda function
                }
            }
        });
        ajaxRequest.fail(function (xhr, msg) {
            //console.log(xhr);
            //console.log(msg);
        });
        ajaxRequest.always(function () {

        });

    });
});