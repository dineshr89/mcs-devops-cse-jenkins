﻿$(document).ready(function () {
    var isAutoSearch = 0;
    isAutoSearch = getQueryStringParameterByName("isautosubmit");


    if (isAutoSearch == 1) {
        $('#btnVendor_Filter_Search').click();
    }
    else {
        //Do nothing
    }


    function getQueryStringParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

});