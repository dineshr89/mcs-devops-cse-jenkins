﻿
$(document).on('click', '.dataprogram-reprocess', function (e) {
    e.preventDefault();
    var intDataProgramID;
    var intEntityID;
    var intEntityTypeID;
    var dtReportDate;

    var btn_reprocess = $(this);
    //Disable button
    $(this).removeClass("dataprogram-reprocess");
    $(this).addClass("disabled");
    $(this).attr("disabled", "disabled");
    $(this).text("processing");

    //Validate entityid
    if ($.isNumeric($(this).data("entityid"))) {
        intEntityID = $(this).data("entityid");
    } else {
        intEntityID = -1;
    }
    //Validate entitytypeid
    if ($.isNumeric($(this).data("entitytypeid"))) {
        intEntityTypeID = $(this).data("entitytypeid");
    } else {
        intEntityTypeID = -1;
    }

    //Validate programID
    if ($.isNumeric($(this).data("programid"))) {
        intDataProgramID = $(this).data("programid");
    } else {
        intDataProgramID = -1;
    }

    //Validate reportDate
    if (EJUParseDateOnly($(this).data("reportdate")) !== null) {
        dtReportDate = $(this).data("reportdate");
    } else {
        dtReportDate = '';
    }

    var str_url = '/CStoreProAdmin/Content/MCS/Resources/Handler/DataprogramReprocess.ashx?ReportDate=' + dtReportDate + '&DataProgramID=' + intDataProgramID + '&EntityID=' + intEntityID + '&EntityTypeID=' + intEntityTypeID;

    var ajaxRequest = $.ajax({
        type: "POST"
        , url: str_url
        , beforesend: function () { btn_reprocess.parent('div').next('div').removeClass('hide').html("Request is being processed");}
    });

    ajaxRequest.done(function (response) {

        var contentType;
        var responseStatus = "0";
        var responseMsg = "";
        var responseData = "";

        contentType = ajaxRequest.getResponseHeader("content-type");
        if (contentType.toLowerCase().indexOf("json") >= 0) {

            responseStatus = response.resStatus;
            responseData = response.strResponseData;
            responseMsg = response.strMessage;
            if (responseStatus === "1") {

                btn_reprocess.parent('div').next('div').removeClass('hide').html("Request is being processed");
                /*
                if ($('#frmDataProgramFileUploadRequestOpForm').length === 1) {

                    EnetGrid2Refresh('frmDataProgramFileUploadRequestOpForm');

                } else {
                   
                    //location.reload();

                }
                */

            } else {
                //TODO: on failure call row refresh and say that there was an error while launching lambda function
            }
        }
        else {
            btn_reprocess.parent('div').next('div').removeClass('hide').html("Error processing request");
        }   
    });
    ajaxRequest.fail(function (xhr, msg) {
        //console.log(xhr);
        //console.log(msg);
    });
    ajaxRequest.always(function () {

    });

});

$(document).on('click', '.download-file', function (e) {
    e.preventDefault();
    var requestID;
    var btn_download = $(this);
    //Disable button
    //$(this).removeClass("download-file");
    btn_download.addClass("disabled");
    btn_download.attr("disabled", "disabled");

    requestID = $(this).data("requestid");

    if (requestID !== undefined && requestID !== null) {

        var str_url = '/CStoreProAdmin/Content/MCS/Resources/Handler/DataProgramDocumentView.ashx?RequestID=' + requestID + '';

        var ajaxRequest = $.ajax({
            type: "POST"
            , url: str_url
        });

        ajaxRequest.done(function (response) {

            var contentType;
            var responseStatus = "0";
            var responseMsg = "";
            var responseData = "";

            contentType = ajaxRequest.getResponseHeader("content-type");
            if (contentType.toLowerCase().indexOf("json") >= 0) {

                responseStatus = response.resStatus;
                responseData = response.strResponseData;
                responseMsg = response.strMessage;
                if (responseStatus === "1") {
                    //EnetGrid2Refresh('frmDataProgramFileUploadRequestOpForm');
                    if (responseData.filepath.length > 0) {
                        window.open(responseData.filepath);
                    } else {
                        btn_download.parent('span').next('div.error-container').removeClass('hide').html("<small class='text-danger'> File not found</small>");
                    }
                } else {
                    btn_download.parent('span').next('div.error-container').removeClass('hide').html("<small class='text-danger'> File not found</small>");
                }
            }
        });
        ajaxRequest.fail(function (xhr, msg) {
            //console.log(xhr);
            //console.log(msg);
        });
        ajaxRequest.always(function () {
            btn_download.removeClass("disabled");
            btn_download.removeAttr("disabled", "disabled");
        });


    }

});