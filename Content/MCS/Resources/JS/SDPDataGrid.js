﻿$(document).ready(function () {

    var objDataTable = null;

    $(document).on('click', '.csp-viz-modal', function myfunction() {
       
        let requestID = $(this).data("requestid");

        //Downloads file from s3, parses file to JSON, creates datatable, triggers event sdp-data-loaded on completeion.
        populateData(requestID, '#id-data-table');
        $('#myModal').modal('show');

        //once the data has been loaded to table, show modal
        $(document).on("sdp-data-loaded", function myfunction() {
            $('#placeholder_loading').hide();
        });        

    });

    function populateData(requestID, tableElID) {

        if (requestID !== undefined && requestID !== null) {

            var str_url = '/CStoreProAdmin/Content/MCS/Resources/Handler/DataProgramDocumentView.ashx?RequestID=' + requestID + '&SendDataAsJSON=1';

            var ajaxRequest = $.ajax({
                type: "POST"
                , url: str_url
                , beforeSend: function () {
                    if ($.fn.DataTable.isDataTable(tableElID)) {
                        objDataTable.destroy();
                        $(tableElID).html("<thead></thead>< tbody ></tbody ><tfoot><tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr></tfoot>");
                    }
                    $('#placeholder_loading').show();
                }
            });

            ajaxRequest.done(function (response) {

                var contentType;
                var responseStatus = "0";
                var responseMsg = "";
                var responseData = "";
                contentType = ajaxRequest.getResponseHeader("content-type");
                if (contentType.toLowerCase().indexOf("json") >= 0) {

                    responseStatus = response.resStatus;
                    responseData = response.strResponseData;
                    responseMsg = response.strMessage;
                    if (responseStatus === "1") {

                        let curString = responseData.csvData;
                        //adding coulmn headers to file. Papa.parse return array object if coulmn header not specified. To get JSON obj appening first row
                        curString = "a1|a2|a3|a4|a5|a6|a7|a8|a9|a10|a11|a12|a13|a14|a15|a16|a17|a18|a19|a20|a21|a22|a23|a24|a25|a26|a27|a28|a29|a30|a31|a32|a33|a34|a35|a36|a37\n" + responseData.csvData;

                        Papa.parse(curString, {

                            "dynamicTyping": true,
                            header: true,
                            //preview: 100,
                            "complete": results => {
                                let Summaryheader = results.data.shift(); //Removing first row from file so as to get rid of the summary line
                                //console.table(results);
                                let newData = results.data.map(element => ({
                                    id: element.a5
                                    , "Transaction Date": formatdate(element.a3, element.a4)
                                    , "POS Code": element.a15
                                    , "Item name": element.a16
                                    , "Qty": element.a18
                                    , "Manufacture Promo": fromatYNtoBool(element.a20)
                                    , "Manufacture Promo Qty": element.a21 === null ? 0 : element.a21
                                    , "Manufacture Promo Disc": element.a22 === null ? 0 : element.a22
                                    , "Loyalty": checkLoyalty(element.a29)
                                    , "Loyalty Discount Amount": element.a30 === null ? 0 : element.a30
                                    , "Sale Amount": element.a31
                                })); // Removing non display columns from the data set
                                
                                newData.headers = ["id", "Transaction Date", "POS Code", "Item name", "Qty", "Manufacture Promo", "Manufacture Promo Qty", "Manufacture Promo Disc", "Loyalty", "Loyalty Discount Amount", "Sale Amount"];

                                if ($.fn.DataTable.isDataTable(tableElID)) {
                                    objDataTable.destroy();
                                    $(tableElID).html("<thead></thead>< tbody ></tbody ><tfoot><tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr></tfoot>");
                                }
                                //console.table(newData);
                                objDataTable = $(tableElID).DataTable({
                                    "responsive": true,
                                    "retrieve": true,
                                    "columns": newData.headers.map(c => ({
                                        "title": c,
                                        "data": c,
                                        "visible": c.toLowerCase() !== "id",
                                        "default": ""
                                    })),
                                    "data": newData,
                                    "footerCallback": function (row, data, start, end, display) {
                                        
                                        footersumation(this, 4);
                                        footersumation(this, 6);
                                        footersumationDollar(this, 7);
                                        footersumationDollar(this, 9);
                                        footersumationDollar(this, 10);
                                    }

                                }); //Data table config

                                $(document).trigger("sdp-data-loaded"); //Trigger event to let know that the data had been loaded to table and the modal can be displayed.

                            }
                        });

                    } else {
                        //TODO:: if unable to get data from s3
                    }
                }
            });
            ajaxRequest.fail(function (xhr, msg) {
            });
            ajaxRequest.always(function () {
            });


        }
    }

    function formatdate(strDate, strTime) {
        let transactionDate = moment(strDate, "YYYYMMDD");
        let strdatepart = moment(transactionDate).format("MM/DD/YYYY");
        let transactionDateTime = moment(strdatepart + " " + strTime).format("MM/DD/YYYY hh:mm a");
        return transactionDateTime;
    }
    function fromatYNtoBool(instrYN) {
        if (instrYN === "Y")
            return true;
        else return false;
    }
    function checkLoyalty(strLoyaltyCode) {
        let isTransLoyalty = false;
        if (strLoyaltyCode !== undefined && strLoyaltyCode !== null) {
            console.log(strLoyaltyCode);
            if (strLoyaltyCode.toString().trim().length > 0) {
                isTransLoyalty = true;
            } else {
                isTransLoyalty = false;
            }
        } else {
            isTransLoyalty = false;
        }

        return isTransLoyalty; 
    }
    function footersumationDollar(curObj, col) {       

        var api = curObj.api();

        // Remove the formatting to get integer data for summation
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column(col)
            .data()
            .reduce(function (a, b) {
                return intVal(a) + intVal(b);
            }, 0);

        // Total over this page
        pageTotal = api
            .column(col, { page: 'current' })
            .data()
            .reduce(function (a, b) {
                return intVal(a) + intVal(b);
            }, 0);

        // Update footer
        $(api.column(col).footer()).html(
            '$' + total.toFixed(2)
            //'$' + pageTotal.toFixed(2) + ' ( $' + total.toFixed(2) + ' total)'
        );
        
    }
    function footersumation(curObj, col) {
        var api = curObj.api();

        // Remove the formatting to get integer data for summation
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column(col)
            .data()
            .reduce(function (a, b) {
                return intVal(a) + intVal(b);
            }, 0);

        // Total over this page
        pageTotal = api
            .column(col, { page: 'current' })
            .data()
            .reduce(function (a, b) {
                return intVal(a) + intVal(b);
            }, 0);

        // Update footer
        $(api.column(col).footer()).html(
            total
             //pageTotal + ' ( ' + total + ' total)'
        );


    }

});


