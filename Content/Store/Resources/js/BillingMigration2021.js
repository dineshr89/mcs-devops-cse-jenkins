﻿$(document).ready(function () {

    $('#btnBillingMigration2021Update').click(function (e) {

        $('#txtFormStatus').val('MigrationUpdate');
        var formID = $(this).attr("form");
        $("#" + formID).submit();
    });

    $(document).on("change", "input[type='text'], select", function (e) {
        var strItemID = $(this).attr('id').split('_')[1];
        if (strItemID != undefined) {
            markItemAsChanged(strItemID);
        }
    });


});

//$(document).on('click', '#btnMappingUpdate', function (e) {
//    $('#txtFormStatus').val('MappingUpdate');
//    var formID = $(this).attr("form");
//    $("#" + formID).submit();
//});



function markItemAsChanged(strInItemID) {
    var strCheckBoxID = 'txtStoreToUpdate_' + strInItemID;
    var isCheckBoxChecked = $('#' + strCheckBoxID).is(':checked');
    if (isCheckBoxChecked == false) {
        //if price is set but the update price check box is not marked, mark it
        //var strCurrentPrice = $('#' + strReferencePriceID).val();
        $('#' + strCheckBoxID).attr('checked', 'checked');

    }
}

function refreshParent(responseData, trigger) {
    var formName = $(trigger).attr('id');
    var divName = $(trigger).attr('data-ewf-form-replacement-divid');
    var parentForm = $(trigger).attr("data-ewf-form-parentgrid-form");
    var root = responseData;


    if ($(root).find("status").length > 0) {
        var status = $(root).find("status").text();
        if (status > 0) {
            if ($(responseData).find("ispopup").length > 0) {
                if ($(responseData).find("data").length > 0) {
                    var rowData = $(responseData).find("data").children();
                    var sParsedRowData = $('<div>').html(rowData).html();
                    // $('#' + divName).html(sParsedRowData);
                    $("#" + divName).after(sParsedRowData);
                    $("#" + divName).remove();
                    EJUBindAllUIElements(divName);
                }
                else {
                    EWF_CloseWindows();
                    EJUBindAllUIElements(divName);
                    EnetGrid2Refresh('frmBillingMigration2021OpForm');
                }
            }
            else {
                if ($(responseData).find("redirecturl").length > 0) {
                    var strRedirect = $(responseData).find("redirecturl").text();
                    if (strRedirect == "") {

                        EJUBindAllUIElements(divName);
                        EnetGrid2Refresh(parentForm);
                    }
                    else {
                        window.location.href = strRedirect;
                    }
                }
                else {
                    EnetGrid2Refresh(parentForm);
                    EJUBindAllUIElements(divName);
                }
            }

        } else {
            var strMessage = $(responseData).find("message").text();
            EUIShowErrorInfoBar(strMessage, divName);


        }
    }
    else {

        if (parentForm != "") {
            if ($(document).find("#" + parentForm).length > 0) {

                EJUBindAllUIElements(divName);
                EnetGrid2Refresh(parentForm);
            }
            else {
                EUIShowErrorInfoBar("Cannot find parent form " + parentForm + "in document to replace response", divName)
            }
        }
        else {
            EUIShowErrorInfoBar("Cannot find parent form name ")
        }
    }


}
