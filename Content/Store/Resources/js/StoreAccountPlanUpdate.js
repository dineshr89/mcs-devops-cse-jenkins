﻿$(document).ready(function () {
    showPaymentMethodDetails();
    $(document).on('change', '#StoreListAddUpdate_Form_txtPaymentMethod', function (e) {
        showPaymentMethodDetails();
    });
});


function showPaymentMethodDetails() {
    if ($('#StoreListAddUpdate_Form_txtPaymentMethod option:selected').val() != undefined
        && $('#StoreListAddUpdate_Form_txtPaymentMethod option:selected').val() != null) {

        var curPaymentMode = $('#StoreListAddUpdate_Form_txtPaymentMethod option:selected').val();
        if (curPaymentMode == 'CC') {
            $('#rowCCDetails').show();
            $('#rowACHDetails').hide();
        }
        else if (curPaymentMode == 'ACH') {
            $('#rowCCDetails').hide();
            $('#rowACHDetails').show();
        }
        else {
            $('#rowCCDetails').hide();
            $('#rowACHDetails').hide();
        }
    }
}
