﻿$(document).ready(function () {

    //======================================
    //  Set/Remove expiration date as required
    //======================================
    setExpirationDateReqd();

    if ($('#StoreListAddUpdate_Form_hdnIsMCSStore').length) {
        if ($('#StoreListAddUpdate_Form_hdnIsMCSStore').val() != "" && $('#StoreListAddUpdate_Form_hdnIsMCSStore').val() != undefined) {
            if ($('#StoreListAddUpdate_Form_hdnIsMCSStore').val() == "True") {

                setMCSStore();
                if ($('#StoreListAddUpdate_Form_hdnIsHeadStore').val() != "" && $('#StoreListAddUpdate_Form_hdnIsHeadStore').val() != undefined) {
                    if ($('#StoreListAddUpdate_Form_hdnIsHeadStore').val() == "True") {
                        setUpHeadStoreForm();
                    }
                    else {
                        setUpChildStoreForm();
                    }
                }
                else {
                    //Dont set up as there is no loyalty related information needed
                }
            }
            else {
                //Dont set up as there is no loyalty related information needed
            }
        }
        else {
            //Dont set up as there is no loyalty related information needed
        }
    }
    else {
        //Dont set up as there is no loyalty related information needed
    }




    if ($('#StoreListAddUpdate_Form_hdnIsMCSStore').length) {
        if ($('#StoreListAddUpdate_Form_hdnIsHeadStore').val() != "" && $('#StoreListAddUpdate_Form_hdnIsHeadStore').val() != undefined) {
            if ($('#StoreListAddUpdate_Form_hdnIsHeadStore').val() == "True") {
                //NOTES: have to be executed after setting up store
                manageScandataproviderSetup();
            }
        }
    }

    //Expiration date on license type change
    $(document).on('change', '#StoreListAddUpdate_Form_txtLicenseType', function (e) {
        e.preventDefault();
        var intExpirationDays = 0;
        var dtExpirationDate = '';

        //===============================
        //  Set expiration date
        //===============================
        if ($('#StoreListAddUpdate_Form_txtLicenseType option:selected').attr("data-extra") != undefined
            && $('#StoreListAddUpdate_Form_txtLicenseType option:selected').attr("data-extra") != null) {
            intExpirationDays = $('#StoreListAddUpdate_Form_txtLicenseType option:selected').attr("data-extra");

            if (intExpirationDays == 36500) {
                dtExpirationDate = '12/31/2100';
            }
            else {
                dtExpirationDate = moment().add(intExpirationDays, 'days').calendar();
            }

            $('#StoreListAddUpdate_Form_txtExpirationDate').val(dtExpirationDate);
        }
        else {
            intExpirationDays = 0;
        }

        //======================================
        //  Set/Remove expiration date as required
        //======================================
        setExpirationDateReqd();

    });


    $(document).on('change', '#StoreListAddUpdate_Form_txtScanDataProviderID', function (e) {
        e.preventDefault();
        manageScandataproviderSetup();
    });
    //=====================================================
    //Admin email address, name and phone no. auto populate
    //=====================================================
    $(document).on('change', '#StoreListAddUpdate_Form_txtStorePhoneNo', function (e) {
        if ($(this).val() != undefined && $(this).val() != "") {

            if ($('#StoreListAddUpdate_Form_txtAdminPhoneNo').val() == "") {
                $('#StoreListAddUpdate_Form_txtAdminPhoneNo').val($(this).val());
            }
            else {
                // dont change if there is already a value
            }
        }
        else {
            //Dont change the field
        }
    });

    $(document).on('change', '#StoreListAddUpdate_Form_txtEmailAddress', function (e) {

        if ($(this).val() != undefined && $(this).val() != "") {

            if ($('#StoreListAddUpdate_Form_txtAdminEmailAddress').val() == "") {
                $('#StoreListAddUpdate_Form_txtAdminEmailAddress').val($(this).val());
            }
            else {
                // dont change if there is already a value
            }
        }
        else {
            //Dont change the field
        }

    });


    $(document).on('change', '#StoreListAddUpdate_Form_txtFirstName', function (e) {
        constructAdminName();
    });

    $(document).on('change', '#StoreListAddUpdate_Form_txtLastName', function (e) {
        constructAdminName();
    });
});


function setExpirationDateReqd() {
    if ($('#StoreListAddUpdate_Form_txtLicenseType option:selected').val() != undefined
        && $('#StoreListAddUpdate_Form_txtLicenseType option:selected').val() != null) {

        var curLicenseType = $('#StoreListAddUpdate_Form_txtLicenseType option:selected').val();
        if (curLicenseType == 140) {
            $("#StoreListAddUpdate_Form_txtExpirationDate").prop('readonly', false);
        }
        else {
            $("#StoreListAddUpdate_Form_txtExpirationDate").prop('readonly', true);
        }
    }
}

function manageScandataproviderSetup() {
    if ($('#StoreListAddUpdate_Form_txtScanDataProviderID option:selected').val() != undefined
        && $('#StoreListAddUpdate_Form_txtScanDataProviderID option:selected').val() != null) {

        var curScanDataProvider = $('#StoreListAddUpdate_Form_txtScanDataProviderID option:selected').val();
        if (curScanDataProvider == 100) {
            $("#ScanDataProviderUID").hide();
            $("#ScanDataProviderChainUID").hide();

            $('#StoreListAddUpdate_Form_txtScanDataProviderUID').removeAttr('required');
            $('#StoreListAddUpdate_Form_txtScanDataProviderUID').attr('data-ewf-noclientlivevalidation', 'true');

            $('#StoreListAddUpdate_Form_txtScanDataProviderChainUID').removeAttr('required');
            $('#StoreListAddUpdate_Form_txtScanDataProviderChainUID').attr('data-ewf-noclientlivevalidation', 'true');

        }
        else {
            $("#ScanDataProviderUID").show();
            $("#ScanDataProviderChainUID").show();
        }
    }
}


function constructAdminName() {
    var strFirstName = "";
    var strLastName = "";
    var strAdminContact = "";

    if ($('#StoreListAddUpdate_Form_txtFirstName').val() != undefined && $('#StoreListAddUpdate_Form_txtFirstName').val() != "") {
        strFirstname = $('#StoreListAddUpdate_Form_txtFirstName').val();
        strFirstname = $.trim(strFirstname.replace(",", ""));
    }
    else {
        strFirstname = "";
    }

    if ($('#StoreListAddUpdate_Form_txtLastName').val() != undefined && $('#StoreListAddUpdate_Form_txtLastName').val() != "") {
        strLastName = $('#StoreListAddUpdate_Form_txtLastName').val();
        strLastName = $.trim(strLastName.replace(",", ""));
    }
    else {
        strLastName = "";
    }

    var currentAdminName = $('#StoreListAddUpdate_Form_txtAdminContactPerson').val()
    currentAdminName = $.trim(currentAdminName.replace(",", ""));
        //check if the first and the value in the field are same
    if (currentAdminName == "" || currentAdminName == strLastName || currentAdminName == strFirstname) {
            strAdminContact = strFirstname + " , " + strLastName;
            $('#StoreListAddUpdate_Form_txtAdminContactPerson').val(strAdminContact);
        }
        else {
            // dont change if there is already a value
        }

}


function setMCSStore() {

    $('#StoreListAddUpdate_Form_txtBackOfficeProviderID').attr('required', 'true');
    $('#StoreListAddUpdate_Form_txtBackOfficeProviderID').attr('data-ewf-noclientlivevalidation', 'false');

    $('#StoreListAddUpdate_Form_txtLoyaltyHostID').attr('required', 'true');
    $('#StoreListAddUpdate_Form_txtLoyaltyHostID').attr('data-ewf-noclientlivevalidation', 'false');

    $('#StoreListAddUpdate_Form_txtLoyaltyProgramID').attr('required', 'true');
    $('#StoreListAddUpdate_Form_txtLoyaltyProgramID').attr('data-ewf-noclientlivevalidation', 'false');

    $('#StoreListAddUpdate_Form_txtScanDataProviderID').attr('required', 'true');
    $('#StoreListAddUpdate_Form_txtScanDataProviderID').attr('data-ewf-noclientlivevalidation', 'false');
}

function setUpHeadStoreForm() {
    //============================
    //  Mark as required
    //============================
    $('#StoreListAddUpdate_Form_txtScanDataProviderChainUID').attr('required', 'true');
    $('#StoreListAddUpdate_Form_txtScanDataProviderChainUID').attr('data-ewf-noclientlivevalidation', 'false');


    //============================
    //  Mark as not required
    //============================
    $('#StoreListAddUpdate_Form_txtLoyaltyHostUID').removeAttr('required');
    $('#StoreListAddUpdate_Form_txtLoyaltyHostUID').attr('data-ewf-noclientlivevalidation', 'true');

    $('#StoreListAddUpdate_Form_txtScanDataProviderUID').removeAttr('required');
    $('#StoreListAddUpdate_Form_txtScanDataProviderUID').attr('data-ewf-noclientlivevalidation', 'true');

    $('#StoreListAddUpdate_Form_txtLoyaltyProgramUID').removeAttr('required');
    $('#StoreListAddUpdate_Form_txtLoyaltyProgramUID').attr('data-ewf-noclientlivevalidation', 'true');
}



function setUpChildStoreForm() {
    //============================
    //  Mark as required
    //============================

    $('#StoreListAddUpdate_Form_txtLoyaltyHostUID').attr('required', 'true');
    $('#StoreListAddUpdate_Form_txtLoyaltyHostUID').attr('data-ewf-noclientlivevalidation', 'false');

    $('#StoreListAddUpdate_Form_txtScanDataProviderUID').attr('required', 'true');
    $('#StoreListAddUpdate_Form_txtScanDataProviderUID').attr('data-ewf-noclientlivevalidation', 'false');

    $('#StoreListAddUpdate_Form_txtLoyaltyProgramUID').attr('required', 'true');
    $('#StoreListAddUpdate_Form_txtLoyaltyProgramUID').attr('data-ewf-noclientlivevalidation', 'false');

    //============================
    //  Mark as not required
    //============================
    $('#StoreListAddUpdate_Form_txtScanDataProviderChainUID').removeAttr('required');
    $('#StoreListAddUpdate_Form_txtScanDataProviderChainUID').attr('data-ewf-noclientlivevalidation', 'true');
}