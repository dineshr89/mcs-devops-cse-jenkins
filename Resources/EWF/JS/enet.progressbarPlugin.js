﻿
(function ($) {

    //CONSTANTS
    var strCONSTewfProgressBarClass = 'EWF-JS-ProgressPanel';
    var strCONSTewfHandlerDataAttr = 'data-EWF-Handler';
    var strCONSTOnCompleteCallbackFn = "data-EWF-Progressbar-JSfunc-OnCompleteCallback";
    var strCONSTOnGoBackCallbackFn = "data-EWF-Progressbar-JSfunc-OnGobackcallback";
    var strCONSTBatchIDDataAttr = "data-EWF-Progressbar-BatchID";
    var strCONSTStoreIDDataAttr = "data-EWF-Progressbar-StoreID";
    var strCONSTTimeIntervalDataAttr = "data-EWF-Progressbar-TimeInterval";
    var strCONSTMaxPingsDataAttr = "data-EWF-Progressbar-MaxPings";


    $.fn.ewfProgressBarPlugin = function (options) {
        //Settings list and the default values
        var defaults = {
            bind: "auto"
        };

        var options = $.extend(defaults, options);


        if ((options == null) || (options.bind == "auto")) {
            //Instantiation & event binding 
            $(document).on('beginEwfProgressBar', '.' + strCONSTewfProgressBarClass, function (e) {

                /*===========================================================================
                Read Data Attributes from the progress panel div
                =============================================================================*/
                var intBatchID = -1;
                var intStoreID = -1;
                var intTimeInterval = -1;
                var intMaxPings = 0;
                var strTxtFunctionName, FnOnCompleteCallback, FnOnGoBackCallback;

                var tempIntStoreID, tempIntBatchID, tempIntTimeInterval, tempIntMaxPings;

                //Read BatchID Data Attr
                tempIntBatchID = $(this).attr(strCONSTBatchIDDataAttr);

                if (tempIntBatchID != undefined) {
                    tempIntBatchID = $.trim(tempIntBatchID);
                    if (!isNaN(tempIntBatchID)) {
                        if (tempIntBatchID > 0) {
                            intBatchID = tempIntBatchID;
                        } else {
                            intBatchID = -1;
                        }
                    } else {
                        intBatchID = -1;
                    }
                }
                else {
                    intBatchID = -1;
                }

                //Read StoreID Data Attr
                tempIntStoreID = $(this).attr(strCONSTStoreIDDataAttr);

                if (tempIntStoreID != undefined) {
                    tempIntStoreID = $.trim(tempIntStoreID);
                    if (!isNaN(tempIntStoreID)) {
                        if (tempIntStoreID > 0) {
                            intStoreID = tempIntStoreID;
                        } else {
                            intStoreID = -1;
                        }
                    } else {
                        intStoreID = -1;
                    }
                }
                else {
                    intStoreID = -1;
                }

                //Read Time Interval
                tempIntTimeInterval = $(this).attr(strCONSTTimeIntervalDataAttr);

                if (tempIntTimeInterval != undefined && tempIntTimeInterval >= 2000 && tempIntTimeInterval && 120000) {
                    intTimeInterval = tempIntTimeInterval;
                }
                else {
                    intTimeInterval = 3000;
                }

                tempIntMaxPings = $(this).attr(strCONSTMaxPingsDataAttr);

                if (tempIntMaxPings != undefined && tempIntMaxPings >= 5 && tempIntMaxPings && 1000) {
                    intMaxPings = tempIntMaxPings;
                }
                else {
                    intMaxPings = 100;
                }

                strTxtFunctionName = $(this).attr(strCONSTOnCompleteCallbackFn);
                FnOnCompleteCallback = window[strTxtFunctionName];

                strTxtFunctionName = $(this).attr(strCONSTOnGoBackCallbackFn);
                FnOnGoBackCallback = window[strTxtFunctionName];

                if (typeof FnOnCompleteCallback !== 'function') {

                    FnOnCompleteCallback = defaultOnCompleteCallback;
                }
                if (typeof FnOnGoBackCallback !== 'function') {

                    FnOnGoBackCallback = defaultOnGoBackCallback;
                }

                /*===========================================================================
                Bind Abort, Back and continue buttons
                =============================================================================*/
                $(document).on("click", ".EWF-JS-ProgressPanel-Goback", function () {
                    stopAJAXcalls();
                    cleanupBatchlog(intBatchID, intStoreID);
                    FnOnGoBackCallback();
                });

                $(document).on("click", ".EWF-JS-ProgressPanel-Finish", function () {
                    cleanupBatchlog(intBatchID, intStoreID);
                    FnOnCompleteCallback();
                });

                /*===========================================================================
                do plugin actions
                =============================================================================*/
                if (intBatchID > 0) {
                    $.fn.ewfProgressBarPlugin.doPlugin($(this), intBatchID, intStoreID, intTimeInterval, intMaxPings);
                }
                else {
                    FnOnGoBackCallback();
                    EUIShowErrorInfoBar("<span class='EWF-MGrid-CenterAlign'>BatchID not present in the control instantiation<span> ", "content");

                }

            });


        }
    };

    $.fn.ewfProgressBarPlugin.doPlugin = function (inElement, intBatchID, intStoreID, intTimeInterval, intMaxPings) {
        doPlugin(inElement, intBatchID, intStoreID, intTimeInterval, intMaxPings);
    };
    $.fn.ewfProgressBarPlugin.triggerPluginAction = function () {
        triggerPluginAction();
    };


    //private methods
    var refreshIntervalId;

    function doPlugin(inElement, intBatchID, intStoreID, intTimeInterval, intMaxPings) {

        if (intTimeInterval != undefined && intTimeInterval > 0) {

        }
        else {
            intTimeInterval = 3000;
        }

        var intPingCounter = 0;

        refreshIntervalId = setInterval(function () {
            intPingCounter++;
            //console.log(intPingCounter);

            if (intPingCounter != undefined && intPingCounter <= intMaxPings) {

                doAJAX(intBatchID, intStoreID, inElement);
            }
            else {
                stopAJAXcalls();
            }
        }, intTimeInterval);

    }

    function doAJAX(intBatchID, intStoreID, inElement) {

        var responseDataJSON;
        var strHandlerURL;

        if (intStoreID > 0) {
            strHandlerURL = '/EmagineNETCOSM/Content/GenericHandlers/BatchProcessStatusHandler.ashx?batchID=' + intBatchID + '&storeID=' + intStoreID;
        }
        else {
            strHandlerURL = '/EmagineNETCOSM/Content/GenericHandlers/BatchProcessStatusHandler.ashx?batchID=' + intBatchID;
        }

        var ajaxRequest = $.ajax({
            type: 'POST'
                , url: strHandlerURL
        });

        ajaxRequest.done(function (response) {
            if (response.resStatus == 1) {
                if (response.strResponseData != undefined) {
                    responseDataJSON = response.strResponseData[0];
                    updateUI(inElement, responseDataJSON);

                } else {
                    porgressPanelError(inElement, response.strMessage);

                }
            }
            else {
            //TODO: ??
                porgressPanelError(inElement, " " + response.strMessage + "<span class='pull-right'>[" + response.dtResponseTime + "]</span>");
            }
        });

        ajaxRequest.fail(function (jqXHR, statusText) {

            porgressPanelError(inElement, "AJAX call Failed: " + statusText);
        });


    }

    function updateUI(inElement, responseDataJSON) {

        var intPercentComplete = 0;
        var strProcessStatus = "";
        var isErrorPresent = 0;
        var IsAborted = 0;
        var IsComplete = 0;

        /*===========================================================================
        Read the response JSON from the AJAX call and setup appropriate UI elements
        of the progress bar
        =============================================================================*/
        if (responseDataJSON.PercentComplete != undefined) {
            intPercentComplete = responseDataJSON.PercentComplete;
        }
        else {
            intPercentComplete = 0;
        }

        if (responseDataJSON.ProcessStatus != undefined) {
            strProcessStatus = responseDataJSON.ProcessStatus;
        } else {
            strProcessStatus = "";
        }

        if (responseDataJSON.IsErrorPresent != undefined) {
            isErrorPresent = responseDataJSON.IsErrorPresent;
        } else {
            isErrorPresent = 0;
        }

        if (responseDataJSON.IsAborted != undefined) {
            IsAborted = responseDataJSON.IsAborted;
        } else {
            IsAborted = 0;
        }

        if (responseDataJSON.IsComplete != undefined) {
            IsComplete = responseDataJSON.IsComplete;
        } else {
            IsComplete = 0;
        }

        /*===========================================================================
        Update 
        =============================================================================*/


        if (IsAborted == 1) {

            porgressPanelError(inElement, "<b>Process Aborted due to internal error. Please Go Back and restart process!</b> <br/>" + strProcessStatus);
            stopAJAXcalls();
            $(inElement).find(".EWF-JS-ProgressPanel-Goback").show();
            $(inElement).find(".EWF-JS-ProgressPanel-Finish").hide();

        } else if (IsComplete == 1) {

            porgressPanelSuccess(inElement);
            stopAJAXcalls();
            $(inElement).find(".EWF-JS-ProgressPanel-Goback").hide();
            $(inElement).find(".EWF-JS-ProgressPanel-Finish").show();

        } else if (intPercentComplete >= 0 && intPercentComplete < 100) {

            $(inElement).find(".progress > .EWF-JS-ProgressBar").width(intPercentComplete + '%');
            if (isErrorPresent == 0) {

                $(inElement).find(".EWF-JS-ProgressPanel-Waiting").html(strProcessStatus);
                porgressPanelWaiting(inElement);

            } else {

                $(inElement).find(".EWF-JS-ProgressPanel-Error").html(strProcessStatus);
                porgressPanelError(inElement);
            }
        }
        else {
            //Do nothing, wait for next ajax call.
        }

        //        if (intPercentComplete >= 0 && intPercentComplete < 100) {

        //            $(inElement).find(".progress > .EWF-JS-ProgressBar").width(intPercentComplete + '%');
        //            if (isErrorPresent == 0) {

        //                $(inElement).find(".EWF-JS-ProgressPanel-Waiting").html(strProcessStatus);
        //                porgressPanelWaiting(inElement);

        //            } else {

        //                $(inElement).find(".EWF-JS-ProgressPanel-Error").html(strProcessStatus);
        //                porgressPanelError(inElement);
        //            }


        //        } else if (intPercentComplete == 100 && IsComplete == 1) {
        //            porgressPanelSuccess(inElement);
        //            stopAJAXcalls();

        //        } else if (IsAborted == 1) {
        //            porgressPanelError(inElement,"Fatal error occured. Please Go Back and restart process!");
        //            stopAJAXcalls();
        //        }
        //        else {
        //            //TODO: ??
        //        }

    }

    function stopAJAXcalls() {
        clearInterval(refreshIntervalId);
    }

    function cleanupBatchlog(intBatchID) {
        $.ajax({
            type: 'POST'
                , url: '/EmagineNETCOSM/Content/GenericHandlers/BatchProcessStatusHandler.ashx?batchID=' + intBatchID + '&deletelog=1'
        });
    }

    function porgressPanelSuccess(inElement) {
        $(inElement).find(" .progress > .EWF-JS-ProgressBar").width('100%');
        $(inElement).find(".EWF-JS-ProgressPanel-Error").hide();
        $(inElement).find(".EWF-JS-ProgressPanel-Waiting").hide();
        $(inElement).find(".EWF-JS-ProgressPanel-Complete").show();
    }

    function porgressPanelWaiting(inElement) {
        $(inElement).find(".EWF-JS-ProgressPanel-Error").hide();
        $(inElement).find(".EWF-JS-ProgressPanel-Waiting").show();
        $(inElement).find(".EWF-JS-ProgressPanel-Complete").hide();
    }

    function porgressPanelError(inElement, strMsg) {
        $(inElement).find(".EWF-JS-ProgressPanel-Error").show();
        $(inElement).find(".EWF-JS-ProgressPanel-Waiting").hide();
        $(inElement).find(".EWF-JS-ProgressPanel-Complete").hide();
        $(inElement).find(".EWF-JS-ProgressPanel-Error").html(strMsg);
    }

    function triggerPluginAction() {
        $('.' + strCONSTewfProgressBarClass).trigger('beginEwfProgressBar');
    }

    /*===========================================================================
    Default call back functions for abort and continue buttons in-case 
    user doesnt provide one.
    =============================================================================*/

    function defaultOnCompleteCallback() {

        parent.history.go(-1);
    }
    function defaultOnGoBackCallback() {

    }

    /*===========================================================================
    bind plugin to call and auto trigger plugin actions
    =============================================================================*/
    $('.' + strCONSTewfProgressBarClass).ewfProgressBarPlugin();
    $('.' + strCONSTewfProgressBarClass).trigger('beginEwfProgressBar');
})(jQuery);  







  