﻿
(function ($) {

    //CONSTANTS
    var strCONSTewfFormAJAXmodeClass = 'EWF-Form-AJAXmode';
    var strCONSTFrontEndSuccessUpdaterClass = "data-EWF-Form-JSfunc-FrontEndSuccessUpdater";
    var strCONSTFrontEndFailureUpdaterClass = "data-EWF-Form-JSfunc-FrontEndFailureUpdater";
    var strCONSTAJAXLockSelctor = "data-EWF-AJAX-LockArea-JQSelector";
    var strCONSTAJAXDontLockActionClass = 'EWF-AJAXmode-DontLock';
    $.fn.ewfFormAJAXmodePlugin = function (options) {
        //ajax loader timeout question div appended to body
        if ($('#AJAXmodeQuestion').length <= 0) {
            $(document.body).append('<div id="AJAXmodeQuestion" style="display:none; cursor: default">         <h3>Would you like to contine to wait?.</h3>         <input type="button" id="AJAXmodeYes" value="Yes" />         <input type="button" id="AJAXmodeNo" value="No" /> </div> ');
        }
        else {
            //alert('already there');
        }
        if (options == null) {
            //Instantiation & event binding 
            $(document).on('submit', '.' + 'EWF-Form-AJAXmode', function (e) {


                //console.log('submit triggered');
                //console.log($(this));
                if ($(this).attr('target') == null) {
                    e.preventDefault();
                    var isErrorPresent = EJUIsErrorInForm($(this).attr("id"), true);
                    if (!isErrorPresent) {
                        doPluginAction($(this));
                    }
                    else {
                        e.preventDefault();
                    }
                }

            });
        }
        else {
            //console.log('Error binding ewfFormAJAXmodePlugin: invalid options');
        }

        //Private function for the Plugin
        function doPluginAction(trigger) {

            var strTempQueryString, strHandlerURL, strTxtFunctionName, FnDoFrontEndSuccessUpdate, FnDoFrontEndFailureUpdate, objAJAXData, AJAXLockSelector, objLockArea;

            //console.log("Setting up AJAX caller ");
            //console.log(trigger);

            strTempQueryString = trigger.serialize();
            strHandlerURL = trigger.attr("action");

            strTxtFunctionName = trigger.attr(strCONSTFrontEndSuccessUpdaterClass);
            //console.log(strHandlerURL);
            FnDoFrontEndSuccessUpdate = window[strTxtFunctionName];

            strTxtFunctionName = trigger.attr(strCONSTFrontEndFailureUpdaterClass);
            //console.log(strHandlerURL);
            FnDoFrontEndFailureUpdate = window[strTxtFunctionName];

            AJAXLockSelector = trigger.attr(strCONSTAJAXLockSelctor);

            if (AJAXLockSelector == null || AJAXLockSelector == '') {
                AJAXLockSelector = '';
            } else {
                objLockArea = $(AJAXLockSelector);
            }
            //console.log(objLockArea);

            if (typeof FnDoFrontEndSuccessUpdate !== 'function') {

                FnDoFrontEndSuccessUpdate = defaultSuccessFrontEndUpdater;
            }
            if (typeof FnDoFrontEndFailureUpdate !== 'function') {

                FnDoFrontEndFailureUpdate = defaultFailureFrontEndUpdater;
            }

            objAJAXData = {
                url: strHandlerURL
                    , queryString: strTempQueryString
                    , successCallback: FnDoFrontEndSuccessUpdate
                    , failureCallback: FnDoFrontEndFailureUpdate
                    , ajaxLockArea: AJAXLockSelector
            };

            doAJAXCall(objAJAXData, trigger);

        }

        function doAJAXCall(inData, trigger) {
            //console.log("making ajax call to : " + inData.url);
            //console.log(inData);

            var ajaxRequest = $.ajax({
                type: "POST"
                    , url: inData.url
                    , data: inData.queryString
                    , beforeSend: function () {

                        if (trigger.hasClass('EWF-AJAXmode-DontLock')) {

                        } else if (inData.ajaxLockArea == '' || inData.ajaxLockArea == null) {


                            $.blockUI(
                                    { message: 'loading...',
                                        css: {
                                            border: 'none',
                                            padding: '15px',
                                            backgroundColor: '#000',
                                            '-webkit-border-radius': '10px',
                                            '-moz-border-radius': '10px',
                                            opacity: .5,
                                            color: '#fff'
                                        }
                                    });

                            setTimeout(
                                function () {
                                    if ($.active > 0) {
                                        $.blockUI({ message: $('#AJAXmodeQuestion'), css: { width: '275px'} });
                                    }

                                }
                                , 60000);
                            $('#AJAXmodeNo').click(function () {
                                $.unblockUI();
                                ajaxRequest.abort();
                                return false;
                            });
                            $('#AJAXmodeYes').click(function () {
                                $.blockUI(
                                    { message: 'loading...',
                                        css: {
                                            border: 'none',
                                            padding: '15px',
                                            backgroundColor: '#000',
                                            '-webkit-border-radius': '10px',
                                            '-moz-border-radius': '10px',
                                            opacity: .5,
                                            color: '#fff'
                                        }
                                    });
                                setTimeout(
                                function () {
                                    if ($.active > 0) {
                                        $.blockUI({ message: $('#AJAXmodeQuestion'), css: { width: '275px'} });
                                    }

                                }
                                , 60000);
                            });



                        } else {
                            //                            $(inData.ajaxLockArea).block({
                            //                                message: '',
                            //                                css: {}
                            //                            });
                            $(inData.ajaxLockArea).block(
                                    { message: 'loading...',
                                        css: {
                                            border: 'none',
                                            padding: '15px',
                                            backgroundColor: '#000',
                                            '-webkit-border-radius': '10px',
                                            '-moz-border-radius': '10px',
                                            opacity: .5,
                                            color: '#fff'
                                        }
                                    });

                            setTimeout(
                                function () {
                                    if ($.active > 0) {
                                        $(inData.ajaxLockArea).block({ message: $('#AJAXmodeQuestion'), css: { width: '275px'} });
                                    }

                                }
                                , 60000);
                            $('#AJAXmodeNo').click(function () {
                                $(inData.ajaxLockArea).unblock();
                                ajaxRequest.abort();
                                return false;
                            });
                            $('#AJAXmodeYes').click(function () {
                                $(inData.ajaxLockArea).block(
                                    { message: 'loading...',
                                        css: {
                                            border: 'none',
                                            padding: '15px',
                                            backgroundColor: '#000',
                                            '-webkit-border-radius': '10px',
                                            '-moz-border-radius': '10px',
                                            opacity: .5,
                                            color: '#fff'
                                        }
                                    });
                                setTimeout(
                                function () {
                                    if ($.active > 0) {
                                        $(inData.ajaxLockArea).block({ message: $('#AJAXmodeQuestion'), css: { width: '275px'} });
                                    }

                                }
                                , 60000);
                            });


                        }

                    }

            });

            ajaxRequest.done(function (response) {

                var contentType;
                var responseStatus = "0";
                var responseMsg = "";
                var responseData = "";
                //                console.log("response: ");
                //                console.log(response);

                contentType = ajaxRequest.getResponseHeader("content-type");
                if (contentType.toLowerCase().indexOf("json") >= 0) {

                    responseStatus = response.resStatus;
                    responseData = response.strResponseData;
                    responseMsg = response.strMessage;
                }
                else if (contentType.toLowerCase().indexOf("xml") >= 0) {

                    responseStatus = $(response).find('resStatus').text();
                    responseMsg = $(response).find('strMessage').text();
                    responseData = $(response).find('strResponseData').text();
                    try {
                        if (responseStatus == "1") {
                            responseData = $.parseXML(responseData);
                        }

                    } catch (err) {
                        // Invalid XML
                        //responseStatus = "0";
                        responseData = "";
                        //responseMsg = "Invalid XML";
                    }
                }
                else {

                    responseStatus = "0";
                    responseData = "";
                    responseMsg = "Content type not Defined";
                }

                if (responseStatus === "1") {
                    //responseData = $.parseXML(responseData);
                    /*
                    Check whether it is a postback if it is a postback then 
                    hide the filters for phone screen by collapsing
                    */
                    if (window.innerWidth <= 786) {
                        $('#reportCustomFilter').removeClass('in');
                        $('#divEnetCustomFilter').removeClass('in');
                    }


                    inData.successCallback(responseData, trigger);
                }
                else {
                    inData.failureCallback(responseMsg, trigger, responseData);

                }
            });

            ajaxRequest.fail(function (jqXHR, statusText) {
                inData.failureCallback(jqXHR.statusText, trigger);
            });

            ajaxRequest.always(function () {
                setTimeout(
                function () {
                    if (trigger.hasClass('EWF-AJAXmode-DontLock')) {
                    } else if (inData.ajaxLockArea == '') {
                        $.unblockUI();
                    } else {
                        $(inData.ajaxLockArea).unblock();
                    }
                }
                , 100);

            });

        }

        function defaultSuccessFrontEndUpdater(response, trigger) {

            var responseDataObject = jQuery.parseJSON(response);


            $(trigger).html('<span class="label success">Success:</span> Form updated Successfully');
        }

        function defaultFailureFrontEndUpdater(responseMsg, trigger) {
            //alert('here');
            EUIShowErrorInfoBar("<span class='EWF-MGrid-CenterAlign'> oops! something went wrong that.<span> " + responseMsg, "content");

        }

    };

    $('form.' + strCONSTewfFormAJAXmodeClass).ewfFormAJAXmodePlugin();

})(jQuery);

//==========================================================
// DESCRIPTION: Plugin for filldown UI in the grids
// NOTES: Sample syntax required
//         <input type="hidden" class="EWF-UI-FillDown" data-EWF-FillDown-MatchingName="filldownfieldName">
//          $('.EWF-UI-FillDown').fillDown();
//==========================================================

//TODO: 
//1. Ability to have a dropdown and filldown to a textbox so make it upto the user what they want on the top
//2. Send in class styling
//3. Ability to do some amount of validation
//4. refresh -> developer should be able to reload the control
//5. not error out on empty grid
//6. filldown by class, filldown by name
//7. Find out a way to revert back the last action


//PROBLEMS: 
// 2. The filldown children IDs are not dependable as there is txtDeptID_

(function ($) {
    $.fn.extend({
        fillDown: function (options) {

            //Settings list and the default values
            var defaults = {
                override: true
            };
            var options = $.extend(defaults, options);

            return this.each(function () {

                var o = options;

                //Assign current element to variable
                var objFilldown = $(this);
                var validator = validateFillDown(objFilldown);
                if (validator.isValid) {

                    if ($("[id*='" + validator.MappedField + "']").length > 0) {
                        //generate HTML
                        autogenerateControl(validator.MappedField, objFilldown)

                        //bind click event on the button next to the element
                        // $('#EWF-txtFillParent-' + validator.MappedField + '_Apply').on("click", function (event) {
                        $(document).on("click", '#EWF-txtFillParent-' + validator.MappedField + '_Apply', function (event) {
                            event.preventDefault;

                            //Validate if the value is selected
                            var fillDownValue = $('#EWF-txtFillParent-' + validator.MappedField + '_ID').val();
                            if (fillDownValue != undefined || fillDownValue != '') {

                                $("[id*='" + validator.MappedField + "']").each(function (i) {
                                    //if the overide to true no need to check whether it has values
                                    if (o.override) {
                                        $(this).val(fillDownValue);
                                        $(this).trigger('change');
                                    }
                                    else {
                                        if ($(this).val() == "" || $(this).val() == "-1") {
                                            $(this).val(fillDownValue);
                                            $(this).trigger('change');
                                        }
                                    }
                                });
                            }
                            else {
                                alert('Please enter a select a valid value');
                            }

                        });
                    }

                }

            }); //this.each(function () {
        }
    }); //fillDown: function (options) { 

    function autogenerateControl(mappedField, objFilldown) {
        //find the element type
        var generatedComponent = '';
        var tagName = $("[id*='" + mappedField + "']").get(0).tagName.toLowerCase();

        if (tagName == undefined || tagName == '') {
            EUIShowInfoBar('Unable to to find the tag name of the fill down children');
        }
        else if (tagName == "select") {
            generatedComponent = generateDropDown(mappedField, objFilldown);
        }
        else if (tagName == "input") {
            //find the type
            var fieldType = $("[id*='" + mappedField + "']").attr('type');

            //could be textbox/checkbox
            if (fieldType == 'text') {
                generatedComponent = generateInput(mappedField, objFilldown);
            }
        }
        objFilldown.parent().attr("nowrap", "nowrap"); //Fix to make sure the filldown shows next to the input element
        if (objFilldown.parent().find("#EWF-FillDown-Container").length <= 0) {
            objFilldown.parent().append(generatedComponent);
        }
    }


    function generateDropDown(mappedField, objFilldown) {
        var component = '';
        //decide whether to depend of the current object or the children of the filldown
        component = '<div id="EWF-FillDown-Container" class="btn-group">'
        //component = component + '<select name="' + objFilldown.attr('name') + '" class="span5" id="' + objFilldown.attr('id') + '" form="' + objFilldown.closest("form").attr('id') + '">';
        component = component + '<select name="EWF-txtFillParent-' + mappedField + '" class="EnetUI-InlineBlock ' + $("[id*='" + mappedField + "']").attr("class") + '" id="EWF-txtFillParent-' + mappedField + '_ID" form="' + objFilldown.closest("form").attr('id') + '">';
        component = component + $("[id*='" + mappedField + "']").html();
        component = component + '</select>';
        //component = component + '   <a href="#"><img alt="Copy in all fields below" class="EWF-JQE-UpdateAmt" id="EWF-txtFillParent-' + mappedField + '_Apply" height="16px" width="16px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAnVJREFUeNpck01rFEEQhqv6Yze72SSaYIwKMRADMeYqCHrR4MW7OefkTYgQvAj6G0T9AQp6FRXPHvWuXqKCKAkYEuNudueju7rLmtlM3LWhZ3p66n2q+p0aXH+3A8MDz2t2j5BDSx7ocNMw6nbA2m0A3hyMNgdJOixHnMuSdIW8L9blHjODsRZGmo1ZWQ8Dkiz/vwKK5EUkyRm5v8foCSKlyst6KNo4yTSQvZyRCDhSuQOlggEVgEINUarBAYgxGPpimY5YAgCidxWgqkAAEbTwG5ohoPkHGG/qo+xp5mC/6zmQE8pwBaAiKynuRIPhwI4Ius826tAoGXONmr5ItXz5VycBpVQ/9eGI0avJprkWdX0cGd+Laq8EbO12KgNaIU8fk3PTFIqz4KBXLK7Czn7nwV7S+FBrwgpXFbR7aQX4FJy7E/P0uRooa6AK9M7/MWhv5ZoSqAA5qzKicMLW6y8iuevksjUYYhTGi5Ha3NcKPwZxOlSA5WYORmJ/k4af3gpJ3/NElyTx4lH2Qmxqr5QxT3Y6XTg32oVJy0ACMjenuoWT0I0Knm1Z+ObCdob2bp16r7k4WP8b7DplN6aQ4+pMgKUxLAG59IvKxKyeiI2ErZ1KYe0MwdKEfpMFeOgdQeI8JGg2Flv4dXXaweXjEYpe6Er7FFpTNVGUawQFC/ILXRgL8FbO+/JHuDI/ips3ZvXT5WMaipYrhGLykUVGPsckEZ2W+0KMcYIQRzNmfXUK4ozRn8cttM82w3rPy4mZgzRcT3Rt6ZMv8rxtRATOOS2QhrxoSkBLXlhUanS+pb6LSu1ndBIxJLLvyn8lRmet1TLhrwADADZIT56vKModAAAAAElFTkSuQmCC"></a>';
        component = component + '   <a href="#" alt="Copy in all fields below" class="EWF-JQE-UpdateAmt" id="EWF-txtFillParent-' + mappedField + '_Apply"><i class="glyphicon glyphicon-arrow-down icon-arrow-down"></i></a>';
        component = component + '</div>';
        return component;
    }

    function generateInput(mappedField, objFilldown) {
        var component = '';
        component = '<div id="EWF-FillDown-Container">'
        component = component + '   <input type="text" name="EWF-txtFillParent-' + mappedField + '" class="EnetUI-InlineBlock ' + $("[id*='" + mappedField + "']").attr("class") + '" id="EWF-txtFillParent-' + mappedField + '_ID" form="' + objFilldown.closest("form").attr('id') + '">';
        //component = component + '   <a href="#"><img alt="Copy in all fields below" class="EWF-JQE-UpdateAmt" id="EWF-txtFillParent-' + mappedField + '_Apply" height="16px" width="16px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAnVJREFUeNpck01rFEEQhqv6Yze72SSaYIwKMRADMeYqCHrR4MW7OefkTYgQvAj6G0T9AQp6FRXPHvWuXqKCKAkYEuNudueju7rLmtlM3LWhZ3p66n2q+p0aXH+3A8MDz2t2j5BDSx7ocNMw6nbA2m0A3hyMNgdJOixHnMuSdIW8L9blHjODsRZGmo1ZWQ8Dkiz/vwKK5EUkyRm5v8foCSKlyst6KNo4yTSQvZyRCDhSuQOlggEVgEINUarBAYgxGPpimY5YAgCidxWgqkAAEbTwG5ohoPkHGG/qo+xp5mC/6zmQE8pwBaAiKynuRIPhwI4Ius826tAoGXONmr5ItXz5VycBpVQ/9eGI0avJprkWdX0cGd+Laq8EbO12KgNaIU8fk3PTFIqz4KBXLK7Czn7nwV7S+FBrwgpXFbR7aQX4FJy7E/P0uRooa6AK9M7/MWhv5ZoSqAA5qzKicMLW6y8iuevksjUYYhTGi5Ha3NcKPwZxOlSA5WYORmJ/k4af3gpJ3/NElyTx4lH2Qmxqr5QxT3Y6XTg32oVJy0ACMjenuoWT0I0Knm1Z+ObCdob2bp16r7k4WP8b7DplN6aQ4+pMgKUxLAG59IvKxKyeiI2ErZ1KYe0MwdKEfpMFeOgdQeI8JGg2Flv4dXXaweXjEYpe6Er7FFpTNVGUawQFC/ILXRgL8FbO+/JHuDI/ips3ZvXT5WMaipYrhGLykUVGPsckEZ2W+0KMcYIQRzNmfXUK4ozRn8cttM82w3rPy4mZgzRcT3Rt6ZMv8rxtRATOOS2QhrxoSkBLXlhUanS+pb6LSu1ndBIxJLLvyn8lRmet1TLhrwADADZIT56vKModAAAAAElFTkSuQmCC"></a>';
        component = component + '   <a href="#" alt="Copy in all fields below" class="EWF-JQE-UpdateAmt" id="EWF-txtFillParent-' + mappedField + '_Apply"><i class="glyphicon glyphicon-arrow-down icon-arrow-down"></i></a>';
        component = component + '</div>';
        return component;
    }

    function validateFillDown(objFilldown) {
        var isOpSuccessful = false;

        //get all the current fields
        //TODO: Have to validate for the reference ids and names

        var strMappedFieldID = '';
        strMappedFieldID = objFilldown.attr('data-EWF-FillDown-MatchingName');

        if (strMappedFieldID == undefined || strMappedFieldID == '') {
            EUIShowInfoBar('Unable to setup Fill Down because data-EWF-FillDown-MatchingName element ID is not defined properly or does not exist.');
            isOpSuccessful = false;
        }
        else {
            isOpSuccessful = true;
        }

        return {
            isValid: isOpSuccessful
                    , MappedField: strMappedFieldID
        }
    }

})(jQuery);


//        //EWF Menu
//        (function ($) {
//            $("#EWF-Menu-Main > li.dropdown > ul.dropdown-menu > li.dropdown > ul.dropdown-menu").parent("li.dropdown").addClass("ewf-secodarymenu");
//            $("#EWF-Menu-Main > li.dropdown > ul.dropdown-menu > li.dropdown > ul.dropdown-menu").addClass("ewf-thirdlevelmenu");

//            // To show hide third level menu on click of any second level menu with third level menu.
//            $('.ewf-secodarymenu > a').click(function (e) {
//                e.preventDefault();
//                var flag = false;
//                flag = $(this).parent("li").children("ul").hasClass("ewf-secondarymenu-show");
//                $(this).parent("li").parent("ul").children("li").children("ul").removeClass("ewf-secondarymenu-show");
//                $(this).parent("li").parent("ul").children("li").removeClass("open");
//                if (flag === false) {
//                    $(this).parent("li").children("ul").addClass("ewf-secondarymenu-show");
//                    $(this).parent("li").addClass("open");
//                }

//            });

//            //To stop dismiss on click of overlay
//            $(".ewf-secodarymenu").click(function (e) {
//                e.stopPropagation();
//                e.preventDefault();
//            });
//            $(".ewf-thirdlevelmenu").click(function (event) {
//                event.stopPropagation();
//            });


//            $("ul.EnetSubMenu > li.dropdown > ul.dropdown-menu").parent("li.dropdown").addClass("ewf-secodary-submenu");
//            $("ul.EnetSubMenu > li.dropdown > ul.dropdown-menu").addClass("ewf-thirdlevel-submenu");
//            $(".ewf-secodary-submenu").click(function (e) {
//                e.stopPropagation();
//                e.preventDefault();
//            });

//            $(".ewf-thirdlevel-submenu").click(function (event) {
//                event.stopPropagation();
//            });
//            // To show hide third level menu on click of any second level menu with third level menu.
//            $('.ewf-secodary-submenu > a').click(function (e) {
//                e.preventDefault();
//                //alert('clicked');

//                var flag = false;
//                flag = $(this).parent("li").hasClass("open");
//                $(this).parent("li").removeClass("open");
//                alert('flag ' + flag);
//                if (flag === false) {
//                    $('li.dropdown').removeClass('open');
//                    $(this).parent("li").addClass("open");
//                }

//            });

//        })(jQuery);    
    

/*
This function parses QueryString and returns the value of a specific querystring parameter.
If parameter does not exits, it returns an empty string.
Input 1: QueryString/URL (Required)
Input 2: Parameter Name to search (Required)
Output:  Value of the input parameter in Query String
*/
function EJUParseQueryString(inQS, inFieldName) {
    var arQC = inQS.split('?');
    var strQCOnly = "";
    var strReturnValue = "";

    if (arQC.length > 0) {//If input has ?
        strQCOnly = arQC[arQC.length - 1];
    }
    else {//If input does not have ?
        strQCOnly = inQS;
    }
    var arParamList = strQCOnly.split('&');
    for (var i = 0; i < arParamList.length; i++) {
        var arParam = arParamList[i].split("=");
        if (arParam.length >= 2) {
            if (arParam[0].toUpperCase() == inFieldName.toUpperCase()) {
                strReturnValue = arParam[1];
                break;
            }
        }
    }
    return strReturnValue
}

/*
This function finds a given object's parent form ID
If the object does not have a parent form or the parent form does not have an ID, it returns empty string
Input 1: Current Object (Required)
Output:  String containing the ID of the parent form
*/
function EJUGetParentFormID(obj) {
    var frmList = $(obj).parents("form");
    var strID = "";
    if (frmList.length > 0) {
        strID = $(frmList.get(0)).attr('id');
    }
    else {
        var currTableID = $(obj).parents("table");
        strID = $(currTableID).attr('id');
    }
    return strID;
}

function EJUGetFormID(obj) {
    var frmID = $(obj).attr("form");
    return frmID;
}

/*
This function finds out if an element with the given ID exists in the current document's DOM
*/
function EJUISIDUnique(inStrID, objContainer) {
    var intMatchCount;
    if (objContainer == null) {
        intMatchCount = $(inStrID).length;
    }
    else {
        intMatchCount = objContainer.find(inStrID).length;
    }
    if (intMatchCount == 1) {
        return true;
    }
    else {
        return false;
    }
}

/*
This function finds out if an element with the given ID exists in the current document's DOM
*/
$(document).ready(function () {
    $.fn.serializeObjectForJSON = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
});

/*This function sets a local cookie */
function EWFCookiesSet(inCookieName, inValue, inHours) {
    var dtExpirationTime = new Date();
    var intMinutes;
    var strValue = escape(inValue);
    var strCookieName = escape(inCookieName);
    intMinutes = parseInt(inHours);
    if (isNaN(intMinutes) == true) {
        intMinutes = 1444; //24 Hrs
    }

    dtExpirationTime.setMinutes(dtExpirationTime.getMinutes() + intMinutes);
    var strCookieValue = strCookieName + "=" + strValue + ';expires=' + dtExpirationTime.toGMTString() + '; path=/';
    document.cookie = strCookieValue;
}
/*This function get the value of a local cookie */
function EWFCookiesGet(inCookieName) {
    var strCookieName = escape(inCookieName);
    var strToken = strCookieName + '=';
    var arrCookies = document.cookie.split(';');

    var strReturnCookie = '';
    for (var i = 0; i < arrCookies.length; i++) {
        var strCookieCurrent = arrCookies[i];
        while (strCookieCurrent.charAt(0) == ' ') {
            strCookieCurrent = strCookieCurrent.substring(1, strCookieCurrent.length);
        }
        if (strCookieCurrent.indexOf(strToken) == 0) {
            strReturnCookie = strCookieCurrent;
            break;
        }
    }
    if (strReturnCookie.length > 0) {
        strReturnCookie = strReturnCookie.substring(strToken.length, strReturnCookie.length);
        strReturnCookie = unescape(strReturnCookie);
    }
    return strReturnCookie;
}
/*This function removes a local cookie */
function EWFCookiesDelete(inCookieName) {
    EWFCookiesSet(inCookieName, '', -1);
}

/* This function replaces contents of an HTML DOM and rebinds onload UI events */
function EWFUpdatePageContent(inContainerID, inContent) {
    $(inContainerID).html(inContent);
    EJUBindAllUIElements(inContainerID);
}


/* ENET UI COMPONENETS */
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of function to display Enet Error Bar -------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
var EUIInfoBarLastTimeoutTimer;
var EUIInfoBarIsTimeoutSet = false;
function EUIShowInfoBar(inMessage) {
    var strCloseButton = "<a class=\"EnetInfoBarCloseLink\" href=\"javascript:EUICloseInfoBar();\"><span class=\"EnetInfoBarCloseButton\">&#160;</span></a>";
    if ($(".EnetInfoBar").length <= 0) {
        //If bar does not exist on the current document, create it.
        var strHTML = "<div class=\"EnetInfoBar\"></div>";
        $(document.body).prepend(strHTML);
    }
    $("div.EnetInfoBar").prepend(strCloseButton);
    $("div.EnetInfoBar").prepend(inMessage + "<br />");
    $("div.EnetInfoBar").slideDown(1000);
    if (EUIInfoBarIsTimeoutSet) {
        EUIInfoBarIsTimeoutSet = false;
        window.clearTimeout(EUIInfoBarLastTimeoutTimer);
    }
    EUIInfoBarIsTimeoutSet = true;
    EUIInfoBarLastTimeoutTimer = window.setTimeout(function () {
        EUIInfoBarIsTimeoutSet = true;
        EUICloseInfoBar();
    }, 8000);
}
function EUICloseInfoBar() {
    if (EUIInfoBarIsTimeoutSet) {
        EUIInfoBarIsTimeoutSet = false;
        window.clearTimeout(EUIInfoBarLastTimeoutTimer);
    }
    $(".EnetInfoBar").html("");
    $(".EnetInfoBar").slideUp(500);

}
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of function to display Enet Error Bar -------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of Function to setup Enet Number Range Selector UI----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
function EUINRSSetup(inSliderID) {
    var strSliderID = '#' + inSliderID;
    var strSliderMin = $(strSliderID).attr('data-EWF-ERS-min');
    var strSliderMax = $(strSliderID).attr('data-EWF-ERS-max');
    var strStep = $(strSliderID).attr('data-EWF-ERS-step');
    var strStartValue = $(strSliderID).attr('data-EWF-ERS-startValue');
    var strEndValue = $(strSliderID).attr('data-EWF-ERS-endValue');
    var strStartDataFieldID = $(strSliderID).attr('data-EWF-ERS-startDataFieldID');
    var strEndDataFieldID = $(strSliderID).attr('data-EWF-ERS-endDataFieldID');
    var strDataLabelID = $(strSliderID).attr('data-EWF-ERS-dataLabelID');
    var strDataPrefix = $(strSliderID).attr('data-EWF-ERS-dataDiplayPrefix');
    var isErrorPresent = true;
    var strErrorMessage = "Validation not performed";

    //For non-required fields, set default values if non value is specified
    if (jQuery.trim(strStep) == '') {
        strStep = "1";
    }
    if (jQuery.trim(strStartValue) == '') {
        strStartValue = strSliderMin;
    }
    if (jQuery.trim(strEndValue) == '') {
        strEndValue = strSliderMax;
    }
    if (strDataPrefix == 'undefined') {
        strDataPrefix = '';
    }

    //Convert text to numeric values
    var dblSliderMin = parseFloat(strSliderMin);
    var dblSliderMax = parseFloat(strSliderMax);
    var dblStep = parseFloat(strStep);
    var dblStartValue = parseFloat(strStartValue);
    var dblEndValue = parseFloat(strEndValue);

    //Apply Validation Rules
    if (!isNaN(dblSliderMin)) {
        if (!isNaN(dblSliderMax)) {
            if (!isNaN(dblStep)) {
                if (!isNaN(dblStartValue)) {
                    if (!isNaN(dblEndValue)) {
                        if (EJUISIDUnique('#' + strStartDataFieldID, null)) {
                            if (EJUISIDUnique('#' + strEndDataFieldID, null)) {
                                if (EJUISIDUnique('#' + strDataLabelID, null)) {
                                    isErrorPresent = false;
                                    strErrorMessage = "";
                                }
                                else {
                                    isErrorPresent = true;
                                    strErrorMessage = 'Unable to generate number range UI (' + inSliderID + ')  because end data label element does not exist(' + strDataLabelID + ')';
                                }
                            }
                            else {
                                isErrorPresent = true;
                                strErrorMessage = 'Unable to generate number range UI (' + inSliderID + ')  because end data field element does not exist(' + strEndDataFieldID + ')';
                            }

                        }
                        else {
                            isErrorPresent = true;
                            strErrorMessage = 'Unable to generate number range UI (' + inSliderID + ')  because start data field element does not exist(' + strStartDataFieldID + ')';
                        }
                    }
                    else {
                        isErrorPresent = true;
                        strErrorMessage = 'Unable to generate number range UI (' + inSliderID + ')  because end value is not valid (' + strEndValue + ')';
                    }
                }
                else {
                    isErrorPresent = true;
                    strErrorMessage = 'Unable to generate number range UI (' + inSliderID + ')  because start value is not valid (' + strStartValue + ')';
                }
            }
            else {
                isErrorPresent = true;
                strErrorMessage = 'Unable to generate number range UI (' + inSliderID + ')  because step value is not valid (' + strStep + ')';
            }
        }
        else {
            isErrorPresent = true;
            strErrorMessage = 'Unable to generate number range UI (' + inSliderID + ')  because max is not valid (' + strSliderMax + ')';
        }
    }
    else {
        isErrorPresent = true;
        strErrorMessage = 'Unable to generate number range UI (' + inSliderID + ')  because min is not valid (' + dblSliderMin + ')';
    }

    //Build UI
    if (!isErrorPresent) {
        $(strSliderID).slider({
            range: true
                , animate: true
                , min: dblSliderMin
                , max: dblSliderMax
                , step: dblStep
                , values: [dblStartValue, dblEndValue]
	            , slide: function (event, ui) {
	                $('#' + strDataLabelID).html(strDataPrefix + ui.values[0] + ' - ' + strDataPrefix + ui.values[1]);
	                $('#' + strStartDataFieldID).val(ui.values[0]);
	                $('#' + strEndDataFieldID).val(ui.values[1]);
	            }
	            , change: function (event, ui) {//Change method needed to update data label if user types in values into data fields
	                $('#' + strDataLabelID).html(strDataPrefix + ui.values[0] + ' - ' + strDataPrefix + ui.values[1]);
	                $('#' + strStartDataFieldID).val(ui.values[0]);
	                $('#' + strEndDataFieldID).val(ui.values[1]);
	            }
        });
        $('#' + strDataLabelID).html(strDataPrefix + dblStartValue + ' - ' + strDataPrefix + dblEndValue);
        $('#' + strStartDataFieldID).bind({
            change: function () {
                var strValue = $(this).val();
                var dblValue = parseFloat(strValue);
                $(strSliderID).slider('values', 0, dblValue);
            }
        });
        $('#' + strEndDataFieldID).bind({
            change: function () {
                var strValue = $(this).val();
                var dblValue = parseFloat(strValue);
                $(strSliderID).slider('values', 1, dblValue);
            }
        });
    }
    else {
        //If there was an error
        EUIShowInfoBar(strErrorMessage);
        $(strSliderID).html('Error generating UI.');
    }
}
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of Function to setup Enet Number Range Selector UI----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of Function to setup Enet DATE Range Selector UI----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/

function EUIDRSSetup(inDRID) {
    var strDRSelectorID = '#' + inDRID;
    var strMinDate = $(strDRSelectorID).attr('data-EWF-ERS-min');
    var strMaxDate = $(strDRSelectorID).attr('data-EWF-ERS-max');
    var strStartDataFieldID = $(strDRSelectorID).attr('data-EWF-ERS-startDataFieldID');
    var strStartDataFieldID = $(strDRSelectorID).attr('data-EWF-ERS-startDataFieldID');
    var strEndDataFieldID = $(strDRSelectorID).attr('data-EWF-ERS-endDataFieldID');
    var dtMinDate = null;
    var dtMaxDate = null;
    var strShowArrows = $(strDRSelectorID).attr('data-EWF-ERS-showArrows');
    var isArrowShown = false;
    var intXPosition = $(strDRSelectorID).offset().left;
    var intYPosition = $(strDRSelectorID).offset().top;
    var strControlContainer = strDRSelectorID.replace('_EnetDRS', '_Container');
    var isErrorPresent = true;
    var strErrorMessage = "Validation not performed";

    //For non-required fields, set default values if non value is specified
    if ((strShowArrows == undefined) || (jQuery.trim(strShowArrows).length = 0)) {
        strShowArrows = 'false';
    }
    if ((strMinDate == undefined) || (jQuery.trim(strMinDate).length = 0)) {
        strMinDate = Date.parse('1/1/1990').toString('M/d/yyyy');
    }
    if ((strMaxDate == undefined) || (jQuery.trim(strMaxDate).length = 0)) {

        strMaxDate = Date.today().toString('M/d/yyyy');
    }
    //Type cast values appropriately
    if (strShowArrows == 'true' || strShowArrows == '1' || strShowArrows == 'yes') {
        isArrowShown = true;
    }
    else {
        isArrowShown = false;
    }

    if (jQuery.trim(strMinDate) != '') {
        dtMinDate = Date.parse(strMinDate);
    }
    else {
        dtMinDate = null;
    }
    if (jQuery.trim(strMaxDate) != '') {
        dtMaxDate = Date.parse(strMaxDate);
    }
    else {
        dtMaxDate = null;
    }


    //Validate Settings
    if (dtMinDate != null) {
        if (dtMaxDate != null) {
            if (EJUISIDUnique('#' + strStartDataFieldID, null)) {
                if (EJUISIDUnique('#' + strEndDataFieldID, null)) {
                    isErrorPresent = false;
                    strErrorMessage = "";
                }
                else {
                    isErrorPresent = true;
                    strErrorMessage = 'Unable to generate date range UI (' + inDRID + ')  because end data field element does not exist(' + strEndDataFieldID + ')';
                }

            }
            else {
                isErrorPresent = true;
                strErrorMessage = 'Unable to generate number range UI (' + inSliderID + ')  because start data field element does not exist(' + strStartDataFieldID + ')';
            }


        }
        else {
            isErrorPresent = true;
            strErrorMessage = 'Unable to generate date range UI (' + inDRID + ')  because max date is not the proper format(' + strMaxDate + ')';
        }
    }
    else {
        isErrorPresent = true;
        strErrorMessage = 'Unable to generate date range UI (' + inDRID + ')  because min date is not the proper format(' + strMinDate + ')';
    }
    if (!isErrorPresent) {
        $(strDRSelectorID).daterangepicker({
            arrows: isArrowShown
                        , earliestDate: dtMinDate.toString('M/d/yyyy')
                        , latestDate: dtMaxDate.toString('M/d/yyyy')
                        , onClose: function () { }
                        , onChange: function () { EUIDRChange(inDRID) }
        });
    }
    else {
        EUIShowInfoBar(strErrorMessage);
        $(strDRSelectorID).val('Error generating ui');
    }
}

function EUIDRSSetupBS3(inDRID) {
    var strDRSelectorID = '#' + inDRID;
    var strMinDate = $(strDRSelectorID).attr('data-EWF-ERS-min');
    var strMaxDate = $(strDRSelectorID).attr('data-EWF-ERS-max');
    var strStartDataFieldID = $(strDRSelectorID).attr('data-EWF-ERS-startDataFieldID');
    var strStartDataFieldID = $(strDRSelectorID).attr('data-EWF-ERS-startDataFieldID');
    var strEndDataFieldID = $(strDRSelectorID).attr('data-EWF-ERS-endDataFieldID');
    var strDefaultRange = $(strDRSelectorID).attr('data-EWF-ERS-defaultrange');
    var dtMinDate = null;
    var dtMaxDate = null;
    var strShowArrows = $(strDRSelectorID).attr('data-EWF-ERS-showArrows');
    var isArrowShown = false;
    var intXPosition = $(strDRSelectorID).offset().left;
    var intYPosition = $(strDRSelectorID).offset().top;
    var strControlContainer = strDRSelectorID.replace('_EnetDRS', '_Container');
    var isErrorPresent = true;
    var strErrorMessage = "Validation not performed";

    var strCalendarOpenDirection = $(strDRSelectorID).attr('data-ewf-ers-opendirection');
    var strCalendarDirection = "right";

    if (typeof strCalendarOpenDirection !== typeof undefined && strCalendarOpenDirection !== false) {
        strCalendarDirection = strCalendarOpenDirection;
    }
    else {
        strCalendarDirection = "right"
    }



    //For non-required fields, set default values if non value is specified
    if ((strShowArrows == undefined) || (jQuery.trim(strShowArrows).length = 0)) {
        strShowArrows = 'false';
    }
    if ((strMinDate == undefined) || (jQuery.trim(strMinDate).length = 0)) {
        strMinDate = Date.parse('1/1/1990').toString('M/d/yyyy');
    }
    if ((strMaxDate == undefined) || (jQuery.trim(strMaxDate).length = 0)) {

        strMaxDate = Date.today().toString('M/d/yyyy');
    }
    //Type cast values appropriately
    if (strShowArrows == 'true' || strShowArrows == '1' || strShowArrows == 'yes') {
        isArrowShown = true;
    }
    else {
        isArrowShown = false;
    }

    if (jQuery.trim(strMinDate) != '') {
        dtMinDate = Date.parse(strMinDate);
    }
    else {
        dtMinDate = null;
    }
    if (jQuery.trim(strMaxDate) != '') {
        dtMaxDate = Date.parse(strMaxDate);
    }
    else {
        dtMaxDate = null;
    }


    //Validate Settings
    if (dtMinDate != null) {
        if (dtMaxDate != null) {
            if (EJUISIDUnique('#' + strStartDataFieldID, null)) {
                if (EJUISIDUnique('#' + strEndDataFieldID, null)) {
                    isErrorPresent = false;
                    strErrorMessage = "";
                }
                else {
                    isErrorPresent = true;
                    strErrorMessage = 'Unable to generate date range UI (' + inDRID + ')  because end data field element does not exist(' + strEndDataFieldID + ')';
                }

            }
            else {
                isErrorPresent = true;
                strErrorMessage = 'Unable to generate number range UI (' + inSliderID + ')  because start data field element does not exist(' + strStartDataFieldID + ')';
            }


        }
        else {
            isErrorPresent = true;
            strErrorMessage = 'Unable to generate date range UI (' + inDRID + ')  because max date is not the proper format(' + strMaxDate + ')';
        }
    }
    else {
        isErrorPresent = true;
        strErrorMessage = 'Unable to generate date range UI (' + inDRID + ')  because min date is not the proper format(' + strMinDate + ')';
    }
    if (!isErrorPresent) {

        var strMonth1Name = moment(moment().subtract(1, 'month')).format("MMM") + ' ' + moment().subtract(1, 'month').format("YYYY");
        var strMonth2Name = moment(moment().subtract(2, 'month')).format("MMM") + ' ' + moment().subtract(2, 'month').format("YYYY");
        var strMonth3Name = moment(moment().subtract(3, 'month')).format("MMM") + ' ' + moment().subtract(3, 'month').format("YYYY");
        var strMonth4Name = moment(moment().subtract(4, 'month')).format("MMM") + ' ' + moment().subtract(4, 'month').format("YYYY");
        var strMonth5Name = moment(moment().subtract(5, 'month')).format("MMM") + ' ' + moment().subtract(5, 'month').format("YYYY");



        var ranTemp =
        {
            "This Week": [moment().startOf("week"), moment()]
                        , "This Month": [moment().startOf("month"), moment().endOf("month")]
                        , "This Year": [moment().startOf("year"), moment().endOf("year")]
        }

        ranTemp[strMonth1Name] = [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')];
        ranTemp[strMonth2Name] = [moment().subtract(2, 'month').startOf('month'), moment().subtract(2, 'month').endOf('month')];
        ranTemp[strMonth3Name] = [moment().subtract(3, 'month').startOf('month'), moment().subtract(3, 'month').endOf('month')];
        ranTemp[strMonth4Name] = [moment().subtract(4, 'month').startOf('month'), moment().subtract(4, 'month').endOf('month')];
        ranTemp[strMonth5Name] = [moment().subtract(5, 'month').startOf('month'), moment().subtract(5, 'month').endOf('month')];


    var optionSet1 = {
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month'),
        minDate: '01/01/1990',
        maxDate: '12/31/2099',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: false,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        formName: 'frmInventoryChangeByItemOpForm',
        ranges: ranTemp,
        opens: strCalendarDirection,
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM/DD/YYYY',
        separator: ' - ',
        locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    };

        //Bind Date Range selector
        $(strDRSelectorID).daterangepicker(optionSet1);

        if (strDefaultRange != null && strDefaultRange != "") {
            //================================
            //  THIS MONTH FROM TODAY
            //================================
            if (strDefaultRange == 'MTD') {
                if ($(strDRSelectorID).val() == "") {
                    $(strDRSelectorID).data('daterangepicker').setStartDate(moment().startOf('month'));
                    $(strDRSelectorID).data('daterangepicker').setEndDate(moment());

                    //set the hidden field values
                    $('#' + strStartDataFieldID).val(moment().startOf('month').format('MM/DD/YYYY'));
                    $('#' + strEndDataFieldID).val(moment().format('MM/DD/YYYY'));
                }
            }
            //================================
            //  THIS WEEK FROM TODAY
            //================================
            else if (strDefaultRange == 'WTD') {
                if ($(strDRSelectorID).val() == "") {
                    $(strDRSelectorID).data('daterangepicker').setStartDate(moment().startOf('week'));
                    $(strDRSelectorID).data('daterangepicker').setEndDate(moment());

                    //set the hidden field values
                    $('#' + strStartDataFieldID).val(moment().startOf('week').format('MM/DD/YYYY'));
                    $('#' + strEndDataFieldID).val(moment().format('MM/DD/YYYY'));
                }

            }
            //================================
            //  THIS YEAR FROM TODAY
            //================================
            else if (strDefaultRange == 'YTD') {
                if ($(strDRSelectorID).val() == "") {
                    $(strDRSelectorID).data('daterangepicker').setStartDate(moment().startOf('year'));
                    $(strDRSelectorID).data('daterangepicker').setEndDate(moment());

                    //set the hidden field values
                    $('#' + strStartDataFieldID).val(moment().startOf('year').format('MM/DD/YYYY'));
                    $('#' + strEndDataFieldID).val(moment().format('MM/DD/YYYY'));
                }

            }
            //================
            //  TODAY
            //================
            else if (strDefaultRange == 'TTD') {
                if ($(strDRSelectorID).val() == "") {
                    $(strDRSelectorID).data('daterangepicker').setStartDate(moment());
                    $(strDRSelectorID).data('daterangepicker').setEndDate(moment());

                    //set the hidden field values
                    $('#' + strStartDataFieldID).val(moment().format('MM/DD/YYYY'));
                    $('#' + strEndDataFieldID).val(moment().format('MM/DD/YYYY'));
                }

            }

            //================
            //  YESTERDAY
            //================
            else if (strDefaultRange == 'TTD-1') {
                if ($(strDRSelectorID).val() == "") {
                    $(strDRSelectorID).data('daterangepicker').setStartDate(moment().add('days', -1));
                    $(strDRSelectorID).data('daterangepicker').setEndDate(moment().add('days', -1));

                    //set the hidden field values
                    $('#' + strStartDataFieldID).val(moment().format('MM/DD/YYYY'));
                    $('#' + strEndDataFieldID).val(moment().format('MM/DD/YYYY'));
                }

            }


            //================================
            //  THIS MONTH FROM YESTERDAY
            //================================
            else if (strDefaultRange == 'MTD-1') {
                if ($(strDRSelectorID).val() == "") {
                    $(strDRSelectorID).data('daterangepicker').setStartDate(moment().startOf('month'));
                    $(strDRSelectorID).data('daterangepicker').setEndDate(moment().add('days', -1));

                    //set the hidden field values
                    $('#' + strStartDataFieldID).val(moment().startOf('month').format('MM/DD/YYYY'));
                    $('#' + strEndDataFieldID).val(moment().add('days', -1).format('MM/DD/YYYY'));
                }
            }
            //================================
            //  THIS WEEK FROM YESTERDAY
            //================================
            else if (strDefaultRange == 'WTD-1') {
                if ($(strDRSelectorID).val() == "") {
                    $(strDRSelectorID).data('daterangepicker').setStartDate(moment().startOf('week'));
                    $(strDRSelectorID).data('daterangepicker').setEndDate(moment().add('days', -1));

                    //set the hidden field values
                    $('#' + strStartDataFieldID).val(moment().startOf('week').format('MM/DD/YYYY'));
                    $('#' + strEndDataFieldID).val(moment().add('days', -1).format('MM/DD/YYYY'));
                }

            }
            //================================
            //  THIS YEAR FROM YESTERDAY
            //================================
            else if (strDefaultRange == 'YTD-1') {
                if ($(strDRSelectorID).val() == "") {
                    $(strDRSelectorID).data('daterangepicker').setStartDate(moment().startOf('year'));
                    $(strDRSelectorID).data('daterangepicker').setEndDate(moment().add('days', -1));

                    //set the hidden field values
                    $('#' + strStartDataFieldID).val(moment().startOf('year').format('MM/DD/YYYY'));
                    $('#' + strEndDataFieldID).val(moment().add('days', -1).format('MM/DD/YYYY'));
                }

            }
            else {
                ///leave the range empty
            }
        }
        else {
            ///leave the range empty
            //Question: Should we set the default as month
        }

    }
    else {
        EUIShowInfoBar(strErrorMessage);
        $(strDRSelectorID).val('Error generating ui');
    }
}


//Bind on change event
$(document).on('hide.daterangepicker', '.EnetDateRangeSelectorBS3 ', function (e) {
    e.preventDefault();
    var strDRSelectorID = $(this).attr('id');
    if ((strDRSelectorID != undefined) && ($.trim(strDRSelectorID).length > 0)) {
        EUIDRChange(strDRSelectorID);
    }
    else {
        EUIShowInfoBar('Unable to setup Enet Date Range Selector because element ID is not defined or does not exist.');
    }
});

function EUIDRChange(inDRID) {
    var isErrorPresent = true;
    var strErrorMessage = '';

    var strDRID = '#' + inDRID;
    var strDRValue = $(strDRID).val();
    var strStartDataFieldID = $(strDRID).attr('data-EWF-ERS-startDataFieldID');
    var strEndDataFieldID = $(strDRID).attr('data-EWF-ERS-endDataFieldID');
    var strAutoIncrementEndDate = $(strDRID).attr('data-EWF-ERS-autoIncrementEndDate');

    var isEndDateAutoIncremented = false;

    var strStartDate = '';
    var strEndDate = '';

    var dtStartDate = null;
    var dtEndDate = null;

    //Set values for start date and end date variables
    if (jQuery.trim(strDRValue) != '') {
        var arrDates = strDRValue.split(' - ');
        if (arrDates.length == 0) {
            //If for some reason the array is empty, set start/end field to empty
            strStartDate = '';
            strEndDate = '';
        }
        else if (arrDates.length == 1) {
            //If for some the array has one element, set start field value and set end field to empty
            strStartDate = arrDates[0];
            strEndDate = arrDates[0];

        }
        else if (arrDates.length >= 2) {
            //If for some the array has >=2 element, set start field value to 1st element and end field value to 2nd element
            strStartDate = arrDates[0];
            strEndDate = arrDates[1];
        }
        strAutoIncrementEndDate = jQuery.trim(strAutoIncrementEndDate).toLowerCase();
        if (strAutoIncrementEndDate == 'true' || strAutoIncrementEndDate == '1' || strAutoIncrementEndDate == 'yes') {
            isEndDateAutoIncremented = true;
        }
        else {
            isEndDateAutoIncremented = false;
        }


        //Validate that the start dates and end dates are valid dates
        if (jQuery.trim(strStartDate) != '') {
            dtStartDate = Date.parse(strStartDate);
        }
        else {
            dtStartDate = null;
        }
        if (jQuery.trim(strEndDate) != '') {
            dtEndDate = Date.parse(strEndDate);
        }
        else {
            dtEndDate = null;
        }

        if (dtStartDate != null) {
            if (dtEndDate != null) {
                isErrorPresent = false;
                strErrorMessage = '';
            }
            else {
                isErrorPresent = true;
                strErrorMessage = 'Unable to set end date (' + inDRID + ')  because the specified value is not a valid date(' + strEndDate + ')';
            }
        }
        else {
            isErrorPresent = true;
            strErrorMessage = 'Unable to set start date (' + inDRID + ')  because the specified value is not a valid date(' + strStartDate + ')';
        }


        //Set start and end date field values
        if (!isErrorPresent) {
            if (isEndDateAutoIncremented) {
                dtEndDate.add({ days: 1 });
            }
            $('#' + strStartDataFieldID).val(dtStartDate.toString('M/d/yyyy'));
            $('#' + strEndDataFieldID).val(dtEndDate.toString('M/d/yyyy'));
        }
        else {
            EUIShowInfoBar(strErrorMessage);
            $(strDRID).val('Error building UI');
        }
    }
    else {//Empty out start field and end field
        strStartDate = '';
        strEndDate = '';
        $('#' + strStartDataFieldID).val(strStartDate);
        $('#' + strEndDataFieldID).val(strEndDate);
    }


}
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of Function to setup Enet DATE Range Selector UI----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of Function to setup Enet Multi Select UI----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/

function EUIDatePickerSetup(inDatePickerID) {
    var strDatePickerID = '#' + inDatePickerID;
    //$(strDatePickerID).datepicker();
    $(strDatePickerID).datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true
    })
    .off('focus')
    .click(function () {
        $(this).datepicker('show');
    });

    $(strDatePickerID).datepicker().on('changeDate', function (ev) {
        $(this).change();
        $(this).datepicker('hide');
        $(this).blur();

    });

}

/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of Function to setup Enet DATE Range Selector UI----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of Function to setup Enet Multi Select UI----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
function EUIMUSSetup(inMSID) {
    var strMSID = '#' + inMSID;
    $(strMSID).multiselect();
}
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of Function to setup Enet Multi Select UI----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* --  Start of Function to setup  popover----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
function EUIPopOverSetup() {
    if ($("div[rel=popover]").length != 0) {
        $("div[rel=popover]").popover();
    }
}
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of Function to setup popover----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of Function needed for ENET Menu Bar----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
function EUIMenuSetup(inMenuID) {
    inMenuID = $.trim(inMenuID);

    var strMenuID = '#' + inMenuID;
    var strQSMenuIDParamName = '';
    var strLIMenuIDPrefix = '';
    var strActiveClass = '';
    var strCookieName = '';


    strQSMenuIDParamName = $(strMenuID).attr('data-EWF-Menu-QSMenuIDParamName');
    strLIMenuIDPrefix = $(strMenuID).attr('data-EWF-Menu-LIMenuIDPrefix');
    strActiveClass = $(strMenuID).attr('data-EWF-Menu-ActiveClass');
    strCookieName = $(strMenuID).attr('data-EWF-Menu-CookieName');

    strQSMenuIDParamName = $.trim(strQSMenuIDParamName);
    strLIMenuIDPrefix = $.trim(strLIMenuIDPrefix);
    strActiveClass = $.trim(strActiveClass);
    strCookieName = $.trim(strCookieName);


    if (strQSMenuIDParamName.length == 0) { strQSMenuIDParamName = 'enetFoundationMenuID'; }
    if (strLIMenuIDPrefix.length == 0) { strLIMenuIDPrefix = 'EWF-Menu-LI-'; }
    if (strActiveClass.length == 0) { strActiveClass = 'current'; }
    if (strCookieName.length == 0) { strCookieName = 'EWF-Menu-LastClicked'; }
    var isErrorPresent = EWSMenuBarSetup(inMenuID, strQSMenuIDParamName, strLIMenuIDPrefix, strActiveClass, strCookieName)
    if (isErrorPresent == true) {
        EUIShowInfoBar('Unable to setup menu bar because of unexpected error in method ' + EWSMenuBarSetup);
    }
    else {
        //Note:  The menu binding to superfish class needs to be done after the DOM changes have been made.
        //          If we bind the menu first and then make the DOM changes, the changes do not go into effect.
        //        $(strMenuID).superfish({
        //            pathClass: strActiveClass
        //            , delay: 500                            // one second delay on mouseout 
        //        });
    }

}

//This function does 2 thing:
//  1.  It sets the local cookie for the last menu clicked.
//  2.  It highlights the appropriate tab so that the user can see the menu and submenu of the last clicked item.
function EWSMenuBarSetup(inMenuID, inQSMenuIDParamName, inLIMenuIDPrefix, inActiveClass, inCookieName) {
    var isErrorPresent = true;
    var strErrorMessage = 'Operation not performed';
    inMenuID = $.trim(inMenuID);
    inQSMenuIDParamName = $.trim(inQSMenuIDParamName);
    inLIMenuIDPrefix = $.trim(inLIMenuIDPrefix);
    inActiveClass = $.trim(inActiveClass);
    inCookieName = $.trim(inCookieName);
    $('.ENETUI-subMenuBar').show();

    var strLastClickedMenuID = '';
    strLastClickedMenuID = EWSMenuBarSetCookie(inMenuID, inQSMenuIDParamName, inCookieName);
    strLastClickedMenuID = $.trim(strLastClickedMenuID);
    var strBreadCrums = '';
    if (strLastClickedMenuID.length > 0) {
        strBreadCrums = EWSMenuBarSetMarkCurrent(inMenuID, inLIMenuIDPrefix, inActiveClass, strLastClickedMenuID);
    }
    else {
        //Even if strLastClickedMenuID is empty, call EWSMenuBarSetMarkCurrent so that it can set the 1st tab as active.
        strBreadCrums = EWSMenuBarSetMarkCurrent(inMenuID, inLIMenuIDPrefix, inActiveClass, strLastClickedMenuID);
    }

    //show the corresponding submenu
    //    var subMenuID = inMenuID.replace("Menu", "SubMenu");
    //    var subLIMenuIDPrefix = inLIMenuIDPrefix.replace("Menu", "SubMenu");
    //    strBreadCrums = EWSMenuBarSetMarkCurrent(subMenuID, subLIMenuIDPrefix, inActiveClass, strLastClickedMenuID);

    isErrorPresent = false;

    return isErrorPresent;

}
function EWSMenuBarSetCookie(inMenuID, inQSMenuIDParamName, inCookieName) {
    inMenuID = $.trim(inMenuID);
    inQSMenuIDParamName = $.trim(inQSMenuIDParamName);
    inCookieName = $.trim(inCookieName);
    inCookieName = inCookieName + inMenuID;

    var arrQS = window.location.href.split('?');
    var strQS = arrQS[arrQS.length - 1];

    strQS = strQS.substring(1, strQS.length);
    var strQSMenuIDFieldName = inQSMenuIDParamName; // '';
    var strMenuIDInURL = EJUParseQueryString(strQS, strQSMenuIDFieldName);
    var strCookieName = inCookieName;
    var strIDToMarkAsCurrent = '';
    
    //========================================================================
    //  Notes: Added by siva at 1/21/2015 on add and other hyperlinks
    // # gets added to the menu and that was causing and error
    // on setting of the current selected menu as the id had # appended to it
    //========================================================================
    strMenuIDInURL = strMenuIDInURL.replace("#", '');
    strMenuIDInURL = $.trim(strMenuIDInURL);


    if (strMenuIDInURL.length > 0) {
        //If the URL contains enetFoundationMenuID, Mark that ID as current
        strIDToMarkAsCurrent = strMenuIDInURL;
    }
    else {
        //If the URL does not contain enetFoundationMenuID, look for it in cookie, if found,
        //  that is the last menu bar that the user clicked on.  Mark that as active.
        var strCookieValue = '';
        strCookieValue = EWFCookiesGet(strCookieName);
        strCookieValue = $.trim(strCookieValue);
        if (strCookieValue.length > 0) {
            strIDToMarkAsCurrent = strCookieValue;
        }
        else {
            //Pass back Empty value.  The EWSMenuBarSetMarkCurrent sets 1st tab as active if this value is empty.
        }
    }
    //Set currently marked menu bar as active.          
    EWFCookiesSet(strCookieName, strIDToMarkAsCurrent, 180);
    return strIDToMarkAsCurrent;
}
function EWSMenuBarSetMarkCurrent(inMenuID, inLIMenuIDPrefix, inActiveClass, inLastClickedMenuID) {
    inMenuID = $.trim(inMenuID);
    inLIMenuIDPrefix = $.trim(inLIMenuIDPrefix);
    if (inMenuID != 'EWF-Menu-User')
    inActiveClass = 'sec-nav-color'; //$.trim(inActiveClass);
    inLastClickedMenuID = $.trim(inLastClickedMenuID);
    $('#' + inMenuID + ' li.active').removeClass(inActiveClass);
    if (inLastClickedMenuID.length == 0) {
        $('#' + inMenuID + ' li.dropdown').first().addClass(inActiveClass);
    }
    else {
        var strLIID = inLIMenuIDPrefix + inLastClickedMenuID;
        var jqeActiveLI = $('#' + inMenuID + ' li#' + strLIID);
        $(jqeActiveLI).addClass(inActiveClass);
        var jqeActiveLIParent = $(jqeActiveLI).parent();
        var strActiveLIParentID = $(jqeActiveLIParent).attr('id');
        if (strActiveLIParentID == undefined) { strActiveLIParentID = ''; }

        var intParentCount = 0;

        while (($(jqeActiveLIParent).length > 0) && (strActiveLIParentID != inMenuID)) {
            intParentCount++;
            //            if ($(jqeActiveLIParent).hasClass('dropdown') == true) {
            if ($(jqeActiveLIParent).hasClass(inActiveClass) == false) {
                $(jqeActiveLIParent).addClass(inActiveClass);

            }
            else {
                //DO nothing
            }
            jqeActiveLIParent = $(jqeActiveLIParent).parent();
            strActiveLIParentID = $(jqeActiveLIParent).attr('id');

            if (strActiveLIParentID == undefined) { strActiveLIParentID = ''; }
            if (intParentCount > 10) {//This line was added to avoid an infinate loop.  If the menu is more than 10 layers deep, break.
                break;
            }
        }


        if (intParentCount == 0) {
            //If this is the top level menu Item and its parent is UL with ID = MenuID
            //  the execution never went into the while loop above.
            //            if ($(jqeActiveLI).hasClass('dropdown')) {
            if ($(jqeActiveLI).hasClass(inActiveClass) == false) {
                $(jqeActiveLI).addClass(inActiveClass);

            }
            //            }
        }

        //show submenu
        if (inMenuID.indexOf("SubMenu") > 0) {
            $(jqeActiveLIParent).show();
        }
    }


}

/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of Function needed for ENET Menu Bar----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of Function needed for ENET Grid Bar----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
//====================
// Binding Functions
//====================
/* Enet Grid Operation Link Binding */

$(document).ready(function () {

    $(document).on("click", "a.ENETGridOpPage", function (e) {
        var strFrmID = EJUGetFormID(this);
        if (strFrmID != "") {
            var strPageNo = EJUParseQueryString($(this).attr('href'), 'EGOJumpToPage');
            var intPageNo = parseInt(strPageNo);

            if (!isNaN(intPageNo)) {
                EnetGridJumpToPage(intPageNo, strFrmID);
            }
            else {
                EUIShowInfoBar('Unable to jump to page because page ID is not specified or is not a valid integer. (' + strPageNo.toString() + ')');
            }
        }
        else {
            EUIShowInfoBar('Unable to jump to page because operations form is not found.');
        }
        e.preventDefault();
    });


    $(document).on("change", "select.ENETGridOpPage", function (e) {
        var strFrmID = EJUGetFormID(this);
        if (strFrmID != "") {
            var strPageNo = $(this).val();
            var intPageNo = parseInt(strPageNo);

            if (!isNaN(intPageNo)) {
                EnetGridJumpToPage(intPageNo, strFrmID);
            }
            else {
                EUIShowInfoBar('Unable to jump to page because page ID is not specified or is not a valid integer. (' + strPageNo.toString() + ')');
            }
        }
        else {
            EUIShowInfoBar('Unable to jump to page because operations form is not found.');
        }
        e.preventDefault();
    });


    $(document).on("click", "a.EJS-Grid-Refresh", function (e) {
        var strFrmID = EJUGetFormID(this);
        strFrmID = $.trim(strFrmID);
        if (strFrmID != "") {
            EnetGridRefresh(strFrmID);

        }
        else {
            EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
        }

        e.preventDefault();
    });

    function EnetGridRefresh(inOpFormName) {
        inOpFormName = $.trim(inOpFormName);
        var form = $('#' + inOpFormName);
        if (form.length > 0) {
            if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                form.find('#' + inOpFormName + '_enetGridOperation').val('Refresh');
                form.submit();

            }
            else {
                EUIShowInfoBar('Unable to Refresh the page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
            }

        }
        else {
            EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
        }
    }


    function EnetGridPrint(inOpFormName) {
        inOpFormName = $.trim(inOpFormName);
        var form = $('#' + inOpFormName);
        if (form.length > 0) {
            if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                form.find('#' + inOpFormName + '_enetGridOperation').val('Print');
                form.submit();

            }
            else {
                EUIShowInfoBar('Unable to print the page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
            }

        }
        else {
            EUIShowInfoBar('Unable to print to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
        }
    }

    $(document).on("click", "a.EJS-Grid-Print", function (e) {
        var strFrmID = EJUGetFormID(this);
        strFrmID = $.trim(strFrmID);
        if (strFrmID != "") {
            EnetGridPrint(strFrmID);

        }
        else {
            EUIShowInfoBar('Unable to print to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
        }

        e.preventDefault();
    });


    $("a.EJS-Grid-Sort").click(function (e) {
        var strFrmID = EJUGetFormID(this);


        //IMPORTANT!  Line below is needed because Sort GridOp is acautally in the 
        //              data table section of the Grid and the data table section
        //              is enclosed by the GridForm.  Thus the fuction call above
        //              will return the ID of the GridForm.
        //strFrmID = strFrmID.replace('GridForm', 'OpForm');

        if (strFrmID != "") {
            var strCol = EJUParseQueryString($(this).attr('href'), 'EGOSortCol');
            var intCol = parseInt(strCol);
            if (!isNaN(intCol)) {
                var strDirection = EJUParseQueryString($(this).attr('href'), 'sortDir');
                var intDirection = parseInt(strDirection);
                if (!isNaN(intDirection)) {
                    if ((intDirection == 1) || (intDirection == 0)) {
                        EnetGridSingleColumnSort(strFrmID, intCol, intDirection);
                    }
                    else {
                        EUIShowInfoBar('Unable to sort because sort direction is not valid. (' + intDirection.toString() + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to sort because sort direction is not specified or is not a valid integer. (' + strDirection.toString() + ')');
                }

            }
            else {
                EUIShowInfoBar('Unable to sort because column ID is not specified or is not a valid integer. (' + strDirection.toString() + ')');
            }
        }
        else {
            EUIShowInfoBar('Unable to sort page because operations form is not found.');
        }
        e.preventDefault();
    });

    $(document).on("click", "a.EJS-Grid2-Sort", function (e) {
        var strFrmID = EJUGetFormID(this);
        //IMPORTANT!  Line below is needed because Sort GridOp is acautally in the 
        //              data table section of the Grid and the data table section
        //              is enclosed by the GridForm.  Thus the fuction call above
        //              will return the ID of the GridForm.
        //strFrmID = strFrmID.replace('GridForm', 'OpForm');

        if (strFrmID != "") {
            var strCol = EJUParseQueryString($(this).attr('href'), 'EGOSortCol');
            strCol = $.trim(strCol);
            if (strCol.length > 0) {
                var strDirection = EJUParseQueryString($(this).attr('href'), 'sortDir');
                var intDirection = parseInt(strDirection);
                if (!isNaN(intDirection)) {
                    if ((intDirection == 1) || (intDirection == 0)) {
                        EnetGridSingleColumnSort(strFrmID, strCol, intDirection);
                    }
                    else {
                        EUIShowInfoBar('Unable to sort because sort direction is not valid. (' + intDirection.toString() + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to sort because sort direction is not specified or is not a valid integer. (' + strDirection.toString() + ')');
                }

            }
            else {
                EUIShowInfoBar('Unable to sort because column name is not specified . (' + strCol.toString() + ')');
            }
        }
        else {
            EUIShowInfoBar('Unable to sort page because operations form is not found.');
        }
        e.preventDefault();
    });

    /* Quick Search by any field*/
    $(document).on("click", "a.ENETGridOpFindByField", function (e) {
        var strFrmID = EJUGetParentFormID(this);
        if (strFrmID != "") {
            EnetGridQuickSearch('QuickSearch', '', strFrmID);
        }
        else {
        }
    });


    /* Quick Search by specific field*/
    $(document).on("click", "a.ENETGridOpFindByField", function (e) {
        var strFrmID = EJUGetParentFormID(this);
        if (strFrmID != "") {
            var strFieldNo = EJUParseQueryString($(this).attr('href'), 'EGOFindByField');
            var intFieldNo = parseInt(strFieldNo);
            var strSearchMode = '';
            if (!isNaN(intFieldNo)) {
                if (intFieldNo > 0) {
                    strSearchMode = 'Find';
                }
                else {
                    strSearchMode = 'QuickSearch';
                }
                EnetGridQuickSearch(strSearchMode, intFieldNo, strFrmID);
            }
            else {
                EUIShowInfoBar('Unable to find records because field ID is not specified or is not a valid integer. (' + strFieldNo.toString() + ')');
            }
        }
        else {
            EUIShowInfoBar('Unable to find records because operations form is not found.');
        }
        e.preventDefault();
    });


    $(document).on("click", "a.ENETOpFilter", function (e) {
        var strFrmID = EJUGetFormID(this);
        strFrmID = $.trim(strFrmID);
        if (strFrmID != "") {
            var isErrorPresent = EJUIsErrorInForm(strFrmID, true);
            if (isErrorPresent) {
                e.preventDefault();
            }
            else {
                var strFormValues = $("#" + strFrmID).serializeObjectForJSON();
                if (strFormValues != "") {
                    strFormValues = JSON.stringify(strFormValues);

                    if (strFormValues != "") {
                        EnetGridFilter(strFormValues, strFrmID);
                    }
                    else {
                        EUIShowInfoBar('Unable to filter records because form data cannot be converted to JSON.');
                    }

                }
                else {
                    EUIShowInfoBar('Unable to filter records because form data cannot be serialized.');
                }
            }
        }
        else {
            EUIShowInfoBar('Unable to filter records because operations form is not found.');
        }
        //e.preventDefault();
    });

    $(document).on("click", "a.EJS-Grid-FilterClose", function (e) {
        $(this).parents('.EJS-Grid').find('.EJS-Grid-FilterForm').slideUp('slow');
        $(this).parents('.EJS-Grid').find('.EJS-Grid-FilterIcon').addClass('enetFilterIconDiv-Active');
    });

    $(document).on("click", "a.EJS-Grid-FilterIcon", function (e) {
        $(this).parents('.EJS-Grid').find('.EJS-Grid-FilterForm').slideDown('slow');
        $(this).addClass('disabled');
    });

    $('a.enetFilterIcon').click(function (e) {
        var strFrmID = EJUGetParentFormID(this);
        var strFilterID = strFrmID.replace('frm', '').replace('OpForm', '');
        if (strFrmID != "") {
            var form = $('#' + strFrmID);
            var strFilterDiv = strFilterID + '_EnetFilterOperation';
            if (EJUISIDUnique('#' + strFilterDiv, form)) {
                //NOTE:  isFilterVisibleBeforeToggle is set before toggle because visible value is unpredectable to read for some time after issuing toggle command
                var isFilterVisibleBeforeToggle = $('#' + strFilterDiv).is(':visible');
                $('#' + strFilterDiv).slideToggle(500);
                var strFilterIconDivID = strFrmID + '_enetFilterIconDiv';
                if (EJUISIDUnique('#' + strFilterIconDivID, form)) {
                    if (!isFilterVisibleBeforeToggle) {
                        $('#' + strFilterIconDivID).addClass('enetFilterIconDiv-Active');
                    }
                    else {
                        $('#' + strFilterIconDivID).removeClass('enetFilterIconDiv-Active');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to apply style to icon div because a div with this ID was not found. (' + strFilterID + '_enetFilterIconDiv' + ')');
                }
            }
            else {
                EUIShowInfoBar('Unable to hide/show filter options because _EnetFilterOperation div is not found for this grid. (' + strFilterID + '_EnetFilterOperation' + ')');
            }
        }
        else {
            EUIShowInfoBar('Unable to hide/show filter because operations form is not found.');
        }

        e.preventDefault();
    });



});
//============================
// Grid Operation QuickSearch
//============================
function EnetGridQuickSearch(inMode, inFieldName, inOpFormName) {
    if (EJUISIDUnique('#' + inOpFormName, null)) {
        var form = $('#' + inOpFormName);
        var doSubmit = false;
        if (EJUISIDUnique('#' + inOpFormName + '_txtSearch', form)) {
            if (form.find('#' + inOpFormName + '_txtSearch').val() != "") {

                //SET Search Value
                if (EJUISIDUnique('#' + inOpFormName + '_enetGridSearchValue', form)) {
                    form.find('#' + inOpFormName + '_enetGridSearchValue').val(form.find('#' + inOpFormName + '_txtSearch').val());
                }
                else {
                    EUIShowInfoBar('Unable to search because EnetGridSearchValue field is not found for this grid. (' + inOpFormName + '_enetGridSearchValue' + ')');
                }


                //SET Grid Operation
                if (EJUISIDUnique('#' + inOpFormName + '_enetGridOperation', form)) {
                    form.find('#' + inOpFormName + '_enetGridOperation').val(inMode);
                }
                else {
                    EUIShowInfoBar('Unable to search because EnetGridSearchValue field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
                }

                //SET Grid Search Field (depending on mode)
                if (inMode == 'Find') {
                    if (EJUISIDUnique('#' + inOpFormName + '_enetGridSearchField', form)) {
                        form.find('#' + inOpFormName + '_enetGridSearchField').val(inFieldName);
                        doSubmit = true;
                    }
                    else {//If
                        doSubmit = false;
                        EUIShowInfoBar('Unable to search because EnetGridSearchField field is not found for this grid. (' + inOpFormName + '_enetGridSearchField' + ')');
                    }
                }
                else {
                    form.find('#' + inOpFormName + '_enetGridSearchField').val("");
                    doSubmit = true;
                }
                if (doSubmit) {
                    form.submit();
                }
                else {
                    //DO nothing.  Message was shown above.
                }
            }
            else {
                EUIShowInfoBar('Please enter the search value');
                form.find('#' + inOpFormName + '_txtSearch').focus();
            }
        }
        else {
            EUIShowInfoBar('Unable to search because txtSearch field is not found for this grid. (' + inOpFormName + '_txtSearch' + ')');
        }
    }
    else {
        EUIShowInfoBar('Unable to search because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
    }
}
//====================
// Filter
//====================
/*Filter Function Handler */
function EnetGridFilter(inFilterCriteria, inOpFormName) {
    var form = $('#' + inOpFormName);

    if (form.length > 0) {
        if (form.find('#' + inOpFormName + '_enetGridFilter').length > 0) {
            form.find('#' + inOpFormName + '_enetGridFilter').val(inFilterCriteria)
            if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                form.find('#' + inOpFormName + '_enetGridOperation').val('Filter');
                form.submit();
            }
            else {
                EUIShowInfoBar('Unable to jump to page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
            }
        }
        else {
            EUIShowInfoBar('Unable to jump to page because EnetGridFilter field is not found for this grid. (' + inOpFormName + '_enetGridPage' + ')');
        }
    }
    else {
        EUIShowInfoBar('Unable to filter because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
    }
}



//====================
// Sorting
//====================
/* Sorting Link Binding */

function EnetGridSingleColumnSort(inOpFormName, inFieldName, inDirection) {
    inOpFormName = $.trim(inOpFormName);
    var form = $('#' + inOpFormName);
    if (form.length > 0) {
        if (form.find('#' + inOpFormName + '_enetGridSort').length > 0) {
            form.find('#' + inOpFormName + '_enetGridSort').val(inFieldName + ',' + inDirection);
            if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                form.find('#' + inOpFormName + '_enetGridOperation').val('Sort');
                form.submit();
            }
            else {
                EUIShowInfoBar('Unable to jump to page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
            }
        }
        else {
            EUIShowInfoBar('Unable to sort because EnetGridSort field is not found for this grid. (' + inOpFormName + '_enetGridSort' + ')');
        }
    }
    else {
        EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
    }

}

//====================
// Paging
//====================
/*Paging Function Handler */
function EnetGridJumpToPage(inPageToJumpTo, inOpFormName) {
    inOpFormName = $.trim(inOpFormName);
    var form = $('#' + inOpFormName);
    if (form.length > 0) {
        if (form.find('#' + inOpFormName + '_enetGridPage').length > 0) {
            form.find('#' + inOpFormName + '_enetGridPage').val(inPageToJumpTo)
            if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                form.find('#' + inOpFormName + '_enetGridOperation').val('Page');
                form.submit();

            }
            else {
                EUIShowInfoBar('Unable to jump to page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
            }
        }
        else {
            EUIShowInfoBar('Unable to jump to page because EnetGridPage field is not found for this grid. (' + inOpFormName + '_enetGridPage' + ')');
        }
    }
    else {
        EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
    }
}







//====================
// CheckBox
//====================
$("input[type='checkbox']").click(function () {
    //TODO:  Make sure this function is multi-grid enabled (ie. add '#' + inOpFormName + '_ before field names)
    //TODO:  we need to catch the click even on a specific class, currently, any check box click fires this event
    //          and that causes a problem because not every form will have elements with ID ofenetGridMSNew 
    //          and enetGridMSRemoved elements.
    //          To bypass thi bug, we added if (EJUISIDUnique('#enetGridMSNew', null) == true) { clause for 
    //          enetGridMSNew and enetGridMSRemoved.  
    //          Once the selector is rewritten for a specific class, we can remove the if conditions.
    if ($(this).parents('form:first').attr('id') != undefined) {
        var currFormName = $(this).parents('form:first').attr('id').replace('GridForm', 'OpForm');
        if (currFormName != null && currFormName != "") {
            if ($(this).attr('checked')) {
                if (EJUISIDUnique('#enetGridMSNew', null) == true) {
                    var currChkValue = "";
                    currChkValue = $(this).val();
                    addRemoveSelected(currFormName, '#enetGridMSNew', currChkValue, "Added");
                }
            }
            else {
                if (EJUISIDUnique('#enetGridMSNew', null) == true) {
                    var currChkValue = "";
                    currChkValue = $(this).val();
                    addRemoveSelected(currFormName, '#enetGridMSRemoved', currChkValue, "Removed");
                }
            }

        }
        else {
            //TODO: Have to decide do we uncheck the checkbox or what do we do
        }
    }
});

//==============================
// Add or Remove Checked Items
//==============================
function addRemoveSelected(inFormName, inFieldName, inCurrValue, inMode) {
    //TODO:  Make sure this function is multi-grid enabled (ie. add '#' + inOpFormName + '_ before field names)
    var currHdnValue = "";
    var newhdnValue = "";
    currHdnValue = $('#' + inFormName).find(inFieldName).val();
    if (currHdnValue.indexOf("," + inCurrValue + ",") < 0) {//Check if that value  was already added to it
        if (currHdnValue != "") {
            newhdnValue = currHdnValue + inCurrValue + ",";
        }
        else {
            newhdnValue = "," + inCurrValue + ",";
        }
        $('#' + inFormName).find(inFieldName).val(newhdnValue);

    }
}



//====================
// Printing
//====================
//function print(inOpFormName) {
//    var form = $('#' + inOpFormName);
//    form.find('#' + inOpFormName + '_enetGridOperation').val('Print')
//    form.attr("target", "_blank");
//    form.attr("action", document.location + "?PrintMode=1");
//    form.submit();
//}

//============================
// Reset Print
//============================
function resetPrint(inOpFormName) {
    var form = $('#' + inOpFormName);
    form.attr("target", "_self");
    document.location = document.location.replace("?PrintMode=1", "");
    form.attr("action", document.location);
}



/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of Function needed for ENET Grid Bar----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of Function needed for EWF Client Side Form Validation----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
function EJUFormElementError(inFieldName, inIsErrorPresent, inErrorMessage) {
    this.FieldName = inFieldName;
    this.IsErrorPresent = inIsErrorPresent;
    this.ErrorMessage = inErrorMessage
    return this;
}
function EJUIsErrorInForm(inFormID, inDisplayError) {
    var strFormID = '#' + inFormID;
    $(strFormID + ' .EnetFormError').remove();
    var strClientValidationDisabled = $(strFormID).attr('data-EWF-noClientSubmitValidation');
    if (strClientValidationDisabled == undefined) { strClientValidationDisabled = ''; }
    strClientValidationDisabled = strClientValidationDisabled.toLowerCase();
    var isClientValidationDisabled = false;

    if ((strClientValidationDisabled == 'true') || (strClientValidationDisabled == 'yes') || (strClientValidationDisabled == '1')) {
        isClientValidationDisabled = true;
    }
    else {
        //for any other value, including '', assume the developer wants to do client side validaiton on submit
        isClientValidationDisabled = false;
    }
    var isErrorPresent = false;
    var isTempErrorPresent = false;
    if (isClientValidationDisabled == false) {
        // $.each($(strFormID).serializeArray(), function (i, field) {
        // $.each($(strFormID + ' :input:not(".EWF-UI-ChildForm")').serializeArray(), function (i, field) {
        $.each($(strFormID + ' :input:not(".childformfields :input")').serializeArray(), function (i, field) {
            var strFieldName = field.name;
            var strElementID = $('#' + inFormID + ' [name="' + strFieldName + '"]').attr("id");
            var feeCurrent = EJUValidateFormElementByName(strElementID, inFormID, inDisplayError);
            isErrorPresent = isErrorPresent || feeCurrent.IsErrorPresent;
        });
    }
    else {
        //Skip Client Validation
        isErrorPresent = false;
    }
    return isErrorPresent;
}
function EJUValidateFormElementByName(inElementID, inFormID, inDisplayError) {

    var feeCurrent = EJUFormElementError(inElementID, true, 'No validation performed');
    var strCustomClientValidationDisabled = '';
    var isClientValidationDisabled = false;
    //note if data-ewf-noclientlivevalidation is set to true it will skip validation...its named badly so tat its compatible with previous iteration 
    if ($('#' + inFormID + ' #' + inElementID).attr('data-ewf-noclientlivevalidation') != null && $('#' + inFormID + ' #' + inElementID).attr('data-ewf-noclientlivevalidation') != undefined) {
        strCustomClientValidationDisabled = $('#' + inFormID + ' #' + inElementID).attr('data-ewf-noclientlivevalidation');
    }
    else {

        strCustomClientValidationDisabled = $('#' + inFormID + ' #' + inElementID).attr('data-EWF-noCustomClientValidation');
    }

    if (strCustomClientValidationDisabled == undefined) { strCustomClientValidationDisabled = ''; }
    strCustomClientValidationDisabled = strCustomClientValidationDisabled.toLowerCase();
    if ((strCustomClientValidationDisabled == 'true') || (strCustomClientValidationDisabled == 'yes') || (strCustomClientValidationDisabled == '1')) {
        isClientValidationDisabled = true;
    }
    else {
        //for any other value, including '', assume the developer wants to do client side validaiton on submit
        isClientValidationDisabled = false;
    }

    if (isClientValidationDisabled == false) {
        var isRequired = false;
        var strFormID = '#' + inFormID;
        var strType = '';
        var strMin = '';
        var strMax = '';
        var strMinLength = '';
        var strMaxLength = ''
        var strLessThanEqualField = '';
        var strGreaterThanEqualField = '';
        var strLessThanField = '';
        var strGreaterThanField = '';
        var strEqualToField = '';
        var strFieldValue = '';
        var strHTMLFieldType = '';
        var strErrorDisplayID = '';
        //(strFormID + ' #' + 
        isRequired = $('#' + inElementID).attr('required'); //Note:  Required attribute lookup either returns 'required' or 'undefined' and never the value of the attribute
        strHTMLFieldType = $('#' + inElementID).attr('type');
        strType = $('#' + inElementID).attr('data-ewf-datatype');
        strMin = $('#' + inElementID).attr('min');
        strMax = $('#' + inElementID).attr('max');
        strMinLength = $('#' + inElementID).attr('data-ewf-minlength');
        strMaxLength = $('#' + inElementID).attr('maxlength');
        strLessThanField = $('#' + inElementID).attr('data-ewf-lessthanfield');
        strLessThanEqualField = $('#' + inElementID).attr('data-ewf-lessthanequalfield');
        strGreaterThanField = $('#' + inElementID).attr('data-ewf-greaterthanfield');
        strGreaterThanEqualField = $('#' + inElementID).attr('data-ewf-greaterthanequalfield');
        strEqualToField = $('#' + inElementID).attr('data-ewf-equaltofield');
        strErrorDisplayID = $('#' + inElementID).attr('data-ewf-errordisplayid');
        strFieldValue = $('#' + inElementID).val();

        strFieldValue = $.trim(strFieldValue);
        if (strErrorDisplayID == undefined) { strErrorDisplayID = ''; }
        //  if (strClientValidationDisabled == undefined) { strClientValidationDisabled = ''; }

        feeCurrent = EJUValidateFormElement(inElementID, inFormID, strFieldValue, strType, isRequired, strMin, strMax, strMinLength, strMaxLength, strLessThanField, strLessThanEqualField, strGreaterThanField, strGreaterThanEqualField, strEqualToField);
        EJUFormElementErrorClear(inElementID, inFormID, strErrorDisplayID);
        if (feeCurrent.IsErrorPresent) {
            if (inDisplayError) {
                EJUFormElementErrorShow(inElementID, inFormID, strErrorDisplayID, feeCurrent.ErrorMessage);
            }
            else {
                //Do nothing.
            }
        }
        else {
            //Do nothing.
        }
    }
    else {
        //If LiveValidation is disabled for this element
        feeCurrent = EJUFormElementError(inElementID, false, '');
    }
    return feeCurrent;
}
function EJUFormElementErrorClear(inElementID, inFormID, inErrorDisplayID) {
    EJUFormElementErrorShow(inElementID, inFormID, inErrorDisplayID, '');
}

function EJUFormElementErrorShow(inElementID, inFormID, inErrorElementID, inErrorMessage) {
    inElementID = $.trim(inElementID);
    inFormID = $.trim(inFormID);
    inErrorElementID = $.trim(inErrorElementID);
    inErrorMessage = $.trim(inErrorMessage);
    var isSuccesful = false;
    var strErrorDivID, strDisplayFormID;
    //    if (inFormID.indexOf("frm") == 0) {
    //        strDisplayFormID = inFormID.substring(3);
    //        strDisplayFormID = strDisplayFormID.replace("OpForm", "");
    //    }
    if ((inElementID.length > 0) && (inElementID != '_GLOBAL')) {
        strErrorDivID = 'err' + inFormID + '_' + inElementID;
    }
    else {
        strErrorDivID = 'err' + inFormID;
    }

    if (inErrorMessage.length != 0) {
        var frmCurrent = null;
        var isTempErrorPresent = false;

        //If formID was specified, verify it form exists in DOM.  If no formID was specified, continue with frmCurrent as null.
        if (inFormID.length > 0) {
            if (EJUISIDUnique('#' + inFormID, null) == true) {
                frmCurrent = $('#' + inFormID);
                isTempErrorPresent = false;
            }
            else {//##Note-1:  If the form of the error does not exists (as in Daily Reports), we want to skip this else condition
                if ($('#DailyReport_ID').length > 0) {
                    isTempErrorPresent = false;
                }
                else {
                    EUIShowInfoBar('Unable to display error because #' + inFormID + ' is not found in this document');
                    isTempErrorPresent = false;
                }
            }
        }
        else {//No formID was specified 
            //DO nothing
            isTempErrorPresent = false;
        }


        if (!isTempErrorPresent) {
            var strMessagePlacement = 'default';
            var strErrorDisplayELementID = '';
            //Verify that elementID exists in the specified form.
            if ((inElementID.length > 0) && (inElementID != $.trim("_GLOBAL"))) {
                if (EJUISIDUnique('#' + inElementID, frmCurrent) == true) {
                    var strTempErrorDisplayElementID = inErrorElementID;
                    if (strTempErrorDisplayElementID.length > 0) {
                        //If the user has defined a custom ID @ which they want the error message displayed, make sure it exists and output error there.
                        if (EJUISIDUnique('#' + inErrorElementID, frmCurrent) == true) {
                            // Call Display Error Message Below
                            isTempErrorPresent = false;
                            strMessagePlacement = 'default';
                            strErrorDisplayELementID = inErrorElementID;
                        }
                        else {
                            //Append Form Name to the element ID and look for that element ID
                            //(strFormID + ' #' + POSItemAddNew_Form_txtPOSCodeModifier
                            if (EJUISIDUnique('#' + inFormID + '_Form_' + inErrorElementID, null) == true) {
                                // Call Display Error Message Below
                                isTempErrorPresent = false;
                                strMessagePlacement = 'default';
                                strErrorDisplayELementID = inFormID + '_Form_' + inErrorElementID;
                            }
                            else {
                                //If inErrorElementID does not exists in the form, check in the document.
                                if (EJUISIDUnique('#' + inErrorElementID, null) == true) {
                                    // Call Display Error Message Below
                                    isTempErrorPresent = false;
                                    strMessagePlacement = 'default';
                                    strErrorDisplayELementID = inErrorElementID;
                                }
                                else {
                                    EUIShowInfoBar('Unable to display error because #' + strTempErrorDisplayElementID + ' is not found in this document');
                                    isTempErrorPresent = false;
                                }
                            }

                        }
                    }
                    else {
                        //If the user has not defined a custom ID @ which they want the error message displayed
                        // display error according to field type
                        var strElementType = $('#' + inElementID, frmCurrent).attr('type');
                        if (strElementType == undefined) { strElementType = ''; }
                        strElementType = strElementType.toLowerCase();
                        if ((strElementType == '') || (strElementType == 'hidden')) {
                            //If strElementType is empty or hidden, show error before the 1st element in the Element ID's form.
                            if (inFormID.length > 0) {
                                //If a formID was specified, we already validated above that it exists.  Thus, we can move show error inside the form ID.
                                // Call Display Error Message Below
                                strMessagePlacement = 'prepend';
                                isTempErrorPresent = false;
                                strErrorDisplayELementID = inFormID;
                            }
                            else {
                                //If a form ID was not specified, get the current element's form ID and display it there.  If the formID is empty, show error.
                                var currentElement = $('#' + inElementID);
                                var strCEFrmID = EJUGetParentFormID(currentElement);
                                if (strCEFrmID == undefined) { strCEFrmID = ''; }
                                strCEFrmID = $.trim(strCEFrmID);
                                if (strCEFrmID.length > 0) {
                                    // Call Display Error Message Below
                                    strMessagePlacement = 'prepend';
                                    isTempErrorPresent = false;
                                    strErrorDisplayELementID = strCEFrmID;
                                }
                                else {
                                    EUIShowInfoBar('Unable to display error because #' + inElementID + ' is hidden or non-form element and its parent form ID is empty');
                                    isTempErrorPresent = true;
                                }
                            }
                        }
                        else {
                            //For all other element types, display error underneat the element type
                            // Call Display Error Message Below
                            strMessagePlacement = 'default';
                            isTempErrorPresent = false;
                            strErrorDisplayELementID = inElementID;
                        }
                    }
                }
                else {
                    isTempErrorPresent = true;
                    EUIShowInfoBar('Unable to display error because #' + inElementID + ' is not found in this document');

                }
            }
            else {
                strMessagePlacement = '2';
                isTempErrorPresent = false;
                if (inFormID.length > 0) {
                    strErrorDisplayELementID = inFormID;
                }
                else {
                    isTempErrorPresent = true;
                    EUIShowInfoBar('Unable to display error because #' + inElementID + ' s parent formID is Empty');
                }

            }

            if (!isTempErrorPresent) {
                EJUShowErrorMessagesOnForm(strErrorDisplayELementID, strErrorDivID, strMessagePlacement, inErrorMessage)
            }
        }
        else {
            //Do Nothing, error was displayed above
            isSuccesful = false;
        }
    }
    else {
        $('#' + strErrorDivID).html(''); //If this field has an error visible, hide it.
        $('#' + strErrorDivID).removeAttr('style');
        $('#' + strErrorDivID).addClass("EnetUI-Hidden");

        isSuccesful = true;
    }

}

function EJUShowErrorMessagesOnForm(inErrorDisplayELementID, inErrorDivID, inMessagePlacement, inErrorMessage) {
    var isSuccesful = false;
    if ($('#' + inErrorDivID).hasClass("EnetUI-Hidden")) {
        $('#' + inErrorDivID).removeClass("EnetUI-Hidden")
    }
    //  alert("inErrorDisplayELementID:" + inErrorDisplayELementID + "inErrorDivID:" + inErrorDivID + "inMessagePlacement:" + inMessagePlacement + "inErrorMessage:" + inErrorMessage);
    if ((inMessagePlacement == '') || (inMessagePlacement == '1') || (inMessagePlacement == 'default')) {
        if (EJUISIDUnique('#' + inErrorDivID, null) != true) {
            var parentID = $("#" + inErrorDisplayELementID).parent().attr("id");
            if ($("#" + parentID + " .ErrorContainer").length > 0) {
                $('#' + parentID + " .ErrorContainer").replaceWith('<div id="' + inErrorDivID + '" class="EWF-UI-FormError ">' + inErrorMessage + '</div>');
                $('#' + parentID + " .ErrorContainer").show('slow');
            }
            else {
                //ui-rangepicker-input
                if ($('#' + inErrorDisplayELementID).hasClass('ui-rangepicker-input')) {
                    $('#' + inErrorDisplayELementID).parent().after('<div id="' + inErrorDivID + '" class="EWF-UI-FormError ">' + inErrorMessage + '</div>');

                }
                else {
                    $('#' + inErrorDisplayELementID).after('<div id="' + inErrorDivID + '" class="EWF-UI-FormError ">' + inErrorMessage + '</div>');
                }
                $('#' + inErrorDisplayELementID).show('slow');
                $('#' + inErrorDivID).show('slow');
            }
        }
        else {
            $("#" + inErrorDivID).append(inErrorMessage);
            $("#" + inErrorDivID).show('slow');
        }
        isSuccesful = true;

    }
    else if ((inMessagePlacement == '2') || (inMessagePlacement == 'prepend')) {
        if (EJUISIDUnique('#' + inErrorDivID, null) != true) {
            $('#' + inErrorDisplayELementID).prepend('<div id="' + inErrorDivID + '" class="alert alert-error alert-danger">' + inErrorMessage + '</div>');
            $('#' + inErrorDisplayELementID).show('slow');
        }
        else {
            $("#" + inErrorDivID).append(inErrorMessage);
            $("#" + inErrorDivID).show('slow');
        }
        isSuccesful = true;
    }
    else if ((inMessagePlacement == '3') || (inMessagePlacement == 'append')) {
        if (EJUISIDUnique('#' + inErrorDivID, null) != true) {
            $('#' + inErrorDisplayELementID).append('<div id="' + inErrorDivID + '" class="EWF-UI-FormError ">' + inErrorMessage + '</div>');
            $('#' + inErrorDisplayELementID).show('slow');
        }
        else {
            $("#" + inErrorDivID).append(inErrorMessage);
            $("#" + inErrorDivID).show('slow');
        }

        isSuccesful = true;

    }
    else {
        EUIShowInfoBar('Unable to display error because placement type is invalid (' + inMessagePlacement + ')');
        isSuccesful = false;
    }

}
function EJUValidateFormElement(inElementID, inFormID, inValue, inType, inIsRequired, inMin, inMax, inMinLength, inMaxLength, inLessThanField, inLessThanEqualField, inGreaterThanField, inGreaterThanEqualField, inEqualToField) {
    //Declare internal variables and pre-process input
    var isRequired = true;
    var isErrorPresent = false;
    var strErrorMessage = '';
    var feeCurrent = EJUFormElementError(inElementID, true, 'No validation performed');

    if (inType != undefined) {
        inType = $.trim(inType.toLowerCase());
    }

    if (inIsRequired != undefined) {
        isRequired = $.trim(inIsRequired.toLowerCase());
    }
    else {
        isRequired = false;
    }

    if (inMin != undefined) {
        inMin = $.trim(inMin.toLowerCase());
    }

    if (inMax != undefined) {
        inMax = $.trim(inMax.toLowerCase());
    }
    if (inMinLength != undefined) {
        inMinLength = $.trim(inMinLength.toLowerCase());
    }

    if (inMaxLength != undefined) {
        inMaxLength = $.trim(inMaxLength.toLowerCase());
    }

    if (isRequired.toString().toLowerCase() == 'true'
                || isRequired.toString().toLowerCase() == 'yes'
                || isRequired.toString().toLowerCase() == 'required'
                || isRequired.toString().toLowerCase() == '1') {
        isRequired = true;
    }
    else {
        isRequired = false;
    }
    //TODO:  Validation isRequired cannot be undefined, inElementID and strFieldValue cannot be undefined
    if (inElementID != undefined || inValue != undefined) {
        //Step 1:  Is required checking
        if (inValue.length > 0) {//If the user entered in something, it has to be valid

            //Validate Type
            feeCurrent = EJUValidateElementType(inElementID, inValue, inType);

            if (!feeCurrent.IsErrorPresent) {
                var isTestPassed = false;
                isTestPassed = EJUValidateElementMinLength(inValue, inMinLength);
                if (isTestPassed) {
                    isTestPassed = EJUValidateElementMaxLength(inValue, inMaxLength);
                    if (isTestPassed) {
                        isTestPassed = EJUValidateElementMinValue(inValue, inMin, inType, true);
                        if (isTestPassed) {
                            isTestPassed = EJUValidateElementMaxValue(inValue, inMax, inType, true);
                            if (isTestPassed) {
                                isTestPassed = EJUValidateElementLessThanField(inValue, inLessThanField, false, inElementID, inFormID, inType)
                                if (isTestPassed) {
                                    isTestPassed = EJUValidateElementGreaterThanField(inValue, inGreaterThanField, false, inElementID, inFormID, inType)
                                    if (isTestPassed) {
                                        isTestPassed = EJUValidateElementLessThanField(inValue, inLessThanEqualField, true, inElementID, inFormID, inType)
                                        if (isTestPassed) {
                                            isTestPassed = EJUValidateElementGreaterThanField(inValue, inGreaterThanEqualField, true, inElementID, inFormID, inType)
                                            if (isTestPassed) {
                                                isTestPassed = EJUValidateElementEqualToField(inValue, inEqualToField, inElementID, inFormID, inType)
                                                if (isTestPassed) {

                                                    feeCurrent = EJUFormElementError(inElementID, false, '');
                                                }
                                                else {
                                                    feeCurrent = EJUFormElementError(inElementID, true, 'Value has to be equal to the value in ' + inEqualToField);
                                                }
                                            }
                                            else {
                                                feeCurrent = EJUFormElementError(inElementID, true, 'Value has to be greater than or equal to the value in ' + inGreaterThanEqualField);
                                            }
                                        }
                                        else {
                                            feeCurrent = EJUFormElementError(inElementID, true, 'Value has to be less than or equal to the value in ' + inLessThanEqualField);
                                        }
                                    }
                                    else {
                                        feeCurrent = EJUFormElementError(inElementID, true, 'Value has to be greater than the value in ' + inGreaterThanField);
                                    }
                                }
                                else {
                                    feeCurrent = EJUFormElementError(inElementID, true, 'Value has to be less than the value in ' + inLessThanField);
                                }
                            }
                            else {
                                feeCurrent = EJUFormElementError(inElementID, true, 'Value has to be less than or equal to' + inMax);
                            }
                        }
                        else {
                            feeCurrent = EJUFormElementError(inElementID, true, 'Value has to be greater than or equal to ' + inMin);
                        }

                    }
                    else {
                        feeCurrent = EJUFormElementError(inElementID, true, 'Value has to be less than ' + inMaxLength + ' characters');
                    }
                }
                else {

                    if ($('#' + inElementID).attr('type') == 'password') {
                        var strMsg = 'Value has to be at least 7 characters';
                        strMsg = strMsg + " So please add zeros in front of your password. Example abc becomes 0000abc";
                        feeCurrent = EJUFormElementError(inElementID, true, strMsg);
                    }
                    else {
                        feeCurrent = EJUFormElementError(inElementID, true, 'Value has to be at least ' + inMinLength + ' characters');
                    }
                }
            }
            else {
                //Do nothing, feeCurrent was set up top
            }

        }
        else {//If the user did not enter something in
            if (isRequired == false) {//Field is NOT required and user left it empty, therefore, it is valid
                feeCurrent = EJUFormElementError(inElementID, false, '');
            }
            else {//Field IS required and user left it empty, therefore, it is valid
                feeCurrent = EJUFormElementError(inElementID, true, 'A value is required for this field.');
            }
        }
    }
    else {
        EUIShowInfoBar('Unable to display error because the input elementID or the value is undefined.');
        feeCurrent = EJUFormElementError(inElementID, true, 'Input is not valid- the elementID or value is undefined.');

    }

    return feeCurrent;
}
function EJUValidateElementType(inElementID, inValue, inType) {

    var dtCurrent = null;
    var dtTemp = null;
    var isErrorPresent = true;
    var strErrorMessage = 'No validation performed';
    //TODO:  Validation isRequired cannot be undefined, inElementID and strFieldValue cannot be undefined
    if (inElementID != undefined || inValue != undefined) {
        if (inType == 'date') {
            var dtTemp = null;
            dtTemp = EJUParseDateOnly(inValue);
            if (dtTemp == null) {
                isErrorPresent = true;
                strErrorMessage = 'Please specify a valid date. (i.e. 1/2/2011)';
            }
            else {
                isErrorPresent = false;
                strErrorMessage = '';
            }
        }
        else if ((inType == 'datetime') || (inType == 'datetime-local')) {
            var dtTemp = null;
            dtTemp = EJUParseDateTimeOnly(inValue);
            if (dtTemp == null) {
                isErrorPresent = true;
                strErrorMessage = 'Please specify a valid date time. (i.e. 1/21/2011 8:24 PM)';
            }
            else {
                isErrorPresent = false;
                strErrorMessage = '';
            }
        }
        else if (inType == 'time') {
            var dtTemp = null;
            dtTemp = EJUParseTimeOnly(inValue);
            if (dtTemp == null) {
                isErrorPresent = true;
                strErrorMessage = 'Please specify a valid time. (i.e. 8:24 PM)';
            }
            else {
                isErrorPresent = false;
                strErrorMessage = '';
            }
        }
        else if (inType == 'month') {
            var isTypeValid = false;
            isTypeValid = EJUValidateMonth(inValue);
            if (isTypeValid == false) {
                isErrorPresent = true;
                strErrorMessage = 'Please specify a valid month. (i.e. 1 or January)';
            }
            else {
                isErrorPresent = false;
                strErrorMessage = '';
            }

        }
        else if (inType == 'number') {
            var isTypeValid = false;
            isTypeValid = EJUValidateNumber(inValue);
            if (isTypeValid == false) {
                isErrorPresent = true;
                strErrorMessage = 'Please specify a valid number.';
            }
            else {
                isErrorPresent = false;
                strErrorMessage = '';
            }
        }
        else if (inType == 'int') {
            var isTypeValid = false;
            isTypeValid = EJUValidateInt(inValue);
            if (isTypeValid == false) {
                isErrorPresent = true;
                strErrorMessage = 'Please specify a valid integer.';
            }
            else {
                isErrorPresent = false;
                strErrorMessage = '';
            }
        }
        else if (inType == 'email') {
            var isTypeValid = false;
            isTypeValid = EJUValidateEmail(inValue);
            if (isTypeValid == false) {
                isErrorPresent = true;
                strErrorMessage = 'Please specify a valid email address. (i.e. abc@xyz.com)';
            }
            else {
                isErrorPresent = false;
                strErrorMessage = '';
            }


        }
        else if (inType == 'tel') {
            var isTypeValid = false;
            isTypeValid = EJUValidatePhoneNo(inValue);
            if (isTypeValid == false) {
                isErrorPresent = true;
                strErrorMessage = 'Please specify a valid phone number. (i.e. 281-222-XXXX).';
            }
            else {
                isErrorPresent = false;
                strErrorMessage = '';
            }
        }
        else {
            isErrorPresent = false;
            strErrorMessage = '';
        }

    }
    else {
        EUIShowInfoBar('Unable to display error because the input elementID or the value is undefined.');
        isErrorPresent = true;
        strErrorMessage = 'Input is not valid- the elementID or value is undefined.';
    }

    var feeCurrent = EJUFormElementError(inElementID, isErrorPresent, strErrorMessage);
    return feeCurrent;
}

function EJUValidateElementMinLength(inValue, inMinLength) {
    var isValid = false;
    if ((inMinLength != undefined) && (inMinLength != '')) {
        var intLimitLength = parseInt(inMinLength);
        var intInputLength = inValue.length;
        if (intLimitLength != NaN) {
            if (intInputLength >= intLimitLength) {
                isValid = true;
            }
        }
        else {
            EUIShowInfoBar('Unable to display error because the  min value of the element is not valid (' + inMinLength + ')');
        }
    }
    else {
        isValid = true;
    }
    return isValid;
}
function EJUValidateElementMaxLength(inValue, inMaxLength) {
    var isValid = false;
    if ((inMaxLength != undefined) && (inMaxLength != '')) {
        var intLimitLength = parseInt(inMaxLength);
        var intInputLength = inValue.length;
        if (intLimitLength != NaN) {
            if (intInputLength <= intLimitLength) {
                isValid = true;
            }
        }
        else {
            isValid = false;
            EUIShowInfoBar('Unable to display error because the  max value of the element is not valid (' + inMaxLength + ')');
        }
    }
    else {
        isValid = true;
    }


    return isValid;
}

function EJUValidateElementLessThanField(inValue, inCompareFieldName, inAllowEqual, inElementID, inFormName, inType) {
    var feeCurrent = EJUFormElementError(inElementID, true, 'Validation not performed');
    if ((inCompareFieldName != undefined) && (inCompareFieldName != '')) {
        var strCompareFieldID = '#' + inFormName + ' #' + inCompareFieldName;
        var isCompareFieldFound = false;
        if (EJUISIDUnique(strCompareFieldID, null) == true) {
            isCompareFieldFound = true;
        }
        else {
            //Check for auto generated ID
            strCompareFieldID = '#' + inFormName + '_Form_' + inCompareFieldName;
            if (EJUISIDUnique(strCompareFieldID, null) == true) {
                isCompareFieldFound = true;
            }
            else {
                strCompareFieldID = '#' + inCompareFieldName;
                if (EJUISIDUnique(strCompareFieldID, null) == true) {
                    isCompareFieldFound = true;
                }
                else {
                    isCompareFieldFound = false;
                }

                isCompareFieldFound = false;
            }
        }
        if (isCompareFieldFound == true) {
            var strCompareFieldValue = $(strCompareFieldID).val();
            if ((strCompareFieldValue == undefined) || (strCompareFieldValue == '') || (inValue == undefined) || (inValue == '')) {
                feeCurrent = EJUFormElementError(inElementID, false, '');
            }
            else {
                //If the user has entered values in both current field and comparison field, then only do this test.
                //If we do not have this criteria, during Live validation, the user would be flagged with an error on blur of 1st field because the other will be empty.
                //Note, this condition causes potentially undesired behavior if one of the comparison fields (inValue or inCompareFieldName)
                //is marked as not required. In such a case, if the user leaves the non-required field empty, this test will never be executed.
                isValid = EJUValidateElementMaxValue(inValue, strCompareFieldValue, inType, inAllowEqual);
                if (isValid) {
                    feeCurrent = EJUFormElementError(inElementID, false, '');
                }
                else {
                    var strEqualText = '';
                    if (inAllowEqual == true) {
                        strEqualText = ' or equal to ';
                    }
                    feeCurrent = EJUFormElementError(inElementID, true, 'Value in this field must be less than ' + strEqualText + strCompareFieldValue + ' (' + strCompareFieldValue + ')');
                }
            }
        }
        else {
            feeCurrent = EJUFormElementError(inElementID, true, 'Javascript Error: Comparison field (' + strCompareFieldID + ') does not exists.');
            EUIShowInfoBar('Unable to display error because the comparison field does not exists (' + strCompareFieldID + ')');
        }
    }
    else {
        isValid = true;
    }
    return isValid;
}
//function EJUValidateElementLessThanField(inValue, inCompareFieldValue, inAllowEqual, inElementID, inFormName, inType) {
//    var feeCurrent = EJUFormElementError(inElementID, true, 'Validation not performed');
//    if ((inCompareFieldValue != undefined) && (inCompareFieldValue != '')) {
//        if ((inCompareFieldValue == undefined) || (inCompareFieldValue == '') || (inValue == undefined) || (inValue == '')) {
//            feeCurrent = EJUFormElementError(inElementID, false, '');
//        }
//        else {
//            //If the user has entered values in both current field and comparison field, then only do this test.
//            //If we do not have this criteria, during Live validation, the user would be flagged with an error on blur of 1st field because the other will be empty.
//            //Note, this condition causes potentially undesired behavior if one of the comparison fields (inValue or inCompareFieldName)
//            //is marked as not required. In such a case, if the user leaves the non-required field empty, this test will never be executed.
//            isValid = EJUValidateElementMaxValue(inValue, inCompareFieldValue, inType, inAllowEqual);
//            if (isValid) {
//                feeCurrent = EJUFormElementError(inElementID, false, '');
//            }
//            else {
//                var strEqualText = '';
//                if (inAllowEqual == true) {
//                    strEqualText = ' or equal to ';
//                }
//                feeCurrent = EJUFormElementError(inElementID, true, 'Value in this field must be less than ' + strEqualText + inCompareFieldValue + ' (' + inCompareFieldValue + ')');

//            }
//        }
//    }
//    else {
//        isValid = true;

//    }
//    return isValid;
//}
function EJUValidateElementGreaterThanField(inValue, inCompareFieldName, inAllowEqual, inElementID, inFormName, inType) {
    var feeCurrent = EJUFormElementError(inElementID, true, 'Validation not performed');

    if ((inCompareFieldName != undefined) && (inCompareFieldName != '')) {
        var strCompareFieldID = '#' + inFormName + ' #' + inCompareFieldName;
        var isCompareFieldFound = false;
        if (EJUISIDUnique(strCompareFieldID, null) == true) {
            isCompareFieldFound = true;
        }
        else {
            //Check for auto generated ID
            strCompareFieldID = '#' + inFormName + '_Form_' + inCompareFieldName;
            if (EJUISIDUnique(strCompareFieldID, null) == true) {
                isCompareFieldFound = true;
            }
            else {
                strCompareFieldID = '#' + inCompareFieldName;
                if (EJUISIDUnique(strCompareFieldID, null) == true) {
                    isCompareFieldFound = true;
                }
                else {
                    isCompareFieldFound = false;
                }

                isCompareFieldFound = false;
            }
        }
        if (isCompareFieldFound == true) {
            var strCompareFieldValue = $(strCompareFieldID).val();
            if ((strCompareFieldValue == undefined) || (strCompareFieldValue == '') || (inValue == undefined) || (inValue == '')) {
                feeCurrent = EJUFormElementError(inElementID, false, '');
            }
            else {
                //If the user has entered values in both current field and comparison field, then only do this test.
                //If we do not have this criteria, during Live validation, the user would be flagged with an error on blur of 1st field because the other will be empty.
                //Note, this condition causes potentially undesired behavior if one of the comparison fields (inValue or inCompareFieldName)
                //is marked as not required. In such a case, if the user leaves the non-required field empty, this test will never be executed.
                isValid = EJUValidateElementMinValue(inValue, strCompareFieldValue, inType, inAllowEqual);
                if (isValid) {
                    feeCurrent = EJUFormElementError(inElementID, false, '');
                }
                else {
                    var strEqualText = '';
                    if (inAllowEqual == true) {
                        strEqualText = ' or equal to ';
                    }
                    feeCurrent = EJUFormElementError(inElementID, true, 'Value in this field must be more than ' + strEqualText + strCompareFieldValue + ' (' + strCompareFieldValue + ')');
                }
            }
        }
        else {
            feeCurrent = EJUFormElementError(inElementID, true, 'Javascript Error: Comparison field (' + strCompareFieldID + ') does not exists.');
            EUIShowInfoBar('Unable to display error because the comparison field does not exists (' + strCompareFieldID + ')');
        }
    }
    else {
        isValid = true;
    }
    return isValid;
}
function EJUValidateElementEqualToField(inValue, inCompareFieldName, inElementID, inFormName, inType) {
    var feeCurrent = EJUFormElementError(inElementID, true, 'Validation not performed');
    if ((inCompareFieldName != undefined) && (inCompareFieldName != '')) {
        var strCompareFieldID = '#' + inFormName + ' #' + inCompareFieldName;
        var isCompareFieldFound = false;
        if (EJUISIDUnique(strCompareFieldID, null) == true) {
            isCompareFieldFound = true;
        }
        else {
            //Check for auto generated ID
            strCompareFieldID = '#' + inFormName + '_Form_' + inCompareFieldName;
            if (EJUISIDUnique(strCompareFieldID, null) == true) {
                isCompareFieldFound = true;
            }
            else {
                strCompareFieldID = '#' + inCompareFieldName;
                if (EJUISIDUnique(strCompareFieldID, null) == true) {
                    isCompareFieldFound = true;
                }
                else {
                    isCompareFieldFound = false;
                }

                isCompareFieldFound = false;
            }
        }
        if (isCompareFieldFound == true) {
            var strCompareFieldValue = $(strCompareFieldID).val();
            if ((strCompareFieldValue == undefined) || (strCompareFieldValue == '') || (inValue == undefined) || (inValue == '')) {
                feeCurrent = EJUFormElementError(inElementID, false, '');
            }
            else {
                //If the user has entered values in both current field and comparison field, then only do this test.
                //If we do not have this criteria, during Live validation, the user would be flagged with an error on blur of 1st field because the other will be empty.
                //Note, this condition causes potentially undesired behavior if one of the comparison fields (inValue or inCompareFieldName)
                //is marked as not required. In such a case, if the user leaves the non-required field empty, this test will never be executed.
                isValid = EJUValidateElementEqualValue(inValue, strCompareFieldValue, inType);
                if (isValid) {
                    feeCurrent = EJUFormElementError(inElementID, false, '');
                }
                else {
                    feeCurrent = EJUFormElementError(inElementID, true, 'Value in this field must be equal to ' + strCompareFieldValue + ' (' + strCompareFieldValue + ')');
                }
            }
        }
        else {
            feeCurrent = EJUFormElementError(inElementID, true, 'Javascript Error: Comparison field (' + strCompareFieldID + ') does not exists.');
            EUIShowInfoBar('Unable to display error because the comparison field does not exists (' + strCompareFieldID + ')');
        }
    }
    else {
        isValid = true;
    }
    return isValid;
}
function EJUValidateElementMinValue(inValue, inMinValue, inType, inAllowEqual) {
    var isValid = false;
    if ((inMinValue != undefined) && (inMinValue != '')) {

        var intLength;
        var intValueLength;
        var strLimitValue = inMinValue;
        var isInputTypeValid = false;
        var isLimitTypeValid = false;
        var strValue = inValue;


        if (inType == 'date') {
            /* Handle Date ranges */
            var dtValues = inValue.split(' - '); //To handle date/time ranges.  Note this has to be the same as the delimiter in DateRangePicker control
            if (dtValues.length >= 1) {
                //If the array has at least 1 element, there was no ' - ' thus set value as the 0th element
                strValue = dtValues[0];
            }
            else {
                //If for some reason, the length = 0 or less (which should never happen because split method always returns an array with 1 element
                strValue = inValue;
            }
            /* End of Handle Date ranges */

            var dtValue = EJUParseDateOnly(strValue);
            if (dtValue != null) {
                var dtLimit = EJUParseDateOnly(strLimitValue);
                if (dtLimit != null) {
                    var intDateCompare = dtValue.compareTo(dtLimit);  //-1 if dtValue < dtLimit; 0 if equal; 1 if dtValue > dtLimit
                    if (intDateCompare == 0) {
                        if (inAllowEqual) {
                            isValid = true;
                        }
                        else {
                            isValid = false;
                        }
                    }
                    else if (intDateCompare > 0) {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    isValid = false;
                    EUIShowInfoBar('Unable to display error because the min limit value is not a valid date (' + strLimitValue + ')');
                }
            }
            else {
                isValid = false;
                EUIShowInfoBar('Unable to display error because the input value is not a valid date (' + strValue + ')');
            }
        }
        else if ((inType == 'datetime') || (inType == 'datetime-local')) {
            /* Handle Date ranges */
            var dtValues = inValue.split(' - '); //To handle date/time ranges.  Note this has to be the same as the delimiter in DateRangePicker control

            if (dtValues.length >= 1) {
                //If the array has at least 1 element, there was no ' - ' thus set value as the 0th element
                strValue = dtValues[0];
            }
            else {
                //If for some reason, the length = 0 or less (which should never happen because split method always returns an array with 1 element
                strValue = inValue;
            }
            /* End of Handle Date ranges */


            var dtValue = EJUParseDateTimeOnly(strValue);
            if (dtValue != null) {
                var dtLimit = EJUParseDateTimeOnly(strLimitValue);
                if (dtLimit != null) {
                    var intDateCompare = dtValue.compareTo(dtLimit);  //-1 if dtValue < dtLimit; 0 if equal; 1 if dtValue > dtLimit
                    if (intDateCompare == 0) {
                        if (inAllowEqual) {
                            isValid = true;
                        }
                        else {
                            isValid = false;
                        }
                    }
                    else if (intDateCompare > 0) {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    isValid = false;
                    EUIShowInfoBar('Unable to display error because the limit is not a valid date time (' + strLimitValue + ')');
                }
            }
            else {
                isValid = false;
                EUIShowInfoBar('Unable to display error because the input value is not a valid date time (' + strValue + ')');
            }
        }
        else if (inType == 'time') {
            /* Handle Date ranges */
            var dtValues = inValue.split(' - '); //To handle date/time ranges.  Note this has to be the same as the delimiter in DateRangePicker control

            if (dtValues.length >= 1) {
                //If the array has at least 1 element, there was no ' - ' thus set value as the 0th element
                strValue = dtValues[0];
            }
            else {
                //If for some reason, the length = 0 or less (which should never happen because split method always returns an array with 1 element
                strValue = inValue;
            }
            /* End of Handle Date ranges */

            var dtValue = EJUParseTimeOnly(strValue);
            if (dtValue != null) {
                var dtLimit = EJUParseTimeOnly(strLimitValue);
                if (dtLimit != null) {
                    var intDateCompare = dtValue.compareTo(dtLimit);  //-1 if dtValue < dtLimit; 0 if equal; 1 if dtValue > dtLimit
                    if (intDateCompare == 0) {
                        if (inAllowEqual) {
                            isValid = true;
                        }
                        else {
                            isValid = false;
                        }
                    }
                    else if (intDateCompare > 0) {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    isValid = false;
                    EUIShowInfoBar('Unable to display error because the limit is not a valid time (' + strLimitValue + ')');
                }
            }
            else {
                isValid = false;
                EUIShowInfoBar('Unable to display error because the input value is not a valid time (' + strValue + ')');
            }
        }
        else if ((inType == 'int') || (inType == 'number')) {
            var lgValue = parseFloat(inValue);
            var lgLimit = parseFloat(strLimitValue);
            if (lgLimit != null) {
                if (lgValue == lgLimit) {
                    if (inAllowEqual) {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
                else if (lgValue > lgLimit) {
                    isValid = true;
                }
                else {
                    isValid = false;
                }
            }
            else {
                isValid = false;
                EUIShowInfoBar('Unable to display error because the min limt is not a valid date (' + strLimitValue + ')');
            }
        }
        else {
            isValid = false;
            EUIShowInfoBar('Unable to display error because min value test is not allowed for this data type (' + inType + ')');
        }
    }
    else {
        isValid = true;
    }

    return isValid;
}
function EJUValidateElementMaxValue(inValue, inMaxValue, inType, inAllowEqual) {
    var isValid = false;
    if ((inMaxValue != undefined) && (inMaxValue != '')) {
        var intLength;
        var intValueLength;
        var strLimitValue = inMaxValue;
        var isInputTypeValid = false;
        var isLimitTypeValid = false;
        var strValue = inValue;
        if (inType == 'date') {
            /* Handle Date ranges */
            var dtValues = inValue.split(' - '); //To handle date/time ranges.  Note this has to be the same as the delimiter in DateRangePicker control

            if (dtValues.length = 1) {
                //If the array has only 1 element, there was no ' - ' and thus, no range specified, thus set value as the 0th element
                strValue = dtValues[0];
            }
            else if (dtValues.length > 1) {
                //If the array has more than 1 element, there was at least 1 ' - ' and thus, range was specified, thus set value as the 1th element
                strValue = dtValues[1];
            }
            else {
                //If for some reason, the length = 0 or less (which should never happen because split method always returns an array with 1 element
                strValue = inValue;
            }
            /* End of Handle Date ranges */

            var dtValue = EJUParseDateOnly(strValue);
            if (dtValue != null) {
                var dtLimit = EJUParseDateOnly(strLimitValue);
                if (dtLimit != null) {
                    var intDateCompare = dtValue.compareTo(dtLimit);  //-1 if dtValue < dtLimitEJUValida 0 if equal; 1 if dtValue > dtLimit
                    if (intDateCompare == 0) {
                        if (inAllowEqual) {
                            isValid = true;
                        }
                        else {
                            isValid = false;
                        }

                    }
                    if (intDateCompare <= 0) {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    isValid = false;
                    EUIShowInfoBar('Unable to display error because max limit is not a valid date (' + strLimitValue + ')');

                }
            }
            else {
                isValid = false;
                EUIShowInfoBar('Unable to display error because input is not a valid date (' + strLimitValue + ')');
            }
        }
        else if ((inType == 'datetime') || (inType == 'datetime-local')) {
            /* Handle Date ranges */
            var dtValues = inValue.split(' - '); //To handle date/time ranges.  Note this has to be the same as the delimiter in DateRangePicker control

            if (dtValues.length = 1) {
                //If the array has only 1 element, there was no ' - ' and thus, no range specified, thus set value as the 0th element
                strValue = dtValues[0];
            }
            else if (dtValues.length > 1) {
                //If the array has more than 1 element, there was at least 1 ' - ' and thus, range was specified, thus set value as the 1th element
                strValue = dtValues[1];
            }
            else {
                //If for some reason, the length = 0 or less (which should never happen because split method always returns an array with 1 element
                strValue = inValue;
            }
            /* End of Handle Date ranges */


            var dtValue = EJUParseDateTimeOnly(strValue);
            if (dtValue != null) {
                var dtLimit = EJUParseDateTimeOnly(strLimitValue);
                if (dtLimit != null) {
                    var intDateCompare = dtValue.compareTo(dtLimit);  //-1 if dtValue < dtLimitEJUValida 0 if equal; 1 if dtValue > dtLimit
                    if (intDateCompare == 0) {
                        if (inAllowEqual) {
                            isValid = true;
                        }
                        else {
                            isValid = false;
                        }

                    }
                    if (intDateCompare < 0) {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    isValid = false;
                    EUIShowInfoBar('Unable to display error because max limit is not a valid date time (' + strLimitValue + ')');
                }
            }
            else {
                isValid = false;
                EUIShowInfoBar('Unable to display error because input is not a valid date (' + strValue + ')');
            }
        }
        else if (inType == 'time') {
            /* Handle Date ranges */
            var dtValues = inValue.split(' - '); //To handle date/time ranges.  Note this has to be the same as the delimiter in DateRangePicker control

            if (dtValues.length = 1) {
                //If the array has only 1 element, there was no ' - ' and thus, no range specified, thus set value as the 0th element
                strValue = dtValues[0];
            }
            else if (dtValues.length > 1) {
                //If the array has more than 1 element, there was at least 1 ' - ' and thus, range was specified, thus set value as the 1th element
                strValue = dtValues[1];
            }
            else {
                //If for some reason, the length = 0 or less (which should never happen because split method always returns an array with 1 element
                strValue = inValue;
            }
            /* End of Handle Date ranges */

            var dtValue = EJUParseTimeOnly(strValue);
            if (dtValue != null) {
                var dtLimit = EJUParseTimeOnly(strLimitValue);
                if (dtLimit != null) {
                    var intDateCompare = dtValue.compareTo(dtLimit);  //-1 if dtValue < dtLimitEJUValida 0 if equal; 1 if dtValue > dtLimit
                    if (intDateCompare == 0) {
                        if (inAllowEqual) {
                            isValid = true;
                        }
                        else {
                            isValid = false;
                        }

                    }
                    if (intDateCompare < 0) {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    isValid = false;
                    EUIShowInfoBar('Unable to display error because max limit is not a valid time (' + strLimitValue + ')');
                }
            }
            else {
                isValid = false;
                EUIShowInfoBar('Unable to display error because input is not a valid time (' + strValue + ')');
            }
        }
        else if ((inType == 'int') || (inType == 'number')) {
            var lgValue = parseFloat(strValue);
            var lgLimit = parseFloat(strLimitValue);

            if (lgLimit != null) {
                if (lgValue == lgLimit) {
                    if (inAllowEqual) {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
                else if (lgValue < lgLimit) {
                    isValid = true;
                }
                else {
                    isValid = false;
                }
            }
            else {
                isValid = false;
                EUIShowInfoBar('Unable to display error because max limit is not a valid number (' + strLimitValue + ')');
            }
        }
        else {
            isValid = false;
            EUIShowInfoBar('Unable to display error because input is not a valid number (' + strValue + ')');

        }
    }
    else {
        isValid = true;
    }

    return isValid;
}
function EJUValidateElementEqualValue(inValue, inMinValue, inType) {
    var isValid = false;
    if ((inMinValue != undefined) && (inMinValue != '')) {

        var intLength;
        var intValueLength;
        var strLimitValue = inMinValue;
        var isInputTypeValid = false;
        var isLimitTypeValid = false;
        var strValue = inValue;


        if (inType == 'date') {
            /* Handle Date ranges */
            var dtValues = inValue.split(' - '); //To handle date/time ranges.  Note this has to be the same as the delimiter in DateRangePicker control
            if (dtValues.length >= 1) {
                //If the array has at least 1 element, there was no ' - ' thus set value as the 0th element
                strValue = dtValues[0];
            }
            else {
                //If for some reason, the length = 0 or less (which should never happen because split method always returns an array with 1 element
                strValue = inValue;
            }
            /* End of Handle Date ranges */

            var dtValue = EJUParseDateOnly(strValue);
            if (dtValue != null) {
                var dtLimit = EJUParseDateOnly(strLimitValue);
                if (dtLimit != null) {
                    var intDateCompare = dtValue.compareTo(dtLimit);  //-1 if dtValue < dtLimit; 0 if equal; 1 if dtValue > dtLimit
                    if (intDateCompare == 0) {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    isValid = false;
                    EUIShowInfoBar('Unable to display error because equal comparison value for this field is not a valid date (' + strLimitValue + ')');

                }
            }
            else {
                isValid = false;
                EUIShowInfoBar('Unable to display error because input value for this field is not a valid date (' + strValue + ')');

            }
        }
        else if ((inType == 'datetime') || (inType == 'datetime-local')) {
            /* Handle Date ranges */
            var dtValues = inValue.split(' - '); //To handle date/time ranges.  Note this has to be the same as the delimiter in DateRangePicker control

            if (dtValues.length >= 1) {
                //If the array has at least 1 element, there was no ' - ' thus set value as the 0th element
                strValue = dtValues[0];
            }
            else {
                //If for some reason, the length = 0 or less (which should never happen because split method always returns an array with 1 element
                strValue = inValue;
            }
            /* End of Handle Date ranges */


            var dtValue = EJUParseDateTimeOnly(strValue);
            if (dtValue != null) {
                var dtLimit = EJUParseDateTimeOnly(strLimitValue);
                if (dtLimit != null) {
                    var intDateCompare = dtValue.compareTo(dtLimit);  //-1 if dtValue < dtLimit; 0 if equal; 1 if dtValue > dtLimit
                    if (intDateCompare == 0) {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    isValid = false;
                    EUIShowInfoBar('Unable to display error because equal comparison field value for this field is not a valid datetime (' + strLimitValue + ')');
                }
            }
            else {
                isValid = false;
                EUIShowInfoBar('Unable to display error because input value for this field is not a valid datetime(' + strValue + ')');
            }
        }
        else if (inType == 'time') {
            /* Handle Date ranges */
            var dtValues = inValue.split(' - '); //To handle date/time ranges.  Note this has to be the same as the delimiter in DateRangePicker control

            if (dtValues.length >= 1) {
                //If the array has at least 1 element, there was no ' - ' thus set value as the 0th element
                strValue = dtValues[0];
            }
            else {
                //If for some reason, the length = 0 or less (which should never happen because split method always returns an array with 1 element
                strValue = inValue;
            }
            /* End of Handle Date ranges */

            var dtValue = EJUParseTimeOnly(strValue);
            if (dtValue != null) {
                var dtLimit = EJUParseTimeOnly(strLimitValue);
                if (dtLimit != null) {
                    var intDateCompare = dtValue.compareTo(dtLimit);  //-1 if dtValue < dtLimit; 0 if equal; 1 if dtValue > dtLimit
                    if (intDateCompare == 0) {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    isValid = false;
                    EUIShowInfoBar('Unable to display error because equal comparison field value for this field is not a valid time (' + strLimitValue + ')');
                }
            }
            else {
                isValid = false;
                EUIShowInfoBar('Unable to display error because input value for this field is not a valid time (' + strValue + ')');
            }
        }
        else if ((inType == 'int') || (inType == 'number')) {
            var lgValue = parseFloat(inValue);
            var lgLimit = parseFloat(strLimitValue);
            if (lgLimit != null) {
                if (lgValue == lgLimit) {
                    isValid = true;
                }
                else {
                    isValid = false;
                }
            }
            else {
                isValid = false;
                EUIShowInfoBar('Unable to display error because equal comparison value value for this field is not a valid date (' + strLimitValue + ')');
            }
        }
        else {
            isValid = false;
            EUIShowInfoBar('Unable to display error because equal value test is not allowed for this data type (' + inType + ')');
        }
    }
    else {
        isValid = true;
    }

    return isValid;
}


function EJUValidateMonth(inValue) {
    var isValid = false;

    var dtCurrent = Date.parseExact(inValue, 'M');
    if (dtCurrent != null) {
        isValid = true;
    }
    else {
        isValid = false;
    }

    return isValid;
}
function EJUValidateNumber(inValue) {
    var isValid = false;

    var lgNumber = parseFloat(inValue);
    if (isNaN(lgNumber) == false) {
        isValid = true;
    }
    else {
        isValid = false;
    }

    return isValid;
}
function EJUValidateInt(inValue) {
    var isValid = false;

    var lgNumber = parseInt(inValue);
    if (isNaN(lgNumber) == false) {
        isValid = true;
    }
    else {
        isValid = false;
    }

    return isValid;
}
function EJUValidateEmail(inValue) {
    var isValid = false;

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(inValue) != false) {
        isValid = true;
    }
    else {
        isValid = false;
    }

    return isValid;
}
function EJUValidatePhoneNo(inValue) {
    var isValid = false;
    var reg = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
    if (reg.test(inValue) != false) {
        isValid = true;
    }
    else {
        isValid = false;
    }


    return isValid;
}
function EJUParseDateOnly(inValue) {
    var dtCurrent = null;
    var arrValidFormats = [
    //Standard Date Only formats
                                    'M/d/yyyy'
                                    , 'M/d/yy'
                                    , 'M-d-yyyy'
                                    , 'M-d-yy'
                                    , 'yyyy-M-d'
                                    , 'yy-M-d'
                                    ];
    //Note:  when we pass the entire array into parseExact, it sometimes 
    //      doesn't parse certain formats even though the input is proper
    //      thus we are loop through until one format matches.
    for (intFormat in arrValidFormats) {
        dtCurrent = Date.parseExact(inValue, arrValidFormats[intFormat]);
        if (dtCurrent != null) { break; }
    }
    return dtCurrent;
}
function EJUParseDateTimeOnly(inValue) {
    var dtCurrent = null;
    var arrValidFormats = [
    //Date Time Only (No Seconds or AM/PM)
                                    'M/d/yyyy h:m'
                                    , 'M/d/yy h:m'
                                    , 'M-d-yyyy h:m'
                                    , 'M-d-yy h:m'
                                    , 'yyyy-M-d h:m'
                                    , 'yy-M-d h:m'
    //Date Time w/ Seconds (No AM/PM)
                                    , 'M/d/yyyy h:m:s'
                                    , 'M/d/yy h:m:s'
                                    , 'M-d-yyyy h:m:s'
                                    , 'M-d-yy h:m:s'
                                    , 'yyyy-M-d h:m:s'
                                    , 'yy-M-d h:m:s'
    //Date Time w/ AM/PM (NO Seconds)
                                    , 'M/d/yyyy h:m tt'
                                    , 'M/d/yy h:m tt'
                                    , 'M-d-yyyy h:m tt'
                                    , 'M-d-yy h:m tt'
                                    , 'yyyy-M-d h:m tt'
                                    , 'yy-M-d h:m tt'
    //Date Time w/ Seconds and AM/PM
                                    , 'M/d/yyyy h:m:s tt'
                                    , 'M/d/yy h:m:s tt'
                                    , 'M-d-yyyy h:m:s tt'
                                    , 'M-d-yy h:m:s tt'
                                    , 'yyyy-M-d h:m:s tt'
                                    , 'yy-M-d h:m:s tt'
                                    ];
    //Note:  when we pass the entire array into parseExact, it sometimes 
    //      doesn't parse certain formats even though the input is proper
    //      thus we are loop through until one format matches.
    for (intFormat in arrValidFormats) {
        dtCurrent = Date.parseExact(inValue, arrValidFormats[intFormat]);
        if (dtCurrent != null) { break; }
    }
    return dtCurrent;
}
function EJUParseTimeOnly(inValue) {
    var dtCurrent = null;
    var arrValidFormats = [
    // Time Only
                                    'h:m:s tt'  //Require seconds and AM/MP
                                    , 'h:m:s'   //Require seconds but not AM/MP
                                    , 'h:m tt'  //Require AM/MP but not seconds
                                    , 'h:m'     //Require neither AM/MP nor seconds

                                    ];
    //Note:  when we pass the entire array into parseExact, it sometimes 
    //      doesn't parse certain formats even though the input is proper
    //      thus we are loop through until one format matches.
    for (intFormat in arrValidFormats) {
        dtCurrent = Date.parseExact(inValue, arrValidFormats[intFormat]);
        if (dtCurrent != null) { break; }
    }
    return dtCurrent;
}
function EJUParseAnyDateOrTime(inValue) {
    var dtCurrent = null;
    dtCurrent = EJUParseDateOnly(inValue);
    if (dtCurrent == null) { dtCurrent = EJUParseDateTimeOnly(inValue); }
    if (dtCurrent == null) { dtCurrent = EJUParseTimeOnly(inValue); }
    return dtCurrent;
}
function EJUShowFormConfirmationMessages() {
    var strFormID = "";
    var strMessage = "";
    if (typeof (arrFormMessages) != 'undefined') {
        jQuery.each(arrFormMessages, function (arrIndex, arrFieldID) {
            strMessage = $.trim(arrFormMessages[arrIndex]);
            strFormID = $.trim(arrFormMessagesFrmNames[arrIndex]);
            if (strMessage != '') {
                if (strFormID.length <= 0) {
                    strFormID = $("form:last").attr('id')
                }
                if (strFormID.length > 0) {
                    if (EJUISIDUnique('#' + strFormID, null) == true) {
                        if ($('#' + strFormID + ' .EWF-UI-FormSuccessMessage').length > 0) {
                            $('#' + strFormID + ' .EWF-UI-FormSuccessMessage').append(strMessage);
                            $('#' + strFormID + ' .EWF-UI-FormSuccessMessage').delay(500).slideDown('slow');
                        }
                        else {
                            $('#' + strFormID).Prepend(strMessage);
                        }
                    }
                    else {
                        EUIShowInfoBar('Unable to show form commit confirmation because form with this ID(' + strFormID + ') is not found.');
                    }

                }
                else {//If FormID Does not exist, prepend message in last form.
                    EUIShowInfoBar('Unable to show form commit confirmation because form ID (' + strFormID + ') is empty.');
                }
            }
            else {
                //Since message is empty, do nothing.
            }
        });
    }
}
function EJUShowServerErrors() {
    var strFieldID = "";
    var strFormID = "";
    var strFieldName = "";
    var strErrorMessage = "";
    if (typeof (arrErrorFields) != 'undefined') {
        jQuery.each(arrErrorFields, function (arrIndex, arrFieldID) {
            strFieldName = $.trim(arrErrorFields[arrIndex]);
            if (typeof (arrErrorForm) != 'undefined') {
                strFormID = $.trim(arrErrorForm[arrIndex]);
            }
            strErrorMessage = $.trim(arrErrorMessages[arrIndex]);
            if (strFormID.length <= 0) {
                strFormID = $("form:last").attr('id')
            }
            if (strFormID == undefined) { strFormID = ''; }
            if (strFormID.length > 0) {
                if (EJUISIDUnique('#' + strFormID, null) == true) {
                    if ((strFieldName.length > 0) && strFieldName != ("_GLOBAL")) {
                        strFieldID = $('#' + strFormID + ' [name="' + strFieldName + '"]').attr('id');
                    }
                    else {
                        strFieldID = strFieldName;
                    }
                }
                else {
                    strFieldID = $(':input[name="' + strFieldName + '"]').attr('id');
                    //strFormID = ''; //Needed for ##Note-1
                }
            }
            else {
                strFieldID = $(':input[name="' + strFieldName + '"]').attr('id');
                strFormID = ''; //Needed for ##Note-1
            }
            EJUFormElementErrorShow(strFieldID, strFormID, strFieldID, strErrorMessage)

        });
    }
}
function EJUSetFocusOnFirstElement() {
    if ($(':input[autofocus]').length > 0) {
        //If one or ore elements on the form have autofocus set, set the focus on the 1st of the inputs
        $(':input[autofocus]:visible:enabled:first').focus();
    }
    else if ($('a.autofocus:visible:first').length > 0) {
        $('a.autofocus:visible:first').focus();
    }
    //    else if ($('form').length > 0) {
    //        //If there are no autofocus fields but there are forms, set focus on 1st element of last form.
    //        $('form:last :input:visible:enabled:first').focus(); //Note: 
    //    }
    else {//If there are no autofocus fields or forms on the page, set focus on 1st button.
        $(':input:visible:enabled:first').focus(); //Note: 
    }
}
function EJUSetupEWFForms() {
    if ($('form').length > 0) {
        EJUShowServerErrors();
        EJUShowFormConfirmationMessages();
        EJUSetFocusOnFirstElement();
    }
    else {
        //DO Nothing
    }
}
function EJUProcessFormKeyPress(objActiveElement) {
    var isSubmitContinued = false;
    if ($(objActiveElement).is(':submit') == true) {
        isSubmitContinued = true;
    }
    else {
        if ($(objActiveElement).is(':button') == true) {
            $(objActiveElement).click();
        }
        isSubmitContinued = false;
    }
    return isSubmitContinued;
}
//function EJUFocusNextElement(inCurrentElementID) {
//    var isOpSuccesful = false;
//    inCurrentElementID = $.trim(inCurrentElementID);
//    if (inCurrentElementID.length > 0) {
//        if (EJUISIDUnique('#' + inCurrentElementID, null) == true) {
//            var lstFocusableFields = $(':input:visible:enabled');

//            var objCurrentElementID = $('#' + inCurrentElementID);
//            var intCurrentIndex = lstFocusableFields.index(objCurrentElementID);
//            if (intCurrentIndex > -1 && (intCurrentIndex + 1) < lstFocusableFields.length) {
//                lstFocusableFields.eq(intCurrentIndex + 1).focus().select();
//            }
//            else {//If it is not the enter Key, continue as normal.
//            }
//            isOpSuccesful = true;
//        }
//        else {
//            isOpSuccesful = false;
//        }
//    }
//    else {
//        isOpSuccesful = false;
//    }
//    return isOpSuccesful;
//}


function EJUFocusNextElement(inCurrentElementID) {
    var isOpSuccesful = false;
    inCurrentElementID = $.trim(inCurrentElementID);
    if (inCurrentElementID.length > 0) {
        if (EJUISIDUnique('#' + inCurrentElementID, null) == true) {
            var lstFocusableFields = $(":input:visible:enabled, .ewf-focus-onkeydown");
            var objCurrentElementID = $('#' + inCurrentElementID);
            var intCurrentIndex = lstFocusableFields.index(objCurrentElementID);
            if (intCurrentIndex > -1 && (intCurrentIndex + 1) < lstFocusableFields.length) {
                var nextElement = lstFocusableFields.eq(intCurrentIndex + 1)
                //console.log(intCurrentIndex);
                var tempCnt = 1;
                while ($(nextElement).hasClass("EWF-DonotFocus")) {
                    tempCnt++;
                    var nextIndex = intCurrentIndex + tempCnt;
                    if (lstFocusableFields.length < intCurrentIndex + tempCnt) {
                        nextElement = null;
                        break;
                    }
                    else if (tempCnt > 15) {
                        nextElement = null;
                        break;
                    }
                    else {
                        var nextElement = lstFocusableFields.eq(intCurrentIndex + tempCnt)
                    }
                } //while
                if (nextElement != null) {
                    $(nextElement).focus().select();
                }
            }
            else {//If it is not the enter Key, continue as normal.
            }
            isOpSuccesful = true;
        }
        else {
            isOpSuccesful = false;
        }
    }
    else {
        isOpSuccesful = false;
    }
    return isOpSuccesful;
}


/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of Function needed for EWF Form Validation----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of Function needed for ENET Check-All Functionality----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/

function EJUCheckAllSetup(inCheckAllID) {
    var strReturnValue = false;
    var strCheckAllID = '#' + inCheckAllID;
    var strSearchContainerID = '';  //'#{ID of some element}'
    var strMatchingName = '';
    var strMatchingClass = '';
    var strOperation = '';  //checkUncheck, checkOnly, unCheckOnly, toggle
    var strTriggerEvent = ''; //true or false
    var isTriggerEvent = true;
    strSearchContainerID = $(strCheckAllID).attr('data-EWF-CheckAll-ContainerID');
    strMatchingName = $(strCheckAllID).attr('data-EWF-CheckAll-MatchingName');
    strMatchingClass = $(strCheckAllID).attr('data-EWF-CheckAll-MatchingClass');
    strOperation = $(strCheckAllID).attr('data-EWF-CheckAll-Operation');
    strTriggerEvent = $(strCheckAllID).attr('data-EWF-CheckAll-TriggerEvent');

    if (strSearchContainerID == undefined) { strSearchContainerID = ''; }
    if (strMatchingName == undefined) { strMatchingName = ''; }
    if (strMatchingClass == undefined) { strMatchingClass = ''; }
    if (strOperation == undefined) { strOperation = ''; }
    if (strTriggerEvent == undefined) { strTriggerEvent = ''; }



    strSearchContainerID = $.trim(strSearchContainerID);
    strMatchingName = $.trim(strMatchingName);
    strMatchingClass = $.trim(strMatchingClass);
    strOperation = $.trim(strOperation);
    strTriggerEvent = $.trim(strTriggerEvent);

    if (strSearchContainerID.length == 0) {
        var thisCheckBox = $(strCheckAllID);
        strSearchContainerID = EJUGetParentFormID(thisCheckBox);
    }
    if (strMatchingName.length == 0) {
        strMatchingName = '';
    }
    if (strMatchingClass.length == 0) {
        strMatchingClass = '';
    }
    if (strOperation.length == 0) {
        strOperation = 'checkUncheck';
    }
    if (strTriggerEvent.length == 0) {
        strTriggerEvent = 'true';
    }
    if (strTriggerEvent == 'false' || strTriggerEvent == '0' || strTriggerEvent == 'no') {
        isTriggerEvent = false;
    }
    strReturnValue = EJUCheckAll(inCheckAllID, strSearchContainerID, strMatchingName, strMatchingClass, strOperation, isTriggerEvent);

    return strReturnValue
}


function EJUCheckAll(inCheckAllElementID, inSearchContainerID, inMatchingName, inMatchingClass, inOperation, inTriggerEvent) {
    var strCheckAllID = inCheckAllElementID;
    var strSearchContainerID = inSearchContainerID;  //'#{ID of some element}'
    var strMatchingName = inMatchingName;
    var strMatchingClass = inMatchingClass;
    var strOperation = inOperation;  //checkUncheck, checkOnly, unCheckOnly, toggle
    var strTriggerEvent = inTriggerEvent; //true or false

    if (strSearchContainerID.substr(0, 1) != '#') {
        strSearchContainerID = '#' + strSearchContainerID;
    }
    if (strCheckAllID.substr(0, 1) != '#') {
        strCheckAllID = '#' + strCheckAllID;
    }

    var strMatchCriteria = strSearchContainerID + ' input';
    if (strMatchingClass.length > 0) {
        strMatchCriteria = strMatchCriteria + '.' + strMatchingClass;
    }
    strMatchCriteria = strMatchCriteria + '[type=\'checkbox\']';

    if (strMatchingName.length > 0) {
        strMatchCriteria = strMatchCriteria + '[name=\'' + strMatchingName + '\']';
    }
    $(strMatchCriteria).each(function (i) {
        var strCurrentCheckboxID = $(this).attr('id');
        var isCBChecked = false;
        var isCBChecked = $(this).is(':checked');
        var isCheckAllChecked = $(strCheckAllID).is('  :checked');

        switch (strOperation.toLowerCase()) {
            case 'toggle':
                if (inTriggerEvent == true) {
                    $('#' + strCurrentCheckboxID).click();
                }
                else {
                    $('#' + strCurrentCheckboxID).attr('checked', 'checked');
                }
                break;
            case 'checkonly':
                if (isCBChecked == false) {
                    $('#' + strCurrentCheckboxID).click();
                }
                break;
            case 'uncheckonly':
                if (isCBChecked == true) {
                    $('#' + strCurrentCheckboxID).click();
                }
                break;
            case 'checkuncheck':
                if (isCheckAllChecked) {
                    //Note this value read after the change event on checkall button is fired. 
                    //  Thus, this variable contains value after the user clicked on checkall box.

                    if (isCBChecked == false) {
                        if (inTriggerEvent == true) {
                            $('#' + strCurrentCheckboxID).click();
                        }
                        else {
                            $('#' + strCurrentCheckboxID).attr('checked', 'checked');
                        }
                    }
                }
                else {
                    if (isCBChecked == true) {
                        if (inTriggerEvent == true) {
                            $('#' + strCurrentCheckboxID).click();
                        }
                        else {
                            $('#' + strCurrentCheckboxID).removeAttr('checked');
                        }
                    }
                }
                break;
        }
    });
}



function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}





/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of  Bindings for Universal ENet UI Classes-------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of  Bindings for Universal ENet UI Classes-------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
$(document).ready(function () {
    EJUBindAllUIElements('');
});

function EJUBindAllUIElements(inContainerID) {
    inContainerID = $.trim(inContainerID);
    if (inContainerID.length > 0) {
        //Add a space after the container path
        inContainerID = inContainerID + ' * ';
    }

    // $(inContainerID + ' .EWF-Image-AJAXmode').ewfImageAJAXmodePlugin();


    //hide subnavbar
    $(inContainerID + '.subnav ul.nav').hide();

    //show right menu
    $(inContainerID + '.EnetUI-RightMenu').show();

    //For each EnetMenuBar
    $(inContainerID + '.navbar ul.nav').each(function (i) {
        var strMenuID = $(this).attr('id');
        if ((strMenuID != undefined) && ($.trim(strMenuID).length > 0)) {
            EUIMenuSetup(strMenuID);
        }
        else {
            //EUIShowInfoBar('Unable to setup Menubar because element ID is not defined or does not exist.');
        }
    });

    $(inContainerID + '.nav-main').each(function (i) {
        var strMenuID = $(this).attr('id');
        if ((strMenuID != undefined) && ($.trim(strMenuID).length > 0)) {
            EUIMenuSetup(strMenuID);
        }
        else {
            //EUIShowInfoBar('Unable to setup Menubar because element ID is not defined or does not exist.');
        }
    });

    $(inContainerID + '.subnav ul.nav').each(function (i) {
        var strMenuID = $(this).attr('id');
        if ((strMenuID != undefined) && ($.trim(strMenuID).length > 0)) {
            EUIMenuSetup(strMenuID);
        }
        else {
            EUIShowInfoBar('Unable to setup Menubar because element ID is not defined or does not exist.');
        }
    });

    //    $("[rel=popover]").each(function (i) {
    //        var divID = $(this).attr('data-ewf-divpopover');
    //        var html = "";
    //        if (divID != "") {
    //            html = $("#" + divID).html();

    //        }
    //        if (html != "") {

    //            $(this).popover({ html: true,
    //                content: html
    //            }

    //        );
    //        }
    //    });

    //For each EnetMultiSelect Class, call EUIMUSSetup
    //NOTE:  EnetMultiSelect binding needs to happen before EnetDateRangeSelector otherwise, the position of the date range dropdown if
    //      off on pages that contain EnetMultiSelect and EnetDateRangeSelector.  This is because EnetDateRangeSelector positions the dropdown
    //      relative to its postion at the time of binding.  If the postion of the EnetDateRangeSelector textbox changes after the binding, 
    //      the dropdown still stays at the old position.
    $(inContainerID + '.EnetMultiSelect').each(function (i) {
        var strMSID = $(this).attr('id');
        if ((strMSID != undefined) && ($.trim(strMSID).length > 0)) {
            EUIMUSSetup(strMSID);
        }
        else {
            EUIShowInfoBar('Unable to setup Enet Multi Selector because element ID is not defined or does not exist.');
        }
    });


    //For each EentRangeSelector Class, call EUINRSSetup
    $(inContainerID + '.EnetRangeSlider').each(function (i) {
        var strSliderID = $(this).attr('id');
        if ((strSliderID != undefined) && ($.trim(strSliderID).length > 0)) {
            EUINRSSetup(strSliderID);
        }
        else {
            EUIShowInfoBar('Unable to setup Enet Number Range Selector because element ID is not defined or does not exist.');
        }
    });

    //For each EnetDateRangeSelector Class, call EUIDRSSetup
    $(inContainerID + '.EnetDateRangeSelector').each(function (i) {
        var strDRSelectorID = $(this).attr('id');
        if ((strDRSelectorID != undefined) && ($.trim(strDRSelectorID).length > 0)) {
            EUIDRSSetup(strDRSelectorID);
        }
        else {
            EUIShowInfoBar('Unable to setup Enet Date Range Selector because element ID is not defined or does not exist.');
        }
    });


    $(inContainerID + '.EnetDateRangeSelectorBS3').each(function (i) {
        var strDRSelectorID = $(this).attr('id');
        if ((strDRSelectorID != undefined) && ($.trim(strDRSelectorID).length > 0)) {
            EUIDRSSetupBS3(strDRSelectorID);
        }
        else {
            EUIShowInfoBar('Unable to setup Enet Date Range Selector because element ID is not defined or does not exist.');
        }
    });


    //For each EWSJQDateCalendar, call EUIDatePickerSetup
    $(inContainerID + 'input.EWSJQDateCalendar').each(function (i) {
        var strDPSelectorID = $(this).attr('id');
        if ((strDPSelectorID != undefined) && ($.trim(strDPSelectorID).length > 0)) {
            EUIDatePickerSetup(strDPSelectorID);
        }
        else {
            EUIShowInfoBar('Unable to setup Enet Date Range Selector because element ID is not defined or does not exist.');
        }
    });

    //Rebind Popover
    EUIPopOverSetup();


    //For each EWF-UI-CheckAll class (Checks all checkboxes in a given form)
    $(document).on("click", inContainerID + '.EWF-UI-CheckAll', function (e) {
        //    $(inContainerID + '.EWF-UI-CheckAll').click(function () {
        var strID = $(this).attr('id');
        if ((strID != undefined) && ($.trim(strID).length > 0)) {
            EJUCheckAllSetup(strID);
        }
        else {
            EUIShowInfoBar('Unable to setup Enet Checkall Selector because element ID is not defined or does not exist.');
        }
    });

    //Bindings for Enet Form Validation and EnetLiveValidation
    $(inContainerID + 'form.EWSJQValidateClientSide').submit(function (e) {
        var strFormID = $(this).attr('id');

        //Reset all global and form errors
        EJUFormElementErrorClear('_GLOBAL', strFormID, '_GLOBAL', '');
        EJUFormElementErrorClear('', strFormID, ''); 

        var isErrorPresent = EJUIsErrorInForm(strFormID, true);
        if (isErrorPresent) {
            e.preventDefault();

        }
        else {
            //Continue and Submit Form

        }
    });
    $(document).on("blur", inContainerID + '.EWSJQValidateLive', function (e) {
        //$(inContainerID + '.EWSJQValidateLive').blur(function (e) {
        var strFormID = EJUGetParentFormID(this);
        var strElementID = $(this).attr('id');
        var feeCurrent = EJUValidateFormElementByName(strElementID, strFormID, true);
        //         var isClientLiveValidationDisabled = false;

        //         var strClientLiveValidationDisabled = $('#' + strFormID + ' #' + strElementID).attr('data-EWF-noClientLiveValidation');
        //         if (strClientLiveValidationDisabled == undefined) { strClientLiveValidationDisabled = ''; }
        //         strClientLiveValidationDisabled = strClientLiveValidationDisabled.toLowerCase();
        //         if ((strClientLiveValidationDisabled == 'true') || (strClientLiveValidationDisabled == 'yes') || (strClientLiveValidationDisabled == '1')) {
        //             isClientLiveValidationDisabled = true;
        //         }
        //         else {
        //             //for any other value, including '', assume the developer wants to do client side validaiton on submit
        //             isClientLiveValidationDisabled = false;
        //         }
        //         if (isClientLiveValidationDisabled == false) {
        //             var feeCurrent = EJUValidateFormElementByName(strElementID, strFormID, true);
        //         }
    });

    //    //Bindings for Form's Navigation by Enter
    $("input:text,input:checkbox, input:radio, input:radio, input:password, select, input[type='text']").keydown(function (e) {
        var isAutoKeyDown = false;
        if ($(this).attr('data-ewf-noautokeydown') != null && $(this).attr('data-ewf-noautokeydown') != undefined) {
            if ($(this).attr('data-ewf-noautokeydown') == 'true') {
                isAutoKeyDown = true;
            }
            else {
                isAutoKeyDown = false;
            }
        }
        else {
            isAutoKeyDown = false;
        }

        if (!isAutoKeyDown) {

            if (e.which == 13) { //Enter key
                var strCurrentElementID = $(this).attr('id');
                EJUFocusNextElement(strCurrentElementID);
                e.preventDefault(); //Skip default behavior of the enter key
                //return false;
            }
        }
    });

    $(document).on("focus", 'input:text,input:checkbox, input:radio, input:radio, input:password', function (e) {
        $(this).select();
    });

    if ($(".EWF-UI-FillDown").length != 0) {
        $('.EWF-UI-FillDown').fillDown();
    }

    if ($("[data-toggle='tooltip']").length != 0) {
        $("[data-toggle='tooltip']").tooltip();
    }

    if ($("[data-toggle='popover']").length != 0) {
        $('[data-toggle="popover"]').popover();
    }

    OFModal.rebind(true);
    OFModal.transverseHTML();
    if ($('.signature-pad').length > 0) {
        bindSignature(); 
    }


    $(".ewf-js-number").each(function (index, value) {
        EWFFormatNumber($(this));
    });


    // Bind Tooltip to the eclipse
    if ($(".OF-table-row-truncate").length > 0) {
        EWFEclipseToolTip();
    }



    //    $('input:TEXT,input:Text').keydown(function (e) {
    //        if (e.which == 13) { //Enter key
    //            var strCurrentElementID = $(this).attr('id');
    //            EJUFocusNextElement(strCurrentElementID);
    //            e.preventDefault(); //Skip default behavior of the enter key
    //            //return false;
    //        }
    //    });


    //Code to submit form when the user is on a button or input type submit
    //Note this cannot be made into a sep
    //    $(inContainerID + 'form').keydown(function (e) {

    //            if (e.which == 13) { //Enter key
    //                var objActiveElement = $(document.activeElement);
    //                var isSubmitContinued = false;
    //                isSubmitContinued = EJUProcessFormKeyPress(objActiveElement);
    //                return isSubmitContinued;
    //            }
    //    });
    //Bind all Twitter Buttons click functionality to twitter JS function that shows loading message and disables the button.
    //    $('button.EWF-JS-AutoDisable, input.EWF-JS-AutoDisable a.EWF-JS-AutoDisable').click(function (e) {
    //        setTimeout(function () {
    //            $(this).button('reset')
    //        }
    //            , 3000);
    //    });
}
//NOTE:  Form setup is in a different document.ready because we want it to be the absolute last thing that happens on the page.
$(document).ready(function () {
    if ($('form').length > 0) {
        EJUSetupEWFForms();
    }
});

//Bind popovers
$(document).ready(function () {
    EUIPopOverSetup();
});

$(document).on('blur', '.ewf-js-number', function (e) {
    EWFFormatNumber($(this))
});


$(document).on('change', '.ewf-js-number', function (e) {
    EWFFormatNumber($(this))
});

function EWFFormatNumber(inElement) {
    var currencyRoundoff = $(inElement).attr("ewf-data-number-roundoff");
    var currValue = $(inElement).val();
    var formattedCurrValue = accounting.formatNumber(currValue, currencyRoundoff)
    $(inElement).val(formattedCurrValue);
    if ($(inElement).attr("ewf-data-number-align-override") != 'true') {
        $(inElement).addClass("text-right");
    }
}

//
//$('nav a:first').tab('show');
function showFormSuccess(responseData, trigger) {
    var formName = $(trigger).attr('id');
    var divID = "div" + formName + "Form"
    var root = responseData;
    var strFieldName;
    var strFormID;
    var strMessage;
    var strFieldID;
    var sParsedRowData;
    //console.log(root);

    if ($(root).find("enetformsection[type='FullHTML']").length > 0) {
        var rowData = $(root).find("enetformsection[type='FullHTML']").children();
        if ($(rowData).length > 0) {

            sParsedRowData = $('<div>').html(rowData).html();
            if ($(document).find("#" + "div" + formName + "ConfrmMsg").length > 0) {
                $(document).find("#" + "div" + formName + "ConfrmMsg").remove();
            }
            if ($(document).find("#" + divID).length > 0) {
                if (sParsedRowData.length > 0) {
                    EWFUpdatePageContent("#" + divID, sParsedRowData);
                    //$(document).find("#" + divID).replaceWith(sParsedRowData);
                }
                else {
                    EWFUpdatePageContent("#" + divID, sParsedRowData);
                    //$(document).find("#" + divID).html(sParsedRowData);
                }
            }

        }

    }
    else {
        if ($(root).find("#" + divID).length == 0) {
            divID = $(root).first("div").children().attr("id");
        }

        if ($(root).find("#" + divID).length > 0) {
            if ($(document).find("#" + divID).length > 0) {
                var sparsed = $(root).find("#" + divID);
                sParsedRowData = $('<div>').html(sparsed).html();
                $("#" + divID).html(sParsedRowData);
            }
            else {
                var sparsed = $(root).find("#" + divID);
                sParsedRowData = $('<div>').html(sparsed).html();
                $("#" + formName).after(sParsedRowData);
                $("#" + formName).remove();
            }
        }
    }

    if ($(root).find("enetformsection[type='ValidationErrors']").length > 0) {
        EJUFormElementErrorClear('', formName, ''); //Reset all the old errors in the form
        if ($(root).find("enetformsection[type='ValidationErrors']").children().length > 0) {
            var allChildren = $(root).find("enetformsection[type='ValidationErrors']").children().find("FormError");
            $(allChildren).each(function () {
                strFieldName = $(this).attr("fieldName");
                strFormID = $(this).attr("formName");
                strMessage = $(this).text();
                if (strFormID.length > 0) {
                    if (EJUISIDUnique('#' + strFormID, null) == true) {
                        if (strFieldName.length > 0) {
                            if (strFieldName != '_GLOBAL') {
                                strFieldID = $('#' + strFormID + ' [name="' + strFieldName + '"]').attr('id');
                            }
                            else {
                                strFieldID = strFieldName;
                            }
                        }
                        else {
                            strFieldID = strFieldName;
                        }
                    }
                }
                EJUFormElementErrorShow(strFieldID, strFormID, strFieldID, strMessage);
            });
        }
    }

    if ($(root).find("enetformsection[type='CommitSuccessful']").length > 0) {
        var commitSuccess = $(root).find("enetformsection[type='CommitSuccessful']");
        var strMsg = $(commitSuccess).clone().children().remove().end().text();
        if ($(document).find("#" + divID).length > 0) {
            $(document).find("#" + divID).prepend($.trim(strMsg));

        }

    }

    if ($(root).find("enetformsection[type='CommitFailure']").length > 0) {
        EJUFormElementErrorClear('', formName, ''); //Reset all the old errors in the form
        var children = $(root).find("enetformsection[type='CommitFailure']").children();
        if ($(children).length > 0) {
            var strFormID = $(children).attr("formName");
            var strMessage = $(children).html();//$(children).text();
            if (strFormID.length > 0) {
                EJUFormElementErrorClear('_GLOBAL', strFormID, '_GLOBAL', '');
                EJUFormElementErrorShow('_GLOBAL', strFormID, '_GLOBAL', strMessage);
            }
        }



    }



    if ($(root).find("enetformsection[type='Custom']").length > 0) {
        var rowData = $(root).find("enetformsection[type='Custom']").children();
        if ($(rowData).length > 0) {
            //============================================
            //Notes 1/21/2015
            // we have used custom node only to output javascript so
            // this solution should work 
            //============================================
            sParsedRowData = $('<div>').html(rowData).text();
            var script = document.createElement("script");
            script.setAttribute("type", "text/javascript");
            script.appendChild(document.createTextNode(sParsedRowData));
            document.body.appendChild(script);
        }
    }

    //needed for fancybox


    if ($(root).find("enetformsection[type='Custom']").length > 0) {
        var rowData = $(root).find("enetformsection[type='Custom']").children();
        if ($(rowData).length > 0) {
            sParsedRowData = $('<div>').html(rowData).html();
        }
    }


    return sParsedRowData;
}

function showFormFailureErrors(responseMsg, trigger, responseData) {
    var formName = $(trigger).attr('id');
    var divID = "div" + formName + "Form";
    EJUFormElementErrorClear('', formName, '');
    EJUFormElementErrorShow('', formName, '', responseMsg);
    if (responseData != "") {
        $(document).find("#" + divID).html(responseData);
        displayEWFSecurityModal(divID);
    }

}

function updateParentGrid(inRowID, inMode, inRefreshRowID) {
    var arrQS = window.location.href.split('?');
    var strQS = arrQS[arrQS.length - 1];
    var strGridID = "";
    var strRowID = "";
    var intMode = "";
    var strRefreshRowID = "";
    var strGridFormName ="";
    if ($.isArray(inRowID)) {
        var arrList = inRowID;
        strRowID = arrList[0];
        strGridID = arrList[1];
        strMode = arrList[3];
        strRefreshRowID = arrList[4];

    }
    else {
        strRowID = inRowID;
        strMode = inMode;
        strRefreshRowID = inRefreshRowID;
    }

    if (window.opener != null) {
        strGridID = EJUParseQueryString(strQS, "RefreshGridID");
        strRefreshRowID = EJUParseQueryString(strQS, "RefreshRowID");
        strGridFormName = "frm"+strGridID+"OpForm";
        if (strRefreshRowID == "-1") {
         window.opener.EnetGrid2Refresh(strGridFormName);
        }
        else {
            window.opener.RefreshGridRow(strGridID, strRowID, strMode, strRefreshRowID);
        }
    }
    else {
        if (strRefreshRowID == "-1") {
            strGridFormName = "frm"+strGridID+"OpForm";
            EnetGrid2Refresh(strGridFormName);
        }
        else {
            RefreshGridRow(strGridID, strRowID, strMode, strRefreshRowID);
        }
    }
}

function BatchStatusRecord(responseData) {
    var formName = $(trigger).attr('id');
    var divName = formName.replace('frm', 'div').replace('DeleteForm', 'Grid');
    var root = responseData;
    var strGridName;
    var strGridDeleteRowID;
    var strFieldID;
    var sParsedRowData;
    if ($(root).find("enetdelete").length > 0) {
        if ($(root).find("status").length > 0) {
            var status = $(root).find("status").text();
            if (status == 'True' || status == '1') {
                $(root).find('additionalvalue').each(function () {

                    var nameID = $(this).attr('name');
                    if (nameID == 'enetGridDeleteRowID') {
                        strGridDeleteRowID = $(this).text();
                    }
                    else if (nameID == 'enetGridDeleteLinked') {
                        strGridName = $(this).text();
                    }

                });
                if (strGridDeleteRowID != "" && strGridName != "") {
                    RefreshGridRow(strGridName, strGridDeleteRowID, 1);
                }
            }
            else {
                var strMessage = $(root).find("message").text();
                EUIShowErrorInfoBar(strMessage, divName);
            }
        }
    }
}


function hideDeleteRecord(responseData, trigger) {
    var formName = $(trigger).attr('id');
    var divName = formName.replace('frm', 'div').replace('DeleteForm', 'Grid');
    var root = responseData;
    var strGridName;
    var strGridDeleteRowID;
    var strFieldID;
    var sParsedRowData;
    if ($(root).find("enetdelete").length > 0) {
        if ($(root).find("status").length > 0) {
            var status = $(root).find("status").text();
            if (status == 'True' || status == '1') {
                $(root).find('additionalvalue').each(function () {

                    var nameID = $(this).attr('name');
                    if (nameID == 'enetGridDeleteRowID') {
                        strGridDeleteRowID = $(this).text();
                    }
                    else if (nameID == 'enetGridDeleteLinked') {
                        strGridName = $(this).text();
                    }

                });
                if (strGridDeleteRowID != "" && strGridName != "") {
                    if ($(root).find("mode").length > 0) {
                        var mode = $(root).find("mode").text();
                        RefreshGridRow(strGridName, strGridDeleteRowID, mode);
                    }
                    else {
                        RefreshGridRow(strGridName, strGridDeleteRowID, 0);
                    }
                }
            }
            else {
                var strMessage = $(root).find("message").text();
                EUIShowErrorInfoBar(strMessage, divName);
            }
        }


    }
}
function showGridDeleteError(responseMsg, trigger,responseData) {
    var formName = $(trigger).attr('id');
    var divName = formName.replace('frm', 'div').replace('DeleteForm', 'Grid');
    EUIShowErrorInfoBar(responseMsg, divName);
    if (responseData.length > 0) {
        if ($(document).find("#" + divName + " div.EWF_Access_Denied").length > 0) {
            $("#" + divName).html(responseData);
        }
        else {
            $("#" + divName).append(responseData);
        }
        displayEWFSecurityModal(divName);
    }
//    $("#" + divName).append(responseData);
//    displayEWFSecurityModal("#" + divName);
}

function showUserTimeRestrictionError(responseMsg, trigger) {
    var formName = $(trigger).attr('id');
    var divName = formName.replace('frm', 'div');
    EUIShowErrorInfoBar(responseMsg, divName);
}

function showUserTimeRestrictionSuccess(responseMsg, trigger) {
    var formName = $(trigger).attr('id');
    var divName = formName.replace('frm', 'div');

}

/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of  Bindings for Universal ENet UI Classes-------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/

function filterOut(inOriginalString, inFilterString) { //string, term
    return $.grep(inOriginalString.split(','), function (v) { return v != inFilterString; }).join(',');
}

$(document).on("change", ".EWF_GRS_CheckChange", function (e) {
    var id = $(this).attr("id");
    var isChecked = $('#' + id).is(':checked');
    var parentFieldID = $(this).attr("data-ewf-grs-parentid");

    var originalSelectedValue = $(this).attr("data-ewf-grs-originalselected");


    var originalDeletedID = $('#' + parentFieldID).attr("data-EWF-GRS-OriginalDeletedID");
    var newSelectedID = $('#' + parentFieldID).attr("data-EWF-GRS-NewSelectedID");

    var orgDeletedList = $('#' + originalDeletedID).val();
    var newSelectedList = $('#' + newSelectedID).val();
    //        alert("existin orgsel=" + originalSelectedValue);
    //        alert("existin orgDeletedList=" + orgDeletedList);
    //        alert("existin newSelectedList=" + newSelectedList);


    var val = $('#' + id).val();
    // alert(val);
    if (isChecked == true) {

        if (originalSelectedValue > 0) {
        }
        else {
            if (newSelectedList.indexOf(val) >= 0) {
            }
            else {
                newSelectedList = val + "," + newSelectedList;
                //alert(val);
                //alert(newSelectedList);
            }
        }

        if (orgDeletedList.indexOf(val) >= 0) {
            orgDeletedList = filterOut(orgDeletedList, val);
        }
        else {
        }

    }
    else {
        if (newSelectedList.indexOf(val) >= 0) {
            newSelectedList = filterOut(newSelectedList, val);
        }
        else {
        }

        if (originalSelectedValue > 0) {
            if (orgDeletedList.indexOf(val) >= 0) {
            }
            else {

                //orgDeletedList = val + orgDeletedList + ",";
                orgDeletedList = val + "," + orgDeletedList;
            }

        } else {

        }
    }

    if (orgDeletedList != "") {
        if (orgDeletedList.substring(orgDeletedList.length - 1) == ',') {
            orgDeletedList = orgDeletedList.slice(0, -1);
        }
    }
    if (newSelectedList != "") {
        if (newSelectedList.substring(newSelectedList.length - 1) == ',') {
            newSelectedList = newSelectedList.slice(0, -1);
        }
    }

    //        alert("org del=" + orgDeletedList);
    //        alert("new sel=" + newSelectedList);
    $('#' + originalDeletedID).val(orgDeletedList);
    $('#' + newSelectedID).val(newSelectedList);

    var callbakfun = $('#' + parentFieldID).attr("data-EWF-GRS-OnChangeCallback");
    if (callbakfun != "") {
        var fun = window[callbakfun];
        fun(this);
    }
});

$(document).on("click", ".EWF_GRS_FinishComplete", function (e) {
    var parentFieldID = $(this).attr("data-ewf-grs-parentid");
    var callbakfun = $('#' + parentFieldID).attr("data-ewf-grs-onfinishcallback");
        if (callbakfun != "") {
            var fun = window[callbakfun];
            fun(this);
        }
   
});

function functonTemp(responseData, trigger) {
    var formName = $(trigger).attr('id');
    var divID = "div" + formName + "Form"
    var root = responseData;
    var strFieldName;
    var strFormID;
    var strMessage;
    var strFieldID;
    var sParsedRowData;

}


$(document).ready(function () {

    fixConsole(false);

    //Function to enable console.log for IE
    function fixConsole(alertFallback) {
        if (typeof console === "undefined") {
            console = {}; // define it if it doesn't exist already
        }
        if (typeof console.log === "undefined") {
            if (alertFallback) { console.log = function (msg) { alert(msg); }; }
            else { console.log = function () { }; }
        }
        if (typeof console.dir === "undefined") {
            if (alertFallback) {
                // THIS COULD BE IMPROVED… maybe list all the object properties?
                console.dir = function (obj) { alert("DIR: " + obj); };
            }
            else { console.dir = function () { }; }
        }
    }



});

$(document).on("click", ".EWF_AccessDenied_Close", function (e) {
    $(".EWF_AccessDenied_Show").modal('hide')

});

$(document).ready(function () {
    displayEWFSecurityModal("");
});

function displayEWFSecurityModal(inDivID) {
    if ($.trim(inDivID).length > 0) {
        $("#" + inDivID + "  .EWF_AccessDenied_Show").modal("show");
    }
    else {
        if ($(document).find(".EWF_AccessDenied_Show").length > 0) {
            $(".EWF_AccessDenied_Show").first().modal("show");
        }
    }

}

//$('a.EJS-Rep-Print').printPreview();
$(document).on("click", "a.EJS-Rep-Print", function (e) {
    window.print();
});



$(document).on("click", "a.EJS-Rep-MultiPage-Print", function (e) {
    $('.ewf-multipageprint-page-*').remove();
    $('.ewf-multipageprint-page-header-inserted').remove();
    $('.ewf-multipageprint-page-footer-inserted').remove();
    $(".ewf-multipageprint").each(function (index) {
        var strPagesToSplit = $(this).data('ewfmultipageprint-pages');
        var strHeaderToAppend = $(this).data('ewfmultipageprint-header');
        var strFooterToAppend = $(this).data('ewfmultipageprint-footer');

        strPagesToSplit = $.trim(strPagesToSplit);
        strHeaderToAppend = $.trim(strHeaderToAppend);
        strFooterToAppend = $.trim(strFooterToAppend);
        //If strHeaderToAppend is not a class defination or id defination, make it a class defination
        if (strHeaderToAppend.length > 0) {
            switch (strHeaderToAppend.substring(0, 1)) {
                case '#':
                case '.':
                    break;
                default:
                    strHeaderToAppend = '.' + strHeaderToAppend;
            }
        }

        //If strFooterToAppend is not a class defination or id defination, make it a class defination
        if (strFooterToAppend.length > 0) {
            switch (strFooterToAppend.substring(0, 1)) {
                case '#':
                case '.':
                    break;
                default:
                    strFooterToAppend = '.' + strFooterToAppend;
            }
        }


        if ($.isNumeric(strPagesToSplit)) {
            strPagesToSplit = parseInt(strPagesToSplit);
        }
        else {
            strPagesToSplit = 2;
        }

        var intTempCounter = strPagesToSplit;
        while (intTempCounter >= 1) {
            //Note:  we are outputting footer first because we are using insertAfter.  This means the thing we inser first will appear last on the page
            if (intTempCounter != strPagesToSplit) {//Do not add a page break on the last page.
                $('<div class="ewf-multipageprint-pagebreak"></div>').insertAfter(this);
            }
            if (strFooterToAppend.length > 0) {
                $('.ewf-multipageprint-page-footer').addClass('ewf-multipageprint-page-footer-inserted').clone().removeClass('ewf-multipageprint-page-footer').insertAfter(this);
            }
            $(this).clone().addClass('ewf-multipageprint-page-' + intTempCounter).removeClass('ewf-multipageprint').insertAfter(this);
            if (strHeaderToAppend.length > 0) {
                $('.ewf-multipageprint-page-header').addClass('ewf-multipageprint-page-header-inserted').clone().removeClass('ewf-multipageprint-page-header').insertAfter(this);
            }
            //Note
            intTempCounter--;
        }
        window.print();
    });
});




function EnetGridToRefresh(inOpFormName) {
    window.opener.EnetGrid2Refresh(inOpFormName);
}

/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of Function needed for Parent Child Form----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/

$(document).on("click", ".EWF_PCF_ChildForm", function (e) {
    var parentFieldID = $(this).attr("data-ewf-pcf-parentid");
   // showEmptyTemplate(parentFieldID);
    createTableFromJSON(parentFieldID);
    showParentFieldError(parentFieldID);

    var callbakfun = $('#' + parentFieldID).attr("data-ewf-pcf-onchildformloadcallback");
    if (callbakfun != "" && callbakfun != undefined) {
        var fun = window[callbakfun];
        fun(this);
    }
    if ($('div.fancybox-opened').length > 0) {
        $('.fancybox-close').css('z-index', 1040);
        $('#ui-datepicker-div').css('z-index', 8051);
        //        var divID = parentFieldID.replace('EnetPCF', 'EnetPCFModal');
        //        EJUBindAllUIElements('#' + divID);
    }
});

$(document).on("click", ".EWF_PCF_AddNewItem", function (e) {
    var id = $(this).attr("id");
    var parentFieldID = $(this).attr("data-ewf-pcf-parentid");
    var modalID = id.replace('EnetPCFAddNewItem', 'EnetPCFModal');
    var isInsertValid = false;
    var isErrorInChildForm = false;
    isErrorInChildForm = validateChildForm(parentFieldID);

    if (!isErrorInChildForm) {
        var validateFun = $('#' + parentFieldID).attr("data-EWF-PCF-OnAddItemValidateCallback");
        if (validateFun != "") {
            var fun = window[validateFun];
            isInsertValid = fun(this);
        }
        else {
            isInsertValid = true;
        }

        if (isInsertValid) {
            var data = {};
            var jsonFieldID = $('#' + parentFieldID).attr("data-EWF-PCF-JSONID");

            var jsonString = $('#' + jsonFieldID).val();
            if (jsonString == '') {
                jsonString = '{"records":[';
            }
            else {
                jsonString = jsonString.slice(0, -2);
                jsonString = jsonString + ",";
            }
            var strFieldName, strFieldValue, strFieldLabel, strDisplayValue, strFieldID, strShowInGrid, strFormat, strColCSS;
            var allInputs = $('#' + modalID + " .EWF-UI-ShowJSON ");

            jsonString = jsonString + '{';
            $.each(allInputs, function (index, item) {
                strFieldName = $(item).attr("name");
                strFieldValue = $(item).val();
                strShowInGrid = $(item).attr("data-ewf-showinchildgrid");
                strFormat = $(item).attr("data-ewf-format");
                strColCSS = $(item).attr("data-ewf-colcss");

                if (strShowInGrid != undefined) {
                    if (strShowInGrid.toLowerCase() == 'true' || strShowInGrid.toLowerCase() == "1" || strShowInGrid.toLowerCase() == 'yes') {
                        strShowInGrid = 1;
                    }
                    else {
                        strShowInGrid = 0;
                    }
                }
                else {
                    strShowInGrid = 0;
                }
                if ($(item).is('select')) {
                    strDisplayValue = $(item).find("option:selected").text();
                } else {
                    strDisplayValue = strFieldValue;
                }

                if (strFormat != undefined) {
                    //do nothing
                }
                else {
                    strFormat = "";
                }

                if (strColCSS != undefined) {
                    //do nothing
                }
                else {
                    strColCSS = "";
                }

                strFieldLabel = $('#' + modalID + ' label[for=' + strFieldName + ']').text()
                if (strFieldName != "" && strFieldName != undefined) {
                    jsonString = jsonString + '"' + strFieldName + '":{"label":"' + strFieldLabel +
                                            '","value":"' + strFieldValue +
                                            '","DisplayValue":"' + strDisplayValue +
                                            '","showInGrid":"' + strShowInGrid +
                                            '","formatType":"' + strFormat +
                                            '","colCSS":"' + strColCSS +
                                            '"},'
                }



            });
            jsonString = jsonString.slice(0, -1);
            jsonString = jsonString + '}]}'

            $('#' + jsonFieldID).val(jsonString);
            createTableFromJSON(parentFieldID);
            var allVisibleInputs = $('#' + modalID).find('input[type=text],textarea,select').filter(':visible');
            $.each(allVisibleInputs, function (index, item) {
                $(item).val('');
            });
            var callbakfun = $('#' + parentFieldID).attr("data-ewf-pcf-onaddnewitemcallback");
            if (callbakfun != "" && callbakfun != undefined) {
                var fun = window[callbakfun];
                fun(this);
            }
        }
    }
    else {
        //alert("error in child form");
    }


});

function showEmptyTemplate(inParentID) {
    var jsonID = $('#' + inParentID).attr('data-EWF-PCF-JSONID');
    var strJson = $('#' + jsonID).val();
    var modalID = $('#' + inParentID).attr('data-ewf-pcf-modalid');
    if (strJson == "") {
        $("#" + modalID + " .PCF-Empty-Template").show();
        $("#" + modalID + " .modalpagefooter").hide();
    }
    else {
        $("#" + modalID + " .PCF-Empty-Template").hide();
        $("#" + modalID + " .modalpagefooter").show();
      
    }
}

function createTableFromJSON(inParentID) {
    var jsonID = $('#' + inParentID).attr('data-EWF-PCF-JSONID');
    var tableDivID = inParentID.replace('EnetPCF', 'EnetPCFModalTable');
    var strJson = $('#' + jsonID).val();
    var tempIndex = 0;
    if (strJson != "") {
        var dataObj = $.parseJSON(strJson);
        $('#' + tableDivID).html("");
        var htmlTable = '<table class="table table-hover">';
        $.each(dataObj.records, function (idx, val) {
            /* Show ID */

            if (tempIndex == 0) {
                htmlTable += '<thead>';
                htmlTable += '<tr>';
                for (var i in val) {
                    if (val[i].showInGrid == "1") {

                        htmlTable += '<th class="'+ val[i].colCSS+ '">' + val[i].label + '</th>';
                    }
                }
                htmlTable += '<th> </th>';
                htmlTable += '</tr>';
                htmlTable += '</thead>';
                htmlTable += '<tr>';

                for (var i in val) {
                    if (val[i].showInGrid == "1") {
                        var format = val[i].formatType;
                        var dispVal = val[i].DisplayValue;
                        var colCss = val[i].colCSS;
                        var calculatedDisp = getCalculatedDispValue(dispVal, format);
                        htmlTable += '<td data-ewf-colid="' + i + '" data-ewf-colval="' + val[i].value + '" class="'+ colCss+ '">' + calculatedDisp + '</td>';
                    }

                }
                var rowID = val.RowID.value;
                var isDelete;
                if (val.IsDelete != null) {
                    isDelete = val.IsDelete.value
                }
                else {
                    isDelete = 1;
                }
                if (isDelete == "0") {
                    //htmlTable += '<td><span class="disabled btn"><i class="icon-trash"></i></span> </td>'
                    htmlTable += '<td>&nbsp;</td>';
                }
                else {
                    htmlTable += '<td style="width:13px"><a href="#" class="EWF_PCF_DeleteItem " data-ewf-pcf-parentid="' + inParentID + '" data-ewf-pcf-rowid="' + rowID + '" data-ewf-index="' + tempIndex + '"><i class="glyphicon glyphicon-trash icon-trash"></i></a> </td>';

                }

                htmlTable += '</tr>';

            }
            else {
                htmlTable += '<tr>';
                for (var i in val) {
                    if (val[i].showInGrid == "1") {
                        var format = val[i].formatType;
                        var dispVal = val[i].DisplayValue;
                        var colCss = val[i].colCSS;
                        var calculatedDisp = getCalculatedDispValue(dispVal, format);
                        htmlTable += '<td data-ewf-colid="' + i + '" data-ewf-colval="' + val[i].value + '" class="' + colCss + '">' + calculatedDisp + '</td>';
                    }

                }
                var rowID = val.RowID.value;
                var isDelete;
                if (val.IsDelete != null) {
                    isDelete = val.IsDelete.value
                }
                else {
                    isDelete = 1;
                }
                if (isDelete == "0") {
                    //htmlTable += '<td><span class="disabled btn"><i class="icon-trash"></i></span> </td>'
                    htmlTable += '<td>&nbsp;</td>';
                }
                else {
                    htmlTable += '<td style="width:13px"><a href="#" class="EWF_PCF_DeleteItem " data-ewf-pcf-parentid="' + inParentID + '" data-ewf-pcf-rowid="' + rowID + '" data-ewf-index="' + tempIndex + '"><i class="glyphicon glyphicon-trash icon-trash"></i></a> </td>';
                }
                htmlTable += '</tr>';


            }
            tempIndex++;

        });
        htmlTable += '</tr>';
        $('#' + tableDivID).append(htmlTable);
    }
    else {
        $('#' + tableDivID).html('');
    }
    showEmptyTemplate(inParentID);
}


function getCalculatedDispValue(inDispVal, inFormat) {
    var strCalDispVal = "";
    if (inFormat == "") {
        strCalDispVal = inDispVal;
    }
    else if (inFormat == "$") {
    if (!isNaN(parseFloat(inDispVal))) {
        strCalDispVal = "$" + parseFloat(inDispVal).toFixed(2);
    }
    else {
        strCalDispVal = inDispVal;
    }
}
return strCalDispVal;
}

$(document).on("click", ".EWF_PCF_DeleteItem", function (e) {
    var id = $(this).attr("id");
    var parentFieldID = $(this).attr("data-ewf-pcf-parentid");
    var originalDeletedID = $('#' + parentFieldID).attr("data-EWF-PCF-OriginalDeletedID");
    var isDeleteValid = false;
    var validateFun = $('#' + parentFieldID).attr("data-EWF-PCF-OnDeleteItemValidateCallback");
    if (validateFun != "" && validateFun != undefined) {
        var fun = window[validateFun];
        isDeleteValid = fun(this);
    }
    else {
        isDeleteValid = true;
    }

    if (isDeleteValid) {
        var rowid = $(this).attr("data-ewf-pcf-rowid");

        var orgDeletedList = $('#' + originalDeletedID).val();
        if (rowid != "") {
            orgDeletedList = orgDeletedList + rowid + ",";
        }
        $('#' + originalDeletedID).val(orgDeletedList);

        var index = $(this).attr("data-ewf-index");
        var jsonFieldID = $('#' + parentFieldID).attr("data-EWF-PCF-JSONID");
        var jsonString = $('#' + jsonFieldID).val();

        if (jsonString != "") {
            var dataObj = $.parseJSON(jsonString);
            //delete dataObj.records[index];
            dataObj.records.splice(index, 1);

            var splicedJSon;
            if (dataObj.records.length == 0) {
                splicedJSon = "";
            }
            else {
                splicedJSon = JSON.stringify(dataObj);
            }



            $('#' + jsonFieldID).val(splicedJSon);
        }
        createTableFromJSON(parentFieldID);
        var callbakfun = $('#' + parentFieldID).attr("data-ewf-pcf-ondeleteitemcallback");
        if (callbakfun != "" && callbakfun != undefined) {
            var fun = window[callbakfun];
            fun(this);
        }
    }

});

function validateChildForm(inParentFieldID) {
    var modalID = $('#' + inParentFieldID).attr("data-ewf-pcf-modalid");
    var isErrorPresent = false;
    var stFormID = EJUGetParentFormID('#' + inParentFieldID);
    $('#' + modalID).find(':input').each(function (i, field) {
        var strFieldName = field.name;
        //var strElementID = $('#' + stFormID + ' #' + modalID + ' [name="' + strFieldName + '"]').attr("id");

        //Have to remove the form identifier as we have moved the modals outside of the form
        var strElementID = $('#' + modalID + ' [name="' + strFieldName + '"]').attr("id");
        var feeCurrent = EJUValidateFormElementByName(strElementID, stFormID, true);
        isErrorPresent = isErrorPresent || feeCurrent.IsErrorPresent;
    });
    return isErrorPresent;

}

$(document).on('click', '.EWF_PCF_FinishComplete', function (e) {
    var parentFieldID = $(this).attr("data-ewf-pcf-parentid");
    var callbakfun = $('#' + parentFieldID).attr("data-EWF-PCF-OnFinishCallback");
    if (callbakfun != "" && callbakfun != undefined) {
        var fun = window[callbakfun];
        fun(this);
    }

});

$(document).on('click', '.EWF_PCF_Close', function (e) {
    var parentFieldID = $(this).attr("data-ewf-pcf-parentid");
    //var jsonFieldID = $('#' + parentFieldID).attr("data-EWF-PCF-JSONID");
    //$('#' + jsonFieldID).val('');
    var callbakfun = $('#' + parentFieldID).attr("data-EWF-PCF-OnCloseCallback");
    if (callbakfun != "" && callbakfun != undefined) {
        var fun = window[callbakfun];
        fun(this);
    }

});

function showParentFieldError(inParentFieldID) {
    var stFormID = EJUGetParentFormID('#' + inParentFieldID);
    var parentFieldErrorID = "err" + stFormID + '_' + inParentFieldID;
    var strError = $('#' + parentFieldErrorID).text();
    strError = $.trim(strError);
    if (strError != "") {
        var modalID = $('#' + inParentFieldID).attr('data-ewf-pcf-modalid');
        var modalErrorID = modalID + "errorDiv";
        $('#' + modalErrorID).html('');
        $('#' + modalErrorID).append(strError);
        $('#' + modalErrorID).show('slow');
    }

}




/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of Function needed for Parent Child Form Validation----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- start of Function needed for Additional value on select----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/


$(document).on("click", ".EWF_AdditionalSelectVal_Show", function (e) {

    var parentContainerID = $(this).attr('data-ewf-quickaddparent-divid');
    var quickAddDivID = $(this).attr('data-ewf-additionalselectval-divid');

    if ($('#' + quickAddDivID).hasClass('hide')) {
        $('#' + parentContainerID).hide();
        CSPBS3Show($('#' + quickAddDivID));
        $('#' + quickAddDivID).find('input').focus();
    }
    else {
        $('#' + quickAddDivID).addClass('hide');
    }
    var inputID = $('#' + quickAddDivID + ' input[type="text"]').attr('id');
    $('#' + inputID).val('');

});

$(document).on("click", ".EWF_AdditionalSelectVal_Close", function (e) {

    var parentContainerID = $(this).attr('data-ewf-quickaddparent-divid');
    var quickAddDivID = $(this).attr('data-ewf-additionalselectval-divid');

    //Clear errors
    var inputID = $('#' + quickAddDivID + ' input[type="text"]').attr('id');
    var stFormID = EJUGetParentFormID('#' + inputID);
    EJUFormElementErrorClear(inputID, stFormID, inputID);

    // Hide Quick Add
    $('#' + quickAddDivID).hide();

    //Show parent
    CSPBS3Show($('#' + parentContainerID));

    $('#' + quickAddDivID).addClass('hide');

});


$(document).on("click", ".EWF_AdditionalSelectVal_Add", function (e) {
    var divID = $(this).attr('data-ewf-additionalselectval-divid');
    var inputID = $('#' + divID + ' input[type="text"]').attr('id');
    var inputVal = $('#' + inputID).val();
    var stFormID = EJUGetParentFormID('#' + inputID);
    var strHandler = $(this).attr('data-ewf-handler');
    var strParentID = $(this).attr('data-ewf-additionalselectval-parentid');
    var parentContainerID = $(this).attr('data-ewf-quickaddparent-divid');
    if (inputVal.trim() != "") {
        insertSelectAdditionalVal(inputVal.trim(), strHandler, strParentID, stFormID, inputID, divID, parentContainerID);
    }
    else {
        EJUFormElementErrorClear(inputID, stFormID, inputID);
        EJUFormElementErrorShow(inputID, stFormID, inputID, "Value is required for this field");
    }
    $('#' + divID).addClass('hide');
});

function insertSelectAdditionalVal(inDataVal, inHandler, inParentField, inFormID, inElementID, inDivID, inParentContainerID) {
    var strMessage = "";
    var strHandler = inHandler;
    if (strHandler.indexOf("?") > -1) {
        strHandler = inHandler + "&dataformat=JSON";
    }
    else {
        strHandler = inHandler + "?dataformat=JSON";
    }
    $.ajax({
        type: "POST",
        url: strHandler,
        data: "InputData=" + inDataVal
    })
        .done(function (data) {
            try {
                if (data.resStatus == "1") {

                    if (data.strResponseData.Status == "1") {
                        var strPrimaryKey = data.strResponseData.Data;
                        CSPBS3Show($('#' + inParentContainerID));
                        $('#' + inParentField).append($("<option></option>").attr("value", strPrimaryKey).text(inDataVal).attr('selected', 'selected'));
                        $(".EWF_AdditionalSelectVal_Show").bind("click");
                        $('#' + inDivID).hide();
                    }
                    else {
                        strMessage = data.strResponseData.Message;
                        EJUFormElementErrorClear(inElementID, inFormID, inElementID);
                        EJUFormElementErrorShow(inElementID, inFormID, inElementID, strMessage)
                    }
                }
                else {
                    if (data.resStatus == "-3" || data.resStatus == "-4") {
                        strMessage = "You account does not have permissions to add this. Access can be changed from owner or supervisor accounts."
                    }
                    else {
                        strMessage = (data.strMessage);
                    }
                    EJUFormElementErrorClear(inElementID, inFormID, inElementID);
                    EJUFormElementErrorShow(inElementID, inFormID, inElementID, strMessage)

                }

            }
            catch (e) {
                alert("error exc");
            }
        }).fail(function (data, statusText) {

            alert(statusText);

        }).always(function (data) {

        });
}


/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of Function needed for Additional value on select----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- Start of Function needed for Modal fancy box----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
$(document).on("click", ".EWF-Modal-Form", function (e) {
    var href = $(this).attr('href');
    var calBckFnSuc = $(this).attr('data-ewf-form-jsfunc-frontendsuccessupdater');
    var calBckFnFail = $(this).attr('data-ewf-form-jsfunc-frontendfailureupdater');
    var FnDoFrontEndSuccessUpdate = window[calBckFnSuc];
    var FnDoFrontEndFailureUpdate = window[calBckFnFail];

    var modalWidth = $(this).attr('data-ewf-modalwidth');
    var modalHeight = $(this).attr('data-ewf-modalheight');
    var strAutoSize = $(this).attr('data-ewf-modalautosize');
    if (strAutoSize != undefined) {
        strAutoSize = strAutoSize.toUpperCase();
    }
    var isAutoSize = strAutoSize == "FALSE" ? false : true; //had to do this since fancy box takes boolean value , by default we assume we want to autosize height and width
    var strContent = "";
    e.preventDefault();
    var trigger = $(this);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: href

    })
    ajaxRequest.done(function (response) {
        var contentType;
        var responseStatus = "0";
        var responseMsg = "";
        var responseData = "";

        contentType = ajaxRequest.getResponseHeader("content-type");
        if (contentType.toLowerCase().indexOf("json") >= 0) {

            responseStatus = response.resStatus;
            responseData = response.strResponseData;
            responseMsg = response.strMessage;
        }
        else if (contentType.toLowerCase().indexOf("xml") >= 0) {

            responseStatus = $(response).find('resStatus').text();
            responseMsg = $(response).find('strMessage').text();
            responseData = $(response).find('strResponseData').text();
            try {
                if (responseStatus == "1") {
                    responseData = $.parseXML(responseData);
                }

            } catch (err) {
                // Invalid XML
                //responseStatus = "0";
                responseData = "";
                //responseMsg = "Invalid XML";
            }
        }
        else if (contentType.toLowerCase().indexOf("html") >= 0) {
            responseStatus = "1";
            responseData = response;
            responseMsg = "";
        }
        else {

            responseStatus = "0";
            responseData = "";
            responseMsg = "Content type not Defined";
        }

        if (responseStatus === "1") {
            //strContent = responseData; //FnDoFrontEndSuccessUpdate(responseData, trigger);
            if (FnDoFrontEndSuccessUpdate != undefined) {
                strContent = FnDoFrontEndSuccessUpdate(responseData, trigger);
            }
            else {
                strContent = responseData;
            }

            strContent = '<div class="ENET-UI-Modal-Form">'+strContent+'</div>';
            $.fancybox({
                width: modalWidth,
                height: modalHeight,
                fitToView: true,
                autoSize: false,
                scrolling: 'auto',
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                modal: true,
                content: strContent

            });
            EJUBindAllUIElements(".fancybox-inner");
            $(".ui-daterangepickercontain").css('z-index', 8031);
            $("#ui-datepicker-div").css('z-index', 8031);
            $(".ui-datepicker").css('z-index', 8031);


        }
        else {
            // FnDoFrontEndFailureUpdate(responseMsg, trigger, responseData);
            if ($.trim(responseData) != '') {
                strContent = responseData;
            }
            else if ($.trim(responseMsg) != "") {
                strContent = responseMsg;
            }
            else {
                strContent = "unknown error";
            }
            $.fancybox({
                //                    maxWidth: 500,
                //                    maxHeight:500,
                fitToView: true,
                autoSize: true,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                modal: true,
                content: strContent

            });
        }
    });

    ajaxRequest.fail(function (jqXHR, statusText) {
        inData.failureCallback(jqXHR.statusText, trigger);
    });
    ajaxRequest.always(function () {


    });

});


function EWF_CloseWindows() {
    if (window.opener != null) {
        
        if ($('#ofmodal-content').length > 0) {
            if ($('#ofmodal-create-another').length > 0) {
                if ($('input#ofmodal-create-another').prop('checked')) {
                    OFModal.rebind(true);
                    OFModal.transverseHTML();

                    //Set focus on the 1st element
                    var inputs = $('#ofmodal-body input');

                    for (var i = 0; i < inputs.length; i++) {
                        var input = $(inputs[i]);
                        if (input.css('display') !== 'none') {
                            $(input).focus();
                            break;
                        };
                    };

                    for (var k = 0; k < inputs.length; k++) {
                        var input = $(inputs[k]);
                        if (input.attr('autofocus')) {
                            break;
                        };
                    };
                }
                else {
                    setTimeout('window.close()', 2000);
                }
            }
            else {
                setTimeout('window.close()', 2000);
            }
        }
        else {
            setTimeout('window.close()', 2000);
        }

    }
    else {
        if ($('div.fancybox-opened').length > 0) {
            setTimeout('$.fancybox.close()', 2000);
            $(".ui-daterangepickercontain").css('z-index', 999);
            $("#ui-datepicker-div").css('z-index', 999);
        }
        else if ($('#ofmodal-content').length > 0) {
            if ($('#ofmodal-create-another').length > 0) {
                if ($('input#ofmodal-create-another').prop('checked')) {

                    OFModal.rebind(true);
                    OFModal.transverseHTML();

                    //Set focus on the 1st element
                    var inputs = $('#ofmodal-body input');

                    for (var i = 0; i < inputs.length; i++) {
                        var input = $(inputs[i]);
                        if (input.css('display') !== 'none') {
                            $(input).focus();
                            break;
                        };
                    };

                    for (var k = 0; k < inputs.length; k++) {
                        var input = $(inputs[k]);
                        if (input.attr('autofocus')) {
                            break;
                        };
                    };


                }
                else {
                    OFModal.closeModal();
                }
            }
            else {
                OFModal.closeModal();
            }
        }
    }
}

/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/* -- End of Function needed for modal fancy box----------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------------------------------*/

function universalCommitSuccess(inRowID, inGridID, inFunctionName, inMode,inRefreshRowID) {
    var strRowID = $.trim(inRowID);
    var strGridID = $.trim(inGridID);
    var strFunctionName = $.trim(inFunctionName);
    var strMode = $.trim(inMode);
    var strRefreshRowID = $.trim(inRefreshRowID);

    if (strFunctionName.length > 0) {
        if (window.opener != null || $('div.fancybox-opened').length > 0) {
            if (strRowID.length > 0 && strGridID.length > 0) {

                var arrParams = [strRowID, strGridID, strFunctionName, strMode, strRefreshRowID];
                var fun = window[strFunctionName];
                fun(arrParams);
            }
            else {
                EUIShowInfoBar('Unable to update because EnetGridUpdateRow field is not found for this grid. (' + inOpFormName + '_enetGridUpdate' + ')');
            }

        }
        else {
            var arrParams = [strRowID, strGridID, strFunctionName, strMode,strRefreshRowID];
            var fun = window[strFunctionName];
            fun(arrParams);
        }
    }

}

$(document).on("hover", ".EWF-ToolTip", function (e) {
    $(".EWF-ToolTip").tooltip(
           {

       });

});


$(document).on("click", ".enetquickdropdownshow", function (e) {
    $(this).siblings("div.dropdowndetails").css("display", "block");
});

$(document).on("click", ".EWF_DropDownClose", function (e) {
    $(this).parent().parent("div.dropdowndetails").css("display", "none");
});


$(document).on("blur", ".enetcal", function (e) {
    if ($(this).val() != null && $(this).val() != undefined) {
        $(this).val(mathEval($(this).val()));
    }
    else {

    } 
});


function mathEval(exp) {
    var reg = /(?:[a-z$_][a-z0-9$_]*)|(?:[;={}\[\]"'!&<>^\\?:])/ig,
        valid = true;
    var returnValue = '';

    // Detect valid JS identifier names and replace them
    exp = exp.replace(reg, function ($0) {
        // If the name is a direct member of Math, allow
        if (Math.hasOwnProperty($0))
            return "Math." + $0;
        // Otherwise the expression is invalid
        else
            valid = false;
    });

    // Don't eval if our replace function flagged as invalid
    if (!valid)
        returnValue = exp;
    else
        try { returnValue = eval(exp); } catch (e) { returnValue = exp; };
    return returnValue;
}


function formatCurrency(inData, inDigits) {
    var strFormatedString = '';
    var strTempInData = inData.toString();
    strTempInData = strTempInData.replace(',', '').replace(' ', '').replace('$', '');
    strTempInData = $.trim(strTempInData);
    if (strTempInData.substring(0, 1) == '(' && strTempInData.substring(strTempInData.length - 1, strTempInData.length) == ')') {
        strTempInData = strTempInData.replace('(', '').replace(')', '');
        strTempInData = '-' + strTempInData;
    }
    if (!isNumber(strTempInData) || strTempInData === '' || strTempInData === null) {
        strFormatedString = strTempInData;
    }
    else {
        strFormatedString = parseFloat(strTempInData).toFixed(inDigits).toString();
    }
    return strFormatedString;
}


(function ($) {

    //CONSTANTS
    var strCONSTENETGridOpPageClass = 'EJS_Grid2_OpPage';
    var strCONSTENETPredefinedFilterClass = 'EJS_Grid2_OpPredefinedFilter';
    var strCONSTGridRefreshClass = 'EJS-Grid2-OpRefresh';
    var strCONSTGridSortClass = 'EJS-Grid2-OpSort';
    var strCONSTGridCustomSortClass = 'EJS-Grid2-OpCustomSort';
    var strCONSTENETGridOpFindByFieldClass = 'EJS_Grid2_OpFindByField';
    var strCONSTENETOpFilterClass = 'ENETOpFilter';
    var strCONSTENETGridDelete = 'EWF-GridRecord-Delete';
    var strCONSTGridFilterCloseClass = 'EJS-Grid2-FilterClose';
    var strCONSTGridFilterIconClass = 'EJS-Grid2-FilterIcon';
    var strCONSTClass = 'EJS-Grid2-FilterIcon';
    var strCONSTewfGridClass = 'EnetGrid2';

    $.fn.ewfGridPlugin = function (options, params) {

        //Instantiation & event binding 
        if (options == null) {
            $(document).on("click", "a." + strCONSTENETGridOpPageClass, function (e) {
                var strFrmID = EJUGetFormID(this);
                if (strFrmID != "") {
                    var strPageNo = EJUParseQueryString($(this).attr('href'), 'EGOJumpToPage');
                    var intPageNo = parseInt(strPageNo);

                    if (!isNaN(intPageNo)) {
                        EnetGrid2JumpToPage(intPageNo, strFrmID);
                    }
                    else {
                        EUIShowInfoBar('Unable to jump to page because page ID is not specified or is not a valid integer. (' + strPageNo.toString() + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to jump to page because operations form is not found.');
                }
                e.preventDefault();
            });

            $(document).on("click", "a." + strCONSTENETPredefinedFilterClass, function (e) {
                var strFrmID = EJUGetFormID(this);
                strFrmID = $.trim(strFrmID);
                if (strFrmID != "") {
                    var strViewID = $(this).attr('viewid');
                    strViewID = $.trim(strViewID);
                    if (strViewID != "") {
                        EnetGrid2PredefinedFilter(strViewID, strFrmID);

                    }
                    else {
                        EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
                }

                e.preventDefault();
            });



            $(document).on("change", "select." + strCONSTENETPredefinedFilterClass, function (e) {
                var strFrmID = EJUGetFormID(this);
                strFrmID = $.trim(strFrmID);
                if (strFrmID != "") {
                    var strViewID = $(this).val();
                    strViewID = $.trim(strViewID);
                    if (strViewID != "") {
                        EnetGrid2PredefinedFilter(strViewID, strFrmID);

                    }
                    else {
                        EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
                }

                e.preventDefault();
            });



            $(document).on("click", "a." + strCONSTGridRefreshClass, function (e) {
                var strFrmID = EJUGetFormID(this);
                strFrmID = $.trim(strFrmID);
                if (strFrmID != "") {
                    EnetGrid2Refresh(strFrmID);

                }
                else {
                    EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
                }

                e.preventDefault();
            });



            $(document).on("click", "a." + strCONSTGridSortClass, function (e) {
                var strFrmID = EJUGetFormID(this);
                //IMPORTANT!  Line below is needed because Sort GridOp is acautally in the 
                //              data table section of the Grid and the data table section
                //              is enclosed by the GridForm.  Thus the fuction call above
                //              will return the ID of the GridForm.
                //strFrmID = strFrmID.replace('GridForm', 'OpForm');

                if (strFrmID != "") {
                    var strCol = EJUParseQueryString($(this).attr('href'), 'EGOSortCol');
                    strCol = $.trim(strCol);
                    if (strCol.length > 0) {
                        var strDirection = EJUParseQueryString($(this).attr('href'), 'sortDir');
                        var intDirection = parseInt(strDirection);
                        if (!isNaN(intDirection)) {
                            if ((intDirection == 1) || (intDirection == 0)) {
                                EnetGrid2SingleColumnSort(strFrmID, strCol, intDirection);
                            }
                            else {
                                EUIShowInfoBar('Unable to sort because sort direction is not valid. (' + intDirection.toString() + ')');
                            }
                        }
                        else {
                            EUIShowInfoBar('Unable to sort because sort direction is not specified or is not a valid integer. (' + strDirection.toString() + ')');
                        }

                    }
                    else {
                        EUIShowInfoBar('Unable to sort because column name is not specified . (' + strCol.toString() + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to sort page because operations form is not found.');
                }
                e.preventDefault();
            });

            $(document).on("change", "select." + strCONSTGridCustomSortClass, function (e) {
                var strFrmID = EJUGetFormID(this);
                if (strFrmID != "") {
                    // var strCol = 
                    var strCol = $(this).val();
                    strCol = $.trim(strCol);
                    if (strCol.length > 0) {
                        EnetGrid2MultiColumnSort(strFrmID, strCol);
                    }
                    else {
                        EUIShowInfoBar('Unable to sort because column name is not specified . (' + strCol.toString() + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to sort page because operations form is not found.');
                }
                e.preventDefault();
            });

            /*Bootstrap3 sorting UI change to glyphicon*/
            $(document).on("click", "a." + strCONSTGridCustomSortClass, function (e) {
                var strFrmID = EJUGetFormID(this);
                if (strFrmID != "") {
                    // var strCol = 
                    var strCol = $(this).attr('data-sortvalue');
                    strCol = $.trim(strCol);
                    if (strCol.length > 0) {
                        EnetGrid2MultiColumnSort(strFrmID, strCol);
                    }
                    else {
                        EUIShowInfoBar('Unable to sort because column name is not specified . (' + strCol.toString() + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to sort page because operations form is not found.');
                }
                e.preventDefault();
            });



            /* Quick Search by specific field*/
            $(document).on("click", "a." + strCONSTENETGridOpFindByFieldClass, function (e) {
                var strFrmID = EJUGetParentFormID(this);
                if (strFrmID != "") {
                    var strFieldNo = EJUParseQueryString($(this).attr('href'), 'EGOFindByField');
                    var intFieldNo = parseInt(strFieldNo);
                    var strSearchMode = '';
                    if (!isNaN(intFieldNo)) {
                        if (intFieldNo > 0) {
                            strSearchMode = 'Find';
                        }
                        else {
                            strSearchMode = 'QuickSearch';
                        }
                        EnetGrid2QuickSearch(strSearchMode, intFieldNo, strFrmID);
                    }
                    else {
                        EUIShowInfoBar('Unable to find records because field ID is not specified or is not a valid integer. (' + strFieldNo.toString() + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to find records because operations form is not found.');
                }
                e.preventDefault();
            });


            //        $(document).on("click", "a." + strCONSTENETOpFilterClass, function (e) {
            //            var strFrmID = EJUGetFormID(this);
            //            strFrmID = $.trim(strFrmID);
            //            if (strFrmID != "") {
            //                var isErrorPresent = EJUIsErrorInForm(strFrmID, true);
            //                if (isErrorPresent) {
            //                    e.preventDefault();
            //                }
            //                else {
            //                    var strFormValues = $("#" + strFrmID).serializeObjectForJSON();
            //                    if (strFormValues != "") {
            //                        strFormValues = JSON.stringify(strFormValues);

            //                        if (strFormValues != "") {
            //                            EnetGrid2Filter(strFormValues, strFrmID);
            //                        }
            //                        else {
            //                            EUIShowInfoBar('Unable to filter records because form data cannot be converted to JSON.');
            //                        }

            //                    }
            //                    else {
            //                        EUIShowInfoBar('Unable to filter records because form data cannot be serialized.');
            //                    }
            //                }
            //            }
            //            else {
            //                EUIShowInfoBar('Unable to filter records because operations form is not found.');
            //            }
            //            e.preventDefault();
            //        });

            $(document).on("click", "a." + strCONSTGridFilterCloseClass, function (e) {
                $(this).parents('.EJS-Grid').find('.EJS-Grid-FilterForm').slideUp('slow');
                $(this).parents('.EJS-Grid').find('.EJS-Grid2-FilterIcon').addClass('enetFilterIconDiv-Active');
            });

            $(document).on("click", "a." + strCONSTGridFilterIconClass, function (e) {
                $(this).parents('.EJS-Grid').find('.EJS-Grid-FilterForm').slideDown('slow');
                $(this).addClass('disabled');
            });

            $(document).on("click", "button." + strCONSTENETGridDelete + ",a." + strCONSTENETGridDelete, function (e) {
                var strGridID = $(this).attr('data-EWF-GridID');
                var strDataAttributes = $(this).attr('data-ewf-grid-attributes');
                var strRowID = $(this).attr('data-ewf-grid-rowid');
                var strMsg = $(this).attr('data-ewf-grid-deletemsg');
                strMsg = $.trim(strMsg);
                if (strMsg.length <= 0) {
                    strMsg = "<br/><b>Are you sure you want to permanently delete this record?</b><br/><br/>";
                }
            



                bootbox.dialog({
                  message: strMsg,
                  title: "Delete confirmation",
                  buttons: {
                            btnNo: {
                              label: "No",
                              className: "pull-left btn-default",
                              callback: function() {
                              }
                            },
                            btnYes: {
                              label: "Yes",
                              className: "btn-danger",
                              callback: function() {
                                if (strGridID != "") {
                                    if (strDataAttributes != "") {
                                        if (strRowID != "") {
                                            var strFrmID = "frm" + strGridID + "DeleteForm";
                                            EnetGridDeleteRecord(strFrmID, strDataAttributes, strRowID);
                                        }
                                        else {
                                            EUIShowInfoBar('Unable to delete to page because EnetGrid form is not found for this grid. (' + strFrmID + ')');
                                        }
                                    }
                                    else {
                                        EUIShowInfoBar('Unable to delete to page because EnetGrid form is not found for this grid. (' + strFrmID + ')');
                                    }

                                }
                                else {
                                    EUIShowInfoBar('Unable to delete to page because EnetGrid id is not found for this grid. ');
                                }  
                              }
                            }
                        }
                    });

                e.preventDefault();
            });

            //====================
            // CheckBox
            //====================
            $("input[type='checkbox']").click(function () {
                //TODO:  Make sure this function is multi-grid enabled (ie. add '#' + inOpFormName + '_ before field names)
                //TODO:  we need to catch the click even on a specific class, currently, any check box click fires this event
                //          and that causes a problem because not every form will have elements with ID ofenetGridMSNew 
                //          and enetGridMSRemoved elements.
                //          To bypass thi bug, we added if (EJUISIDUnique('#enetGridMSNew', null) == true) { clause for 
                //          enetGridMSNew and enetGridMSRemoved.  
                //          Once the selector is rewritten for a specific class, we can remove the if conditions.
                if ($(this).parents('form:first').attr('id') != undefined) {
                    var currFormName = $(this).parents('form:first').attr('id').replace('GridForm', 'OpForm');
                    if (currFormName != null && currFormName != "") {
                        if ($(this).attr('checked')) {
                            if (EJUISIDUnique('#enetGridMSNew', null) == true) {
                                var currChkValue = "";
                                currChkValue = $(this).val();
                                addRemoveSelected(currFormName, '#enetGridMSNew', currChkValue, "Added");
                            }
                        }
                        else {
                            if (EJUISIDUnique('#enetGridMSNew', null) == true) {
                                var currChkValue = "";
                                currChkValue = $(this).val();
                                addRemoveSelected(currFormName, '#enetGridMSRemoved', currChkValue, "Removed");
                            }
                        }

                    }
                    else {
                        //TODO: Have to decide do we uncheck the checkbox or what do we do
                    }
                }
            });

            $(document).on("click", "a.EJS-Grid2-Export", function (e) {
                var strFrmID = EJUGetFormID(this);
                strFrmID = $.trim(strFrmID);
                if (strFrmID != "") {
                    EnetGridExport(strFrmID);

                }
                else {
                    EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
                }

                e.preventDefault();
            });
        }
        else if (options == 'EnetGridUpdateRow' && params != null) {
            var strFrmID = $(this).attr("id");
            EnetGridUpdateRow(strFrmID, params, 1);

        }
        else if (options == 'EnetGridDeleteRow' && params != null) {

            var strFrmID = $(this).attr("id");
            EnetGridUpdateRow(strFrmID, params, 0);
        }
        else {
            alert('invalid option in plugin: ewfGridPlugin');
        }


        //Private function for the Plugin

        //====================
        // Paging
        //====================
        /*Paging Function Handler */
        function EnetGrid2JumpToPage(inPageToJumpTo, inOpFormName) {
            inOpFormName = $.trim(inOpFormName);
            var form = $('#' + inOpFormName);
            if (form.length > 0) {
                if (form.find('#' + inOpFormName + '_enetGridPage').length > 0) {
                    form.find('#' + inOpFormName + '_enetGridPage').val(inPageToJumpTo)
                    if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                        form.find('#' + inOpFormName + '_enetGridOperation').val('Page');
                        form.attr('data-EWF-noClientSubmitValidation', 'true');
                        form.submit();
                        form.attr('data-EWF-noClientSubmitValidation', 'false');
                    }
                    else {
                        EUIShowInfoBar('Unable to jump to page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to jump to page because EnetGridPage field is not found for this grid. (' + inOpFormName + '_enetGridPage' + ')');
                }
            }
            else {
                EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
            }
        }

        //====================
        // PredefinedFilter
        //====================
        /*PredefinedFilter Function Handler */
        function EnetGrid2PredefinedFilter(inSelectedFilter, inOpFormName) {
            inOpFormName = $.trim(inOpFormName);
            var form = $('#' + inOpFormName);
            if (form.length > 0) {
                if (form.find('#' + inOpFormName + '_enetGridPredefinedFilter').length > 0) {
                    form.find('#' + inOpFormName + '_enetGridPredefinedFilter').val(inSelectedFilter)
                    if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                        form.find('#' + inOpFormName + '_enetGridOperation').val('PredefinedFilter');
                        form.attr('data-EWF-noClientSubmitValidation', 'true');
                        form.submit();
                        form.attr('data-EWF-noClientSubmitValidation', 'false');

                    }
                    else {
                        EUIShowInfoBar('Unable to jump to page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to jump to page because EnetGridPage field is not found for this grid. (' + inOpFormName + '_enetGridPage' + ')');
                }
            }
            else {
                EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
            }
        }






        //============================
        // Grid Operation QuickSearch
        //============================
        function EnetGrid2QuickSearch(inMode, inFieldName, inOpFormName) {
            if (EJUISIDUnique('#' + inOpFormName, null)) {
                var form = $('#' + inOpFormName);
                var doSubmit = false;
                if (EJUISIDUnique('#' + inOpFormName + '_txtSearch', form)) {
                    if (form.find('#' + inOpFormName + '_txtSearch').val() != "") {

                        //SET Search Value
                        if (EJUISIDUnique('#' + inOpFormName + '_enetGridSearchValue', form)) {
                            form.find('#' + inOpFormName + '_enetGridSearchValue').val(form.find('#' + inOpFormName + '_txtSearch').val());
                        }
                        else {
                            EUIShowInfoBar('Unable to search because EnetGridSearchValue field is not found for this grid. (' + inOpFormName + '_enetGridSearchValue' + ')');
                        }


                        //SET Grid Operation
                        if (EJUISIDUnique('#' + inOpFormName + '_enetGridOperation', form)) {
                            form.find('#' + inOpFormName + '_enetGridOperation').val(inMode);
                        }
                        else {
                            EUIShowInfoBar('Unable to search because EnetGridSearchValue field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
                        }

                        //SET Grid Search Field (depending on mode)
                        if (inMode == 'Find') {
                            if (EJUISIDUnique('#' + inOpFormName + '_enetGridSearchField', form)) {
                                form.find('#' + inOpFormName + '_enetGridSearchField').val(inFieldName);
                                doSubmit = true;
                            }
                            else {//If
                                doSubmit = false;
                                EUIShowInfoBar('Unable to search because EnetGridSearchField field is not found for this grid. (' + inOpFormName + '_enetGridSearchField' + ')');
                            }
                        }
                        else {
                            form.find('#' + inOpFormName + '_enetGridSearchField').val("");
                            doSubmit = true;
                        }
                        if (doSubmit) {
                            form.submit();
                        }
                        else {
                            //DO nothing.  Message was shown above.
                        }
                    }
                    else {
                        EUIShowInfoBar('Please enter the search value');
                        form.find('#' + inOpFormName + '_txtSearch').focus();
                    }
                }
                else {
                    EUIShowInfoBar('Unable to search because txtSearch field is not found for this grid. (' + inOpFormName + '_txtSearch' + ')');
                }
            }
            else {
                EUIShowInfoBar('Unable to search because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
            }
        }

        //====================
        // Filter
        //====================
        /*Filter Function Handler */
        function EnetGrid2Filter(inFilterCriteria, inOpFormName) {
            var form = $('#' + inOpFormName);

            if (form.length > 0) {
                if (form.find('#' + inOpFormName + '_enetGridFilter').length > 0) {
                    form.find('#' + inOpFormName + '_enetGridFilter').val(inFilterCriteria)
                    if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                        form.find('#' + inOpFormName + '_enetGridOperation').val('Filter');
                        form.attr('data-EWF-noClientSubmitValidation', 'false');
                        form.submit();
                        form.attr('data-EWF-noClientSubmitValidation', 'true');

                    }
                    else {
                        EUIShowInfoBar('Unable to filter to page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to filter to page because EnetGridFilter field is not found for this grid. (' + inOpFormName + '_enetGridFilter' + ')');
                }
            }
            else {
                EUIShowInfoBar('Unable to filter because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
            }
        }


        //==============================
        // Add or Remove Checked Items
        //==============================
        function addRemoveSelected(inFormName, inFieldName, inCurrValue, inMode) {
            //TODO:  Make sure this function is multi-grid enabled (ie. add '#' + inOpFormName + '_ before field names)
            var currHdnValue = "";
            var newhdnValue = "";
            currHdnValue = $('#' + inFormName).find(inFieldName).val();
            if (currHdnValue.indexOf("," + inCurrValue + ",") < 0) {//Check if that value  was already added to it
                if (currHdnValue != "") {
                    newhdnValue = currHdnValue + inCurrValue + ",";
                }
                else {
                    newhdnValue = "," + inCurrValue + ",";
                }
                $('#' + inFormName).find(inFieldName).val(newhdnValue);

            }
        }

        //=======================================
        // Export
        //=======================================
        function EnetGridExport(inOpFormName) {
            var form = $('#' + inOpFormName);
            if (form.length > 0) {
                if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                    form.find('#' + inOpFormName + '_enetGridOperation').val('Export');
                    window.open('', 'formpopup', 'width=400,height=400,resizeable,scrollbars');
                    form.attr("target", "formpopup");
                    form.attr('data-EWF-noClientSubmitValidation', 'true');
                    form.submit();
                    form.attr('data-EWF-noClientSubmitValidation', 'false');
                    form.removeAttr('target');
                } else {
                    EUIShowInfoBar('Unable to jump to page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
                }
            }
            else {
                EUIShowInfoBar('Unable to export to excel because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
            }
        }

        //====================
        // Multi Sorting
        //====================
        /* Sorting Link Binding */

        function EnetGrid2MultiColumnSort(inOpFormName, inFieldName) {
            inOpFormName = $.trim(inOpFormName);
            var form = $('#' + inOpFormName);
            if (form.length > 0) {
                if (form.find('#' + inOpFormName + '_enetGridSort').length > 0) {
                    form.find('#' + inOpFormName + '_enetGridSort').val(inFieldName);
                    if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                        form.find('#' + inOpFormName + '_enetGridOperation').val('Sort');
                        form.attr('data-EWF-noClientSubmitValidation', 'true');
                        form.submit();
                        form.attr('data-EWF-noClientSubmitValidation', 'false');
                    }
                    else {
                        EUIShowInfoBar('Unable to jump to page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to sort because EnetGridSort field is not found for this grid. (' + inOpFormName + '_enetGridSort' + ')');
                }
            }
            else {
                EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
            }

        }


        //=======================================
        // Refresh
        //=======================================
        function EnetGridRefresh(inOpFormName) {
            inOpFormName = $.trim(inOpFormName);
            var form = $('#' + inOpFormName);
            if (form.length > 0) {
                if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                    form.find('#' + inOpFormName + '_enetGridOperation').val('Refresh');
                    form.attr('data-EWF-noClientSubmitValidation', 'true');
                    form.submit();
                    form.attr('data-EWF-noClientSubmitValidation', 'false');

                }
                else {
                    EUIShowInfoBar('Unable to Refresh the page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
                }

            }
            else {
                EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
            }
        }
        //=======================================
        // Printing
        //=======================================
        function print(inOpFormName) {
            var form = $('#' + inOpFormName);
            form.find('#' + inOpFormName + '_enetGridOperation').val('Print')
            form.attr("target", "_blank");
            form.attr("action", document.location + "?PrintMode=1");
            form.submit();
        }

        //=========================================
        // Reset Print
        //=========================================
        function resetPrint(inOpFormName) {
            var form = $('#' + inOpFormName);
            form.attr("target", "_self");
            document.location = document.location.replace("?PrintMode=1", "");
            form.attr("action", document.location);
        }

        function EnetGridDeleteRecord(inOpFormName, strDataAttributes, strRowID) {
            inOpFormName = $.trim(inOpFormName);
            var form = $('#' + inOpFormName);
            if (form.length > 0) {
                if (form.find('#' + inOpFormName + '_enetDeleteRecordID').length > 0) {
                    form.find('#' + inOpFormName + '_enetDeleteRecordID').val(strDataAttributes);
                    if (form.find('#' + inOpFormName + '_enetGridDeleteRowID').length > 0) {
                        form.find('#' + inOpFormName + '_enetGridDeleteRowID').val(strRowID);
                        form.submit();
                    }
                    else {
                        EUIShowInfoBar('Unable to jump to page because _enetGridDeleteAttributes form is not found for this grid. (' + inOpFormName + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to jump to page because _enetGridDeleteAttributes form is not found for this grid. (' + inOpFormName + ')');
                }
            }
            else {
                EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
            }
        }


        //====================
        // RowUpdates
        //====================
        /*RowUpdates Function Handler */
        function EnetGridUpdateRow(inOpFormName, inValue, inMode) {
            inOpFormName = $.trim(inOpFormName);
            var form = $('#' + inOpFormName);
            var strRowID = "";
            var strRefreshRowID ="";
                if ($.isArray(inValue)) {
                var arrList = inValue;
                strRowID = arrList[0];
                strRefreshRowID = arrList[1];

            }
            else {
                strRowID = inValue;
            }
            if (form.length > 0) {
                if (form.find('#' + inOpFormName + '_enetGridUpdate').length > 0) {
                if(strRefreshRowID != "" && strRefreshRowID != undefined)
                {
                   inValue = strRefreshRowID; 
                }
                else 
                {
                    inValue= strRowID
                }
                    form.find('#' + inOpFormName + '_enetGridUpdate').val(inValue);
                    if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                        if (inMode == 0) {
                            form.find('#' + inOpFormName + '_enetGridOperation').val('DeleteRow');
                        }
                        else {
                            form.find('#' + inOpFormName + '_enetGridOperation').val('UpdateRow');
                        }
                        form.attr('data-EWF-noClientSubmitValidation', 'true');
                        form.submit();
                        form.attr('data-EWF-noClientSubmitValidation', 'false');
                    }
                    else {
                        EUIShowInfoBar('Unable to jump to page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to sort because EnetGridUpdateRow field is not found for this grid. (' + inOpFormName + '_enetGridUpdate' + ')');
                }

            }
            else {
                EUIShowInfoBar('Unable to updaterow  because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
            }

        }


        $(document).on('click', '.EWF-EmptyGrid-Addnew', function (e) {
            e.preventDefault();
            var currOpFormName = '';
            
            if($(this).attr("form") != undefined && $(this).attr("form") != '')
            {
                currOpFormName = $(this).attr("form");

                if(currOpFormName.trim().length > 0)
                {
                      if($("[form='"+currOpFormName+"'][ID='EWF-Grid-AddNew']").length)
                      {

                           $("[form='"+currOpFormName+"'][ID='EWF-Grid-AddNew']")[0].click();// .trigger("click");
                      }
                      else {
                            EUIShowInfoBar('Unable to find the add button for this grid.(' + inOpFormName + ')');    
                      }
                }
                else {
                    EUIShowInfoBar('Unable to find the form attribute of the add new button');
                }
            }
            else {
                EUIShowInfoBar('Unable to find the form attribute of the add new button');
            }
        });

        ///&isautoloadaddnew=true
        //Read Query string to find out if the form has auto add enabled
        // and the query string has auto add enabled true
//        var IsAutoLoadAddNew = "";
//        IsAutoLoadAddNew =   getQueryStringParameterByName("isautoloadaddnew")
//        if(IsAutoLoadAddNew == "true")
//        {
//            if($('div.' + strCONSTewfGridClass + ' #EWF-Grid-AddNew').attr("data-ewf-autoloadaddnew") == "1")
//            {
//                $('div.' + strCONSTewfGridClass + ' #EWF-Grid-AddNew').click();
//            }
//            else {
//                // Do not trigger the click event to load the modal
//            }
//        }
//        else {
//            // Do not trigger the click event to load the modal  
//        }


    };





    $('div.' + strCONSTewfGridClass).ewfGridPlugin();

})(jQuery);

$(document).ready(function () {
        var IsAutoLoadAddNew = "";
        IsAutoLoadAddNew =   getQueryStringParameterByName("isautoloadaddnew")
        if(IsAutoLoadAddNew == "true")
        {
            if($('div.EnetGrid2 #EWF-Grid-AddNew').attr("data-ewf-autoloadaddnew") == "1")
            {
                $('div.EnetGrid2 #EWF-Grid-AddNew').click();
            }
            else {
                // Do not trigger the click event to load the modal
            }
        }
        else {
            // Do not trigger the click event to load the modal  
        }
 });

function getQueryStringParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

//===========================
// Function to center 
//===========================
$.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
    this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
    return this;
}


//=========================================
// Frontend updated for AJAXPlugin
//=========================================

function replaceGridSectionHTML(responseData, trigger) {
    var formName = $(trigger).attr('id');
    var tabname = $(trigger).attr('id') + '_enetTable';
    var divName = formName.replace('frm', 'div').replace('OpForm', 'Grid');
    var strDivBind = '#' + formName.replace('frm', 'div').replace('OpForm', 'Grid');
    var root = responseData;


    if ($(root).find("enetgridsection[type='Print']").length > 0) {

        var w = 800;
        var h = 650;
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;


        var popupParams = "directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,width=" + w + ",height=" + h + ",scrollbars=yes, top=" + top + ", left=" + left;
        var winPrint;
        console.log(applicationSettings.printURL);
        winPrint = window.open(applicationSettings.printURL, "Print", popupParams);

        var printContent = $(root).find("enetgridsection[type='Print']").children();
        var sParsedRowData = $('<div>').html(printContent).html();

        setTimeout(function () {

            var strPrintMessage = " Print window is open. To continue, please close it. <br/><br/>"
            strPrintMessage = strPrintMessage + "<a href='#' class='btn btn-primary' id='btnEnetPrintPopClose'>Close print</a>&nbsp; <a href='#' class='btn btn-primary' id='btnEnetPrintPopShow'>Show print</a>"
            $('body').block({
                message: strPrintMessage,
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            }).center();




            $(winPrint.document).contents().find('#contentToPrint').html(sParsedRowData);
            winPrint.onunload = function () {
                $('body').unblock();
            };

        }, 300);


        $(document).on('click', '#btnEnetPrintPopClose', function (e) {
            winPrint.close();
        });


        $(document).on('click', '#btnEnetPrintPopShow', function (e) {
            winPrint.focus();
        });

        return
    }





    if ($(root).find("#" + divName).length == 0) {
        divName = $(root).first("div").children().attr("id");
    }



    if ($(root).find("#" + divName).length > 0) {
        if ($(document).find("#" + divName).length > 0) {
            var sparsed = $(root).find("#" + divName);
            var sParsedRowData = $('<div>').html(sparsed).html();
            $("#" + divName).html(sParsedRowData);
        }
        else {
            var sparsed = $(root).find("#" + divName);
            //var sParsedRowData = $('<div>').append(sparsed.clone()).remove().html();
            var sParsedRowData = $('<div>').html(sparsed).html();
            $("#" + formName).after(sParsedRowData);
            $("#" + formName).remove();


        }

        if ($(document).find(strDivBind).length > 0) {
            EJUBindAllUIElements(strDivBind);
        }
        else {
            //do not bind the elements if the div is not present
        }
    }

    if ($(root).find("enetgridsection[type='GridHeader']").length > 0) {
        var gridHeader = $(root).find("enetgridsection[type='GridHeader']").children();
        var sParsedRowData = $('<div>').html(gridHeader).html();
        if ($("#" + tabname + " thead[class='gridheader']").html() != null) {
            $("#" + tabname + " thead[class='gridheader']").html(sParsedRowData);
        }
        else if ($("#" + tabname + "_gridHeader").html() != null) {
            $("#" + tabname + "_gridHeader").html(sParsedRowData);
        }
    }

    if ($(root).find("enetgridsection[type='RowData']").length > 0) {
        var rowData = $(root).find("enetgridsection[type='RowData']").children();
        var sParsedRowData = $('<div>').html(rowData).html();
        $('#' + tabname + ' tbody').html(sParsedRowData);

        if ($(document).find(strDivBind).length > 0) {
            
            EJUBindAllUIElements(strDivBind);
        }
        else {
            //do not bind the elements if the div is not present
        }
    }

    if ($(root).find("enetgridsection[type='RowUpdate']").length > 0) {
        var rowID = 0;
        var rowData;
        if ($(root).find("enetgriddata[name='rowid']").length > 0) {
            rowID = $(root).find("enetgriddata[name='rowid']").text();
        }
        if ($(root).find("enetgridsection[type='RowUpdate']").children()[1] != undefined) {
            rowData = $(root).find("enetgridsection[type='RowUpdate']").children()[1];
            var sParsedRowData = $('<div>').html(rowData).html();
            if ($("#" + tabname + " tbody" + " tr[data-EJS-rowid='" + rowID + "']").length > 0) {
                $("#" + tabname + " tbody" + " tr[data-EJS-rowid='" + rowID + "']").replaceWith(sParsedRowData);
                $("#" + tabname + " tbody" + " tr[data-EJS-rowid='" + rowID + "']").css("background", "#F3C2C2");



//                if (rowData.innerHTML.indexOf("data-ofmodal") > -1)
//                {
//                    OFModal.rebind(true);
//                }
//                else {
//                    // if the row does not have modal then do not enable modal
//                }
            }
            else {
                $("#" + tabname + " tbody").prepend(sParsedRowData);
                $("#" + tabname + " tbody" + " tr[data-EJS-rowid='" + rowID + "']").fadeIn(3000);
                $("#" + tabname + " tbody" + " tr[data-EJS-rowid='" + rowID + "']").css("background", "#F3C2C2");

                hideEmptyGridTemplate(formName)
            }

            if ($(document).find(strDivBind).length > 0) {
                EJUBindAllUIElements(strDivBind);

            }
            else {
                //do not bind the elements if the div is not present
            }
        }

    }

    if ($(root).find("enetgridsection[type='RowDelete']").length > 0) {
        var rowID = 0;
        var rowData;
        if ($(root).find("enetgriddata[name='rowid']").length > 0) {
            rowID = $(root).find("enetgriddata[name='rowid']").text();
        }
        if ($("#" + tabname + " tbody" + " tr[data-EJS-rowid='" + rowID + "']").length > 0) {
            $("#" + tabname + " tbody" + " tr[data-EJS-rowid='" + rowID + "']").fadeOut("slow");

        }
    }


    if ($(root).find("enetgridsection[type='RowHeader']").length > 0) {
        var rowHeader = $(root).find("enetgridsection[type='RowHeader']").children();
        var sParsedRowData = $('<div>').html(rowHeader).html();
        $("#" + tabname + " thead[class='rowheader']").html(sParsedRowData);

        if ($(document).find(strDivBind).length > 0) {
            EJUBindAllUIElements(strDivBind);
        }
        else {
            //do not bind the elements if the div is not present
        }
    }

    if ($(root).find("enetgridsection[type='PredefinedFilters']").length > 0) {
        var rowHeader = $(root).find("enetgridsection[type='PredefinedFilters']").children();
        var sParsedRowData = $('<div>').html(rowHeader).html();
        if ($("#" + tabname + " thead[class='predefinedheader']").html() != null) {
            $("#" + tabname + " thead[class='predefinedheader']").html(sParsedRowData);
        }
        else if ($("#" + tabname + "_predefinedFilter").html() != null) {
            $("#" + tabname + "_predefinedFilter").html(sParsedRowData);
        }
    }

    if ($(root).find("enetgridsection[type='GridFooter']").length > 0) {
        var rowFooter = $(root).find("enetgridsection[type='GridFooter']").children();
        var sParsedRowData = $('<div>').html(rowFooter).html();
        var footerID = "#" + tabname + "_footer"
        $(footerID).html(sParsedRowData);
    }


    if ($(root).find("enetgridsection[type='CustomFilters']").length > 0) {
        var rowHeader = $(root).find("enetgridsection[type='CustomFilters']").children();
        var sParsedRowData = $('<div>').html(rowHeader).html();
        var divid = $(trigger).attr('id') + '_enetFilterOperation';
        var grid = $(sParsedRowData).find('#' + divid);
        var strContainer = '#' + divid;
        var strContents = grid.html();
        EWFUpdatePageContent(strContainer, strContents);
        //$("#" + divid).html(grid.html());

    }

    if ($(root).find("enetgridsection[type='ViewTotals']").length > 0) {
        var rowHeader = $(root).find("enetgridsection[type='ViewTotals']").children();
        var sParsedRowData = $('<div>').html(rowHeader).html();
        $("#" + tabname + " tfoot").html(sParsedRowData);
    }

    if ($(root).find("enetgridsection[type='DataTotals']").length > 0) {
        var rowHeader = $(root).find("enetgridsection[type='DataTotals']").children();
        var sParsedRowData = $('<div>').html(rowHeader).html();
        $("#" + tabname + " thead[class='datatotals']").html(sParsedRowData);
    }

    if ($(root).find("enetgridsection[type='SortRow']").length > 0) {
        var rowHeader = $(root).find("enetgridsection[type='SortRow']").children();
        var sParsedRowData = $('<div>').html(rowHeader).html();
        $("#" + tabname + " thead[class='sortrow']").html(sParsedRowData);
    }

}

//function showEmptyGrid(inFormName) {
//    var emptySectionName = '.ewf-emptygrid-panel-' + inFormName;
//    var gridSectionName = '.ewf-grid-panel-' + inFormName;

//    if ($(emptySectionName).length) {
//        if ($(gridSectionName).length) {
//            //validate whether all the rows are deleted

//            CSPBS3Show($(gridSectionName));
//            $(emptySectionName).hide();
//        }
//        else {
//            // There is no grid available for the form to show so leave the empty template
//        }
//    }
//    else {
//        // There is no empty template present for this grid so we do not have to switch
//    }
//}


function hideEmptyGridTemplate(inFormName) {
    var emptySectionName = '.ewf-emptygrid-panel-' + inFormName;
    var gridSectionName = '.ewf-grid-panel-' + inFormName;
    if ($(emptySectionName).length) {
        if ($(gridSectionName).length) {
            CSPBS3Show($(gridSectionName));
            $(emptySectionName).hide();
        }
        else {
            // There is no grid available for the form to show so leave the empty template
        }
    }
    else {
      // There is no empty template present for this grid so we do not have to switch
    }


}

function showGridError(responseMsg, trigger, responseData) {
    var formName = $(trigger).attr('id');
    var divName = formName.replace('frm', 'div').replace('OpForm', 'Grid');
    responseData = $.trim(responseData);
    if (responseData.length > 0) {
        if ($(document).find("#" + divName + " div.EWF_Access_Denied").length > 0) {
            $("#" + divName).html(responseData);
        }
        else {
            $("#" + divName).html(responseData);
        }
        displayEWFSecurityModal(divName);
    }
    EUIShowErrorInfoBar(responseMsg, divName);

}
var EUIInfoBarLastTimeoutTimer;
var EUIInfoBarIsTimeoutSet = false;
function EUIShowErrorInfoBar(inMessage, inGridDivName) {
    var strCloseButton = "<a class=\"EnetInfoBarCloseLink\" href=\"javascript:EUICloseErrorInfoBar('" + inGridDivName + "');\"><span class=\"EnetInfoBarCloseButton\">&#160;</span></a>";
    var strHTML = "<div class=\"EnetInfoBar\"></div>";
    if ($("#" + inGridDivName).length > 0) {
        if ($("#" + inGridDivName + " .EnetInfoBar").length <= 0) {
            $("#" + inGridDivName).prepend(strHTML);
        }
        $("#" + inGridDivName + " div.EnetInfoBar").prepend(strCloseButton);
        $("#" + inGridDivName + " div.EnetInfoBar").prepend(inMessage + "<br />");
        $("#" + inGridDivName + " div.EnetInfoBar").slideDown(1000);
    }
    else {
        if ($(".EnetInfoBar").length <= 0) {
            $(document.body).prepend(strHTML);
        }
        $("div.EnetInfoBar").prepend(strCloseButton);
        $("div.EnetInfoBar").prepend(inMessage + "<br />");
        $("div.EnetInfoBar").slideDown(1000);
    }

    if (EUIInfoBarIsTimeoutSet) {
        EUIInfoBarIsTimeoutSet = false;
        window.clearTimeout(EUIInfoBarLastTimeoutTimer);
    }
    EUIInfoBarIsTimeoutSet = true;
    EUIInfoBarLastTimeoutTimer = window.setTimeout(function () {
        EUIInfoBarIsTimeoutSet = true;
        EUICloseErrorInfoBar(inGridDivName);
    }, 8000);
}
function EUICloseErrorInfoBar(inGridDivName) {
    if (EUIInfoBarIsTimeoutSet) {
        EUIInfoBarIsTimeoutSet = false;
        window.clearTimeout(EUIInfoBarLastTimeoutTimer);
    }
    if ($("#" + inGridDivName).length > 0) {
        $("#" + inGridDivName + " .EnetInfoBar").html("");
        $("#" + inGridDivName + " .EnetInfoBar").slideUp(500);

    } else {
        $(".EnetInfoBar").html("");
        $(".EnetInfoBar").slideUp(500);

    }

}

function RefreshGridRow(inRefreshGridID, inRecordID, inMode, inRefreshRowID) {
    var strGridID = "";
    inRefreshGridID = inRefreshGridID.replace('#', '');
    strGridID = $.trim(inRefreshGridID);
    var arrParams = [inRecordID,inRefreshRowID];
    
    if (strGridID != "") {
        var strFormID = "frm" + strGridID + "OpForm";
        if (inMode == 1) {
            $('#' + strFormID).ewfGridPlugin("EnetGridUpdateRow", arrParams);
        } else {
            $('#' + strFormID).ewfGridPlugin("EnetGridDeleteRow", inRecordID);

        }
    }
}

//====================
// Sorting
//====================
/* Sorting Link Binding */

function EnetGrid2SingleColumnSort(inOpFormName, inFieldName, inDirection) {
    inOpFormName = $.trim(inOpFormName);
    var form = $('#' + inOpFormName);
    if (form.length > 0) {
        if (form.find('#' + inOpFormName + '_enetGridSort').length > 0) {
            form.find('#' + inOpFormName + '_enetGridSort').val(inFieldName + ',' + inDirection);
            if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
                form.find('#' + inOpFormName + '_enetGridOperation').val('Sort');
                form.attr('data-EWF-noClientSubmitValidation', 'true');
                form.submit();
                form.attr('data-EWF-noClientSubmitValidation', 'false');
            }
            else {
                EUIShowInfoBar('Unable to jump to page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
            }
        }
        else {
            EUIShowInfoBar('Unable to sort because EnetGridSort field is not found for this grid. (' + inOpFormName + '_enetGridSort' + ')');
        }
    }
    else {
        EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
    }

}



//====================
// Refresh
//====================
/*Refresh Function Handler */
function EnetGrid2Refresh(inOpFormName) {
    inOpFormName = $.trim(inOpFormName);
    var form = $('#' + inOpFormName);
    if (form.length > 0) {
        if (form.find('#' + inOpFormName + '_enetGridOperation').length > 0) {
            form.find('#' + inOpFormName + '_enetGridOperation').val('Refresh');
            form.attr('data-EWF-noClientSubmitValidation', 'true');
            form.submit();
            form.attr('data-EWF-noClientSubmitValidation', 'false');

        }
        else {
            EUIShowInfoBar('Unable to Refresh the page because EnetGridOperation field is not found for this grid. (' + inOpFormName + '_enetGridOperation' + ')');
        }

    }
    else {
        EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
    }
}

//====================
// GridForm Refresh standard function
//====================
function refreshGridFormHTML(responseData, trigger) {
    var formName = $(trigger).attr('id');
    var divName = $(trigger).attr('data-ewf-form-replacement-divid');
    var parentForm = $(trigger).attr("data-ewf-form-parentgrid-form");
    var root = responseData;


    if ($(root).find("status").length > 0) {
        var status = $(root).find("status").text();
        if (status > 0) {
            if ($(responseData).find("ispopup").length > 0) {
                if ($(responseData).find("data").length > 0) {
                    var rowData = $(responseData).find("data").children();
                    var sParsedRowData = $('<div>').html(rowData).html();
                    // $('#' + divName).html(sParsedRowData);
                    $("#" + divName).after(sParsedRowData);
                    $("#" + divName).remove();
                    EJUBindAllUIElements(divName);
                }
                else {
                    window.close();
                    self.opener.parent.location.reload(true);
                }
            }
            else {
                if ($(responseData).find("redirecturl").length > 0) {
                    var strRedirect = $(responseData).find("redirecturl").text();
                    if (strRedirect == "") {

                        EJUBindAllUIElements(divName);
                        EnetGrid2Refresh(parentForm);
                    }
                    else {
                        window.location.href = strRedirect;
                    }
                }
                else {
                    EnetGrid2Refresh(parentForm);
                    EJUBindAllUIElements(divName);
                }
            }

        } else {
            var strMessage = $(responseData).find("message").text();
            EUIShowErrorInfoBar(strMessage, divName);
                           
                      
        }
    }
    else {

        if (parentForm != "") {
            if ($(document).find("#" + parentForm).length > 0) {

                EJUBindAllUIElements(divName);
                EnetGrid2Refresh(parentForm);
            }
            else {
                EUIShowErrorInfoBar("Cannot find parent form " + parentForm + "in document to replace response", divName)
            }
        }
        else {
            EUIShowErrorInfoBar("Cannot find parent form name ")
        }
    }


}

(function ($) {

    //CONSTANTS

    var strCONSTTabRefreshClass = 'EJS-Tab-OpRefresh';
    var strCONSTTabViewClass = 'EJS_Tab_OpView';
    var strCONSTewfGridClass = 'EJS-Tab-Form';

    $.fn.ewfTabPlugin = function (options) {

        //Instantiation & event binding 
        if (options == null) {
            $(document).on("click", "a." + strCONSTTabRefreshClass, function (e) {
                var strDivID = $(this).attr("ejs_tab_divid")
                if (strDivID != "") {
                    var selectedView = $("#" + strDivID + " li[class='active'] a");
                    var strFrmID = $(selectedView).attr("form");
                    var strHrefID = $(selectedView).attr('href');
                    strFrmID = $.trim(strFrmID);
                    if (selectedView != "") {
                        if (strFrmID != "") {
                            if (strHrefID != "") {
                                EnetTabShowView(strHrefID, 0, strFrmID);
                            }
                            else {
                                EUIShowInfoBar('Unable to Refresh tab because EnetTab Href is not found for this Tab view. (' + selectedView + ')');
                            }

                        }
                        else {
                            EUIShowInfoBar('Unable to Refresh tab because EnetTab form is not found for this Tab view. (' + selectedView + ')');
                        }
                    }

                    else {
                        EUIShowInfoBar('Unable to Refresh tab because Active Tab view is not found for this tab div. (' + strDivID + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to Refresh tab because EnetTab div is not found for this Tab. (' + strDivID + ')');
                }

                e.preventDefault();
            });

            $(document).on("click", "a." + strCONSTTabViewClass, function (e) {
                var strFrmID = EJUGetFormID(this);
                strFrmID = $.trim(strFrmID);
                if (strFrmID != "") {
                    var strViewID = $(this).attr('viewid');
                    var strHrefID = $(this).attr('href');
                    var strDataLoaded = $(this).attr('EJS_Tab_DataLoaded');
                    strViewID = $.trim(strViewID);
                    strHrefID = $.trim(strHrefID);
                    strDataLoaded = $.trim(strDataLoaded);
                    if (strHrefID != "") {
                        EnetTabShowView(strHrefID, strDataLoaded, strFrmID);

                    }
                    else {
                        EUIShowInfoBar('Unable to view to Tab because EnetTab Href  is not found for this tab view. (' + strViewID + ')');
                    }
                }
                else {
                    EUIShowInfoBar('Unable to view to Tab because EnetTab form is not found for this Tab. (' + strFrmID + ')');
                }

                e.preventDefault();
            });


            LoadTab();
        }
        else {
            alert('invalid option in plugin: ewfGridPlugin');
        }

        //Private function for the Plugin

        //==============================
        // Tab Refresh
        //==============================
        function EnetTabRefresh(inOpFormName) {
            inOpFormName = $.trim(inOpFormName);
            var form = $('#' + inOpFormName);
            if (form.length > 0) {
                if (form.find('#' + inOpFormName + '_enetTabOperation').length > 0) {
                    form.find('#' + inOpFormName + '_enetTabOperation').val('Refresh');
                    form.submit();

                }
                else {
                    EUIShowInfoBar('Unable to Refresh the page because EnetTabOperation field is not found for this Tab. (' + inOpFormName + '_enetTabOperation' + ')');
                }

            }
            else {
                EUIShowInfoBar('Unable to Refresh  page because EnetTab form is not found for this Tab. (' + inOpFormName + ')');
            }
        }

        //==============================
        // Tab View
        //==============================
        function EnetTabShowView(inSelectedHrefID, inDataLoaded, inOpFormName) {
            inOpFormName = $.trim(inOpFormName);
            var form = $('#' + inOpFormName);
            if (form.length > 0) {
                if (inDataLoaded == 0) {
                    form.submit();
                }
                else {
                    $(inSelectedHrefID).tab("show");
                }

            }
            else {
                EUIShowInfoBar('Unable to jump to page because EnetGrid form is not found for this grid. (' + inOpFormName + ')');
            }
        }


        //==============================
        // Initial Tab Load
        //==============================
        function LoadTab() {

            $(".EJS-Tab-Form").each(function () {

                var selectedView = $("li[class='active'] a");
                var strFrmID = $(selectedView).attr("form");
                var strHrefID = $(selectedView).attr('href');
                strFrmID = $.trim(strFrmID);
                strHrefID = $.trim(strHrefID);
                selectedView = $.trim(selectedView);
                if (selectedView != "") {
                    if (strFrmID != "") {
                        if (strHrefID != "") {
                            EnetTabShowView(strHrefID, 0, strFrmID);
                        }
                        else {
                            EUIShowInfoBar('Unable to Refresh tab because EnetTab Href is not found for this Tab view. (' + selectedView + ')');
                        }

                    }
                    else {
                        EUIShowInfoBar('Unable to Refresh tab because EnetTab form is not found for this Tab view. (' + selectedView + ')');
                    }
                }

                else {
                    EUIShowInfoBar('Unable to load tab because Active Tab view is not found ');
                }
            });
        }

    };
    $('div.' + strCONSTewfGridClass).ewfTabPlugin();

})(jQuery);


//=========================================
// Frontend updated for AJAXPlugin
//=========================================

function replaceTabSectionHTML(responseData, trigger) {
    var formName = $(trigger).attr('id');
    var divName = formName.replace('frm', 'div').replace('OpForm', 'Tab');
    var root = responseData;

    if ($(root).find("#" + divName).length == 0) {
        divName = $(root).first("div").children().attr("id");
    }


    if ($(root).find("#" + divName).length > 0) {

        if ($(document).find("#" + divName).length > 0) {
            var sparsed = $(root).find("#" + divName);
            var sParsedRowData = $('<div>').html(sparsed).html();
            var strContainer = '#' + divName;
            $("#" + divName).replaceWith(sParsedRowData);
            EJUBindAllUIElements(strContainer);
        }
        else {
            var index = formName.indexOf("_TabOpForm_");
            var divOriginal = formName.substring(index + "_TabOpForm_".length);
            var sparsed = $(root).find("#" + divName);
            var sParsedRowData = $('<div>').html(sparsed).html();
            $("#" + divOriginal).append(sParsedRowData);
           EJUBindAllUIElements(strContainer);
            if ($(document).find("a[form='" + formName + "']").length > 0) {
                if (sparsed.length > 0) {
                    $(document).find("a[form='" + formName + "']").attr("EJS_Tab_DataLoaded", 1)
                }
            }

        }
    }
}

(function ($) {

    //CONSTANTS
    var strCONSTewfbtnLoadClass = 'EWF-btnLoad';
    var strCONSTewfbtnLoadClassAjax = 'EWF-btnLoad-Ajax';
    var strCONSTewfLoadTextDataAttr = 'data-EWF-LoadText';


    var currElementText = '';

    $.fn.ewfButtonLoadPlugin = function (options) {
        //Settings list and the default values
        var defaults = {
            bind: "auto"
        };

        var options = $.extend(defaults, options);


        if (options == null) {
            //Instantiation & event binding 
            $(document).on('click', '.' + strCONSTewfbtnLoadClass, function (e) {
                currElementText = $(this).html();
                $.fn.ewfButtonLoadPlugin.enetLoadingButtonTurnOn($(this));
            });
        }
        else if (options.bind == "auto") {
            $(document).on('click', '.' + strCONSTewfbtnLoadClass, function (e) {
                currElementText = $(this).html();
                $.fn.ewfButtonLoadPlugin.enetLoadingButtonTurnOn($(this));
            });

        }
        else if (options.bind == "manual") {
            $(document).on('click', '.' + strCONSTewfbtnLoadClass, function (e) {
                currElementText = $(this).html();
            });
        }

    };



    $.fn.ewfButtonLoadPlugin.enetLoadingButtonTurnOn = function (inElement) {
        enetLoadingButtonOn(inElement);
    };

    $.fn.ewfButtonLoadPlugin.enetLoadingButtonTurnOff = function (inElement) {
        enetLoadingButtonOff(inElement);
    }

    //private methods
    function enetLoadingButtonOn(inElement) {
        var loadingText = '';
        if ($(inElement).attr("data-EWF-LoadText") != null && $(inElement).attr("data-EWF-LoadText") != undefined) {
            loadingText = $(inElement).attr("data-EWF-LoadText");
        }
        else {
            loadingText = 'Please wait...';
        }

        $(inElement).html(loadingText);
        $(inElement).addClass("disabled");
//        $(inElement).attr("disabled", "disabled");
    }


    function enetLoadingButtonOff(inElement) {
        $(inElement).html(currElementText);
        $(inElement).removeClass("disabled");
        $(inElement).removeAttr("disabled");
    }


    $('.' + strCONSTewfbtnLoadClass).ewfButtonLoadPlugin();
    $('.' + strCONSTewfbtnLoadClassAjax).ewfButtonLoadPlugin();

})(jQuery);  
    
/*

(function ($) {

    //CONSTANTS
    var strCONSTewfChartClass = 'EWF-Chart';
    var strCONSTDataArrayAttr = "data-EWF-Graph-DataArray";
    var strCONSTContainerIDAttr = "data-EWF-Graph-containerid";
    var strCONSTChartTypeAttr = "data-EWF-Graph-charttype";
    var strCONSTChartOptionsAttr = "data-EWF-Graph-chartoptions";
    var strCONSTChartViewAttr = "data-ewf-graph-view";

    google.load('visualization', '1', { packages: ['corechart'] });

    $.fn.ewfChartPlugin = function (options) {

        if (options == null) {
            //Instantiation & event binding 

            $(document).on('click', '.' + 'EWF-Chart', function (e) {


//                console.log('submit triggered');
//                console.log($(this));

                e.preventDefault();
                doPluginAction($(this));

            });
         
        }
        else {
//            console.log('Error binding ewfFormAJAXmodePlugin: invalid options');
        }

        //Private function for the Plugin
        function doPluginAction(trigger) {

            var strDataArrayName, dataArray, strGraphContainerID, strChartType, strChartOptions, dataChartOptions, strChartView, dataChartView;

            // Data Array
            strDataArrayName = trigger.attr(strCONSTDataArrayAttr);
            dataArray = window[strDataArrayName];

            //Chart Type
            strChartType = trigger.attr(strCONSTChartTypeAttr);

            //Graph Container Div ID
            strGraphContainerID = trigger.attr(strCONSTContainerIDAttr);

            //chart Options
            strChartOptions = trigger.attr(strCONSTChartOptionsAttr);
            dataChartOptions = eval("(" + strChartOptions + ")");

            //chart view
            strChartView = trigger.attr(strCONSTChartViewAttr);


            if ($('#' + strGraphContainerID).length != 0) {
                $('#' + strGraphContainerID).removeClass('EnetUI-Hidden');
                $('#' + strGraphContainerID).removeClass('hide');
                $('#' + strGraphContainerID).show();

                var strTemp = "";
                strTemp =  strGraphContainerID.replace("ReportDataGraph", "").trim();
                strTemp = strTemp.replace("div", "").trim();
                $('#frm'+strTemp+'OpForm_enetTable').hide();
                //$('#' + strListContainerID).hide();

                drawVisualization(strChartType, strGraphContainerID, dataArray, dataChartOptions, strChartView);

            }
            else {
//                console.log('graph container div not found');
            }

        }

        function drawVisualization(strChartType, strGraphContainerID, dataArray, dataChartOptions, strChartView) {

            // Create and populate the data table.
            var dataChartView;
            var data = google.visualization.arrayToDataTable(dataArray);
            dataChartView = eval("(" + strChartView + ")");
            if (strChartType === 'piechart') {

                // Define a Pie chart
                var pie = new google.visualization.ChartWrapper({
                    'chartType': 'PieChart',
                    'containerId': strGraphContainerID,
                    'options': dataChartOptions,
                    'dataTable': data,
                    'view': dataChartView
                });

                pie.draw();
            }
            else if (strChartType === 'linechart') {

                // Define a Line chart
                var lineGraph = new google.visualization.ChartWrapper({
                    'chartType': 'LineChart',
                    'containerId': strGraphContainerID,
                    'options': dataChartOptions,
                    'dataTable': data,
                    'view': dataChartView
                });

                lineGraph.draw();
            }
            else if (strChartType === 'barchart') {

                // Define a bar chart
                var barGraph = new google.visualization.ChartWrapper({
                    'chartType': 'BarChart',
                    'containerId': strGraphContainerID,
                    'options': dataChartOptions,
                    'dataTable': data,
                    'view': dataChartView
                });

                barGraph.draw();
            }
            else {
//                console.log("undefined chrat type");
            }

        }
    };

    $('button.' + strCONSTewfChartClass).ewfChartPlugin();
    $(document).ready(function () {
        $('.EWF-Chart').trigger("click");
    });           

})(jQuery); 

           */

(function ($) {

    //CONSTANTS
    var strCONSTewfChartClass = 'EWF-Chart';
    var strCONSTDataArrayAttr = "data-EWF-Graph-DataArray";
    var strCONSTContainerIDAttr = "data-EWF-Graph-containerid";

    var strCONSTChartTypeAttr = "data-EWF-Graph-charttype";
    var strCONSTChartOptionAttr = "data-EWF-Graph-chartoptions";
    var strCONSTChartColorsAttr = "data-EWF-Graph-chartcolors";
    var strCONSTChartLabelsAttr = "data-EWF-Graph-chartlabels";
    var strCONSTChartLegendAttr = "data-EWF-Graph-chartlegend";
    var strCONSTChartNoDataAttr = "data-EWF-Graph-chartnoData";
    var strCONSTChartTitleAttr = "data-EWF-Graph-charttitle";
    var strCONSTChartSubTitleAttr = "data-EWF-Graph-chartsubtitle";
    var strCONSTChartToolTipAttr = "data-EWF-Graph-charttooltip";
    var strCONSTChartPlotOptionsAttr = "data-EWF-Graph-chartplotoptions";

    var strCONSTChartXAxisIndex = "data-EWF-Graph-XAxis";
    var strCONSTChartYAxisIndex = "data-EWF-Graph-YAxis";
    var strCONSTChartYAxisName = "data-ewf-graph-yaxisnames";
    var strCONSTChartyAxisAttr = "data-EWF-Graph-YAxisOptions";
    
    var dataChartPlotOptions,dataChartOptions, dataChartTypeOptions, dataColorOptions, dataLabelOptions, dataLegendOptions, dataNoDataOptions
        , dataTitleOptions, dataSubTitleOptions,  dataToolTipOptions, datayAxisAttr;

    $.fn.ewfChartPlugin = function (options) {

        if (options == null) {
            //Instantiation & event binding 

            $(document).on('click', '.' + 'EWF-Chart', function (e) {

//                console.log('submit triggered');
//                console.log($(this));

                e.preventDefault();
                doPluginAction($(this));

            });
         
        }
        else {
//            console.log('Error binding ewfFormAJAXmodePlugin: invalid options');
        }

        //Private function for the Plugin
        function doPluginAction(trigger) {

            var strDataArrayName, dataArray, strGraphContainerID, strChartType, strChartPlotOptions,  strXAxisIndex, strYAxisIndex, stryAxisAttr;
            var strChartOptions, strColorOptions, strLabelOptions, strLegendOptions, strNoDataOptions, strTitleOptions, strSubTitleOptions,  strToolTipOptions;

            // Data Array
            strDataArrayName = trigger.attr(strCONSTDataArrayAttr);
            dataArray = window[strDataArrayName];

            //Chart Type
            // no type is specified it will default to line chart
            strChartType = trigger.attr(strCONSTChartTypeAttr);
            if(strChartType == "")
            {
                strChartType = "line";
            }

            // This attribute is defaulted to line graph in the control if the user hasnt specified anything    
            strChartOptions = trigger.attr(strCONSTChartOptionAttr);
            dataChartOptions = eval("(" + strChartOptions + ")");

            //Graph Container Div ID
            strGraphContainerID = trigger.attr(strCONSTContainerIDAttr);

            //Chart Plot Options
            strChartPlotOptions = trigger.attr(strCONSTChartPlotOptionsAttr);
            dataChartPlotOptions = eval("(" + strChartPlotOptions + ")");

            //Color Options
            strColorOptions = trigger.attr(strCONSTChartColorsAttr);
            dataColorOptions = eval("(" + strColorOptions + ")");

            //Label Options
            strLabelOptions = trigger.attr(strCONSTChartLabelsAttr);
            dataLabelOptions = eval("(" + strLabelOptions + ")");

            //Legend Options
            strLegendOptions = trigger.attr(strCONSTChartLegendAttr);
            dataLegendOptions = eval("(" + strLegendOptions + ")");

            //No Data Options
            strNoDataOptions = trigger.attr(strCONSTChartNoDataAttr);
            dataNoDataOptions = eval("(" + strNoDataOptions + ")");

            //Title Options
            strTitleOptions = trigger.attr(strCONSTChartTitleAttr);
            dataTitleOptions = eval("(" + strTitleOptions + ")");

            //Sub Title Options
            strSubTitleOptions = trigger.attr(strCONSTChartSubTitleAttr);
            dataSubTitleOptions = eval("(" + strSubTitleOptions + ")");

            //Tool Tip Options
            strToolTipOptions = trigger.attr(strCONSTChartToolTipAttr);
            dataToolTipOptions = eval("(" + strToolTipOptions + ")");

            stryAxisAttr    = trigger.attr(strCONSTChartyAxisAttr);
            datayAxisAttr  = eval("(" + stryAxisAttr + ")");

            strXAxisIndex = trigger.attr(strCONSTChartXAxisIndex); 
            strYAxisIndex = trigger.attr(strCONSTChartYAxisIndex); 
            strYAxisNames = trigger.attr(strCONSTChartYAxisName); 

            if ($('#' + strGraphContainerID).length != 0) {
                $('#' + strGraphContainerID).removeClass('EnetUI-Hidden');
                $('#' + strGraphContainerID).removeClass('hide');
                $('#' + strGraphContainerID).show();

                var strTemp = "";
                strTemp =  strGraphContainerID.replace("ReportDataGraph", "").trim();
                strTemp = strTemp.replace("div", "").trim();
                //$('#frm'+strTemp+'OpForm_enetTable').hide();
                $('#div'+strTemp+'ReportData').hide();
                drawVisualization(strChartType, strGraphContainerID, dataArray,strXAxisIndex, strYAxisIndex, strYAxisNames);

            }
            else {
//                console.log('graph container div not found');
            }

        }

        function drawVisualization(strChartType, strGraphContainerID, dataArray, strXAxisIndex, strYAxisIndex, strYAxisNames) {
            var strDataArrayTransposeName, dataArrayTranspose;

            if(dataArrayTranspose == undefined && dataArrayTranspose == null)
            {
                dataArrayTranspose = $.transpose(dataArray)
            }
            //console.log(dataArrayTranspose);


            // Create and populate the data table.
            var dataChartView;
            if (strChartType === 'pie') {
                var dataArrayPieChart =  [];

                    for(i=0;i<dataArray.length;i++) {
                        var arr = []
                        arr[0] = dataArray[i][strXAxisIndex]
                        arr[1] = dataArray[i][strYAxisIndex]
                        dataArrayPieChart.push(arr);
                    }

                $('#' + strGraphContainerID).highcharts({
                        chart: dataChartOptions
                        , plotOptions:  dataChartPlotOptions
                        , color: dataColorOptions
                        , label: dataLabelOptions
                        , legend: dataLegendOptions
                        , noData: dataNoDataOptions
                        , title: dataTitleOptions
                        , subtitle: dataSubTitleOptions
                        , tooltip: dataToolTipOptions
                        , credits: {
                              enabled: false
                          }
                        , xAxis:   {
                            categories: dataArrayTranspose[strXAxisIndex]
                        }
                        ,series:   [{
                                    type: 'pie'
                                    , innerSize:'50%'
                                    , data : dataArrayPieChart
                                  }]
                        ,exporting: { enabled: false }

                 });
                
            }
            else {
                var graphDataArray = [];

                if(strYAxisIndex.indexOf(",") > -1)
                {
                    arrYAxis = strYAxisIndex.split(",")
                    arrYAxisNames = strYAxisNames.split(',,')
                    var arrayLength = arrYAxis.length;
                    for (var i = 0; i < arrayLength; i++) {
                        var newElement = {};
                        newElement['name'] = arrYAxisNames[i];
                        newElement['data'] = dataArrayTranspose[arrYAxis[i]];
                          //TODO: only used in case of dual axis
//                        if(i == 1)
//                        {
//                            newElement['yAxis']  = 1;
//                        }
                        graphDataArray.push(newElement);
                    }
                }
                else {
                        var newElement = {};
                        newElement['name'] = strYAxisNames;
                        newElement['data'] = dataArrayTranspose[strYAxisIndex];
                        graphDataArray.push(newElement);    
                }

                $('#' + strGraphContainerID).highcharts({
                        chart: dataChartOptions
                        , plotOptions:  dataChartPlotOptions
                        , color: dataColorOptions
                        , label: dataLabelOptions
                        , legend: dataLegendOptions
                        , noData: dataNoDataOptions
                        , title: dataTitleOptions
                        , subtitle: dataSubTitleOptions
                        , tooltip: dataToolTipOptions
                        , credits: {
                              enabled: false
                          }
                        , xAxis:   {
                            categories: dataArrayTranspose[strXAxisIndex]
                        }
                        , yAxis: datayAxisAttr
                        , series:  graphDataArray
                        ,exporting: { enabled: false }
                 });

            }

        }
    };

    $('button.' + strCONSTewfChartClass).ewfChartPlugin();
    $(document).ready(function () {
         if($("[ID*='ReportDataGraph']").attr("data-ewf-graph-mode") == "GraphMode" )
         {
              $('.EWF-Chart').trigger("click");
         }
         else {
         // do not bind the plugin
        }
    });           

})(jQuery); 



$(document).on('click', '.EJS-Rep-List', function (e) {
            var strListContainerID = "";
            var strGraphContainerID = "";
            strListContainerID = $(this).attr("data-ewf-graph-containerid");
            $('#'+strListContainerID+ 'Graph').hide();
            $('#' + strListContainerID).show();
            $('#' + strListContainerID).removeClass('EnetUI-Hidden');

});

/*!
 * accounting.js v0.3.2, copyright 2011 Joss Crowcroft, MIT license, http://josscrowcroft.github.com/accounting.js
 */
(function(p,z){function q(a){return!!(""===a||a&&a.charCodeAt&&a.substr)}function m(a){return u?u(a):"[object Array]"===v.call(a)}function r(a){return"[object Object]"===v.call(a)}function s(a,b){var d,a=a||{},b=b||{};for(d in b)b.hasOwnProperty(d)&&null==a[d]&&(a[d]=b[d]);return a}function j(a,b,d){var c=[],e,h;if(!a)return c;if(w&&a.map===w)return a.map(b,d);for(e=0,h=a.length;e<h;e++)c[e]=b.call(d,a[e],e,a);return c}function n(a,b){a=Math.round(Math.abs(a));return isNaN(a)?b:a}function x(a){var b=c.settings.currency.format;"function"===typeof a&&(a=a());return q(a)&&a.match("%v")?{pos:a,neg:a.replace("-","").replace("%v","-%v"),zero:a}:!a||!a.pos||!a.pos.match("%v")?!q(b)?b:c.settings.currency.format={pos:b,neg:b.replace("%v","-%v"),zero:b}:a}var c={version:"0.3.2",settings:{currency:{symbol:"$",format:"%s%v",decimal:".",thousand:",",precision:2,grouping:3},number:{precision:0,grouping:3,thousand:",",decimal:"."}}},w=Array.prototype.map,u=Array.isArray,v=Object.prototype.toString,o=c.unformat=c.parse=function(a,b){if(m(a))return j(a,function(a){return o(a,b)});a=a||0;if("number"===typeof a)return a;var b=b||".",c=RegExp("[^0-9-"+b+"]",["g"]),c=parseFloat((""+a).replace(/\((.*)\)/,"-$1").replace(c,"").replace(b,"."));return!isNaN(c)?c:0},y=c.toFixed=function(a,b){var b=n(b,c.settings.number.precision),d=Math.pow(10,b);return(Math.round(c.unformat(a)*d)/d).toFixed(b)},t=c.formatNumber=function(a,b,d,i){if(m(a))return j(a,function(a){return t(a,b,d,i)});var a=o(a),e=s(r(b)?b:{precision:b,thousand:d,decimal:i},c.settings.number),h=n(e.precision),f=0>a?"-":"",g=parseInt(y(Math.abs(a||0),h),10)+"",l=3<g.length?g.length%3:0;return f+(l?g.substr(0,l)+e.thousand:"")+g.substr(l).replace(/(\d{3})(?=\d)/g,"$1"+e.thousand)+(h?e.decimal+y(Math.abs(a),h).split(".")[1]:"")},A=c.formatMoney=function(a,b,d,i,e,h){if(m(a))return j(a,function(a){return A(a,b,d,i,e,h)});var a=o(a),f=s(r(b)?b:{symbol:b,precision:d,thousand:i,decimal:e,format:h},c.settings.currency),g=x(f.format);return(0<a?g.pos:0>a?g.neg:g.zero).replace("%s",f.symbol).replace("%v",t(Math.abs(a),n(f.precision),f.thousand,f.decimal))};c.formatColumn=function(a,b,d,i,e,h){if(!a)return[];var f=s(r(b)?b:{symbol:b,precision:d,thousand:i,decimal:e,format:h},c.settings.currency),g=x(f.format),l=g.pos.indexOf("%s")<g.pos.indexOf("%v")?!0:!1,k=0,a=j(a,function(a){if(m(a))return c.formatColumn(a,f);a=o(a);a=(0<a?g.pos:0>a?g.neg:g.zero).replace("%s",f.symbol).replace("%v",t(Math.abs(a),n(f.precision),f.thousand,f.decimal));if(a.length>k)k=a.length;return a});return j(a,function(a){return q(a)&&a.length<k?l?a.replace(f.symbol,f.symbol+Array(k-a.length+1).join(" ")):Array(k-a.length+1).join(" ")+a:a})};if("undefined"!==typeof exports){if("undefined"!==typeof module&&module.exports)exports=module.exports=c;exports.accounting=c}else"function"===typeof define&&define.amd?define([],function(){return c}):(c.noConflict=function(a){return function(){p.accounting=a;c.noConflict=z;return c}}(p.accounting),p.accounting=c)})(this);



// Simple Set Clipboard System
// Author: Joseph Huckaby

var ZeroClipboard_TableTools = {
	
	version: "1.0.4-TableTools2",
	clients: {}, // registered upload clients on page, indexed by id
	moviePath: '', // URL to movie
	nextId: 1, // ID of next movie
	
	$: function(thingy) {
		// simple DOM lookup utility function
		if (typeof(thingy) == 'string') thingy = document.getElementById(thingy);
		if (!thingy.addClass) {
			// extend element with a few useful methods
			thingy.hide = function() { this.style.display = 'none'; };
			thingy.show = function() { this.style.display = ''; };
			thingy.addClass = function(name) { this.removeClass(name); this.className += ' ' + name; };
			thingy.removeClass = function(name) {
				this.className = this.className.replace( new RegExp("\\s*" + name + "\\s*"), " ").replace(/^\s+/, '').replace(/\s+$/, '');
			};
			thingy.hasClass = function(name) {
				return !!this.className.match( new RegExp("\\s*" + name + "\\s*") );
			}
		}
		return thingy;
	},
	
	setMoviePath: function(path) {
		// set path to ZeroClipboard.swf
		this.moviePath = path;
	},
	
	dispatch: function(id, eventName, args) {
		// receive event from flash movie, send to client		
		var client = this.clients[id];
		if (client) {
			client.receiveEvent(eventName, args);
		}
	},
	
	register: function(id, client) {
		// register new client to receive events
		this.clients[id] = client;
	},
	
	getDOMObjectPosition: function(obj) {
		// get absolute coordinates for dom element
		var info = {
			left: 0, 
			top: 0, 
			width: obj.width ? obj.width : obj.offsetWidth, 
			height: obj.height ? obj.height : obj.offsetHeight
		};
		
		if ( obj.style.width != "" )
			info.width = obj.style.width.replace("px","");
		
		if ( obj.style.height != "" )
			info.height = obj.style.height.replace("px","");

		while (obj) {
			info.left += obj.offsetLeft;
			info.top += obj.offsetTop;
			obj = obj.offsetParent;
		}

		return info;
	},
	
	Client: function(elem) {
		// constructor for new simple upload client
		this.handlers = {};
		
		// unique ID
		this.id = ZeroClipboard_TableTools.nextId++;
		this.movieId = 'ZeroClipboard_TableToolsMovie_' + this.id;
		
		// register client with singleton to receive flash events
		ZeroClipboard_TableTools.register(this.id, this);
		
		// create movie
		if (elem) this.glue(elem);
	}
};

ZeroClipboard_TableTools.Client.prototype = {
	
	id: 0, // unique ID for us
	ready: false, // whether movie is ready to receive events or not
	movie: null, // reference to movie object
	clipText: '', // text to copy to clipboard
	fileName: '', // default file save name
	action: 'copy', // action to perform
	handCursorEnabled: true, // whether to show hand cursor, or default pointer cursor
	cssEffects: true, // enable CSS mouse effects on dom container
	handlers: null, // user event handlers
	sized: false,
	
	glue: function(elem, title) {
		// glue to DOM element
		// elem can be ID or actual DOM element object
		this.domElement = ZeroClipboard_TableTools.$(elem);
		
		// float just above object, or zIndex 99 if dom element isn't set
		var zIndex = 99;
		if (this.domElement.style.zIndex) {
			zIndex = parseInt(this.domElement.style.zIndex) + 1;
		}
		
		// find X/Y position of domElement
		var box = ZeroClipboard_TableTools.getDOMObjectPosition(this.domElement);
		
		// create floating DIV above element
		this.div = document.createElement('div');
		var style = this.div.style;
		style.position = 'absolute';
		style.left = '0px';
		style.top = '0px';
		style.width = (box.width) + 'px';
		style.height = box.height + 'px';
		style.zIndex = zIndex;
		
		if ( typeof title != "undefined" && title != "" ) {
			this.div.title = title;
		}
		if ( box.width != 0 && box.height != 0 ) {
			this.sized = true;
		}
		
		// style.backgroundColor = '#f00'; // debug
		if ( this.domElement ) {
			this.domElement.appendChild(this.div);
			this.div.innerHTML = this.getHTML( box.width, box.height );
		}
	},
	
	positionElement: function() {
		var box = ZeroClipboard_TableTools.getDOMObjectPosition(this.domElement);
		var style = this.div.style;
		
		style.position = 'absolute';
		//style.left = (this.domElement.offsetLeft)+'px';
		//style.top = this.domElement.offsetTop+'px';
		style.width = box.width + 'px';
		style.height = box.height + 'px';
		
		if ( box.width != 0 && box.height != 0 ) {
			this.sized = true;
		} else {
			return;
		}
		
		var flash = this.div.childNodes[0];
		flash.width = box.width;
		flash.height = box.height;
	},
	
	getHTML: function(width, height) {
		// return HTML for movie
		var html = '';
		var flashvars = 'id=' + this.id + 
			'&width=' + width + 
			'&height=' + height;
			
		if (navigator.userAgent.match(/MSIE/)) {
			// IE gets an OBJECT tag
			var protocol = location.href.match(/^https/i) ? 'https://' : 'http://';
			html += '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="'+protocol+'download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0" width="'+width+'" height="'+height+'" id="'+this.movieId+'" align="middle"><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="false" /><param name="movie" value="'+ZeroClipboard_TableTools.moviePath+'" /><param name="loop" value="false" /><param name="menu" value="false" /><param name="quality" value="best" /><param name="bgcolor" value="#ffffff" /><param name="flashvars" value="'+flashvars+'"/><param name="wmode" value="transparent"/></object>';
		}
		else {
			// all other browsers get an EMBED tag
			html += '<embed id="'+this.movieId+'" src="'+ZeroClipboard_TableTools.moviePath+'" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="'+width+'" height="'+height+'" name="'+this.movieId+'" align="middle" allowScriptAccess="always" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="'+flashvars+'" wmode="transparent" />';
		}
		return html;
	},
	
	hide: function() {
		// temporarily hide floater offscreen
		if (this.div) {
			this.div.style.left = '-2000px';
		}
	},
	
	show: function() {
		// show ourselves after a call to hide()
		this.reposition();
	},
	
	destroy: function() {
		// destroy control and floater
		if (this.domElement && this.div) {
			this.hide();
			this.div.innerHTML = '';
			
			var body = document.getElementsByTagName('body')[0];
			try { body.removeChild( this.div ); } catch(e) {;}
			
			this.domElement = null;
			this.div = null;
		}
	},
	
	reposition: function(elem) {
		// reposition our floating div, optionally to new container
		// warning: container CANNOT change size, only position
		if (elem) {
			this.domElement = ZeroClipboard_TableTools.$(elem);
			if (!this.domElement) this.hide();
		}
		
		if (this.domElement && this.div) {
			var box = ZeroClipboard_TableTools.getDOMObjectPosition(this.domElement);
			var style = this.div.style;
			style.left = '' + box.left + 'px';
			style.top = '' + box.top + 'px';
		}
	},
	
	clearText: function() {
		// clear the text to be copy / saved
		this.clipText = '';
		if (this.ready) this.movie.clearText();
	},
	
	appendText: function(newText) {
		// append text to that which is to be copied / saved
		this.clipText += newText;
		if (this.ready) { this.movie.appendText(newText) ;}
	},
	
	setText: function(newText) {
		// set text to be copied to be copied / saved
		this.clipText = newText;
		if (this.ready) { this.movie.setText(newText) ;}
	},
	
	setCharSet: function(charSet) {
		// set the character set (UTF16LE or UTF8)
		this.charSet = charSet;
		if (this.ready) { this.movie.setCharSet(charSet) ;}
	},
	
	setBomInc: function(bomInc) {
		// set if the BOM should be included or not
		this.incBom = bomInc;
		if (this.ready) { this.movie.setBomInc(bomInc) ;}
	},
	
	setFileName: function(newText) {
		// set the file name
		this.fileName = newText;
		if (this.ready) this.movie.setFileName(newText);
	},
	
	setAction: function(newText) {
		// set action (save or copy)
		this.action = newText;
		if (this.ready) this.movie.setAction(newText);
	},
	
	addEventListener: function(eventName, func) {
		// add user event listener for event
		// event types: load, queueStart, fileStart, fileComplete, queueComplete, progress, error, cancel
		eventName = eventName.toString().toLowerCase().replace(/^on/, '');
		if (!this.handlers[eventName]) this.handlers[eventName] = [];
		this.handlers[eventName].push(func);
	},
	
	setHandCursor: function(enabled) {
		// enable hand cursor (true), or default arrow cursor (false)
		this.handCursorEnabled = enabled;
		if (this.ready) this.movie.setHandCursor(enabled);
	},
	
	setCSSEffects: function(enabled) {
		// enable or disable CSS effects on DOM container
		this.cssEffects = !!enabled;
	},
	
	receiveEvent: function(eventName, args) {
		// receive event from flash
		eventName = eventName.toString().toLowerCase().replace(/^on/, '');
		
		// special behavior for certain events
		switch (eventName) {
			case 'load':
				// movie claims it is ready, but in IE this isn't always the case...
				// bug fix: Cannot extend EMBED DOM elements in Firefox, must use traditional function
				this.movie = document.getElementById(this.movieId);
				if (!this.movie) {
					var self = this;
					setTimeout( function() { self.receiveEvent('load', null); }, 1 );
					return;
				}
				
				// firefox on pc needs a "kick" in order to set these in certain cases
				if (!this.ready && navigator.userAgent.match(/Firefox/) && navigator.userAgent.match(/Windows/)) {
					var self = this;
					setTimeout( function() { self.receiveEvent('load', null); }, 100 );
					this.ready = true;
					return;
				}
				
				this.ready = true;
				this.movie.clearText();
				this.movie.appendText( this.clipText );
				this.movie.setFileName( this.fileName );
				this.movie.setAction( this.action );
				this.movie.setCharSet( this.charSet );
				this.movie.setBomInc( this.incBom );
				this.movie.setHandCursor( this.handCursorEnabled );
				break;
			
			case 'mouseover':
				if (this.domElement && this.cssEffects) {
					//this.domElement.addClass('hover');
					if (this.recoverActive) this.domElement.addClass('active');
				}
				break;
			
			case 'mouseout':
				if (this.domElement && this.cssEffects) {
					this.recoverActive = false;
					if (this.domElement.hasClass('active')) {
						this.domElement.removeClass('active');
						this.recoverActive = true;
					}
					//this.domElement.removeClass('hover');
				}
				break;
			
			case 'mousedown':
				if (this.domElement && this.cssEffects) {
					this.domElement.addClass('active');
				}
				break;
			
			case 'mouseup':
				if (this.domElement && this.cssEffects) {
					this.domElement.removeClass('active');
					this.recoverActive = false;
				}
				break;
		} // switch eventName
		
		if (this.handlers[eventName]) {
			for (var idx = 0, len = this.handlers[eventName].length; idx < len; idx++) {
				var func = this.handlers[eventName][idx];
			
				if (typeof(func) == 'function') {
					// actual function reference
					func(this, args);
				}
				else if ((typeof(func) == 'object') && (func.length == 2)) {
					// PHP style object + method, i.e. [myObject, 'myMethod']
					func[0][ func[1] ](this, args);
				}
				else if (typeof(func) == 'string') {
					// name of function
					window[func](this, args);
				}
			} // foreach event handler defined
		} // user defined handler for event
	}
	
};
  

/*
 * File:        TableTools.js
 * Version:     2.1.4
 * Description: Tools and buttons for DataTables
 * Author:      Allan Jardine (www.sprymedia.co.uk)
 * Language:    Javascript
 * License:	    GPL v2 or BSD 3 point style
 * Project:	    DataTables
 * 
 * Copyright 2009-2012 Allan Jardine, all rights reserved.
 *
 * This source file is free software, under either the GPL v2 license or a
 * BSD style license, available at:
 *   http://datatables.net/license_gpl2
 *   http://datatables.net/license_bsd
 */

/* Global scope for TableTools */
var TableTools;

(function($, window, document) {

/** 
 * TableTools provides flexible buttons and other tools for a DataTables enhanced table
 * @class TableTools
 * @constructor
 * @param {Object} oDT DataTables instance
 * @param {Object} oOpts TableTools options
 * @param {String} oOpts.sSwfPath ZeroClipboard SWF path
 * @param {String} oOpts.sRowSelect Row selection options - 'none', 'single' or 'multi'
 * @param {Function} oOpts.fnPreRowSelect Callback function just prior to row selection
 * @param {Function} oOpts.fnRowSelected Callback function just after row selection
 * @param {Function} oOpts.fnRowDeselected Callback function when row is deselected
 * @param {Array} oOpts.aButtons List of buttons to be used
 */
TableTools = function( oDT, oOpts )
{
	/* Santiy check that we are a new instance */
	if ( ! this instanceof TableTools )
	{
		alert( "Warning: TableTools must be initialised with the keyword 'new'" );
	}
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Public class variables
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	/**
	 * @namespace Settings object which contains customisable information for TableTools instance
	 */
	this.s = {
		/**
		 * Store 'this' so the instance can be retrieved from the settings object
		 * @property that
		 * @type	 object
		 * @default  this
		 */
		"that": this,
		
		/** 
		 * DataTables settings objects
		 * @property dt
		 * @type	 object
		 * @default  <i>From the oDT init option</i>
		 */
		"dt": oDT.fnSettings(),
		
		/**
		 * @namespace Print specific information
		 */
		"print": {
			/** 
			 * DataTables draw 'start' point before the printing display was shown
			 *  @property saveStart
			 *  @type	 int
			 *  @default  -1
		 	 */
		  "saveStart": -1,
			
			/** 
			 * DataTables draw 'length' point before the printing display was shown
			 *  @property saveLength
			 *  @type	 int
			 *  @default  -1
		 	 */
		  "saveLength": -1,
		
			/** 
			 * Page scrolling point before the printing display was shown so it can be restored
			 *  @property saveScroll
			 *  @type	 int
			 *  @default  -1
		 	 */
		  "saveScroll": -1,
		
			/** 
			 * Wrapped function to end the print display (to maintain scope)
			 *  @property funcEnd
		 	 *  @type	 Function
			 *  @default  function () {}
		 	 */
		  "funcEnd": function () {}
	  },
	
		/**
		 * A unique ID is assigned to each button in each instance
		 * @property buttonCounter
		 *  @type	 int
		 * @default  0
		 */
	  "buttonCounter": 0,
		
		/**
		 * @namespace Select rows specific information
		 */
		"select": {
			/**
			 * Select type - can be 'none', 'single' or 'multi'
			 * @property type
			 *  @type	 string
			 * @default  ""
			 */
			"type": "",
			
			/**
			 * Array of nodes which are currently selected
			 *  @property selected
			 *  @type	 array
			 *  @default  []
			 */
			"selected": [],
			
			/**
			 * Function to run before the selection can take place. Will cancel the select if the
			 * function returns false
			 *  @property preRowSelect
			 *  @type	 Function
			 *  @default  null
			 */
			"preRowSelect": null,
			
			/**
			 * Function to run when a row is selected
			 *  @property postSelected
			 *  @type	 Function
			 *  @default  null
			 */
			"postSelected": null,
			
			/**
			 * Function to run when a row is deselected
			 *  @property postDeselected
			 *  @type	 Function
			 *  @default  null
			 */
			"postDeselected": null,
			
			/**
			 * Indicate if all rows are selected (needed for server-side processing)
			 *  @property all
			 *  @type	 boolean
			 *  @default  false
			 */
			"all": false,
			
			/**
			 * Class name to add to selected TR nodes
			 *  @property selectedClass
			 *  @type	 String
			 *  @default  ""
			 */
			"selectedClass": ""
		},
		
		/**
		 * Store of the user input customisation object
		 *  @property custom
		 *  @type	 object
		 *  @default  {}
		 */
		"custom": {},
		
		/**
		 * SWF movie path
		 *  @property swfPath
		 *  @type	 string
		 *  @default  ""
		 */
		"swfPath": "",
		
		/**
		 * Default button set
		 *  @property buttonSet
		 *  @type	 array
		 *  @default  []
		 */
		"buttonSet": [],
		
		/**
		 * When there is more than one TableTools instance for a DataTable, there must be a 
		 * master which controls events (row selection etc)
		 *  @property master
		 *  @type	 boolean
		 *  @default  false
		 */
		"master": false,
		
		/**
		 * Tag names that are used for creating collections and buttons
		 *  @namesapce
		 */
		"tags": {}
	};
	
	
	/**
	 * @namespace Common and useful DOM elements for the class instance
	 */
	this.dom = {
		/**
		 * DIV element that is create and all TableTools buttons (and their children) put into
		 *  @property container
		 *  @type	 node
		 *  @default  null
		 */
		"container": null,
		
		/**
		 * The table node to which TableTools will be applied
		 *  @property table
		 *  @type	 node
		 *  @default  null
		 */
		"table": null,
		
		/**
		 * @namespace Nodes used for the print display
		 */
		"print": {
			/**
			 * Nodes which have been removed from the display by setting them to display none
			 *  @property hidden
			 *  @type	 array
		 	 *  @default  []
			 */
		  "hidden": [],
			
			/**
			 * The information display saying telling the user about the print display
			 *  @property message
			 *  @type	 node
		 	 *  @default  null
			 */
		  "message": null
	  },
		
		/**
		 * @namespace Nodes used for a collection display. This contains the currently used collection
		 */
		"collection": {
			/**
			 * The div wrapper containing the buttons in the collection (i.e. the menu)
			 *  @property collection
			 *  @type	 node
		 	 *  @default  null
			 */
			"collection": null,
			
			/**
			 * Background display to provide focus and capture events
			 *  @property background
			 *  @type	 node
		 	 *  @default  null
			 */
			"background": null
		}
	};

	/**
	 * @namespace Name space for the classes that this TableTools instance will use
	 * @extends TableTools.classes
	 */
	this.classes = $.extend( true, {}, TableTools.classes );
	if ( this.s.dt.bJUI )
	{
		$.extend( true, this.classes, TableTools.classes_themeroller );
	}
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Public class methods
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	/**
	 * Retreieve the settings object from an instance
	 *  @method fnSettings
	 *  @returns {object} TableTools settings object
	 */
	this.fnSettings = function () {
		return this.s;
	};
	
	
	/* Constructor logic */
	if ( typeof oOpts == 'undefined' )
	{
		oOpts = {};
	}
	
	this._fnConstruct( oOpts );
	
	return this;
};



TableTools.prototype = {
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Public methods
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	/**
	 * Retreieve the settings object from an instance
	 *  @returns {array} List of TR nodes which are currently selected
	 *  @param {boolean} [filtered=false] Get only selected rows which are  
	 *    available given the filtering applied to the table. By default
	 *    this is false -  i.e. all rows, regardless of filtering are 
	      selected.
	 */
	"fnGetSelected": function ( filtered )
	{
		var
			out = [],
			data = this.s.dt.aoData,
			displayed = this.s.dt.aiDisplay,
			i, iLen;

		if ( filtered )
		{
			// Only consider filtered rows
			for ( i=0, iLen=displayed.length ; i<iLen ; i++ )
			{
				if ( data[ displayed[i] ]._DTTT_selected )
				{
					out.push( data[ displayed[i] ].nTr );
				}
			}
		}
		else
		{
			// Use all rows
			for ( i=0, iLen=data.length ; i<iLen ; i++ )
			{
				if ( data[i]._DTTT_selected )
				{
					out.push( data[i].nTr );
				}
			}
		}

		return out;
	},


	/**
	 * Get the data source objects/arrays from DataTables for the selected rows (same as
	 * fnGetSelected followed by fnGetData on each row from the table)
	 *  @returns {array} Data from the TR nodes which are currently selected
	 */
	"fnGetSelectedData": function ()
	{
		var out = [];
		var data=this.s.dt.aoData;
		var i, iLen;

		for ( i=0, iLen=data.length ; i<iLen ; i++ )
		{
			if ( data[i]._DTTT_selected )
			{
				out.push( this.s.dt.oInstance.fnGetData(i) );
			}
		}

		return out;
	},
	
	
	/**
	 * Check to see if a current row is selected or not
	 *  @param {Node} n TR node to check if it is currently selected or not
	 *  @returns {Boolean} true if select, false otherwise
	 */
	"fnIsSelected": function ( n )
	{
		var pos = this.s.dt.oInstance.fnGetPosition( n );
		return (this.s.dt.aoData[pos]._DTTT_selected===true) ? true : false;
	},

	
	/**
	 * Select all rows in the table
	 *  @param {boolean} [filtered=false] Select only rows which are available 
	 *    given the filtering applied to the table. By default this is false - 
	 *    i.e. all rows, regardless of filtering are selected.
	 */
	"fnSelectAll": function ( filtered )
	{
		var s = this._fnGetMasterSettings();
		
		this._fnRowSelect( (filtered === true) ?
			s.dt.aiDisplay :
			s.dt.aoData
		);
	},

	
	/**
	 * Deselect all rows in the table
	 *  @param {boolean} [filtered=false] Deselect only rows which are available 
	 *    given the filtering applied to the table. By default this is false - 
	 *    i.e. all rows, regardless of filtering are deselected.
	 */
	"fnSelectNone": function ( filtered )
	{
		var s = this._fnGetMasterSettings();

		this._fnRowDeselect( this.fnGetSelected(filtered) );
	},

	
	/**
	 * Select row(s)
	 *  @param {node|object|array} n The row(s) to select. Can be a single DOM
	 *    TR node, an array of TR nodes or a jQuery object.
	 */
	"fnSelect": function ( n )
	{
		if ( this.s.select.type == "single" )
		{
			this.fnSelectNone();
			this._fnRowSelect( n );
		}
		else if ( this.s.select.type == "multi" )
		{
			this._fnRowSelect( n );
		}
	},

	
	/**
	 * Deselect row(s)
	 *  @param {node|object|array} n The row(s) to deselect. Can be a single DOM
	 *    TR node, an array of TR nodes or a jQuery object.
	 */
	"fnDeselect": function ( n )
	{
		this._fnRowDeselect( n );
	},
	
	
	/**
	 * Get the title of the document - useful for file names. The title is retrieved from either
	 * the configuration object's 'title' parameter, or the HTML document title
	 *  @param   {Object} oConfig Button configuration object
	 *  @returns {String} Button title
	 */
	"fnGetTitle": function( oConfig )
	{
		var sTitle = "";
		if ( typeof oConfig.sTitle != 'undefined' && oConfig.sTitle !== "" ) {
			sTitle = oConfig.sTitle;
		} else {
			var anTitle = document.getElementsByTagName('title');
			if ( anTitle.length > 0 )
			{
				sTitle = anTitle[0].innerHTML;
			}
		}
		
		/* Strip characters which the OS will object to - checking for UTF8 support in the scripting
		 * engine
		 */
		if ( "\u00A1".toString().length < 4 ) {
			return sTitle.replace(/[^a-zA-Z0-9_\u00A1-\uFFFF\.,\-_ !\(\)]/g, "");
		} else {
			return sTitle.replace(/[^a-zA-Z0-9_\.,\-_ !\(\)]/g, "");
		}
	},
	
	
	/**
	 * Calculate a unity array with the column width by proportion for a set of columns to be
	 * included for a button. This is particularly useful for PDF creation, where we can use the
	 * column widths calculated by the browser to size the columns in the PDF.
	 *  @param   {Object} oConfig Button configuration object
	 *  @returns {Array} Unity array of column ratios
	 */
	"fnCalcColRatios": function ( oConfig )
	{
		var
			aoCols = this.s.dt.aoColumns,
			aColumnsInc = this._fnColumnTargets( oConfig.mColumns ),
			aColWidths = [],
			iWidth = 0, iTotal = 0, i, iLen;
		
		for ( i=0, iLen=aColumnsInc.length ; i<iLen ; i++ )
		{
			if ( aColumnsInc[i] )
			{
				iWidth = aoCols[i].nTh.offsetWidth;
				iTotal += iWidth;
				aColWidths.push( iWidth );
			}
		}
		
		for ( i=0, iLen=aColWidths.length ; i<iLen ; i++ )
		{
			aColWidths[i] = aColWidths[i] / iTotal;
		}
		
		return aColWidths.join('\t');
	},
	
	
	/**
	 * Get the information contained in a table as a string
	 *  @param   {Object} oConfig Button configuration object
	 *  @returns {String} Table data as a string
	 */
	"fnGetTableData": function ( oConfig )
	{
		/* In future this could be used to get data from a plain HTML source as well as DataTables */
		if ( this.s.dt )
		{
			return this._fnGetDataTablesData( oConfig );
		}
	},
	
	
	/**
	 * Pass text to a flash button instance, which will be used on the button's click handler
	 *  @param   {Object} clip Flash button object
	 *  @param   {String} text Text to set
	 */
	"fnSetText": function ( clip, text )
	{
		this._fnFlashSetText( clip, text );
	},
	
	
	/**
	 * Resize the flash elements of the buttons attached to this TableTools instance - this is
	 * useful for when initialising TableTools when it is hidden (display:none) since sizes can't
	 * be calculated at that time.
	 */
	"fnResizeButtons": function ()
	{
		for ( var cli in ZeroClipboard_TableTools.clients )
		{
			if ( cli )
			{
				var client = ZeroClipboard_TableTools.clients[cli];
				if ( typeof client.domElement != 'undefined' &&
					 client.domElement.parentNode )
				{
					client.positionElement();
				}
			}
		}
	},
	
	
	/**
	 * Check to see if any of the ZeroClipboard client's attached need to be resized
	 */
	"fnResizeRequired": function ()
	{
		for ( var cli in ZeroClipboard_TableTools.clients )
		{
			if ( cli )
			{
				var client = ZeroClipboard_TableTools.clients[cli];
				if ( typeof client.domElement != 'undefined' &&
					 client.domElement.parentNode == this.dom.container &&
					 client.sized === false )
				{
					return true;
				}
			}
		}
		return false;
	},
	
	
	/**
	 * Programmatically enable or disable the print view
	 *  @param {boolean} [bView=true] Show the print view if true or not given. If false, then
	 *    terminate the print view and return to normal.
	 *  @param {object} [oConfig={}] Configuration for the print view
	 *  @param {boolean} [oConfig.bShowAll=false] Show all rows in the table if true
	 *  @param {string} [oConfig.sInfo] Information message, displayed as an overlay to the
	 *    user to let them know what the print view is.
	 *  @param {string} [oConfig.sMessage] HTML string to show at the top of the document - will
	 *    be included in the printed document.
	 */
	"fnPrint": function ( bView, oConfig )
	{
		if ( oConfig === undefined )
		{
			oConfig = {};
		}

		if ( bView === undefined || bView )
		{
			this._fnPrintStart( oConfig );
		}
		else
		{
			this._fnPrintEnd();
		}
	},
	
	
	/**
	 * Show a message to the end user which is nicely styled
	 *  @param {string} message The HTML string to show to the user
	 *  @param {int} time The duration the message is to be shown on screen for (mS)
	 */
	"fnInfo": function ( message, time ) {
		var nInfo = document.createElement( "div" );
		nInfo.className = this.classes.print.info;
		nInfo.innerHTML = message;

		document.body.appendChild( nInfo );
		
		setTimeout( function() {
			$(nInfo).fadeOut( "normal", function() {
				document.body.removeChild( nInfo );
			} );
		}, time );
	},
	
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Private methods (they are of course public in JS, but recommended as private)
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	/**
	 * Constructor logic
	 *  @method  _fnConstruct
	 *  @param   {Object} oOpts Same as TableTools constructor
	 *  @returns void
	 *  @private 
	 */
	"_fnConstruct": function ( oOpts )
	{
		var that = this;
		
		this._fnCustomiseSettings( oOpts );
		
		/* Container element */
		this.dom.container = document.createElement( this.s.tags.container );
		this.dom.container.className = this.classes.container;
		
		/* Row selection config */
		if ( this.s.select.type != 'none' )
		{
			this._fnRowSelectConfig();
		}
		
		/* Buttons */
		this._fnButtonDefinations( this.s.buttonSet, this.dom.container );
		
		/* Destructor - need to wipe the DOM for IE's garbage collector */
		this.s.dt.aoDestroyCallback.push( {
			"sName": "TableTools",
			"fn": function () {
				that.dom.container.innerHTML = "";
			}
		} );
	},
	
	
	/**
	 * Take the user defined settings and the default settings and combine them.
	 *  @method  _fnCustomiseSettings
	 *  @param   {Object} oOpts Same as TableTools constructor
	 *  @returns void
	 *  @private 
	 */
	"_fnCustomiseSettings": function ( oOpts )
	{
		/* Is this the master control instance or not? */
		if ( typeof this.s.dt._TableToolsInit == 'undefined' )
		{
			this.s.master = true;
			this.s.dt._TableToolsInit = true;
		}
		
		/* We can use the table node from comparisons to group controls */
		this.dom.table = this.s.dt.nTable;
		
		/* Clone the defaults and then the user options */
		this.s.custom = $.extend( {}, TableTools.DEFAULTS, oOpts );
		
		/* Flash file location */
		this.s.swfPath = this.s.custom.sSwfPath;
		if ( typeof ZeroClipboard_TableTools != 'undefined' )
		{
			ZeroClipboard_TableTools.moviePath = this.s.swfPath;
		}
		
		/* Table row selecting */
		this.s.select.type = this.s.custom.sRowSelect;
		this.s.select.preRowSelect = this.s.custom.fnPreRowSelect;
		this.s.select.postSelected = this.s.custom.fnRowSelected;
		this.s.select.postDeselected = this.s.custom.fnRowDeselected;

		// Backwards compatibility - allow the user to specify a custom class in the initialiser
		if ( this.s.custom.sSelectedClass )
		{
			this.classes.select.row = this.s.custom.sSelectedClass;
		}

		this.s.tags = this.s.custom.oTags;

		/* Button set */
		this.s.buttonSet = this.s.custom.aButtons;
	},
	
	
	/**
	 * Take the user input arrays and expand them to be fully defined, and then add them to a given
	 * DOM element
	 *  @method  _fnButtonDefinations
	 *  @param {array} buttonSet Set of user defined buttons
	 *  @param {node} wrapper Node to add the created buttons to
	 *  @returns void
	 *  @private 
	 */
	"_fnButtonDefinations": function ( buttonSet, wrapper )
	{
		var buttonDef;
		
		for ( var i=0, iLen=buttonSet.length ; i<iLen ; i++ )
		{
			if ( typeof buttonSet[i] == "string" )
			{
				if ( typeof TableTools.BUTTONS[ buttonSet[i] ] == 'undefined' )
				{
					alert( "TableTools: Warning - unknown button type: "+buttonSet[i] );
					continue;
				}
				buttonDef = $.extend( {}, TableTools.BUTTONS[ buttonSet[i] ], true );
			}
			else
			{
				if ( typeof TableTools.BUTTONS[ buttonSet[i].sExtends ] == 'undefined' )
				{
					alert( "TableTools: Warning - unknown button type: "+buttonSet[i].sExtends );
					continue;
				}
				var o = $.extend( {}, TableTools.BUTTONS[ buttonSet[i].sExtends ], true );
				buttonDef = $.extend( o, buttonSet[i], true );
			}
			
			wrapper.appendChild( this._fnCreateButton( 
				buttonDef, 
				$(wrapper).hasClass(this.classes.collection.container)
			) );
		}
	},
	
	
	/**
	 * Create and configure a TableTools button
	 *  @method  _fnCreateButton
	 *  @param   {Object} oConfig Button configuration object
	 *  @returns {Node} Button element
	 *  @private 
	 */
	"_fnCreateButton": function ( oConfig, bCollectionButton )
	{
	  var nButton = this._fnButtonBase( oConfig, bCollectionButton );
		
		if ( oConfig.sAction.match(/flash/) )
		{
			this._fnFlashConfig( nButton, oConfig );
		}
		else if ( oConfig.sAction == "text" )
		{
			this._fnTextConfig( nButton, oConfig );
		}
		else if ( oConfig.sAction == "div" )
		{
			this._fnTextConfig( nButton, oConfig );
		}
		else if ( oConfig.sAction == "collection" )
		{
			this._fnTextConfig( nButton, oConfig );
			this._fnCollectionConfig( nButton, oConfig );
		}
		
		return nButton;
	},
	
	
	/**
	 * Create the DOM needed for the button and apply some base properties. All buttons start here
	 *  @method  _fnButtonBase
	 *  @param   {o} oConfig Button configuration object
	 *  @returns {Node} DIV element for the button
	 *  @private 
	 */
	"_fnButtonBase": function ( o, bCollectionButton )
	{
		var sTag, sLiner, sClass;

		if ( bCollectionButton )
		{
			sTag = o.sTag !== "default" ? o.sTag : this.s.tags.collection.button;
			sLiner = o.sLinerTag !== "default" ? o.sLiner : this.s.tags.collection.liner;
			sClass = this.classes.collection.buttons.normal;
		}
		else
		{
			sTag = o.sTag !== "default" ? o.sTag : this.s.tags.button;
			sLiner = o.sLinerTag !== "default" ? o.sLiner : this.s.tags.liner;
			sClass = this.classes.buttons.normal;
		}

		var
		  nButton = document.createElement( sTag ),
		  nSpan = document.createElement( sLiner ),
		  masterS = this._fnGetMasterSettings();
		
		nButton.className = sClass+" "+o.sButtonClass;
		nButton.setAttribute('id', "ToolTables_"+this.s.dt.sInstance+"_"+masterS.buttonCounter );
		nButton.appendChild( nSpan );
		nSpan.innerHTML = o.sButtonText;
		
		masterS.buttonCounter++;
		
		return nButton;
	},
	
	
	/**
	 * Get the settings object for the master instance. When more than one TableTools instance is
	 * assigned to a DataTable, only one of them can be the 'master' (for the select rows). As such,
	 * we will typically want to interact with that master for global properties.
	 *  @method  _fnGetMasterSettings
	 *  @returns {Object} TableTools settings object
	 *  @private 
	 */
	"_fnGetMasterSettings": function ()
	{
		if ( this.s.master )
		{
			return this.s;
		}
		else
		{
			/* Look for the master which has the same DT as this one */
			var instances = TableTools._aInstances;
			for ( var i=0, iLen=instances.length ; i<iLen ; i++ )
			{
				if ( this.dom.table == instances[i].s.dt.nTable )
				{
					return instances[i].s;
				}
			}
		}
	},
	
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Button collection functions
	 */
	
	/**
	 * Create a collection button, when activated will present a drop down list of other buttons
	 *  @param   {Node} nButton Button to use for the collection activation
	 *  @param   {Object} oConfig Button configuration object
	 *  @returns void
	 *  @private
	 */
	"_fnCollectionConfig": function ( nButton, oConfig )
	{
		var nHidden = document.createElement( this.s.tags.collection.container );
		nHidden.style.display = "none";
		nHidden.className = this.classes.collection.container;
		oConfig._collection = nHidden;
		document.body.appendChild( nHidden );
		
		this._fnButtonDefinations( oConfig.aButtons, nHidden );
	},
	
	
	/**
	 * Show a button collection
	 *  @param   {Node} nButton Button to use for the collection
	 *  @param   {Object} oConfig Button configuration object
	 *  @returns void
	 *  @private
	 */
	"_fnCollectionShow": function ( nButton, oConfig )
	{
		var
			that = this,
			oPos = $(nButton).offset(),
			nHidden = oConfig._collection,
			iDivX = oPos.left,
			iDivY = oPos.top + $(nButton).outerHeight(),
			iWinHeight = $(window).height(), iDocHeight = $(document).height(),
		 	iWinWidth = $(window).width(), iDocWidth = $(document).width();
		
		nHidden.style.position = "absolute";
		nHidden.style.left = iDivX+"px";
		nHidden.style.top = iDivY+"px";
		nHidden.style.display = "block";
		$(nHidden).css('opacity',0);
		
		var nBackground = document.createElement('div');
		nBackground.style.position = "absolute";
		nBackground.style.left = "0px";
		nBackground.style.top = "0px";
		nBackground.style.height = ((iWinHeight>iDocHeight)? iWinHeight : iDocHeight) +"px";
		nBackground.style.width = ((iWinWidth>iDocWidth)? iWinWidth : iDocWidth) +"px";
		nBackground.className = this.classes.collection.background;
		$(nBackground).css('opacity',0);
		
		document.body.appendChild( nBackground );
		document.body.appendChild( nHidden );
		
		/* Visual corrections to try and keep the collection visible */
		var iDivWidth = $(nHidden).outerWidth();
		var iDivHeight = $(nHidden).outerHeight();
		
		if ( iDivX + iDivWidth > iDocWidth )
		{
			nHidden.style.left = (iDocWidth-iDivWidth)+"px";
		}
		
		if ( iDivY + iDivHeight > iDocHeight )
		{
			nHidden.style.top = (iDivY-iDivHeight-$(nButton).outerHeight())+"px";
		}
	
		this.dom.collection.collection = nHidden;
		this.dom.collection.background = nBackground;
		
		/* This results in a very small delay for the end user but it allows the animation to be
		 * much smoother. If you don't want the animation, then the setTimeout can be removed
		 */
		setTimeout( function () {
			$(nHidden).animate({"opacity": 1}, 500);
			$(nBackground).animate({"opacity": 0.25}, 500);
		}, 10 );

		/* Resize the buttons to the Flash contents fit */
		this.fnResizeButtons();
		
		/* Event handler to remove the collection display */
		$(nBackground).click( function () {
			that._fnCollectionHide.call( that, null, null );
		} );
	},
	
	
	/**
	 * Hide a button collection
	 *  @param   {Node} nButton Button to use for the collection
	 *  @param   {Object} oConfig Button configuration object
	 *  @returns void
	 *  @private
	 */
	"_fnCollectionHide": function ( nButton, oConfig )
	{
		if ( oConfig !== null && oConfig.sExtends == 'collection' )
		{
			return;
		}
		
		if ( this.dom.collection.collection !== null )
		{
			$(this.dom.collection.collection).animate({"opacity": 0}, 500, function (e) {
				this.style.display = "none";
			} );
			
			$(this.dom.collection.background).animate({"opacity": 0}, 500, function (e) {
				this.parentNode.removeChild( this );
			} );
			
			this.dom.collection.collection = null;
			this.dom.collection.background = null;
		}
	},
	
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Row selection functions
	 */
	
	/**
	 * Add event handlers to a table to allow for row selection
	 *  @method  _fnRowSelectConfig
	 *  @returns void
	 *  @private 
	 */
	"_fnRowSelectConfig": function ()
	{
		if ( this.s.master )
		{
			var
				that = this, 
				i, iLen, 
				dt = this.s.dt,
				aoOpenRows = this.s.dt.aoOpenRows;
			
			$(dt.nTable).addClass( this.classes.select.table );
			
			$('tr', dt.nTBody).live( 'click', function(e) {
				/* Sub-table must be ignored (odd that the selector won't do this with >) */
				if ( this.parentNode != dt.nTBody )
				{
					return;
				}
				
				/* Check that we are actually working with a DataTables controlled row */
				if ( dt.oInstance.fnGetData(this) === null )
				{
				    return;
				}

				if ( that.fnIsSelected( this ) )
				{
					that._fnRowDeselect( this, e );
				}
				else if ( that.s.select.type == "single" )
				{
					that.fnSelectNone();
					that._fnRowSelect( this, e );
				}
				else if ( that.s.select.type == "multi" )
				{
					that._fnRowSelect( this, e );
				}
			} );

			// Bind a listener to the DataTable for when new rows are created.
			// This allows rows to be visually selected when they should be and
			// deferred rendering is used.
			dt.oApi._fnCallbackReg( dt, 'aoRowCreatedCallback', function (tr, data, index) {
				if ( dt.aoData[index]._DTTT_selected ) {
					$(tr).addClass( that.classes.select.row );
				}
			}, 'TableTools-SelectAll' );
		}
	},

	/**
	 * Select rows
	 *  @param   {*} src Rows to select - see _fnSelectData for a description of valid inputs
	 *  @private 
	 */
	"_fnRowSelect": function ( src, e )
	{
		var
			that = this,
			data = this._fnSelectData( src ),
			firstTr = data.length===0 ? null : data[0].nTr,
			anSelected = [],
			i, len;

		// Get all the rows that will be selected
		for ( i=0, len=data.length ; i<len ; i++ )
		{
			if ( data[i].nTr )
			{
				anSelected.push( data[i].nTr );
			}
		}
		
		// User defined pre-selection function
		if ( this.s.select.preRowSelect !== null && !this.s.select.preRowSelect.call(this, e, anSelected, true) )
		{
			return;
		}

		// Mark them as selected
		for ( i=0, len=data.length ; i<len ; i++ )
		{
			data[i]._DTTT_selected = true;

			if ( data[i].nTr )
			{
				$(data[i].nTr).addClass( that.classes.select.row );
			}
		}

		// Post-selection function
		if ( this.s.select.postSelected !== null )
		{
			this.s.select.postSelected.call( this, anSelected );
		}

		TableTools._fnEventDispatch( this, 'select', anSelected, true );
	},

	/**
	 * Deselect rows
	 *  @param   {*} src Rows to deselect - see _fnSelectData for a description of valid inputs
	 *  @private 
	 */
	"_fnRowDeselect": function ( src, e )
	{
		var
			that = this,
			data = this._fnSelectData( src ),
			firstTr = data.length===0 ? null : data[0].nTr,
			anDeselectedTrs = [],
			i, len;

		// Get all the rows that will be deselected
		for ( i=0, len=data.length ; i<len ; i++ )
		{
			if ( data[i].nTr )
			{
				anDeselectedTrs.push( data[i].nTr );
			}
		}

		// User defined pre-selection function
		if ( this.s.select.preRowSelect !== null && !this.s.select.preRowSelect.call(this, e, anDeselectedTrs, false) )
		{
			return;
		}

		// Mark them as deselected
		for ( i=0, len=data.length ; i<len ; i++ )
		{
			data[i]._DTTT_selected = false;

			if ( data[i].nTr )
			{
				$(data[i].nTr).removeClass( that.classes.select.row );
			}
		}

		// Post-deselection function
		if ( this.s.select.postDeselected !== null )
		{
			this.s.select.postDeselected.call( this, anDeselectedTrs );
		}

		TableTools._fnEventDispatch( this, 'select', anDeselectedTrs, false );
	},
	
	/**
	 * Take a data source for row selection and convert it into aoData points for the DT
	 *   @param {*} src Can be a single DOM TR node, an array of TR nodes (including a
	 *     a jQuery object), a single aoData point from DataTables, an array of aoData
	 *     points or an array of aoData indexes
	 *   @returns {array} An array of aoData points
	 */
	"_fnSelectData": function ( src )
	{
		var out = [], pos, i, iLen;

		if ( src.nodeName )
		{
			// Single node
			pos = this.s.dt.oInstance.fnGetPosition( src );
			out.push( this.s.dt.aoData[pos] );
		}
		else if ( typeof src.length !== 'undefined' )
		{
			// jQuery object or an array of nodes, or aoData points
			for ( i=0, iLen=src.length ; i<iLen ; i++ )
			{
				if ( src[i].nodeName )
				{
					pos = this.s.dt.oInstance.fnGetPosition( src[i] );
					out.push( this.s.dt.aoData[pos] );
				}
				else if ( typeof src[i] === 'number' )
				{
					out.push( this.s.dt.aoData[ src[i] ] );
				}
				else
				{
					out.push( src[i] );
				}
			}

			return out;
		}
		else
		{
			// A single aoData point
			out.push( src );
		}

		return out;
	},
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Text button functions
	 */
	
	/**
	 * Configure a text based button for interaction events
	 *  @method  _fnTextConfig
	 *  @param   {Node} nButton Button element which is being considered
	 *  @param   {Object} oConfig Button configuration object
	 *  @returns void
	 *  @private 
	 */
	"_fnTextConfig": function ( nButton, oConfig )
	{
		var that = this;
		
		if ( oConfig.fnInit !== null )
		{
			oConfig.fnInit.call( this, nButton, oConfig );
		}
		
		if ( oConfig.sToolTip !== "" )
		{
			nButton.title = oConfig.sToolTip;
		}
		
		$(nButton).hover( function () {
			if ( oConfig.fnMouseover !== null )
			{
				oConfig.fnMouseover.call( this, nButton, oConfig, null );
			}
		}, function () {
			if ( oConfig.fnMouseout !== null )
			{
				oConfig.fnMouseout.call( this, nButton, oConfig, null );
			}
		} );
		
		if ( oConfig.fnSelect !== null )
		{
			TableTools._fnEventListen( this, 'select', function (n) {
				oConfig.fnSelect.call( that, nButton, oConfig, n );
			} );
		}
		
		$(nButton).click( function (e) {
			//e.preventDefault();
			
			if ( oConfig.fnClick !== null )
			{
				oConfig.fnClick.call( that, nButton, oConfig, null );
			}
			
			/* Provide a complete function to match the behaviour of the flash elements */
			if ( oConfig.fnComplete !== null )
			{
				oConfig.fnComplete.call( that, nButton, oConfig, null, null );
			}
			
			that._fnCollectionHide( nButton, oConfig );
		} );
	},
	
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Flash button functions
	 */
	
	/**
	 * Configure a flash based button for interaction events
	 *  @method  _fnFlashConfig
	 *  @param   {Node} nButton Button element which is being considered
	 *  @param   {o} oConfig Button configuration object
	 *  @returns void
	 *  @private 
	 */
	"_fnFlashConfig": function ( nButton, oConfig )
	{
		var that = this;
		var flash = new ZeroClipboard_TableTools.Client();
		
		if ( oConfig.fnInit !== null )
		{
			oConfig.fnInit.call( this, nButton, oConfig );
		}
		
		flash.setHandCursor( true );
		
		if ( oConfig.sAction == "flash_save" )
		{
			flash.setAction( 'save' );
			flash.setCharSet( (oConfig.sCharSet=="utf16le") ? 'UTF16LE' : 'UTF8' );
			flash.setBomInc( oConfig.bBomInc );
			flash.setFileName( oConfig.sFileName.replace('*', this.fnGetTitle(oConfig)) );
		}
		else if ( oConfig.sAction == "flash_pdf" )
		{
			flash.setAction( 'pdf' );
			flash.setFileName( oConfig.sFileName.replace('*', this.fnGetTitle(oConfig)) );
		}
		else
		{
			flash.setAction( 'copy' );
		}
		
		flash.addEventListener('mouseOver', function(client) {
			if ( oConfig.fnMouseover !== null )
			{
				oConfig.fnMouseover.call( that, nButton, oConfig, flash );
			}
		} );
		
		flash.addEventListener('mouseOut', function(client) {
			if ( oConfig.fnMouseout !== null )
			{
				oConfig.fnMouseout.call( that, nButton, oConfig, flash );
			}
		} );
		
		flash.addEventListener('mouseDown', function(client) {
			if ( oConfig.fnClick !== null )
			{
				oConfig.fnClick.call( that, nButton, oConfig, flash );
			}
		} );
		
		flash.addEventListener('complete', function (client, text) {
			if ( oConfig.fnComplete !== null )
			{
				oConfig.fnComplete.call( that, nButton, oConfig, flash, text );
			}
			that._fnCollectionHide( nButton, oConfig );
		} );
		
		this._fnFlashGlue( flash, nButton, oConfig.sToolTip );
	},
	
	
	/**
	 * Wait until the id is in the DOM before we "glue" the swf. Note that this function will call
	 * itself (using setTimeout) until it completes successfully
	 *  @method  _fnFlashGlue
	 *  @param   {Object} clip Zero clipboard object
	 *  @param   {Node} node node to glue swf to
	 *  @param   {String} text title of the flash movie
	 *  @returns void
	 *  @private 
	 */
	"_fnFlashGlue": function ( flash, node, text )
	{
		var that = this;
		var id = node.getAttribute('id');
		
		if ( document.getElementById(id) )
		{
			flash.glue( node, text );
		}
		else
		{
			setTimeout( function () {
				that._fnFlashGlue( flash, node, text );
			}, 100 );
		}
	},
	
	
	/**
	 * Set the text for the flash clip to deal with
	 * 
	 * This function is required for large information sets. There is a limit on the 
	 * amount of data that can be transferred between Javascript and Flash in a single call, so
	 * we use this method to build up the text in Flash by sending over chunks. It is estimated
	 * that the data limit is around 64k, although it is undocumented, and appears to be different
	 * between different flash versions. We chunk at 8KiB.
	 *  @method  _fnFlashSetText
	 *  @param   {Object} clip the ZeroClipboard object
	 *  @param   {String} sData the data to be set
	 *  @returns void
	 *  @private 
	 */
	"_fnFlashSetText": function ( clip, sData )
	{
		var asData = this._fnChunkData( sData, 8192 );
		
		clip.clearText();
		for ( var i=0, iLen=asData.length ; i<iLen ; i++ )
		{
			clip.appendText( asData[i] );
		}
	},
	
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Data retrieval functions
	 */
	
	/**
	 * Convert the mixed columns variable into a boolean array the same size as the columns, which
	 * indicates which columns we want to include
	 *  @method  _fnColumnTargets
	 *  @param   {String|Array} mColumns The columns to be included in data retrieval. If a string
	 *			 then it can take the value of "visible" or "hidden" (to include all visible or
	 *			 hidden columns respectively). Or an array of column indexes
	 *  @returns {Array} A boolean array the length of the columns of the table, which each value
	 *			 indicating if the column is to be included or not
	 *  @private 
	 */
	"_fnColumnTargets": function ( mColumns )
	{
		var aColumns = [];
		var dt = this.s.dt;
		
		if ( typeof mColumns == "object" )
		{
			for ( i=0, iLen=dt.aoColumns.length ; i<iLen ; i++ )
			{
				aColumns.push( false );
			}
			
			for ( i=0, iLen=mColumns.length ; i<iLen ; i++ )
			{
				aColumns[ mColumns[i] ] = true;
			}
		}
		else if ( mColumns == "visible" )
		{
			for ( i=0, iLen=dt.aoColumns.length ; i<iLen ; i++ )
			{
				aColumns.push( dt.aoColumns[i].bVisible ? true : false );
			}
		}
		else if ( mColumns == "hidden" )
		{
			for ( i=0, iLen=dt.aoColumns.length ; i<iLen ; i++ )
			{
				aColumns.push( dt.aoColumns[i].bVisible ? false : true );
			}
		}
		else if ( mColumns == "sortable" )
		{
			for ( i=0, iLen=dt.aoColumns.length ; i<iLen ; i++ )
			{
				aColumns.push( dt.aoColumns[i].bSortable ? true : false );
			}
		}
		else /* all */
		{
			for ( i=0, iLen=dt.aoColumns.length ; i<iLen ; i++ )
			{
				aColumns.push( true );
			}
		}
		
		return aColumns;
	},
	
	
	/**
	 * New line character(s) depend on the platforms
	 *  @method  method
	 *  @param   {Object} oConfig Button configuration object - only interested in oConfig.sNewLine
	 *  @returns {String} Newline character
	 */
	"_fnNewline": function ( oConfig )
	{
		if ( oConfig.sNewLine == "auto" )
		{
			return navigator.userAgent.match(/Windows/) ? "\r\n" : "\n";
		}
		else
		{
			return oConfig.sNewLine;
		}
	},
	
	
	/**
	 * Get data from DataTables' internals and format it for output
	 *  @method  _fnGetDataTablesData
	 *  @param   {Object} oConfig Button configuration object
	 *  @param   {String} oConfig.sFieldBoundary Field boundary for the data cells in the string
	 *  @param   {String} oConfig.sFieldSeperator Field separator for the data cells
	 *  @param   {String} oConfig.sNewline New line options
	 *  @param   {Mixed} oConfig.mColumns Which columns should be included in the output
	 *  @param   {Boolean} oConfig.bHeader Include the header
	 *  @param   {Boolean} oConfig.bFooter Include the footer
	 *  @param   {Boolean} oConfig.bSelectedOnly Include only the selected rows in the output
	 *  @returns {String} Concatenated string of data
	 *  @private 
	 */
	"_fnGetDataTablesData": function ( oConfig )
	{
		var i, iLen, j, jLen;
		var aRow, aData=[], sLoopData='', arr;
		var dt = this.s.dt, tr, child;
		var regex = new RegExp(oConfig.sFieldBoundary, "g"); /* Do it here for speed */
		var aColumnsInc = this._fnColumnTargets( oConfig.mColumns );
		var bSelectedOnly = (typeof oConfig.bSelectedOnly != 'undefined') ? oConfig.bSelectedOnly : false;
		
		/*
		 * Header
		 */
		if ( oConfig.bHeader )
		{
			aRow = [];
			
			for ( i=0, iLen=dt.aoColumns.length ; i<iLen ; i++ )
			{
				if ( aColumnsInc[i] )
				{
					sLoopData = dt.aoColumns[i].sTitle.replace(/\n/g," ").replace( /<.*?>/g, "" ).replace(/^\s+|\s+$/g,"");
					sLoopData = this._fnHtmlDecode( sLoopData );
					
					aRow.push( this._fnBoundData( sLoopData, oConfig.sFieldBoundary, regex ) );
				}
			}

			aData.push( aRow.join(oConfig.sFieldSeperator) );
		}
		
		/*
		 * Body
		 */
		var aDataIndex = dt.aiDisplay;
		var aSelected = this.fnGetSelected();
		if ( this.s.select.type !== "none" && bSelectedOnly && aSelected.length !== 0 )
		{
			aDataIndex = [];
			for ( i=0, iLen=aSelected.length ; i<iLen ; i++ )
			{
				aDataIndex.push( dt.oInstance.fnGetPosition( aSelected[i] ) );
			}
		}
		
		for ( j=0, jLen=aDataIndex.length ; j<jLen ; j++ )
		{
			tr = dt.aoData[ aDataIndex[j] ].nTr;
			aRow = [];
			
			/* Columns */
			for ( i=0, iLen=dt.aoColumns.length ; i<iLen ; i++ )
			{
				if ( aColumnsInc[i] )
				{
					/* Convert to strings (with small optimisation) */
					var mTypeData = dt.oApi._fnGetCellData( dt, aDataIndex[j], i, 'display' );
					if ( oConfig.fnCellRender )
					{
						sLoopData = oConfig.fnCellRender( mTypeData, i, tr, aDataIndex[j] )+"";
					}
					else if ( typeof mTypeData == "string" )
					{
						/* Strip newlines, replace img tags with alt attr. and finally strip html... */
						sLoopData = mTypeData.replace(/\n/g," ");
						sLoopData =
						 	sLoopData.replace(/<img.*?\s+alt\s*=\s*(?:"([^"]+)"|'([^']+)'|([^\s>]+)).*?>/gi,
						 		'$1$2$3');
						sLoopData = sLoopData.replace( /<.*?>/g, "" );
					}
					else
					{
						sLoopData = mTypeData+"";
					}
					
					/* Trim and clean the data */
					sLoopData = sLoopData.replace(/^\s+/, '').replace(/\s+$/, '');
					sLoopData = this._fnHtmlDecode( sLoopData );
					
					/* Bound it and add it to the total data */
					aRow.push( this._fnBoundData( sLoopData, oConfig.sFieldBoundary, regex ) );
				}
			}
      
			aData.push( aRow.join(oConfig.sFieldSeperator) );
      
			/* Details rows from fnOpen */
			if ( oConfig.bOpenRows )
			{
				arr = $.grep(dt.aoOpenRows, function(o) { return o.nParent === tr; });
				
				if ( arr.length === 1 )
				{
					sLoopData = this._fnBoundData( $('td', arr[0].nTr).html(), oConfig.sFieldBoundary, regex );
					aData.push( sLoopData );
				}
			}
		}
		
		/*
		 * Footer
		 */
		if ( oConfig.bFooter && dt.nTFoot !== null )
		{
			aRow = [];
			
			for ( i=0, iLen=dt.aoColumns.length ; i<iLen ; i++ )
			{
				if ( aColumnsInc[i] && dt.aoColumns[i].nTf !== null )
				{
					sLoopData = dt.aoColumns[i].nTf.innerHTML.replace(/\n/g," ").replace( /<.*?>/g, "" );
					sLoopData = this._fnHtmlDecode( sLoopData );
					
					aRow.push( this._fnBoundData( sLoopData, oConfig.sFieldBoundary, regex ) );
				}
			}
			
			aData.push( aRow.join(oConfig.sFieldSeperator) );
		}
		
		_sLastData = aData.join( this._fnNewline(oConfig) );
		return _sLastData;
	},
	
	
	/**
	 * Wrap data up with a boundary string
	 *  @method  _fnBoundData
	 *  @param   {String} sData data to bound
	 *  @param   {String} sBoundary bounding char(s)
	 *  @param   {RegExp} regex search for the bounding chars - constructed outside for efficiency
	 *			 in the loop
	 *  @returns {String} bound data
	 *  @private 
	 */
	"_fnBoundData": function ( sData, sBoundary, regex )
	{
		if ( sBoundary === "" )
		{
			return sData;
		}
		else
		{
			return sBoundary + sData.replace(regex, sBoundary+sBoundary) + sBoundary;
		}
	},
	
	
	/**
	 * Break a string up into an array of smaller strings
	 *  @method  _fnChunkData
	 *  @param   {String} sData data to be broken up
	 *  @param   {Int} iSize chunk size
	 *  @returns {Array} String array of broken up text
	 *  @private 
	 */
	"_fnChunkData": function ( sData, iSize )
	{
		var asReturn = [];
		var iStrlen = sData.length;
		
		for ( var i=0 ; i<iStrlen ; i+=iSize )
		{
			if ( i+iSize < iStrlen )
			{
				asReturn.push( sData.substring( i, i+iSize ) );
			}
			else
			{
				asReturn.push( sData.substring( i, iStrlen ) );
			}
		}
		
		return asReturn;
	},
	
	
	/**
	 * Decode HTML entities
	 *  @method  _fnHtmlDecode
	 *  @param   {String} sData encoded string
	 *  @returns {String} decoded string
	 *  @private 
	 */
	"_fnHtmlDecode": function ( sData )
	{
		if ( sData.indexOf('&') === -1 )
		{
			return sData;
		}
		
		var n = document.createElement('div');

		return sData.replace( /&([^\s]*);/g, function( match, match2 ) {
			if ( match.substr(1, 1) === '#' )
			{
				return String.fromCharCode( Number(match2.substr(1)) );
			}
			else
			{
				n.innerHTML = match;
				return n.childNodes[0].nodeValue;
			}
		} );
	},
	
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Printing functions
	 */
	
	/**
	 * Show print display
	 *  @method  _fnPrintStart
	 *  @param   {Event} e Event object
	 *  @param   {Object} oConfig Button configuration object
	 *  @returns void
	 *  @private 
	 */
	"_fnPrintStart": function ( oConfig )
	{
	  var that = this;
	  var oSetDT = this.s.dt;
	  
		/* Parse through the DOM hiding everything that isn't needed for the table */
		this._fnPrintHideNodes( oSetDT.nTable );
		
		/* Show the whole table */
		this.s.print.saveStart = oSetDT._iDisplayStart;
		this.s.print.saveLength = oSetDT._iDisplayLength;

		if ( oConfig.bShowAll )
		{
			oSetDT._iDisplayStart = 0;
			oSetDT._iDisplayLength = -1;
			oSetDT.oApi._fnCalculateEnd( oSetDT );
			oSetDT.oApi._fnDraw( oSetDT );
		}
		
		/* Adjust the display for scrolling which might be done by DataTables */
		if ( oSetDT.oScroll.sX !== "" || oSetDT.oScroll.sY !== "" )
		{
			this._fnPrintScrollStart( oSetDT );

			// If the table redraws while in print view, the DataTables scrolling
			// setup would hide the header, so we need to readd it on draw
			$(this.s.dt.nTable).bind('draw.DTTT_Print', function () {
				that._fnPrintScrollStart( oSetDT );
			} );
		}
		
		/* Remove the other DataTables feature nodes - but leave the table! and info div */
		var anFeature = oSetDT.aanFeatures;
		for ( var cFeature in anFeature )
		{
			if ( cFeature != 'i' && cFeature != 't' && cFeature.length == 1 )
			{
				for ( var i=0, iLen=anFeature[cFeature].length ; i<iLen ; i++ )
				{
					this.dom.print.hidden.push( {
						"node": anFeature[cFeature][i],
						"display": "block"
					} );
					anFeature[cFeature][i].style.display = "none";
				}
			}
		}
		
		/* Print class can be used for styling */
		$(document.body).addClass( this.classes.print.body );

		/* Show information message to let the user know what is happening */
		if ( oConfig.sInfo !== "" )
		{
			this.fnInfo( oConfig.sInfo, 3000 );
		}

		/* Add a message at the top of the page */
		if ( oConfig.sMessage )
		{
			this.dom.print.message = document.createElement( "div" );
			this.dom.print.message.className = this.classes.print.message;
			this.dom.print.message.innerHTML = oConfig.sMessage;
			document.body.insertBefore( this.dom.print.message, document.body.childNodes[0] );
		}
		
		/* Cache the scrolling and the jump to the top of the page */
		this.s.print.saveScroll = $(window).scrollTop();
		window.scrollTo( 0, 0 );

		/* Bind a key event listener to the document for the escape key -
		 * it is removed in the callback
		 */
		$(document).bind( "keydown.DTTT", function(e) {
			/* Only interested in the escape key */
			if ( e.keyCode == 27 )
			{
				e.preventDefault();
				that._fnPrintEnd.call( that, e );
			}
		} );
	},
	
	
	/**
	 * Printing is finished, resume normal display
	 *  @method  _fnPrintEnd
	 *  @param   {Event} e Event object
	 *  @returns void
	 *  @private 
	 */
	"_fnPrintEnd": function ( e )
	{
		var that = this;
		var oSetDT = this.s.dt;
		var oSetPrint = this.s.print;
		var oDomPrint = this.dom.print;
		
		/* Show all hidden nodes */
		this._fnPrintShowNodes();
		
		/* Restore DataTables' scrolling */
		if ( oSetDT.oScroll.sX !== "" || oSetDT.oScroll.sY !== "" )
		{
			$(this.s.dt.nTable).unbind('draw.DTTT_Print');

			this._fnPrintScrollEnd();
		}
		
		/* Restore the scroll */
		window.scrollTo( 0, oSetPrint.saveScroll );
		
		/* Drop the print message */
		if ( oDomPrint.message !== null )
		{
			document.body.removeChild( oDomPrint.message );
			oDomPrint.message = null;
		}
		
		/* Styling class */
		$(document.body).removeClass( 'DTTT_Print' );
		
		/* Restore the table length */
		oSetDT._iDisplayStart = oSetPrint.saveStart;
		oSetDT._iDisplayLength = oSetPrint.saveLength;
		oSetDT.oApi._fnCalculateEnd( oSetDT );
		oSetDT.oApi._fnDraw( oSetDT );
		
		$(document).unbind( "keydown.DTTT" );
	},
	
	
	/**
	 * Take account of scrolling in DataTables by showing the full table
	 *  @returns void
	 *  @private 
	 */
	"_fnPrintScrollStart": function ()
	{
		var 
			oSetDT = this.s.dt,
			nScrollHeadInner = oSetDT.nScrollHead.getElementsByTagName('div')[0],
			nScrollHeadTable = nScrollHeadInner.getElementsByTagName('table')[0],
			nScrollBody = oSetDT.nTable.parentNode;

		/* Copy the header in the thead in the body table, this way we show one single table when
		 * in print view. Note that this section of code is more or less verbatim from DT 1.7.0
		 */
		var nTheadSize = oSetDT.nTable.getElementsByTagName('thead');
		if ( nTheadSize.length > 0 )
		{
			oSetDT.nTable.removeChild( nTheadSize[0] );
		}
		
		if ( oSetDT.nTFoot !== null )
		{
			var nTfootSize = oSetDT.nTable.getElementsByTagName('tfoot');
			if ( nTfootSize.length > 0 )
			{
				oSetDT.nTable.removeChild( nTfootSize[0] );
			}
		}
		
		nTheadSize = oSetDT.nTHead.cloneNode(true);
		oSetDT.nTable.insertBefore( nTheadSize, oSetDT.nTable.childNodes[0] );
		
		if ( oSetDT.nTFoot !== null )
		{
			nTfootSize = oSetDT.nTFoot.cloneNode(true);
			oSetDT.nTable.insertBefore( nTfootSize, oSetDT.nTable.childNodes[1] );
		}
		
		/* Now adjust the table's viewport so we can actually see it */
		if ( oSetDT.oScroll.sX !== "" )
		{
			oSetDT.nTable.style.width = $(oSetDT.nTable).outerWidth()+"px";
			nScrollBody.style.width = $(oSetDT.nTable).outerWidth()+"px";
			nScrollBody.style.overflow = "visible";
		}
		
		if ( oSetDT.oScroll.sY !== "" )
		{
			nScrollBody.style.height = $(oSetDT.nTable).outerHeight()+"px";
			nScrollBody.style.overflow = "visible";
		}
	},
	
	
	/**
	 * Take account of scrolling in DataTables by showing the full table. Note that the redraw of
	 * the DataTable that we do will actually deal with the majority of the hard work here
	 *  @returns void
	 *  @private 
	 */
	"_fnPrintScrollEnd": function ()
	{
		var 
			oSetDT = this.s.dt,
			nScrollBody = oSetDT.nTable.parentNode;
		
		if ( oSetDT.oScroll.sX !== "" )
		{
			nScrollBody.style.width = oSetDT.oApi._fnStringToCss( oSetDT.oScroll.sX );
			nScrollBody.style.overflow = "auto";
		}
		
		if ( oSetDT.oScroll.sY !== "" )
		{
			nScrollBody.style.height = oSetDT.oApi._fnStringToCss( oSetDT.oScroll.sY );
			nScrollBody.style.overflow = "auto";
		}
	},
	
	
	/**
	 * Resume the display of all TableTools hidden nodes
	 *  @method  _fnPrintShowNodes
	 *  @returns void
	 *  @private 
	 */
	"_fnPrintShowNodes": function ( )
	{
	  var anHidden = this.dom.print.hidden;
	  
		for ( var i=0, iLen=anHidden.length ; i<iLen ; i++ )
		{
			anHidden[i].node.style.display = anHidden[i].display;
		}
		anHidden.splice( 0, anHidden.length );
	},
	
	
	/**
	 * Hide nodes which are not needed in order to display the table. Note that this function is
	 * recursive
	 *  @method  _fnPrintHideNodes
	 *  @param   {Node} nNode Element which should be showing in a 'print' display
	 *  @returns void
	 *  @private 
	 */
	"_fnPrintHideNodes": function ( nNode )
	{
	  var anHidden = this.dom.print.hidden;
	  
		var nParent = nNode.parentNode;
		var nChildren = nParent.childNodes;
		for ( var i=0, iLen=nChildren.length ; i<iLen ; i++ )
		{
			if ( nChildren[i] != nNode && nChildren[i].nodeType == 1 )
			{
				/* If our node is shown (don't want to show nodes which were previously hidden) */
				var sDisplay = $(nChildren[i]).css("display");
			 	if ( sDisplay != "none" )
				{
					/* Cache the node and it's previous state so we can restore it */
					anHidden.push( {
						"node": nChildren[i],
						"display": sDisplay
					} );
					nChildren[i].style.display = "none";
				}
			}
		}
		
		if ( nParent.nodeName != "BODY" )
		{
			this._fnPrintHideNodes( nParent );
		}
	}
};



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Static variables
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Store of all instances that have been created of TableTools, so one can look up other (when
 * there is need of a master)
 *  @property _aInstances
 *  @type	 Array
 *  @default  []
 *  @private
 */
TableTools._aInstances = [];


/**
 * Store of all listeners and their callback functions
 *  @property _aListeners
 *  @type	 Array
 *  @default  []
 */
TableTools._aListeners = [];



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Static methods
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Get an array of all the master instances
 *  @method  fnGetMasters
 *  @returns {Array} List of master TableTools instances
 *  @static
 */
TableTools.fnGetMasters = function ()
{
	var a = [];
	for ( var i=0, iLen=TableTools._aInstances.length ; i<iLen ; i++ )
	{
		if ( TableTools._aInstances[i].s.master )
		{
			a.push( TableTools._aInstances[i] );
		}
	}
	return a;
};

/**
 * Get the master instance for a table node (or id if a string is given)
 *  @method  fnGetInstance
 *  @returns {Object} ID of table OR table node, for which we want the TableTools instance
 *  @static
 */
TableTools.fnGetInstance = function ( node )
{
	if ( typeof node != 'object' )
	{
		node = document.getElementById(node);
	}
	
	for ( var i=0, iLen=TableTools._aInstances.length ; i<iLen ; i++ )
	{
		if ( TableTools._aInstances[i].s.master && TableTools._aInstances[i].dom.table == node )
		{
			return TableTools._aInstances[i];
		}
	}
	return null;
};


/**
 * Add a listener for a specific event
 *  @method  _fnEventListen
 *  @param   {Object} that Scope of the listening function (i.e. 'this' in the caller)
 *  @param   {String} type Event type
 *  @param   {Function} fn Function
 *  @returns void
 *  @private
 *  @static
 */
TableTools._fnEventListen = function ( that, type, fn )
{
	TableTools._aListeners.push( {
		"that": that,
		"type": type,
		"fn": fn
	} );
};
	

/**
 * An event has occurred - look up every listener and fire it off. We check that the event we are
 * going to fire is attached to the same table (using the table node as reference) before firing
 *  @method  _fnEventDispatch
 *  @param   {Object} that Scope of the listening function (i.e. 'this' in the caller)
 *  @param   {String} type Event type
 *  @param   {Node} node Element that the event occurred on (may be null)
 *  @param   {boolean} [selected] Indicate if the node was selected (true) or deselected (false)
 *  @returns void
 *  @private
 *  @static
 */
TableTools._fnEventDispatch = function ( that, type, node, selected )
{
	var listeners = TableTools._aListeners;
	for ( var i=0, iLen=listeners.length ; i<iLen ; i++ )
	{
		if ( that.dom.table == listeners[i].that.dom.table && listeners[i].type == type )
		{
			listeners[i].fn( node, selected );
		}
	}
};






/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Constants
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



TableTools.buttonBase = {
	// Button base
	"sAction": "text",
	"sTag": "default",
	"sLinerTag": "default",
	"sButtonClass": "DTTT_button_text",
	"sButtonText": "Button text",
	"sTitle": "",
	"sToolTip": "",

	// Common button specific options
	"sCharSet": "utf8",
	"bBomInc": false,
	"sFileName": "*.csv",
	"sFieldBoundary": "",
	"sFieldSeperator": "\t",
	"sNewLine": "auto",
	"mColumns": "all", /* "all", "visible", "hidden" or array of column integers */
	"bHeader": true,
	"bFooter": true,
	"bOpenRows": false,
	"bSelectedOnly": false,

	// Callbacks
	"fnMouseover": null,
	"fnMouseout": null,
	"fnClick": null,
	"fnSelect": null,
	"fnComplete": null,
	"fnInit": null,
	"fnCellRender": null
};


/**
 * @namespace Default button configurations
 */
TableTools.BUTTONS = {
	"csv": $.extend( {}, TableTools.buttonBase, {
		"sAction": "flash_save",
		"sButtonClass": "DTTT_button_csv",
		"sButtonText": "CSV",
		"sFieldBoundary": '"',
		"sFieldSeperator": ",",
		"fnClick": function( nButton, oConfig, flash ) {
			this.fnSetText( flash, this.fnGetTableData(oConfig) );
		}
	} ),

	"xls": $.extend( {}, TableTools.buttonBase, {
		"sAction": "flash_save",
		"sCharSet": "utf16le",
		"bBomInc": true,
		"sButtonClass": "DTTT_button_xls",
		"sButtonText": "Excel",
		"fnClick": function( nButton, oConfig, flash ) {
			this.fnSetText( flash, this.fnGetTableData(oConfig) );
		}
	} ),

	"copy": $.extend( {}, TableTools.buttonBase, {
		"sAction": "flash_copy",
		"sButtonClass": "DTTT_button_copy",
		"sButtonText": "Copy",
		"fnClick": function( nButton, oConfig, flash ) {
			this.fnSetText( flash, this.fnGetTableData(oConfig) );
		},
		"fnComplete": function(nButton, oConfig, flash, text) {
			var
				lines = text.split('\n').length,
				len = this.s.dt.nTFoot === null ? lines-1 : lines-2,
				plural = (len==1) ? "" : "s";
			this.fnInfo( '<h6>Table copied</h6>'+
				'<p>Copied '+len+' row'+plural+' to the clipboard.</p>',
				1500
			);
		}
	} ),

	"pdf": $.extend( {}, TableTools.buttonBase, {
		"sAction": "flash_pdf",
		"sNewLine": "\n",
		"sFileName": "*.pdf",
		"sButtonClass": "DTTT_button_pdf",
		"sButtonText": "PDF",
		"sPdfOrientation": "portrait",
		"sPdfSize": "A4",
		"sPdfMessage": "",
		"fnClick": function( nButton, oConfig, flash ) {
			this.fnSetText( flash, 
				"title:"+ this.fnGetTitle(oConfig) +"\n"+
				"message:"+ oConfig.sPdfMessage +"\n"+
				"colWidth:"+ this.fnCalcColRatios(oConfig) +"\n"+
				"orientation:"+ oConfig.sPdfOrientation +"\n"+
				"size:"+ oConfig.sPdfSize +"\n"+
				"--/TableToolsOpts--\n" +
				this.fnGetTableData(oConfig)
			);
		}
	} ),

	"print": $.extend( {}, TableTools.buttonBase, {
		"sInfo": "<h6>Print view</h6><p>Please use your browser's print function to "+
		  "print this table. Press escape when finished.",
		"sMessage": null,
		"bShowAll": true,
		"sToolTip": "View print view",
		"sButtonClass": "DTTT_button_print",
		"sButtonText": "Print",
		"fnClick": function ( nButton, oConfig ) {
			this.fnPrint( true, oConfig );
		}
	} ),

	"text": $.extend( {}, TableTools.buttonBase ),

	"select": $.extend( {}, TableTools.buttonBase, {
		"sButtonText": "Select button",
		"fnSelect": function( nButton, oConfig ) {
			if ( this.fnGetSelected().length !== 0 ) {
				$(nButton).removeClass( this.classes.buttons.disabled );
			} else {
				$(nButton).addClass( this.classes.buttons.disabled );
			}
		},
		"fnInit": function( nButton, oConfig ) {
			$(nButton).addClass( this.classes.buttons.disabled );
		}
	} ),

	"select_single": $.extend( {}, TableTools.buttonBase, {
		"sButtonText": "Select button",
		"fnSelect": function( nButton, oConfig ) {
			var iSelected = this.fnGetSelected().length;
			if ( iSelected == 1 ) {
				$(nButton).removeClass( this.classes.buttons.disabled );
			} else {
				$(nButton).addClass( this.classes.buttons.disabled );
			}
		},
		"fnInit": function( nButton, oConfig ) {
			$(nButton).addClass( this.classes.buttons.disabled );
		}
	} ),

	"select_all": $.extend( {}, TableTools.buttonBase, {
		"sButtonText": "Select all",
		"fnClick": function( nButton, oConfig ) {
			this.fnSelectAll();
		},
		"fnSelect": function( nButton, oConfig ) {
			if ( this.fnGetSelected().length == this.s.dt.fnRecordsDisplay() ) {
				$(nButton).addClass( this.classes.buttons.disabled );
			} else {
				$(nButton).removeClass( this.classes.buttons.disabled );
			}
		}
	} ),

	"select_none": $.extend( {}, TableTools.buttonBase, {
		"sButtonText": "Deselect all",
		"fnClick": function( nButton, oConfig ) {
			this.fnSelectNone();
		},
		"fnSelect": function( nButton, oConfig ) {
			if ( this.fnGetSelected().length !== 0 ) {
				$(nButton).removeClass( this.classes.buttons.disabled );
			} else {
				$(nButton).addClass( this.classes.buttons.disabled );
			}
		},
		"fnInit": function( nButton, oConfig ) {
			$(nButton).addClass( this.classes.buttons.disabled );
		}
	} ),

	"ajax": $.extend( {}, TableTools.buttonBase, {
		"sAjaxUrl": "/xhr.php",
		"sButtonText": "Ajax button",
		"fnClick": function( nButton, oConfig ) {
			var sData = this.fnGetTableData(oConfig);
			$.ajax( {
				"url": oConfig.sAjaxUrl,
				"data": [
					{ "name": "tableData", "value": sData }
				],
				"success": oConfig.fnAjaxComplete,
				"dataType": "json",
				"type": "POST", 
				"cache": false,
				"error": function () {
					alert( "Error detected when sending table data to server" );
				}
			} );
		},
		"fnAjaxComplete": function( json ) {
			alert( 'Ajax complete' );
		}
	} ),

	"div": $.extend( {}, TableTools.buttonBase, {
		"sAction": "div",
		"sTag": "div",
		"sButtonClass": "DTTT_nonbutton",
		"sButtonText": "Text button"
	} ),

	"collection": $.extend( {}, TableTools.buttonBase, {
		"sAction": "collection",
		"sButtonClass": "DTTT_button_collection",
		"sButtonText": "Collection",
		"fnClick": function( nButton, oConfig ) {
			this._fnCollectionShow(nButton, oConfig);
		}
	} )
};
/*
 *  on* callback parameters:
 *  	1. node - button element
 *  	2. object - configuration object for this button
 *  	3. object - ZeroClipboard reference (flash button only)
 *  	4. string - Returned string from Flash (flash button only - and only on 'complete')
 */



/**
 * @namespace Classes used by TableTools - allows the styles to be override easily.
 *   Note that when TableTools initialises it will take a copy of the classes object
 *   and will use its internal copy for the remainder of its run time.
 */
TableTools.classes = {
	"container": "DTTT_container",
	"buttons": {
		"normal": "DTTT_button",
		"disabled": "DTTT_disabled"
	},
	"collection": {
		"container": "DTTT_collection",
		"background": "DTTT_collection_background",
		"buttons": {
			"normal": "DTTT_button",
			"disabled": "DTTT_disabled"
		}
	},
	"select": {
		"table": "DTTT_selectable",
		"row": "DTTT_selected"
	},
	"print": {
		"body": "DTTT_Print",
		"info": "DTTT_print_info",
		"message": "DTTT_PrintMessage"
	}
};


/**
 * @namespace ThemeRoller classes - built in for compatibility with DataTables' 
 *   bJQueryUI option.
 */
TableTools.classes_themeroller = {
	"container": "DTTT_container ui-buttonset ui-buttonset-multi",
	"buttons": {
		"normal": "DTTT_button ui-button ui-state-default"
	},
	"collection": {
		"container": "DTTT_collection ui-buttonset ui-buttonset-multi"
	}
};


/**
 * @namespace TableTools default settings for initialisation
 */
TableTools.DEFAULTS = {
	"sSwfPath":        "media/swf/copy_csv_xls_pdf.swf",
	"sRowSelect":      "none",
	"sSelectedClass":  null,
	"fnPreRowSelect":  null,
	"fnRowSelected":   null,
	"fnRowDeselected": null,
	"aButtons":        [ "copy", "csv", "xls", "pdf", "print" ],
	"oTags": {
		"container": "div",
		"button": "a", // We really want to use buttons here, but Firefox and IE ignore the
		                 // click on the Flash element in the button (but not mouse[in|out]).
		"liner": "span",
		"collection": {
			"container": "div",
			"button": "a",
			"liner": "span"
		}
	}
};


/**
 * Name of this class
 *  @constant CLASS
 *  @type	 String
 *  @default  TableTools
 */
TableTools.prototype.CLASS = "TableTools";


/**
 * TableTools version
 *  @constant  VERSION
 *  @type	  String
 *  @default   See code
 */
TableTools.VERSION = "2.1.4";
TableTools.prototype.VERSION = TableTools.VERSION;




/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Initialisation
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 * Register a new feature with DataTables
 */
if ( typeof $.fn.dataTable == "function" &&
	 typeof $.fn.dataTableExt.fnVersionCheck == "function" &&
	 $.fn.dataTableExt.fnVersionCheck('1.9.0') )
{
	$.fn.dataTableExt.aoFeatures.push( {
		"fnInit": function( oDTSettings ) {
			var oOpts = typeof oDTSettings.oInit.oTableTools != 'undefined' ? 
				oDTSettings.oInit.oTableTools : {};
			
			var oTT = new TableTools( oDTSettings.oInstance, oOpts );
			TableTools._aInstances.push( oTT );
			
			return oTT.dom.container;
		},
		"cFeature": "T",
		"sFeature": "TableTools"
	} );
}
else
{
	alert( "Warning: TableTools 2 requires DataTables 1.9.0 or newer - www.datatables.net/download");
}

$.fn.DataTable.TableTools = TableTools;

})(jQuery, window, document);



$(document).ready(function () {


    var isnodatatable = '';
    isnodatatable = $('.EnetReportControl').attr('data-ewf-nodatatable');

    if (isnodatatable == 0 || isNaN(isnodatatable)) {

        //Setup exort excel flash location
        var pathArray, protocol, host;

        pathArray = window.location.href.split('/');
        protocol = pathArray[0];
        host = pathArray[2];

        var sSwfPath = "//" + host + "/CStoreProAdmin/Resources/copy_csv_xls_pdf.swf";

        //Hide report control default print and export options
//                $('.enetReportToolBox').hide();

        var noOfCol = 0;
        var objIndex = [];
        var aryJSONColTable = [];
        noOfCol = $('.EnetReportControl > tbody tr:first td').length
        if (isNaN(noOfCol)) { noOfCol = 0 }

        for (var i = 0; i < noOfCol; i++) {
            datatype = $('.EnetReportControl > thead tr:first th:eq(' + i + ')').attr('data-ewf-datatype');
            if (typeof datatype === "undefined") {
                datatype = $('.EnetReportControl > thead tr:nth-child(2) th:eq(' + i + ')').attr('data-ewf-datatype');     //NOTES: Temporary fix to accomodate for gas report where we have 2 rows as header have to find a better way to do this
                if (typeof datatype === "undefined") {
                    datatype = "";
                    datatype = $('.EnetReportControl > thead tr:nth-child(3) th:eq(' + i + ')').attr('data-ewf-datatype');     //NOTES: Temporary fix to accomodate for gas report where we have 3 rows as header have to find a better way to do this
                    if (typeof datatype === "undefined") {
                        datatype = "";
                    }
                }
            }

            if (datatype.toLowerCase() === "date") { //Find out the datatype for that column
                aryJSONColTable.push({ "sType": "day" });
            }
            else if (datatype.toLowerCase() === "currency") { //Find out the datatype for that column
                aryJSONColTable.push({ "sType": "currency" });
            }
            else {
                aryJSONColTable.push(null);
            }
        }

        //Plugin to auto Detect numbers embedded in HTML 
        jQuery.fn.dataTableExt.aTypes.unshift(function (sData) {
            sData = typeof sData.replace == 'function' ?
        sData.replace(/<[\s\S]*?>/g, "") : sData;
            sData = $.trim(sData);

            var sValidFirstChars = "0123456789-";
            var sValidChars = "0123456789.";
            var Char;
            var bDecimal = false;

            /* Check for a valid first char (no period and allow negatives) */
            Char = sData.charAt(0);
            if (sValidFirstChars.indexOf(Char) == -1) {
                return null;
            }

            /* Check all the other characters are valid */
            for (var i = 1; i < sData.length; i++) {
                Char = sData.charAt(i);
                if (sValidChars.indexOf(Char) == -1) {
                    return null;
                }

                /* Only allowed one decimal place... */
                if (Char == ".") {
                    if (bDecimal) {
                        return null;
                    }
                    bDecimal = true;
                }
            }

            return 'num-html';
        });

        //Plugin to auto Detect currency
        jQuery.fn.dataTableExt.aTypes.unshift(function (sData) {
            sData = typeof sData.replace == 'function' ?
        sData.replace(/<[\s\S]*?>/g, "") : sData;
            sData = $.trim(sData);

            var sValidFirstChars = "$£€c";
            var sValidChars = "$£€c " + "0123456789" + ".-,'"; "0123456789.";
            var Char;
            var bDecimal = false;

            /* Check for a valid first char (no period and allow negatives) */
            Char = sData.charAt(0);
            if (sValidFirstChars.indexOf(Char) == -1) {
                return null;
            }

            /* Check all the other characters are valid */
            for (var i = 1; i < sData.length; i++) {
                Char = sData.charAt(i);
                if (sValidChars.indexOf(Char) == -1) {
                    return null;
                }

                /* Only allowed one decimal place... */
                if (Char == ".") {
                    if (bDecimal) {
                        return null;
                    }
                    bDecimal = true;
                }
            }

            return 'currency';
        });

        jQuery.fn.dataTableExt.aTypes.unshift(
           function (sData) {
               var sValidChars = "0123456789.-,";
               var Char;

               /* Check the numeric part */
               for (i = 0; i < sData.length - 1; i++) {
                   Char = sData.charAt(i);
                   if (sValidChars.indexOf(Char) == -1) {
                       return null;
                   }
               }

               /* Check sufix % */
               if (sData.charAt(sData.length - 1) == "%") {
                   return 'percent';
               }
               return null;
           }
        );


        //Plugin to Sort numbers embedded in HTML
        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "num-html-pre": function (a) {
                var x = String(a).replace(/<[\s\S]*?>/g, "");
                return parseFloat(x);
            },

            "num-html-asc": function (a, b) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "num-html-desc": function (a, b) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            },

            "day-pre": function (a) {
                a = $(a).text();

                var tempa;
                tempa = new Date(a);

                a = ('0' + tempa.getDate()).slice(-2) + '/'
             + ('0' + (tempa.getMonth() + 1)).slice(-2) + '/'
             + tempa.getFullYear();


                var daya = a.split('/');

                var x = (daya[2] + daya[1] + daya[0]) * 1;

                return x;
            },

            "day-asc": function (a, b) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "day-desc": function (a, b) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            },

            "currency-pre": function (a) {
                //notes: added this to avoid error on cases where
                // element "a" had html embedded in it
                //remove commas
                a = a.replace(/,/g, '');
                if ($(a).html() != null) {
                    b = $(a).text();
                }
                else {
                    b = a;
                }
                if (b != "") a = b;
                return accounting.unformat(a);
            },

            "currency-asc": function (a, b) {
                return a - b;
            },

            "currency-desc": function (a, b) {
                return b - a;
            },

            "percent-asc": function (a, b) {
                var x = a == "-" ? 0 : a.replace(/\./g, "");
                var y = b == "-" ? 0 : b.replace(/\./g, "");
                x = x.replace(/,/, ".");
                y = y.replace(/,/, ".");
                x = x.replace(/%/, "");
                y = y.replace(/%/, "");

                x = parseFloat(x);
                y = parseFloat(y);
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            },

            "percent-desc": function (a, b) {
                var x = a == "-" ? 0 : a.replace(/\./g, "");
                var y = b == "-" ? 0 : b.replace(/\./g, "");
                x = x.replace(/,/, ".");
                y = y.replace(/,/, ".");
                x = x.replace(/%/, "");
                y = y.replace(/%/, "");

                x = parseFloat(x);
                y = parseFloat(y);
                return ((x < y) ? 1 : ((x > y) ? -1 : 0));
            }

        });


        oTable = $('.EnetReportControl').dataTable({
            "bAutoWidth": false
            , "bPaginate": true
            , "sDom": 'Tr<"EWF-table-responsive"t>p'
            //, "sDom": 'T<"clear">rtp'
            , "iDisplayLength": 50
            , "sPaginationType": "full_numbers"
            , "aoColumns": aryJSONColTable
            , "oTableTools": {
                "sSwfPath": sSwfPath, "aButtons": [{
                    "sExtends": "xls",
                    "sButtonText": "Excel",
                    "sFileName": "CStoreProReport.xls"
                }
                ]
            }
        });

//        $('.DTTT_container').hide();

//        new $.fn.dataTable.Buttons(oTable, {
//            buttons: [
//                'copy', 'excel', 'pdf'
//            ]
//        });

    }




    $(document).ready(function () {
        if ($('#divUpgradeNotice').length > 0) {
            $('#divUpgradeNotice').modal('show');
        }
    });

});


$('.EJS-Rep-Export').click(function (e) {

    isnodatatable = $('.EnetReportControl').attr('data-ewf-nodatatable');
    if (isnodatatable == 0 || isNaN(isnodatatable)) {
//        alert('datatable excel');
//        $('#ToolTables_frmItemSalesLogOpForm_enetTable_0').trigger( "click" );
    }
    else {
        e.preventDefault();
        tableToExcel($(this).attr('form') + '_enetTable', 'CStore Pro Report');
    }
});




//====================================================================
//  Report filter show or hide according to report view
//====================================================================

$(".EJS-ReportSelector").each(function (index) {
    EJSReportSelectorChange(this);
});
$(".EJS-ReportSelector").change(function () {
    EJSReportSelectorChange(this);
});
function EJSReportSelectorChange(inObj) {
    var strReportType = $(inObj).val().toLowerCase();
    $(inObj).closest("form").find('*[data-DRF-ReportType]').each(function (index) {
        if ($(this).attr('data-DRF-ReportType').toLowerCase() == strReportType) {
            $(this).show();
        }
        else {
            $(this).hide();
        }
    });
}


//==================================
//  Report Header Params on print
//==================================
// filter if there is a  report control in this page
populatePrintParams();
function populatePrintParams()
{
    if ($('#enetReportIsPostback').val() == 'True') {
        var OpFormName = $('#enetReportIsPostback').attr("form");
        var formName = OpFormName.replace("frm", "");
        formName = formName.replace("OpForm", "");

         //TODO: handle if there are multiple reports in the page
    
        // find out if the page has custom template/default template
        var reportPrintTemplateMode = '';
        if ($('#enetreport-custom-printheader-' + OpFormName).length > 0) {

            $('.EWF-Rep-PrintHeaderFilterValue').each(function () {
                var currInputFieldName = $(this).attr("data-EWF-Rep-Source");

                //figure out attribute element 
                var currTagName = $("[name='" + currInputFieldName + "']").prop('tagName').toLowerCase();
                var currInputValue = '';

                switch (currTagName) {
                    case "input":
                        currInputValue = $("[name='" + currInputFieldName + "']").val();
                        break;
                    case "select":
                        currInputValue = $("[name='" + currInputFieldName + "']").find("option:selected").text(); ;
                        break;
                    default:
                        currInputValue = $("[name='" + currInputFieldName + "']").val();
                        break;
                }

                $(this).text(currInputValue);
            });

        }
        else if ($('#enetreport-default-printheader-' + OpFormName).length > 0) {

            var strParamHTML = "";
            console.log(formName);
            $("[ID *= 'lbl" + formName + "']").each(function () {
                var currLabelValue = $(this).text();
                var currInputFieldName = $(this).attr("for");

                //figure out attribute element 
                var currTagName = $("[name='" + currInputFieldName + "']").prop('tagName').toLowerCase();
                var currInputValue = '';

                switch (currTagName) {
                    case "input":
                        currInputValue = $("[name='" + currInputFieldName + "']").val();
                        break;
                    case "select":
                        currInputValue = $("[name='" + currInputFieldName + "']").find("option:selected").text(); ;
                        break;
                    default:
                        currInputValue = $("[name='" + currInputFieldName + "']").val();
                        break;
                }

                strParamHTML = strParamHTML + "<div class=\"row\">"
                strParamHTML = strParamHTML + "<span>" + currLabelValue + ": </span>"
                strParamHTML = strParamHTML + "<span>" + currInputValue + " </span>"
                strParamHTML = strParamHTML + "</div>"
            });

            $('#enetreport-parameters-' + OpFormName).html(strParamHTML);
        }
        else {

        }

    }
}

/*
Check whether it is a postback if it is a postback then 
hide the filters for phone screen by collapsing
*/
if (window.innerWidth <= 786 && $('#enetReportIsPostback').val() == 'True') {
    $('#reportCustomFilter').removeClass('in');
    $('#divEnetCustomFilter').removeClass('in');
}

var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
, base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
, format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
        window.location.href = uri + base64(format(template, ctx))
    }
})()

$(function () { 
    $("[data-toggle='tooltip']").tooltip();
});


$(function () {
    $('[data-toggle="popover"]').popover();
})

if ($(".OF-table-row-truncate,.OF-turncate-at-100").length > 0) {
    EWFEclipseToolTip();
}


function EWFEclipseToolTip() {
    $(document).on('mouseenter', ".OF-table-row-truncate,.OF-turncate-at-100", function () {
         var $this = $(this);
         if(this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
              $this.tooltip({
                   title: $this.text(),
                   placement: "bottom", 
                   container: "body"
              });
              $this.tooltip('show');
         }
    });

}


$(function () {
    var intWindowHeight = $(window).height();
    var intPageHeaderHeight = $('header.header').height() || 0;
    var intContentHeaderHeight = $('.Enet-UI-SmartScroll .SmartScroll-header').height() || 0; ;
    var intContentFooterrHeight = $('.Enet-UI-SmartScroll .SmartScroll-footer').height() || 0; ;
    //alert(intWindowHeight - intPageHeaderHeight - intContentHeaderHeight - intContentFooterrHeight - 150);
    //alert($(document).height());
    $('.SmartScroll-body').height(intWindowHeight - intPageHeaderHeight - intContentHeaderHeight - intContentFooterrHeight - 150)
});


function setFieldValue(inElementID, inElementValue) {
    var isAccountingFormatEnabled = false;
    var strCurrElement = "";

    if (inElementID.substr(0, 1) != '#') {
        strCurrElement = '#' + inElementID;
    }
    else {
        strCurrElement = inElementID;
    }

    //Validate if the element is present in the DOM and Validate if the element value is not null
    if ($(strCurrElement).length > 0 && inElementValue != null && inElementValue != undefined) {
        if ($(strCurrElement).hasClass("ewf-js-number")) {
            $(strCurrElement).val(inElementValue);
            EWFFormatNumber($(strCurrElement));
        }
        else {
            $(strCurrElement).val(inElementValue);
        }
    }
    else {
        //show error message
    }
}


$(document).ready(function () {
    $(document).on('click', '.printBtn', function () {

        var href = $(this).attr('data-EWF-PrintURL')
        var thePopup = window.open(href, 'formpopup', 'width=800,height=350,status=no,scrollbars=yes,resizable=no,top=50,left=200');

    });


});


$(window).resize(function () {
    if ($(window).width() > 768 && $(window).width() < 1024) {
        $('#nav').addClass('nav-xs');
    } else {
        $('#nav').removeClass('nav-xs');
    }
});

if ($(window).width() >= 768 && $(window).width() <= 1024) {
    $('#nav').addClass('nav-xs');
} else {
    $('#nav').removeClass('nav-xs');
}


function CSPBS3Show(inElement) {
    CSPBS3ShowWithAnimation(inElement, '');
}



function CSPBS3ShowWithAnimation(inElement, inElementAnimation) {
    var strElementAnimation = "";
    strElementAnimation = inElementAnimation
    if ($(inElement).length > 0) {
        if (strElementAnimation.length > 0) {
            $(inElement).show(strElementAnimation);
        }
        else {
            $(inElement).show();
        }

        if ($(inElement).hasClass('hide')) {
            $(inElement).removeClass('hide');
        }
        else {
            //dont have to remove the class
        }
    }
    else {
        //dont have to try to show an element 
    }
}
