﻿/**
* NAME: Bootstrap 3 Triple Nested Sub-Menus
* This script will active Triple level multi drop-down menus in Bootstrap 3.*
*/
$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
    // Avoid following the href location when clicking
    event.preventDefault();
    // Avoid having the menu to close when clicking
    event.stopPropagation();
    // Re-add .open to parent sub-menu item
    //Close Any 3rd level menus that may be open.
    $(this).parent().parent().find('li.dropdown').removeClass('open');
    $(this).parent().parent().find('li.dropdown-submenu ul li.dropdown').removeClass('open');


    //Open 3rd level menus for the clicked menu item Any 3rd level menus that may be open.
    $(this).parent().addClass('open');
    $(this).parent().find("ul").parent().find("li.dropdown").addClass('open');
});
//Function to close 3rd level menu on click of 2nd level menus
$('ul.dropdown-menu li').on('click', function (event) {
    $(this).parent().find('li.dropdown').removeClass('open');
    $(this).parent().find('li.dropdown ul li.dropdown').removeClass('open');
});


$(document).on('click', '[data-ride="collapse"] a', function (e) {
    var $this = $(e.target), $active;
    $this.is('a') || ($this = $this.closest('a'));

    $this.next().is('ul') && e.preventDefault();

    if ($this.next().is('ul')) {
        //console.log($this.next());
        var intWindowHeight = $(window).height();
        var intTopOffset = $this.offset().top;
        var intMenuHeight = $this.next().height();
        if ((intTopOffset + intMenuHeight) > intWindowHeight) {
            var intNewTop = (intMenuHeight - 36) * -1;
            $this.next().css('top', intNewTop);
        }
        CSPBS3Show($this.next());
    }

    setTimeout(function () { $(document).trigger('updateNav'); }, 300);
});
$('html').click(function (e) {
    if ($(e.target).closest('.nav').length == 0) {
        $('[data-ride="collapse"] .nav:visible').removeAttr('style').hide();
    } else {
        //Do nothing. Menu is clicked
    }
});