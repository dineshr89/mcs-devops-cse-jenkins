//////////////////////////////////////////////////////////////////////////////
// ofmodal - The best jQuery modal plugin on the face of the earth.
//
// Verion: 0.1.5
// Author: Omar Fouad - https://omarfouad.com - me@omarfouad.com
//
// NOTES:
// Responsive and auto-centered. Loads content from any HTML element or remote file.
// It includes a header and a footer with a scrollable body in between for longer content.
//
// 
//////////////////////////////////////////////////////////////////////////////

var OFModal = {}
OFModal.modals = [];

$.fn.OFModal = function (options) {

    OFModal.theOptions = options;

    var markup = '<div id="ofmodal-overlay"></div>' +
				'<div id="ofmodal-wrapper">' +
					'<div id="ofmodal-content">' +
    //						'<a href="#" id="ofmodal-close">close</a>' + 
						'<div id="ofmodal-header"></div>' +
						'<div id="ofmodal-body"></div>' +
						'<div id="ofmodal-footer">' +
							'<div id="ofmodal-cancel-btn" class="pull-left btn btn-default btn-sm">Cancel</div>' +
                            '<div class="pull-right">' +
						        '<span id="ofmodal-create-another-div" class="EnetUI-PadTop hide">' +
							        '<label for="ofmodal-create-another"><input id="ofmodal-create-another" name="ofmodal-create-another" value="1" type="checkbox" >&nbsp;</input>&nbsp;Create another</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
						        '</span>' +
							    '<button id="ofmodal-submit-btn" name=""ofmodal-submit-btn" class="">Submit/button>' +
                            '</div>' +
						'</div>' +
					'</div>' +
				'</div>';

    this.click(function (event) {
        console.log("calling plugin")

        if ($('#ofmodal-wrapper').length > 0) {
            if (options.localElement != '' && options.localElement != null) {
                OFModal.openSection(options.localElement);
                return;
            }
        }


        OFModal.autoHeight = false;
        OFModal.currentModal = OFModal.modals[options.id];

        $('body').append(markup);
        $('#ofmodal-wrapper').attr("data-ofmodal-id", options.id);

        $('#ofmodal-header').html(options.title || "Modal Window");
        $('#ofmodal-wrapper').css({ width: options.width || 'auto' });
        $('#ofmodal-wrapper').css({ maxWidth: options.maxWidth || '' });
        $('#ofmodal-wrapper').css({ minWidth: options.minWidth || '' });
        $('#ofmodal-overlay').css({ backgroundColor: options.overlayColor || 'white' });
        $('#ofmodal-overlay').css({ opacity: options.overlayOpacity || '0.9' });
        if (options.inlineBodyText == null || options.inlineBodyText == '') {
            $('#ofmodal-body').html('<div class="text-center"><br /><br /><br /><span class="EnetUI-FontSize-Large-XX"><i class="fa fa-circle-o-notch fa-spin" /></span><br />loading please wait</div>');
        }
        else {
            $('#ofmodal-body').html(options.inlineBodyText);
        }



        //                $('#ofmodal-body').load(encodeURI(options.remoteFile), options.paramsFunction ? eval(options.paramsFunction)() : null, function (data) {
        //                    onBodyLoad(options);
        //                    center();
        //                    if (options.postAjaxFunction) {
        //                        $('#ofmodal-body').html(eval(options.postAjaxFunction)(data));
        //                    }
        //                    center();
        //                });

        if (options.remoteFile != null && options.remoteFile != undefined) {
            $('#ofmodal-body').load(encodeURI(options.remoteFile), options.paramsFunction ? eval(options.paramsFunction)() : null, function (data) {
                onBodyLoad(options);
                center();
                if (options.postAjaxFunction) {
                    $('#ofmodal-body').html(eval(options.postAjaxFunction)(data));
                }
                center();
            });
        }
        else {

            $('#ofmodal-body').html($(options.localElement).html());
            setTimeout(center, 200);

        }

        $("#ofmodal-body " + options.omitHeader).css("display", "none");
        $("#ofmodal-body " + options.omitFooter).css("display", "none");

        $('#ofmodal-header').css('display', eval(options.hideHeader) ? 'none' : 'block');
        $('#ofmodal-header').css({
            backgroundColor: options.headerBGColor || "#E7E7E7",
            color: options.headerTextColor || "black",
            borderBottomColor: options.headerBorderColor || "#CCCCCC"
        });

        $('#ofmodal-footer').css('display', eval(options.hideFooter) ? 'none' : 'block');
        $('#ofmodal-footer').css({
            backgroundColor: options.footerBGColor || "#E7E7E7",
            color: options.footerTextColor || "black",
            borderBottomColor: options.footerBorderColor || "#CCCCCC"
        });


        $('#ofmodal-body').css('height', options.maxHeight || Math.max($(window).height()) - 200);
        $('#ofmodal-body').css('max-height', Math.max($(window).height()) - 200);
        $('#ofmodal-body').css('min-height', '150px');

        $('#ofmodal-footer #ofmodal-submit-btn').html(options.okButtonHTML || 'OK');
        $('#ofmodal-footer #ofmodal-submit-btn').attr('onclick', options.okButtonCallback);

        $('#ofmodal-footer #ofmodal-submit-btn').css({
            color: options.okButtonTextColor || "white",
            backgroundColor: options.okButtonBGCOlor || "#0F9DE7"
        });

        $('#ofmodal-footer #ofmodal-cancel-btn').css({
            color: options.cancelButtonColor || "#0F9DE7",
            "text-decoration": "none"
        });
        $('#ofmodal-submit-btn').addClass(options.okButtonClass || '');
        OFModal.autoHeight = options.maxHeight ? false : true;

        $('#ofmodal-overlay').fadeIn(250);
        $('#ofmodal-wrapper').fadeIn(250);

        if (options.allowCreateAnother.toUpperCase() == 'true'.toUpperCase()
            || options.allowCreateAnother == '1'.toUpperCase()
            || options.allowCreateAnother.toUpperCase() == 'yes'.toUpperCase()) {
            CSPBS3Show($('#ofmodal-create-another-div'));
        }
        else {
            $('#ofmodal-create-another-div').hide();
        }

        center();
        addEvents();
        onBodyLoad(options);

    });

    function transverseHTML() {
        if ($('#ofmodal-body .transv-header-active').length > 0) {
            var h = $('#ofmodal-body .transv-header-active');
            var strHeaderHTML = h.html();
            var nh = $('#ofmodal-header').html(strHeaderHTML);
        }

        if ($('#ofmodal-body .transv-submit-active').length > 0) {
            var s = $('#ofmodal-body .transv-submit-active');
            $('#ofmodal-submit-btn').text($(s).text());
            $(s).hide();

            if (s) {
                $('#ofmodal-submit-btn').click(function (event) {
                    $(s).click();
                });
            };
        }


    }

    function assignFocus() {
        var inputs = $('#ofmodal-body input');

        for (var i = 0; i < inputs.length; i++) {
            var input = $(inputs[i]);
            if (input.css('display') !== 'none') {
                $(input).focus();
                break;
            };
        };

        for (var k = 0; k < inputs.length; k++) {
            var input = $(inputs[k]);
            if (input.attr('autofocus')) {
                break;
            };
        };
    };

    function onBodyLoad(options) {
        if ($('#ofmodal-body').find(options.mainSection).length != 0) {
            $('#ofmodal-body ' + options.mainSection + ' .transv-header').addClass('transv-header-active'); //added this
            $('#ofmodal-body ' + options.mainSection + ' .transv-submit').addClass('transv-submit-active'); //added this
        }
        else {
            $('#ofmodal-body .transv-header').addClass('transv-header-active'); //added this
            $('#ofmodal-body .transv-submit').addClass('transv-submit-active'); //added this
        }

        transverseHTML();
        assignFocus();
        if (options.mainSection) {
            OFModal.theOptions.mainSection = options.mainSection;
        };
        center();
        EJUBindAllUIElements("#ofmodal-body");

        if (options.onLoadCallBack != null) {
            var onLoadCallBack = window[options.onLoadCallBack];
            if (typeof onLoadCallBack !== 'function') {
                onLoadCallBack = defaultOnLoadCallBack;
            }
            onLoadCallBack();
        }

    }

    function defaultOnLoadCallBack() {

    }

    function center() {
        var e = $('#ofmodal-wrapper');

        var left = Math.max($(window).width() - e.outerWidth(), 0) / 2;
        var top = Math.max($(window).height() - e.outerHeight(), 0) / 2;

        e.css('left', left + $(window).scrollLeft());
        e.css('top', top + $(window).scrollTop());
        if (OFModal.autoHeight) {
            $('#ofmodal-body').css('height', Math.max($(window).height()) - 200);
        };
    }

    function addEvents() {
        $('#ofmodal-close').click(function (e) { e.preventDefault(); close(); });
        $('#ofmodal-cancel-btn').click(function (e) { e.preventDefault(); close(); });
        $(window).bind('resize.modal', center);
    }

    function close() {

        if (options.onLoadCallBack != null) {
            var closeCallBack = window[options.closeCallback];
            if (typeof closeCallBack !== 'function') {
                closeCallBack = defaultCloseCallBack;
            }
            closeCallBack();
        }
        $('#ofmodal-overlay').remove();
        $('#ofmodal-wrapper').remove();
        $(window).unbind('resize.modal');
    }

    function defaultCloseCallBack() {

    }

    OFModal.center = center;
};

///////////////////////////////
// 
///////////////////////////////


OFModal.isOn = function () {
    if ($('#ofmodal-wrapper').length > 0) {
        return true
    } else {
        return false
    }
}

OFModal.closeModal = function () {
    $('#ofmodal-overlay').remove();
    $('#ofmodal-wrapper').remove();
    $(window).unbind('resize.modal');
}

//OFModal.openSection = function(section, headerTitle,) {
//	$('#ofmodal-body > ' + OFModal.theOptions.mainSection).hide();
//	$('#ofmodal-body > ' + section).show();
//	$('#ofmodal-header').html(headerTitle);

//	OFModal.currentSection = section;
//	OFModal.center();
//}


OFModal.openSection = function (section, headerTitle, inBackSection, isBackClick, inCurrentSection) {
    var strBackSection = inBackSection;
    if ($.trim(inBackSection).length > 0) {
        strBackSection = inBackSection;
    }
    else if (OFModal.theOptions.currentSection != null) {
        strBackSection = OFModal.theOptions.currentSection;
    }
    else if (OFModal.theOptions.mainSection != null) {
        strBackSection = OFModal.theOptions.mainSection;
    }
    else {
        strBackSection = '';
    }
    var strBankLink = '';
    if (!isBackClick) {
        if (strBackSection.length > 0) {
            strBankLink = '<a href="#" class="btn btn-sm btn-default ewf-js-ofmodal-backlink" onclick="OFModal.openSection(\'' + strBackSection + '\', \'\', \'\', true, \'' + section + '\');">Go back</a><br />';
        }
    }
    else {
        if ($.trim(inCurrentSection).length > 0) {
            $('#ofmodal-body ' + inCurrentSection).hide(); //Added this
        }
    }
    $('#ofmodal-body ' + OFModal.theOptions.mainSection).hide();
    $('#ofmodal-body ' + OFModal.theOptions.currentSection).hide(); //Added this
    $('#ofmodal-body ' + OFModal.currentSection).hide(); //Added this

    if ($.trim(inCurrentSection).length > 0) {
        $('#ofmodal-body ' + inCurrentSection).hide(); //Added this
    }

    $('#ofmodal-body ' + section).show();
    $('#ofmodal-body ' + section).removeClass('hide'); //Added this
    $('#ofmodal-header').html(headerTitle);
    //
    $('#ofmodal-body ' + section + ' a.ewf-js-ofmodal-backlink').remove();
    $('#ofmodal-body ' + section).prepend(strBankLink); //added this
    $('#ofmodal-body .transv-header-active').removeClass('transv-header-active');
    $('#ofmodal-body .transv-submit-active').removeClass('transv-submit-active');
    $('#ofmodal-body ' + section + ' .transv-header').addClass('transv-header-active'); //added this
    $('#ofmodal-body ' + section + ' .transv-submit').addClass('transv-submit-active'); //added this
    OFModal.currentSection = section;
    OFModal.transverseHTML(); //added this
    OFModal.center();

}

OFModal.closeSection = function () {
    $('#ofmodal-body ' + OFModal.theOptions.mainSection).show();
    $('#ofmodal-body ' + OFModal.currentSection).hide();
    $('#ofmodal-header').html(OFModal.currentModal.options.title);
    OFModal.restoreFooter();

    OFModal.currentSection = null;
    OFModal.center();
}

OFModal.turnOffFooter = function () {
    $('#ofmodal-footer').css('opacity', 0.4);
    $('#ofmodal-footer').css('pointerEvents', 'none');
}

OFModal.restoreFooter = function () {
    $('#ofmodal-footer').css('opacity', 1);
    $('#ofmodal-footer').css('pointerEvents', 'auto');
}



OFModal.hideOKButton = function () {
    $('#ofmodal-submit-btn').css('display', 'none');
}

OFModal.showOKButton = function () {
    $('#ofmodal-submit-btn').css('display', 'block');
}



OFModal.transverseHTML = function () {
    if ($('#ofmodal-body .transv-header-active').length > 0) {//changed
        $('#ofmodal-body .transv-header').hide();
        var h = $('#ofmodal-body .transv-header-active'); //changed
        var strHeaderHTML = h.html();
        var nh = $('#ofmodal-header').html(strHeaderHTML);
    }

    if ($('#ofmodal-body .transv-submit-active').length > 0) {//changed
        $('#ofmodal-submit-btn').unbind("click");
        var s = $('#ofmodal-body .transv-submit-active'); //changed
        var strBtnName = $(s).text();
        if (strBtnName.length <= 0) {
            strBtnName = $(s).val();
        }
        $('#ofmodal-submit-btn').text(strBtnName);
        // $('#ofmodal-submit-btn').text($(s).text());
        $(s).hide();

        if (s) {
            $('#ofmodal-submit-btn').click(function (event) {
                $(s).click();
            });
        };
    }




}

OFModal.rebind = function (isNew) {
    OFModal.modals = [];
    $('[data-ofmodal]').each(function (index, el) {
        var options = {
            id: index,
            title: $(this).attr('data-ofmodal-title') || null,
            width: $(this).attr('data-ofmodal-width') || '800px',
            maxWidth: $(this).attr('data-ofmodal-maxWidth') || null,
            minWidth: $(this).attr('data-ofmodal-minWidth') || '800px',
            overlayColor: $(this).attr('data-ofmodal-overlayColor') || null,
            overlayOpacity: $(this).attr('data-ofmodal-overlayOpacity') || null,
            inlineBodyText: $(this).attr('data-ofmodal-inlineBodyText') || null,
            localElement: $(this).attr('data-ofmodal-localElement') || null,
            remoteFile: $(this).attr('data-ofmodal-remoteFile') || null,

            hideHeader: $(this).attr('data-ofmodal-hideHeader') || null,
            hideFooter: $(this).attr('data-ofmodal-hideFooter') || null,

            omitHeader: $(this).attr('data-ofmodal-omitHeader') || null,
            omitFooter: $(this).attr('data-ofmodal-omitFooter') || null,

            headerBGColor: $(this).attr('data-ofmodal-headerBGColor') || null,
            headerTextColor: $(this).attr('data-ofmodal-headerTextColor') || null,
            headerBorderColor: $(this).attr('data-ofmodal-headerBorderColor') || null,

            footerBGColor: $(this).attr('data-ofmodal-footerBGColor') || null,
            footerTextColor: $(this).attr('data-ofmodal-footerTextColor') || null,
            footerBorderColor: $(this).attr('data-ofmodal-footerBorderColor') || null,

            okButtonHTML: $(this).attr('data-ofmodal-okButtonHTML') || null,
            okButtonCallback: $(this).attr('data-ofmodal-okButtonCallback') || null,
            okButtonBGCOlor: $(this).attr('data-ofmodal-okButtonBGCOlor') || null,
            okButtonTextColor: $(this).attr('data-ofmodal-okButtonTextColor') || null,
            okButtonClass: $(this).attr('data-ofmodal-okButtonClass') || null,

            cancelButtonColor: $(this).attr('data-ofmodal-cancelButtonColor') || null,

            maxHeight: $(this).attr('data-ofmodal-maxHeight') || 'auto',
            mainSection: $(this).attr('data-ofmodal-mainSection') || '.of-main', //changed this
            paramsFunction: $(this).attr('data-ofmodal-paramsFunction') || null,
            postAjaxFunction: $(this).attr('data-ofmodal-postAjaxFunction') || null,
            onLoadCallBack: $(this).attr('data-ofmodal-onLoadCallBack') || null,
            closeCallback: $(this).attr('data-ofmodal-closeCallback') || null,
            allowCreateAnother: $(this).attr('data-ofmodal-allowCreateAnother') || 'false'.toUpperCase()
        };

        $(this).unbind("click");
        $(this).OFModal(options);
        this.options = options;

        if (isNew) {
            OFModal.modals.push(this);
        };

    });
}

OFModal.rebind(true);
