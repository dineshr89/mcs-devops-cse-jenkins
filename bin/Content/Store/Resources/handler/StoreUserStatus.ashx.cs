﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace CStoreProAdmin.Content.Store.Resources.handler
{
    /// <summary>
    /// Summary description for StoreUserStatus
    /// </summary>
    public class StoreUserStatus : AppBase.AppBaseHandler
    {
        int intRowID = -1;
        protected override bool generateContent()
        {

            bool isOpSuccesful = false;
            string strXml = "";
            string strMsg = "";
            string OutMsg = "";
            bool outStatus = false;

            if (System.Web.HttpContext.Current.Request.Form["RowID"] != null)
            {
                intRowID = Int32.Parse(HttpContext.Current.Request.Form["RowID"]);
            }

            isOpSuccesful = executeQuery(out outStatus, out strMsg);
            getPackageSection(isOpSuccesful, strMsg, out strXml, out OutMsg);
            if (isOpSuccesful)
            {
                sbContent.Append(strXml);
            }
            else
            {
                sbContent.Append(strXml);
                sbInternalErrors.Append(strMsg);
            }

            return isOpSuccesful;

        }

        private bool executeQuery(out bool outStatus, out string outMessage)
        {
            outMessage = "";
            bool isCommitted = false;
            int intAccountID = -1;

            if (System.Web.HttpContext.Current.Request.Form["AccountID"] != null)
            {
                intAccountID = Int32.Parse(HttpContext.Current.Request.Form["AccountID"]);
            }

            int intStatus = 0;
            string strMessage = "";
            int intStoreID = getCurrentUserDefaultStoreID();
            EnetWebFoundation.Classes.EnetDB dbTemp = new EnetWebFoundation.Classes.EnetDB();
            using (SqlConnection cnTemp = new SqlConnection(dbTemp.getConnectionString("DefaultConnection")))
            {
                using (SqlCommand cmdTemp = new SqlCommand())
                {
                    cmdTemp.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdTemp.CommandText = "spEWFResetPasswordByAdmin";

                    cmdTemp.Parameters.Add("@OutStatus", System.Data.SqlDbType.Int);
                    cmdTemp.Parameters[cmdTemp.Parameters.Count - 1].Direction = ParameterDirection.Output;

                    cmdTemp.Parameters.Add("@OutMessage", System.Data.SqlDbType.NVarChar, 250);
                    cmdTemp.Parameters[cmdTemp.Parameters.Count - 1].Direction = ParameterDirection.Output;

                    cmdTemp.Parameters.Add("@InLoginAccountID", System.Data.SqlDbType.NVarChar, 50);
                    cmdTemp.Parameters[cmdTemp.Parameters.Count - 1].Value = intAccountID;

                    cmdTemp.Parameters.Add("@InNewPassword", System.Data.SqlDbType.NVarChar, 50);
                    cmdTemp.Parameters[cmdTemp.Parameters.Count - 1].Value = "welcome";

                    cmdTemp.Connection = cnTemp;
                    cnTemp.Open();
                    cmdTemp.ExecuteNonQuery();

                    int.TryParse(cmdTemp.Parameters[0].Value.ToString(), out intStatus);
                    strMessage = cmdTemp.Parameters[1].Value.ToString();
                }
            }


            if (intStatus > 0)
            {
                isCommitted = true;
            }
            else
            {
                isCommitted = false;
            }
            outStatus = isCommitted;
            outMessage = strMessage;
            return true;
        }
        private bool getPackageSection(bool inStatus, string inMessage, out string outXml, out string outMsg)
        {

            bool isOpSuccess = false;
            StringBuilder sbRV = new StringBuilder("");
            string strErr = "";

            if (hrtCurrent == HandlerResponseType.XML)
            {
                sbRV.Append("<updatebatchstatus>");
                sbRV.Append("<status>" + inStatus + "</status>");
                sbRV.Append("<message>" + inMessage + "</message>");
                sbRV.Append("<rowid>" + intRowID + "</rowid>");
                sbRV.Append("</updatebatchstatus>");
            }

            if (!inStatus)
            {
                strErr = inMessage;
            }
            isOpSuccess = true;

            outXml = sbRV.ToString();
            outMsg = strErr;
            return isOpSuccess;
        }
    }
}