﻿$(document).ready(function () {
    $(document).on('click', '.EnetChangeStoreStatus', function (e) {
        var rowid = $(this).attr("data-rowid");
        var accountID = $(this).attr("data-AccountID");
        changeBatchStatus(accountID, rowid);
    });




    function changeBatchStatus(inaccountID, inRowID) {
        $.ajax({
            type: "POST",
            url: "/CStoreProAdmin/Content/Store/Resources/handler/StoreUserStatus.ashx",
            data: "&RowID=" + inRowID + "&AccountID=" + inaccountID
        }).done(function (data) {
            try {
                var root = data;
                var divName = "divStoreUsers";
                var strGridName = "StoreUsers";
                if ($(root).find("resStatus").length > 0) {
                    var responseStatus = $(root).find('resStatus').text();
                    var responseMsg = $(root).find('strMessage').text();
                    var responseData = $(root).find('strResponseData').text();
                    if (responseStatus == "1") {
                        responseData = $.parseXML(responseData);
                        if ($(responseData).find("status").length > 0) {
                            var status = $(responseData).find("status").text();

                            if (status == '1' || status == 'True') {

                                var strGridRowID = $(responseData).find('rowid').text();

                                if (strGridRowID != "" && strGridName != "") {
                                    RefreshGridRow(strGridName, strGridRowID, 1);
                                }
                                else {
                                    EUIShowErrorInfoBar("Error getting Grid Row", divName);
                                }
                            }
                            else {
                                var strMessage = $(root).find("message").text();
                                EUIShowErrorInfoBar(strMessage, divName);
                            }
                        }
                    }
                    else {
                        EUIShowErrorInfoBar(responseMsg, divName);
                    }


                }
                else {
                    EUIShowErrorInfoBar("Error getting response data", divName);
                }

            }
            catch (e) {
                alert("error exc");
            }
        })

    .fail(function (data, statusText) {
        alert(statusText);
    })
    .always(function (data) {
    });
    }
});
