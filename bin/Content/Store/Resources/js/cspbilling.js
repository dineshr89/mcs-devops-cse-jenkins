'use strict';
var stripe = null;
var c = null;
if ((typeof strCONSTBillingJSKey !== 'undefined') && (strCONSTBillingJSKey!='')) // Any scope
{
    stripe = Stripe(strCONSTBillingJSKey)
}
else {
    console.log('Please set the billing key.')
    showError('Unable to manage account billing because the key is not set.')
}

  // Floating labels

function getUnixTimeStampAsDate(inUnixTimeStamp) {
    var strReturnValue = '';
    if ($.trim(inUnixTimeStamp).length > 0) {
        var intUnixTimeStamp = parseInt(inUnixTimeStamp);
        if (!isNaN(intUnixTimeStamp)) {
            var dtFromUnixTimeStamp = new Date(intUnixTimeStamp * 1000);
            strReturnValue = ("0" + (dtFromUnixTimeStamp.getMonth() + 1)).slice(-2) + "/"
                                     + ("0" + dtFromUnixTimeStamp.getDate()).slice(-2) + "/"
                                     + dtFromUnixTimeStamp.getFullYear();
        }
        else {
            strReturnValue = '';
        }
    }
    else {
        strReturnValue = '';
    }
    return strReturnValue;
}
$(document).ready(function () {
    $(".spnUnixTimeStamp").each(function (index) {
        console.log('in looper');
        var strUnixTimeStamp = $(this).attr('data-value') == null ? '' : $(this).attr('data-value');
        var strFromUnixTimeStamp = getUnixTimeStampAsDate(strUnixTimeStamp);
        console.log('returned: ' + strFromUnixTimeStamp);
        $(this).text(strFromUnixTimeStamp);
    });
    $('#btnAddCoupon').on('click', function(e){
            e.preventDefault();
            $('#divApplyCoupon').hide().removeClass('hide').fadeIn('slow');
            $('#txtCoupon').focus();
    });

    
    $('#btnCloseCoupon').on('click', function(e){
            e.preventDefault();
            $('#divApplyCoupon').fadeOut('slow');
     });
    $('#btnDismissError').on('click', function (e) {
        e.preventDefault();
        $('#divPageError').fadeOut('slow');
    });
    $('#btnCloseAccount').on('click', function (e) {
        e.preventDefault();
        closeAccount();
    });
    $('#btnUndoCloseAccount').on('click', function (e) {
        e.preventDefault();
        undoCloseAccount();
    });
    $('#btnCreateCoupon').on('click', function (e) {
        CSPSetCouponCode(false);
    });
    $('#btnRefreshNewCoupon').on('click', function (e) {
        CSPSetCouponCode(true);
    });
    $('#btnCreateNewCoupon').on('click', function (e) {
        CSPValidateAndCreateCoupon();
    });
    $('#btnSetPaymentMethodStatus').on('click', function (e) {
        CSPValidateAndUpdatePaymentMethodStatus();
    });
    
})
function CSPValidateAndUpdatePaymentMethodStatus() {
    var strFormName = '#frmNewPlan';
    var strFrmModeName = '#txtFormMode';
    var strIsPaymentUpdateReqiredField = '#txtRequirePaymentUpdate';
    var strPaymentUpdateReqiredByField = '#txtRequirePaymentUpdateBy';
    var strPaymentUpdateReasonField = '#txtPaymentUpdateReason';
    var strConstOperation = 'updatepaymentmethodstatus';
    $('#divCreateCouponError').fadeOut('slow');
    if ($(strFormName).length == 1) {
        var strFormAction = $(strFormName).attr('action');
        var strIsPaymentUpdateRequired = $(strIsPaymentUpdateReqiredField).val();
        var strPaymentUpdateReqiredBy = $(strPaymentUpdateReqiredByField).val();
        var strPaymentUpdateReason = $(strPaymentUpdateReasonField).val();

        if (strIsPaymentUpdateRequired !== undefined) {
            if ($.trim(strIsPaymentUpdateRequired).length > 0) {
                if (strPaymentUpdateReqiredBy !== undefined) {
                    if ($.trim(strPaymentUpdateReqiredBy).length > 0) {
                        if (!isNaN(new Date(strPaymentUpdateReqiredBy).valueOf())) {
                            if (strPaymentUpdateReason !== undefined) {
                                if ($.trim(strPaymentUpdateReqiredBy).length > 0) {
                                    if ($.trim(strFormAction).length > 0) {
                                        $(strFrmModeName).val(strConstOperation);
                                        if ($(strFrmModeName).val() == strConstOperation) {
                                            var dtFormData = $(strFormName).serialize();
                                            $.ajax({
                                                method: "POST",
                                                url: strFormAction,
                                                data: dtFormData
                                            })
                                                .done(function (data) {
                                                    if (data.resStatus == 1) {
                                                        if (strIsPaymentUpdateRequired == '1') {
                                                            $('#spnPaymentMethodUpdateRequirementDisplay').text('Update required by ' + strPaymentUpdateReqiredBy);
                                                        }
                                                        else if (strIsPaymentUpdateRequired == '0') {
                                                            $('#spnPaymentMethodUpdateRequirementDisplay').text('Up-to-date');
                                                        }
                                                        $('#modUpdatePaymentMethodStatus').modal('hide');
                                                    }
                                                    else {
                                                        if (data.strMessage) {
                                                            showErrorInUpdatePaymentMethod(data.strMessage);
                                                        }
                                                        else {
                                                            showErrorInUpdatePaymentMethod('Unknown error from server. Error 1005');
                                                        }
                                                    }
                                                })
                                                .fail(function (data) {
                                                    console.log(data);
                                                    showErrorInUpdatePaymentMethod('Unable to create coupon.  Error 1004');
                                                })
                                                .always(function () {
                                                });

                                        }
                                        else {
                                            showErrorInUpdatePaymentMethod('Unable to create coupon.  Error 1003');
                                        }
                                    }
                                    else {
                                        showErrorInUpdatePaymentMethod('Unable to create coupon.  Error 1002');
                                    }

                                }
                                else {
                                    showErrorInUpdatePaymentMethod('Payment update reason is empty.');
                                    $(strPaymentUpdateReasonField).focus();
                                }
                            }
                            else {
                                showErrorInUpdatePaymentMethod('Payment update reason is empty.');
                                $(strPaymentUpdateReasonField).focus();
                            }

                        }
                        else {
                            showErrorInUpdatePaymentMethod('Payment update required by date is not valid.');
                            $(strPaymentUpdateReqiredByField).focus();
                        }
                    }
                    else {
                        showErrorInUpdatePaymentMethod('Payment update required by date is empty.');
                        $(strPaymentUpdateReqiredByField).focus();
                    }
                }
                else {
                    showErrorInUpdatePaymentMethod('Payment update required by date is not found.');
                    $(strPaymentUpdateReqiredByField).focus();
                }

            }
            else {
                showErrorInUpdatePaymentMethod('Payment udpate requirement feild is empty.');
                $(strIsPaymentUpdateReqiredField).focus();
            }
        }
        else {
            showErrorInUpdatePaymentMethod('Payment Updated Required field is not found.');
        }
    }
    else {
        showErrorInUpdatePaymentMethod('Unable to update payment method requirement.  Error 1001');
    }
}
function CSPSetCouponCode(isHardRefresh) {
    if (($.trim($('#txtNewCouponCode').val()) == 0) || isHardRefresh) {
        var strCouponCode = CSPGenerateRandomCouponCode();
        $('#txtNewCouponCode').val(strCouponCode);
    }
}
function CSPGenerateRandomCouponCode() {
    var length = 8;
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}
function CSPValidateAndCreateCoupon() {
    var strFormName = '#frmNewPlan';
    var strFrmModeName = '#txtFormMode';
    var strNewCouponCodeField = '#txtNewCouponCode';
    var strAmountOffField = '#txtAmountOff';
    var strMaxUsesField = '#txtMaxUses';
    var strConstOperation = 'createnewcoupon';
    $('#divCreateCouponError').fadeOut('slow');
    if ($(strFormName).length == 1) {
        var strFormAction = $(strFormName).attr('action');
        var strNewCouponCode = $(strNewCouponCodeField).val();
        var strAmountOff = $(strAmountOffField).val();
        var strMaxUses = $(strMaxUsesField).val();
        var strAllowedPlanValue = $("input[name='txtCouponPlan']:checked").length;

        if (strNewCouponCode !== undefined) {
            if ($.trim(strNewCouponCode).length > 0) {
                if (strAmountOff !== undefined) {
                    if ($.trim(strAmountOff).length > 0) {
                        if (!isNaN(strAmountOff)) {
                            if (strMaxUses !== undefined) {
                                if ($.trim(strMaxUses).length > 0) {
                                    if (!isNaN(strMaxUses)) {
                                        if (!strMaxUses.includes('.')) {
                                            if (strAllowedPlanValue > 0) {
                                                if ($.trim(strFormAction).length > 0) {
                                                    $(strFrmModeName).val(strConstOperation);
                                                    if ($(strFrmModeName).val() == strConstOperation) {
                                                        var dtFormData = $(strFormName).serialize();
                                                        $.ajax({
                                                            method: "POST",
                                                            url: strFormAction,
                                                            data: dtFormData
                                                        })
                                                            .done(function (data) {
                                                                if (data.resStatus == 1) {
                                                                    $('#txtCoupon').val(strNewCouponCode);
                                                                    $('#modCreateCoupon').modal('hide');
                                                                }
                                                                else {
                                                                    if (data.strMessage) {
                                                                        showErrorInCreateCoupon(data.strMessage);
                                                                    }
                                                                    else {
                                                                        showErrorInCreateCoupon('Unknown error from server. Error 1005');
                                                                    }
                                                                }
                                                            })
                                                            .fail(function (data) {
                                                                console.log(data);
                                                                showErrorInCreateCoupon('Unable to create coupon.  Error 1004');
                                                            })
                                                            .always(function () {
                                                            });

                                                    }
                                                    else {
                                                        showErrorInCreateCoupon('Unable to create coupon.  Error 1003');
                                                    }
                                                }
                                                else {
                                                    showErrorInCreateCoupon('Unable to create coupon.  Error 1002');
                                                }
                                            }
                                            else {
                                                showErrorInCreateCoupon('Select valid plans for this coupon.');
                                            }
                                        }
                                        else {
                                            showErrorInCreateCoupon('The max uses includes a decimal.');
                                            $(strMaxUsesField).focus();
                                        }
                                    }
                                    else {
                                        showErrorInCreateCoupon('Max uses not a valid number.');
                                        $(strMaxUsesField).focus();
                                    }

                                }
                                else {
                                    showErrorInCreateCoupon('Max uses is empty.');
                                    $(strMaxUsesField).focus();
                                }
                            }
                            else {
                                showErrorInCreateCoupon('Max uses field not found.');
                                $(strMaxUsesField).focus();
                            }
                        }
                        else {
                            showErrorInCreateCoupon('Amount off is not a valid number.');
                            $(strAmountOffField).focus();
                        }
                    }
                    else {
                        showErrorInCreateCoupon('Amount off is empty.');
                        $(strAmountOffField).focus();
                    }
                }
                else {
                    showErrorInCreateCoupon('Amount off field is not found.');
                    $(strAmountOffField).focus();
                }

            }
            else {
                showErrorInCreateCoupon('Coupon code is empty.');
            }
        }
        else {
            showErrorInCreateCoupon('Coupon code is not found.');
        }
    }
    else {
        showErrorInCreateCoupon('Unable to create new coupon.  Error 1001');
    }
}

function closeAccount() {
    bootbox.confirm({
        title: 'Are you sure you want to close your account?',
        message: 'Are you sure you want to close your account? ' ,
        buttons: {
            confirm: {
                label: 'Keep account',
                className: 'btn-success'
            },
            cancel: {
                label: 'Close account',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
            }
            else {
                markAccountAsClosed();
            }
        }
    });

}
function undoCloseAccount() {
    $('#divCurrentPlanSummary').hide().removeClass('hide').fadeIn('slow');
    $('#divSelectPlan').hide().removeClass('hide').fadeIn('slow');
    $('#divNewPlanSummary').hide().removeClass('hide').fadeIn('slow');
    $('#divConfirmationCloseAccount').hide().removeClass('hide').fadeOut('slow');

}
function markAccountAsClosed() {
    var strFormName = '#frmNewPlan';
    var strFrmModeName = '#txtFormMode';
    var strConstOperation = 'CloseAccount';
    if ($(strFormName).length == 1) {
        var strFormAction = $(strFormName).attr('action');
        if ($.trim(strFormAction).length > 0) {
            $(strFrmModeName).val(strConstOperation);
            if ($(strFrmModeName).val() == strConstOperation) {
                var dtFormData = $(strFormName).serialize();
                $.ajax({
                    method: "POST",
                    url: strFormAction,
                    data: dtFormData
                })
                  .done(function (data) {
                      if (data.resStatus == 1) {
                          console.log(data);
                          setTimeout(function () {
                              $('#divCurrentPlanSummary').hide().removeClass('hide').fadeOut('slow');
                              $('#divSelectPlan').hide().removeClass('hide').fadeOut('slow');
                              $('#divNewPlanSummary').hide().removeClass('hide').fadeOut('slow');
                              $('#divSelectPayment').hide().removeClass('hide').fadeOut('slow');
                              $('#divConfirmationCloseAccount').hide().removeClass('hide').fadeIn('slow');

                          }
                          , 500);
                      }
                      else {
                          if (data.strMessage) {
                              showError(data.strMessage);
                          }
                          else {
                              showError('Unknown error from server. Error 1005');
                          }
                      }
                  })
                  .fail(function () {
                      showError('Unable to close account.  Error 1004');
                  })
                  .always(function () {
                  });
            }
            else {
                showError('Unable to close account.  Error 1003');
            }
        }
        else {
            showError('Unable to close account.  Error 1002');
        }
    }
    else {
        showError('Unable to close account.  Error 1001');
    }
}
function showError(inMessage) {
    if ($.trim('#modError').length > 0) {
        if ($.trim('#modError #divGeneralError').length > 0) {
            $('#divGeneralError').html(inMessage);
            $('#modError').modal('show');

            //$('#divPageError').removeClass('hide').fadeIn('slow');
            //setTimeout(function () { $('#divPageError').fadeOut('slow'); }, 5000);
        }
        else {
            alert(inMessage);
        }
    }
    else {
        alert(inMessage);
    }
}
function showErrorInCreateCoupon(inMessage) {
    if ($.trim('#divCreateCouponError').length > 0) {
        if ($.trim('#divCreateCouponError #msgCreateCouponError').length > 0) {
            $('#msgCreateCouponError').html(inMessage);
            $('#divCreateCouponError').removeClass('hide').fadeIn('slow');
            setTimeout(function () { $('#divCreateCouponError').fadeOut('slow'); }, 5000);
        }
        else {
            alert(inMessage);
        }
    }
    else {
        alert(inMessage);
    }
}
function showErrorInUpdatePaymentMethod(inMessage) {
    if ($.trim('#divCreateCouponError').length > 0) {
        if ($.trim('#modUpdatePaymentMethodStatus #msgUpdatePaymentMethodError').length > 0) {
            $('#msgUpdatePaymentMethodError').html(inMessage);
            $('#divUpdatePaymentMethodError').removeClass('hide').fadeIn('slow');
            setTimeout(function () { $('#divUpdatePaymentMethodError').fadeOut('slow'); }, 5000);
        }
        else {
            alert(inMessage);
        }
    }
    else {
        alert(inMessage);
    }
}
